<?php
	$this->load->helper("utilidades");
	$editable = isset($editable) ? $editable : false;
	$id_tabla = isset($id_tabla) ? $id_tabla : 'table_causas';
	$callback_cambios = isset($callback_cambios) ? '(function(){'.$callback_cambios.'})' : '(function(){})';
?>

<table id="<?=$id_tabla?>" class="display cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>ROL/RIT</th>
			<th>Ingreso</th>
			<th>Materia</th>
			<th>Abogado</th>
			<th>RUT Abogado</th>
			<th>Alumno</th>
			<th>RUT Alumno</th>
			<th>Usuario</th>
			<th>RUT Usuario</th>
			<th>Termino</th>
			<?= $mostrar_detalles ? '<th>Detalles</th>' : ''?>
			<?= $mostrar_opciones ? '<th>Opciones</th>' : ''?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($causas as $ca) { ?>
			<tr>
				<td><?php $this->view('partial/click_detalle_causa', ['id' => $ca->id]); ?></td>
				<td><?= $ca->rol_causa ? $ca->rol_causa : '<div class="purple centro"><b>PENDIENTE</b></div>' ?></td>
				<td><?= $ca->INGRESO?></td>
				<td><?= $ca->nombre_materia ? $ca->nombre_materia : '<div class="purple centro"><b>PENDIENTE</b></div>' ?></td>
				<td><img  title="Haga click aqui para ver las causas del Abogado" class="click" onclick="causas_usuario('abogado', <?= $ca->RUT_ABOGADO?>);" src="../../assets/images/lupa.png" height="15px"><?= $ca->NOMBRE_ABOGADO?></td>
				<td><?= $ca->RUT_ABOGADO."-".$ca->DV_ABOGADO?></td>
				<td>
					<?php if($ca->RUT_ALUMNO): ?>
						<img title="Haga click aqui para ver las causas del Alumno" class="click" onclick="causas_usuario('alumno', <?= $ca->RUT_ALUMNO?>);" src="../../assets/images/lupa_verde.png" height="15px"><?= $ca->NOMBRE_ALUMNO?></td>
					<?php elseif($editable && ! $ca->TERMINO): ?>
						<button class="btn_clinica_compacto" onclick="dialog_cambiar_usuario('alumno', '<?= $ca->id?>', '<span class=\'rojo\'><b>SIN ASIGNAR</b></span>');">
							<img src="../../assets/images/signo_mas.png" height="18px">
							<span>agregar</span>
						</button>
					<?php else: ?>
						<span class="rojo"><b>SIN ASIGNAR</b></span>
					<?php endif; ?>
				<td><?= $ca->RUT_ALUMNO ? $ca->RUT_ALUMNO."-".$ca->DV_ALUMNO : '<DIV class="rojo centro mediano"><b>---</b></div>'?></td>
				<td><img title="Haga click aqui para ver las causas del Usuario" class="click" onclick="causas_usuario('cliente', <?= $ca->rut_cliente?>);" src="../../assets/images/lupa_roja.png" height="15px"><?= $ca->nombre_cliente?></td>
				<td><?= $ca->rut_cliente."-".$ca->dv_cliente?></td>
				<td class="centro">
					<?php if($ca->TERMINO): ?>
						<?= $ca->TERMINO?>
					<?php elseif($editable): ?>
						<button title="terminar causa" class="icono_btn sombra" onclick="terminar_causa('<?= $ca->id?>');">
							<img src="<?= getRutaIcono('terminar')?>">
						</button>
					<?php else: ?>
						<span class="verde"><b>ACTIVA</b></span>
					<?php endif; ?>
				</td>
				<?php if($mostrar_detalles): ?>
					<td>
						<div style="margin:3px 3px">
							<button class="icono_btn" title="Audiencias" onclick="trae_audiencias_2('<?= $ca->rol_causa?>');">
								<img src="<?=getRutaIcono('audiencia')?>">
								<span class="medalla_<?= $ca->cantidad_audiencias ? 'azul">'.$ca->cantidad_audiencias : 'gris">0'?></span>
							</button>
							<button class="icono_btn" title="Agendaciones" onclick="trae_agendaciones('<?= $ca->id?>');">
								<img src="<?=getRutaIcono('agenda')?>">
								<span class="medalla_<?= $ca->cantidad_agendaciones ? 'azul">'.$ca->cantidad_agendaciones : 'gris">0'?></span>
							</button>
							<button class="icono_btn" title="Orientaciones" onclick="dialogo('dialog1000', 'causa/vista_orientaciones/<?= $ca->id?>', {'nuevo_dialogo':true, titulo:'Orientaciones de la Causa ID <?= $ca->id?>'});">
								<img src="<?=getRutaIcono('orientacion')?>">
								<span class="medalla_<?= $ca->cantidad_orientaciones ? 'azul">'.$ca->cantidad_orientaciones : 'gris">0'?></span>
							</button>
							<button class="icono_btn" title="Trámites" onclick="dialogo_tramites('<?= $ca->id?>')">
								<img src="<?=getRutaIcono('tramite')?>">
								<span class="medalla_<?= $ca->cantidad_tramites ? 'azul">'.$ca->cantidad_tramites : 'gris">0'?></span>
							</button>
						</div>
					</td>
				<?php endif; ?>
				<?php if($mostrar_opciones): ?>
					<td>
						<a title="generar ficha de la causa" class="icono_btn sombra" href="/ficha/ingreso_causa/<?= $ca->id?>" target="_blank">
							<img src="<?= getRutaIcono('imprimir')?>">
						</a>
						<button title="más detalles de la causa" class="icono_btn sombra" onclick="dialogo('dialog80p', 'busqueda/detalle_causa/<?= $ca->id?>');">
							<img src="<?= getRutaIcono('info')?>">
						</button>
						<button title="editar causa" class="icono_btn sombra" onclick="dialog_editar_causa('<?= $ca->id?>',<?=$callback_cambios?>);">
							<img src="<?= getRutaIcono('editar')?>">
						</button>
						<button title="vincular una orientación a esta causa" class="icono_btn sombra" onclick="dialogo('dialog800', 'causa/vista_vincular_orientaciones/<?= $ca->id?>', {nuevo_dialogo:1});">
							<img src="<?= getRutaIcono('vincular_causa_orientacion')?>">
						</button>
						<button title="eliminar causa" class="icono_btn sombra" onclick="dialog_eliminar_causa('<?= $ca->id?>',<?=$callback_cambios?>);">
							<img src="<?= getRutaIcono('eliminar')?>">
						</button>
					</td>
				<?php endif; ?>
			</tr>
		<?php } ?>
	</tbody>
</table>


<script>
	$('#<?=$id_tabla?>').DataTable({
		"retrieve":true,
		"responsive":false,
		"autoWidth":false,
		"pagingType": "simple_numbers",
		"order": [[ 2, "desc" ]],
		"dom": '<"top"l>rt<"bottom"ip><"clear">'
	});
</script>
