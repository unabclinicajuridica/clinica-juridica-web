<?php $this->load->helper("utilidades"); ?>
<?php
	$activar_terminar = isset($activar_terminar) ? $activar_terminar : false;
	$activar_cambiar_usuario = isset($activar_cambiar_usuario) ? $activar_cambiar_usuario : false;
	$this->load->helper('utilidades');
?>
<div class="grande padding">Detalle de la Causa ROL '<?= $causa->rol_causa?>'</div>
<hr>
<table id="table_detalle_causa" class="display cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ID</th>
			<th>ROL/RIT</th>
			<th style="width: 64px;";>Ingreso</th>
			<th>Materia</th>
			<th>Abogado</th>
			<th>RUT Abogado</th>
			<th>Alumno</th>
			<th>RUT Alumno</th>
			<th>Usuario</th>
			<th>RUT Usuario</th>
			<?php if($causa->CAUSAL_TERMINO) { ?>
					<th>Causal Termino</th>
			<?php } else if($activar_terminar) { ?>
					<th>Termino</th>
			<?php } ?>
			<th>Tribunal</th>
			<th>Otros Detalles</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?= $causa->id?></td>
			<td><?= $causa->rol_causa ? $causa->rol_causa : '<div class="purple centro"><b>PENDIENTE</b></div>' ?></td>
			<td><?= $causa->INGRESO?></td>
			<td><?= $causa->nombre_materia ? $causa->nombre_materia : '<div class="purple centro"><b>PENDIENTE</b></div>' ?></td>
			<td><img title="Haga click aqui para ver las causas del Abogado" class="click" onclick="causas_usuario('abogado', <?= $causa->RUT_ABOGADO?>);" src="../../assets/images/lupa.png" height="15px"><?= $causa->NOMBRE_ABOGADO?></td>
			<td class="centro">
				<span><?= $causa->RUT_ABOGADO."-".$causa->DV_ABOGADO?></span>
				<?php if($activar_cambiar_usuario): ?>
					<div>
					<img title="Haga click aqui para cambiar de Profesor" class="click" onclick="dialog_cambiar_usuario('abogado', '<?= $causa->id?>', '<?= $causa->NOMBRE_ABOGADO?>');" src="../../assets/images/edit-icon.png" height="15px">
					</div>
				<?php endif; ?>
			</td>
			<td>
				<img title="Haga click aqui para ver las causas del Alumno" class="click" onclick="causas_usuario('alumno', <?= $causa->RUT_ALUMNO?>);" src="../../assets/images/lupa_verde.png" height="15px"><?= $causa->NOMBRE_ALUMNO?>
			</td>
			<td class="centro">
				<span><?= $causa->RUT_ALUMNO."-".$causa->DV_ALUMNO?></span>
				<?php if($activar_cambiar_usuario): ?>
					<div>
					<img title="Haga click aqui para cambiar de Alumno" class="click" onclick="dialog_cambiar_usuario('alumno', '<?= $causa->id?>', '<?= $causa->NOMBRE_ALUMNO?>');" src="../../assets/images/edit-icon.png" height="15px">
					</div>
				<?php endif; ?>
			</td>
			<td>
				<img title="Haga click aqui para ver las causas del Usuario"  class="click" onclick="causas_usuario('cliente', <?= $causa->rut_cliente?>);" src="../../assets/images/lupa_roja.png" height="15px"><?= $causa->nombre_cliente?>
			</td>
			<td><?= $causa->rut_cliente."-".$causa->dv_cliente?></td>
			<?php
				if($causa->nom_causal):
					echo "<td>".$causa->nom_causal."</td>";
				elseif($activar_terminar):
					if($causa->TERMINO != null && $causa->TERMINO != ""):
						echo "<td>".$causa->TERMINO."</td>";
					else: ?>
						<td>
							<button class="icono_btn" title="terminar la causa" onclick="terminar_causa('<?= $causa->id?>');">
								<img src="<?= getRutaIcono('terminar')?>">
							</button>
						</td>
					<?php
					endif;
				endif;
			?>
			<td><?= $causa->NOMBRE_TRIBUNAL ? $causa->NOMBRE_TRIBUNAL : '<div class="purple centro"><b>PENDIENTE</b></div>' ?></td>
			<td>
				<div style="margin:3px 3px;  display:flex;justify-content:space-around">
					<button class="icono_btn" title="Audiencias" onclick="trae_audiencias_2('<?= $causa->rol_causa?>');">
						<img src="<?=getRutaIcono('audiencia')?>">
						<span class="medalla_<?= $causa->cantidad_audiencias ? 'azul">'.$causa->cantidad_audiencias : 'gris">0'?></span>
					</button>
					<button class="icono_btn" title="Agendaciones" onclick="trae_agendaciones('<?= $causa->id?>');">
						<img src="<?=getRutaIcono('agenda')?>">
						<span class="medalla_<?= $causa->cantidad_agendaciones ? 'azul">'.$causa->cantidad_agendaciones : 'gris">0'?></span>
					</button>
					<button class="icono_btn" title="Orientaciones" onclick="dialogo('dialog80p', 'causa/vista_orientaciones/<?= $causa->id?>', {'nuevo_dialogo':true, titulo:'Orientaciones de la Causa ID <?= $causa->id?>'});">
						<img src="<?=getRutaIcono('orientacion')?>">
						<span class="medalla_<?= $causa->cantidad_orientaciones ? 'azul">'.$causa->cantidad_orientaciones : 'gris">0'?></span>
					</button>
					<button class="icono_btn" title="Trámites" onclick="dialogo_tramites('<?= $causa->id?>')">
						<img src="<?=getRutaIcono('tramite')?>">
						<span class="medalla_<?= $causa->cantidad_tramites ? 'azul">'.$causa->cantidad_tramites : 'gris">0'?></span>
					</button>
				</div>
			</td>
		</tr>
	</tbody>
</table>

<script>
	// console.log(tabla);
	// if(tabla == undefined) {
		// var tabla = $('#table_detalle_causa').dataTable({
		$('#table_detalle_causa').dataTable({
				"retrieve":true,
				"responsive":false,
				"autoWidth":false,
				"ordering": false,
				"dom": "tr"
		} );
	// } else { console.log('DRAWING DATATABLE.'); tabla = $('#table_detalle_causa').dataTable();}
</script>
