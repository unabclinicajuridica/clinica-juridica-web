<h2 class="" style="margin:0px 0px 13px 10px;">Seleccione una Causa</h2>

<div style="">

	<table id="table_seleccion_causa" class="compact cell-border stripe" >
		<thead>
			<tr>
				<th>Ingresada</th>
				<th>ROL/RIT</th>
				<th>Abogado</th>
				<th>Alumno</th>
				<th>Usuario</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($causas as $causa) { ?>
					<tr class="hover click" data-rol="<?= $causa->rol_causa?>" data-id="<?= $causa->id?>">
						<td><?=$causa->INGRESO?></td>
						<td><?= $causa->rol_causa ? $causa->rol_causa : '<div class="purple"><b>pendiente</b></div>' ?></td>
						<td class="abogado" data-rut="<?= $causa->RUT_ABOGADO?>" data-nombre="<?= $causa->NOMBRE_ABOGADO?>"><?= $causa->NOMBRE_ABOGADO?></td>
						<td class="alumno" data-rut="<?= $causa->RUT_ALUMNO?>" data-nombre="<?= $causa->NOMBRE_ALUMNO?>"><?= $causa->NOMBRE_ALUMNO ? $causa->NOMBRE_ALUMNO : '<span class="rojo">sin alumno</span>' ?></td>
						<td class="cliente"><?= $causa->nombre_cliente?></td>
					</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
		function trae_audiencias(objeto_fila) {
			var rol_causa = objeto_fila.getAttribute('data-rol');
			var id_causa = objeto_fila.getAttribute('data-id');
			$("#rol_causa").val(rol_causa);
			$("#id_correlativa_causa").val(id_causa);
			$("#add_audiencia").css('visibility','hidden');
			var miurl = base_url + index_page + "/audiencia/vista_busqueda_audiencias/?rol_causa="+rol_causa;

			$.ajax({url:miurl, success:function(result) {
				$("#add_audiencia").css('visibility','visible');
				$("#res_audiencia").html(result);
			}});
		}

		function rellenar_audiencia(objeto_fila) {
			var $fila = $(objeto_fila);
			var rol_causa = objeto_fila.getAttribute('data-rol');
			var id_causa = objeto_fila.getAttribute('data-id');
			var nomabog = $fila.find('.abogado').attr('data-nombre');
			var rutabog = $fila.find('.abogado').attr('data-rut');
			var nomalum = $fila.find('.alumno').attr('data-nombre');
			var rutalum = $fila.find('.alumno').attr('data-rut');
			if( ! rol_causa) rol_causa = 'ID: '+id_causa+' (ROL Pendiente)';
			$("#rol_causa").val(rol_causa);
			$("#rut_alumno").val(rutalum);
			$("#rut_profesor").val(rutabog);
			$("#nombre_abogado").val(nomabog);
			$("#nombre_alumno").val(nomalum);

		}

		$('#table_seleccion_causa').on('click', 'tbody tr', function() {
			<?= $funcion_callback?>(this);
			cerrar_dialog_conteniendo('table_seleccion_causa');
		});
	</script>

	<script>
		$('#table_seleccion_causa').DataTable({
			"bLengthChange": false,
			"pageLength": 10,
			"pagingType": "simple_numbers",
			"responsive":false,
			"autoWidth":false,
			"order": [[ 0, "desc" ]]
		});
	</script>
</div>
