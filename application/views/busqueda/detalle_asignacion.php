<?php $this->load->helper('configuracion'); ?>
<style type="text/css">
	#table_detalle_bloque_horario_personal td {
		vertical-align: baseline;
		padding-top: 3px;
		padding-bottom: 2px;
	}
	#table_detalle_bloque_horario_personal tr td:first-child {
		font-weight: normal;
		text-align:right;
	}
	#table_detalle_bloque_horario_personal label {
		padding-right: 5px;
		display:block;
	}

</style>

<table id="table_detalle_bloque_horario_personal" style="width:100%;margin:auto;">
	<?php if($agendacion->titulo_evento) { ?>
		<tr>
			<td><label>Título</label></td>
			<td><?= $agendacion->titulo_evento?></td>
		</tr>
	<?php } ?>
	<tr>
		<td><label>Tipo de Evento</label></td>
		<td><?= $agendacion->tipo_asunto?></td>
	</tr>
	<?php if($nombre_usuario) { ?>
		<tr>
			<td><label>Usuario</label></td>
			<td><?= $nombre_usuario?></td>
		</tr>
	<?php } ?>

<!--
	<tr>
		<td>ID</td>
		<td><?= $agendacion->id_asunto?></td>
	</tr>
-->

	<?php if($rol_causa) { ?>
		<tr>
			<td><label>ROL/RIT de Causa</label></td>
			<td><?= $rol_causa?></td>
		</tr>
	<?php } ?>


	<?php if($agendacion->descripcion_evento) { ?>
		<tr>
			<td><label>Descripción</label></td>
			<td><?= $agendacion->descripcion_evento?></td>
		</tr>
	<?php } ?>

	<tr>
		<td><label>Inicio</label></td>
		<td><?= date('H:i', strtotime($agendacion->hora_inicio))?></td>
	</tr>
	<tr>
		<td><label>Termino</label></td>
		<td><?= date('H:i', strtotime($agendacion->hora_fin))?></td>
	</tr>
	<tr>
		<td><label>Fecha</label></td>
		<td><?= date(getFormatoFechaPHP(), strtotime($agendacion->fecha_asignacion))?></td>
	</tr>
</table>
