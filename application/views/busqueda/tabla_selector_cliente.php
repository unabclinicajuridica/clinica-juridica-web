<table class="compact cell-border stripe" id="table_seleccionar_cliente">
	<thead>
		<tr>
			<th>Rut</th>
			<th>Nombre</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($usuarios as $user) { ?>
			<?php $rutformateado = $user->rut_cliente ? ($user->dv_cliente != NULL ? $user->rut_cliente."-".$user->dv_cliente : $user->rut_cliente) : '' ?>

			<tr class="click hover" onclick="asignar_usuario( <?= "'".$user->id."', '".$rutformateado."', '".$user->nombre_cliente."', '".$user->telefono."', '".$user->domicilio."', '".$user->email."', '".$user->edad."'" ?>);">
				<td>
					<label id="rut_<?= $user->rut_cliente?>"><?= $rutformateado?><label>
				</td><td>
					<label id="nombre_<?= $user->rut_cliente?>"><?= $user->nombre_cliente?></label>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<script type="text/javascript">
	$('#table_seleccionar_cliente').DataTable({
		"responsive":false,
		"autoWidth":false,
		'bLengthChange': false,
		'pageLength': 10,
		'pagingType': 'simple_numbers',
		'order': [[ 1, "asc" ]]
	});

	function asignar_usuario(id_cliente, rut_cliente, nombre, telfono, domicilio, email, edad) {
		$('#id_cliente').val('');
		$('#nombre_usuario').val('');
		$('#rut_usuario').val('');
		$('#telefono').val('');
		$('#domicilio').val('');
		$('#correo_electronico').val('');

		$('#id_cliente').val(id_cliente);
		$('#nombre_usuario').val(nombre).change();
		$('#rut_usuario').val(rut_cliente);
		$('#edad_usuario').val(edad);
		$('#telefono').val(telfono);
		$('#correo_electronico').val(email);
		$('#domicilio').val(domicilio);

		cerrar_dialog_conteniendo('table_seleccionar_cliente');
	}
</script>
