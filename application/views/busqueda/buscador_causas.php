
	<h2 class="titulo_fieldset"><?= $titulo ?></h2>

	<hr>
	<div>

	<div style="margin:10px 0px;">
		<input style="margin-left: 40px; margin-right:5px;" type="radio" name="tipo_busqueda" value="rut" >Buscar por RUT
		<input style="margin-left: 40px; margin-right:5px;" type="radio" name="tipo_busqueda" value="nombres" checked>Buscar por Nombres
	</div>

	<div style="margin:10px 0px;display:flex; justify-content:space-between;flex-wrap:wrap;">
		<div>
			<a style="margin-left: 30px; margin-right:5px;display:inline">ROL/RIT:</a>
			<input class="input_busqueda" type="text" name="rol_busqueda" id="rol_busqueda">
		</div>
		<div>
			<a style="margin-left: 30px; margin-right:5px;">Abogado:</a>
			<input class="input_busqueda" type="text" name="abogado" id="abogado">
		</div>
		<div>
			<a style="margin-left: 30px; margin-right:5px;">Alumno:</a>
			<input class="input_busqueda" type="text" name="alumno" id="alumno">
		</div>
		<div>
			<a style="margin-left: 30px; margin-right:5px;">Usuario:</a>
			<input class="input_busqueda" type="text" name="usuario" id="usuario">
		</div>

		<div>
			<a style="margin-left: 30px; margin-right:5px;">ID de Sistema:</a>
			<input class="input_busqueda" type="text" name="id_correlativa" id="id_correlativa">
		</div>
	</div>

	<?php if($tipo_busqueda_causas === 'normal') { ?>
		<div style="display:flex; justify-content:flex-start;flex-wrap:wrap;">
			<div>
				<label style="margin-left: 30px; margin-right:5px;">Buscar Solo Causas Vigentes</label>
				<input type="checkbox" name="vigentes" id="vigentes">
			</div>
			<div>
				<label style="margin-left: 30px; margin-right:5px;">Buscar Sin Alumno Asignado</label>
				<input type="checkbox" name="sin_alumno" id="sin_alumno">
			</div>
		</div>
	<?php } ?>





		<button onClick="buscar_causas('<?= $tipo_busqueda_causas ?>');" class="boton" style="margin-right:15px;float:right;">Buscar</button>

		<div style="clear:both"></div>
	<br><hr>


	</div>
	<h3 id="h3_titulo_tabla" class="titulo_fieldset"><?= isset($titulo_tabla) ? $titulo_tabla : 'Resultado Búsqueda' ?></h3>
	<div id="resultados_busquedas" >
		<?php $this->view('busqueda/tabla_causas', [ 'causas'=>$causas, 'mostrar_detalles'=>FALSE, 'mostrar_opciones'=>TRUE ]) ?>
	</div>

<script>
	var h3_titulo_tabla = document.getElementById('h3_titulo_tabla');

	$('.input_busqueda').keyup(function(e) {
		if(e.keyCode == 13)	{
			buscar_causas('<?= $tipo_busqueda_causas ?>');
		}
	});

	function buscar_causas(tipo_busqueda_causas) {
		$('#dialog').dialog('close');

		var rol = $('#rol_busqueda').val();
		var abogado = $('#abogado').val();
		var alumno = $('#alumno').val();
		var usuario = $('#usuario').val();
		var tipo_busqueda = $('input[name=tipo_busqueda]:checked').val();
		var id_correlativa =  $('input[name=id_correlativa]').val();

		var vigente = 0;
		if(typeof(tipo_busqueda_causas) !== 'undefined' && tipo_busqueda_causas === 'normal') {
			if($('#vigentes').is(":checked")){
				vigente= 1;
			}
		}
		var sin_alumno = $('#sin_alumno').is(":checked") ? 1 : 0;

		var url = "busqueda/buscar_causas/?rol="+rol+"&abogado="+abogado+"&alumno="+alumno+"&usuario="+usuario+"&tipo_busqueda="+tipo_busqueda+"&vigente="+vigente+"&id_correlativa="+id_correlativa+"&tipo_busqueda_causas="+tipo_busqueda_causas+"&sin_alumno="+sin_alumno;
		cargar('resultados_busquedas', url);
		h3_titulo_tabla.innerHTML = 'Resultado Búsqueda';
	}
	function buscar_causas_norm() {buscar_causas('normal');}
	function buscar_causas_hist() {buscar_causas('historico');}
</script>
