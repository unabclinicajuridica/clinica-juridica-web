﻿<h2 class="titulo_fieldset">Búsqueda Unificada de Orientaciones </h2>

<hr>

<div><br>

	<label style="margin-left: 30px; margin-right:5px;">Nombre Abogado:</label>
	<input class="input_busqueda" type="text" name="abogado" id="abogado">

	<label style="margin-left: 30px; margin-right:5px;">Nombre Usuario:</label>
	<input class="input_busqueda" type="text" name="usuario" id="usuario">

	<button onClick="buscar_orientaciones();" class="boton" style="margin-right:15px;float:right;">Buscar</button>

	<div style="clear:both"></div>
<br><hr>


</div>
<h3 class="titulo_fieldset">Resultado Búsqueda </h3>
<div id="resultados_busquedas">
	<?php $this->view('orientacion/tabla_orientaciones', [ 'orientaciones'=>$orientaciones, 'mostrar_vincular'=>true, 'mostrar_lupas'=>true ]);?>
</div>

<script>
	$('.input_busqueda').keyup(function(e) {
		if(e.keyCode == 13)	{
			buscar_orientaciones();
		}
	});
</script>
