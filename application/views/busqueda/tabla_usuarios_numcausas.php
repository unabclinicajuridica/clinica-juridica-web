<head>
	<style>
		.trespecial:hover .tdespecial {background:#819FF7}
	</style>
</head>

<body>
	<form id="resultado_busqueda_profesor">
		<div>
			<table id="table_prof_select" class="stripe" style="width:100%;">
					<thead>
					<tr>
						<td>Rut</td>
						<td>Nombre</td>
						<td>Número de Causas</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($usuarios as $row) { ?>
						<tr class="clickable hover" onclick="asignar_son(this)">
							<td class="td_rut"><?= $row->rut?></td>
							<td class="td_nombre"><?= $row->nombre?></td>
							<td><?= $row->numero_de_causas?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>

			<script>
				$('#table_prof_select').dataTable();
				var input_id_rut = '<?=$input_id_rut?>';
				var input_id_nombre = '<?=$input_id_nombre?>';
				// $('#table_prof_select tr.clickable').click(
				// 	function() {
				// 		var rut = $(this).find('.td_rut').html();
				// 		$('#'+input_id_rut).val(rut);
				// 		var nombre = $(this).find('.td_nombre').html();
				//
				// 		var $nodo_nombre = $('#'+input_id_nombre);
				// 		var tag_nodo = $nodo_nombre[0].tagName;
				//
				// 		if(tag_nodo === 'SPAN') {
				// 			$nodo_nombre.html(nombre);
				// 		} else {
				// 			$nodo_nombre.val(nombre).change();
				// 		}
				//
				// 		<?= isset($callback_exito) && $callback_exito ? $callback_exito.'(rut);' : '' ?> // MUY BUENO.
				// 		cerrar_dialog_conteniendo('resultado_busqueda_profesor');
				// 	}
				// );
				function asignar_son(objeto_fila) {
					var rut = $(objeto_fila).find('.td_rut').html();
					$('#'+input_id_rut).val(rut);
					var nombre = $(objeto_fila).find('.td_nombre').html();

					var $nodo_nombre = $('#'+input_id_nombre);
					var tag_nodo = $nodo_nombre[0].tagName;

					if(tag_nodo === 'SPAN') {
						$nodo_nombre.html(nombre);
					} else {
						$nodo_nombre.val(nombre).change();
					}

					<?= isset($callback_exito) && $callback_exito ? $callback_exito.'(rut);' : '' ?> // MUY BUENO.
					cerrar_dialog_conteniendo('resultado_busqueda_profesor');
				}
			</script>
		</div>
	</form>
</body>
