<?php $this->load->helper("utilidades"); ?>
<?php
	$titulo = isset($titulo) ? $titulo : "Audiencias";
	$columna_nota = isset($columna_nota) ? $columna_nota : "Nota Actual";
	$rellenar_detalles = isset($rellenar_detalles) ? $rellenar_detalles : false;
	$titulo_vacio = isset($titulo_vacio) ? $titulo_vacio : "No hay Audiencias.";
 ?>
<h2 style="text-align:center"><?= $titulo?></h2>
<div>
	<?php if(count($audiencias) > 0) { ?>
		<table cellspacing="0" class="display compact cell-border" id="table_audiencias_evaluar">
			<thead>
				<tr>
					<th>Fecha de la Audiencia</th>
					<th>Hora</th>
					<th>ROL/RIT</th>
					<th>Tipo de Audiencia</th>
					<th><?= $mostrar?></th>
					<th>Estado de Revision</th>
					<th><?= $columna_nota?></th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($audiencias as $audc) { ?>
					<tr>
						<td><?= $audc->fecha?></td>
						<td><?= $audc->hora?></td>
						<td><?php $this->view('partial/click_detalle_causa', ['rol' => $audc->rol_causa]); ?></td>
						<td><?= $audc->tipo_audiencia?></td>
						<td>
							<?php
								if($mostrar == 'Abogado') echo $audc->nombre_abogado;
								if($mostrar == 'Alumno') echo $audc->nombre_alumno;
							?>
						</td>
						<td><?= $audc->estado_revision?></td>
					<?php if($audc->evaluacion_completada) { ?>
						<td><?= $audc->nota_final/10?></td>
					<?php } else { ?>
						<td><?= 'SIN EVALUAR'?></td>
					<?php } ?>
						<td>
							<button title="ver detalles de la audiencia" class="icono_btn" onclick="ver_audiencia(<?= $audc->id_audiencia?>);"><img src="<?= getRutaIcono('detalles')?>"></button>
							<?php if($evaluar) { ?>
								<button title="editar notas de la audiencia" class="icono_btn" onclick="dialogo('dialog', 'audiencia/vista_editar', {'id_audiencia':<?= $audc->id_audiencia?>, 'editar_notas':true, 'funcion_exito':'<?= $funcion_exito?>'});">
									<img src="<?= getRutaIcono('evaluar')?>">
								</button>
							<?php } ?>
							<?php if($rellenar_detalles) { ?>
								<button title="rellenar detalles de la audiencia" class="icono_btn" onclick="dialogo('dialog', 'audiencia/vista_editar', {'id_audiencia':<?= $audc->id_audiencia?>, 'editar_detalles':true, 'funcion_exito':'<?= $funcion_exito?>'});">
									<img src="<?= getRutaIcono('rellenar')?>">
								</button>
							<?php } ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<script>
			$('#table_audiencias_evaluar').DataTable({
				"pagingType": "simple_numbers",
				"order": [[ 0, "desc" ], [ 1, "desc" ]]
			});
		</script>
	<?php } else { ?>
		<div class="area_gris">
			<label><?= $titulo_vacio?></label>
		</div>
	<?php } ?>
</div>
