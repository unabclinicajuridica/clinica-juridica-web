<?php
	$modo_detalle = isset($modo_detalle) ? $modo_detalle : FALSE;
	$disabled_o_no = $modo_detalle ? 'disabled="disabled"' : FALSE;
	$mostrar_boton_salir = isset($mostrar_boton_salir) ? $mostrar_boton_salir : FALSE;

	function rellenar_select_notas($valor_seleccionado=null) {
		$option_notas = '<option value=0 selected="selected">--</option>';
		for ( $i=1, $j=10; $i < 8; $i++, $j=$j+10 ) {
			if($j."" === $valor_seleccionado) {
				$option_notas .= '<option value='.$i.'0 selected="selected">'.$i.',0</option>';
			} else {
				$option_notas .= '<option value='.$i.'0>'.$i.',0</option>';
			}
		}
		return $option_notas;
	}

	$mostrar_selector_causa = isset($mostrar_selector_causa) ? $mostrar_selector_causa : TRUE;
	$mostrar_comentar = isset($mostrar_comentar) ? true : false;
	$fondo_ancho = isset($ancho) && $ancho ? 'wide' : '';
?>


<!-- jonthornton.timepicker -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.css">
<script type="text/javascript" src="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.min.js" ></script>

<div class="fondo_formularios <?=$fondo_ancho?>">
	<form id="form_audiencia" name="form_ingresar_audiencia">

		<h2 style="text-align:center"><?=$titulo_formulario?></h2>
		<br>
		<table style="margin:auto;">
			<tr>
				<td><label for="rol_causa">ROL/RIT:&nbsp;</label></td>
				<td>
					<?php if($modo_detalle): ?>
						<input class="input_readonly" type="text" value="<?= $audiencia->rol_causa ?>" readonly>
					<?php else: ?>
						<input id="rol_causa" name="rol_causa" class="dato_formulario input_readonly required_readonly" type="text" value="<?= isset($causa->rol_causa) ? $causa->rol_causa : '' ?>" required="required">
						<input type="hidden" id="id_causa" name="id_causa" value="<?= isset($causa) ? $causa->id : '' ?>">
					<?php endif; ?>
					<?php if($mostrar_selector_causa): ?>
						<button type="button" onclick="dialogo_seleccionar_causa('rellenar_audiencia', true);"  style="margin-bottom:4px;" title="Buscar una causa">
							<img style="cursor:pointer;"  src="../../assets/images/lupa.png" height="24px">
						</button>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td><label  for="alumno">Profesor: </label></td>
				<td>
					<?php if($modo_detalle): ?>
						<input type="text" id="nombre_abogado" name="nombre_abogado" value="<?= $audiencia->nombre_abogado?>" readonly="readonly" <?= $disabled_o_no?>>
					<?php else: ?>
						<input type="text" id="nombre_abogado" name="nombre_abogado" class="required_readonly" value="<?= $nombre_abogado?>" <?= $disabled_o_no?> required="required">
						<input type="hidden" class="dato_formulario" id="rut_profesor" name="rut_profesor" value="<?= $rut_abogado?>">
						<?php if($seleccionar_abogado): ?>
							<button type="button" class="button_icono" onclick="buscar_abogado('rut_profesor','nombre_abogado');">
								<img style="cursor:pointer;" src="../../assets/images/lupa.png" height="25px">
							</button>
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td><label for="alumno">Alumno: </label></td>
				<td>
					<?php if($modo_detalle): ?>
						<input type="text" id="nombre_alumno" name="nombre_alumno" value="<?= $audiencia->nombre?>" readonly="readonly" <?= $disabled_o_no?>>
					<?php else: ?>
						<input type="text" id="nombre_alumno" name="nombre_alumno" class="required_readonly" value="<?= $nombre_alumno?>" <?= $disabled_o_no?> required="required">
						<input type="hidden" class="dato_formulario" id="rut_alumno" name="rut_alumno" value="<?= $rut_alumno?>">
						<?php if($seleccionar_alumno): ?>
							<button type="button" class="button_icono" onclick="buscar_alumno('rut_alumno', 'nombre_alumno');">
								<img class="click" src='../../assets/images/lupa.png' height='25px'>
							</button>
						<?php endif; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td><label for="tipo_audiencia">Tipo Audiencia: </label></td>
				<td>
					<input id="tipo_audiencia" class="dato_formulario validar" name="tipo_audiencia" type="text" value="<?= $audiencia->tipo_audiencia?>" <?= $disabled_o_no?> pattern=".{4,}" title="mímimo 4 caracteres">
				</td>
			</tr>
			<tr>
				<td><label for="fecha_audiencia">Fecha de inicio: </label></td>
				<td >
					<input id="fecha_audiencia" class="dato_formulario" name="fecha_audiencia" type="text" value="<?= $audiencia->fecha?>" <?= $disabled_o_no?> required="required">
				</td>
			</tr>
			<tr >
				<td><label for="hora_audiencia">Hora de inicio: </label></td>
				<td >
					<input id="hora_audiencia" class="dato_formulario" name="hora_audiencia" type="text" value="<?= $audiencia->hora?>" <?= $disabled_o_no?> required="required">
				</td>
			</tr>
			<tr >
				<td><label for="descripcion">Descripcion: </label></td>
				<td >
					<textarea id="descripcion" name="descripcion" class="dato_formulario validar" style="resize: none;" rows="5" <?= $disabled_o_no?> pattern=".{12,}" title="mímimo 12 caracteres"><?= $audiencia->descripcion?></textarea>
				</td>
			</tr>
		</table>


		<?php if($mostrar_evaluacion) { ?>
			<hr>
			<?php $this->view('partial/partial_audiencia_notas', array('audiencia'=>$audiencia, 'disabled_o_no'=>$disabled_o_no, 'mostrar_comentar'=>$mostrar_comentar, 'notas_editable'=>!($modo_detalle))); ?>
		<?php } ?>
		<br>

		<div class="flex_centro margen">
			<?php if($mostrar_boton_confirmar_revision): ?>
				<button type="button" id="btn_confirmar" style="margin-right:16px;" onclick="confirmar_lectura(this, <?= $audiencia->id_audiencia?>, 'audiencia');"><?= $texto_boton_confirmar?></button>
			<?php elseif(! $modo_detalle): ?>
				<button type="submit" style="margin-right:16px;">Ingresar</button>
			<?php endif; ?>
			<?php if($mostrar_boton_salir): ?>
				<button type="button" onclick="$(this).closest('.ui-dialog-content').dialog('close');">Salir</button>
			<?php endif; ?>
		</div>
	</form>
</div>

<script>
	$("#form_audiencia .required_readonly").keydown( function(e){ e.preventDefault(); } ).on('paste', function(e){ e.preventDefault(); } );

	$("#fecha_audiencia" ).datepicker().datepicker("setDate", new Date());
	$("#hora_audiencia").timepicker( { 'minTime': '08:00', 'maxTime': '22:00', 'timeFormat': 'H:i'} );

	$('#form_audiencia').submit(function (event) {
		event.preventDefault();
		var tipoa = document.getElementById("tipo_audiencia");
		var descr = document.getElementById("descripcion");

		// perfek = tipoa.validity.valid && descr.validity.valid && descr.value.length >= 12;
		perfek = tipoa.validity.valid;

		if($('#nombre_abogado').val() == '' || $('#nombre_alumno').val() == '') {
			perfek = false;
		}
		if(tipoa.value.length < 4) {tipoa.setCustomValidity("mínimo 4 caracteres");}

		if(perfek === true) {
			var formData = $(this).serializeArray();
			ingresar_audiencia(formData, main_ingreso_audiencia);
		} else {
			swal('','Debe completar todos los campos requeridos.', 'warning');
		}
		return false;
	});
</script>
