<?php $this->load->helper("utilidades"); $this->load->helper('configuracion');?>
<div class="grande padding">Audiencias de la Causa</div><hr></hr>
<div>
	<table cellspacing="0"  class="display compact cell-border" id="audiencias">
		<thead>
			<tr>
				<th>número</th>
				<th>Fecha</th>
				<th>Hora</th>
				<th>Tipo</th>
				<th>Descripcion</th>
				<th>Nota Final</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($audiencias->result() as $row) { ?>
				<tr>
					<td class="centro"><label><?= $row->numero_audiencia?></label></td>
					<td><label><?= date(getFormatoFechaPHP(), strtotime($row->fecha))?></label></td>
					<td><label><?= $row->hora?></label></td>
					<td>
						<?php if($row->tipo_audiencia) { ?>
							<label><?= $row->tipo_audiencia?></label>
						<?php } else { ?>
							<div class="rojo centro">SIN TIPO</div>
						<?php } ?>
					</td>
					<td>
						<?php if($row->tipo_audiencia) { ?>
							<label><?= substr($row->descripcion, 0, 32)?><?= strlen($row->descripcion) > 32 ? "..." : "" ?></label>
						<?php } else { ?>
							<div class="rojo centro">SIN DESCRIPCIÓN</div>
						<?php } ?>
					</td>
					<td>
						<?php if($row->evaluacion_completada) { ?>
							<label><?= $row->nota_final/10?></label>
						<?php	} else {?>
							<div class="rojo centro">SIN EVALUAR</div>

						<?php } ?>
					</td>
					<td>
						<button title="ver detalles de la audiencia" class="icono_btn" onclick="ver_audiencia('<?= $row->id_audiencia?>');">
							<img src="<?= getRutaIcono('detalles')?>">
						</button>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
	<script>
		$('#audiencias').DataTable({
			"pagingType": "simple_numbers",
			"order": [[0, "desc"]]
		});
	</script>
</div>
