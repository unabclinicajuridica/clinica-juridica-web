<div class="fondo_formularios">
	<h2 style="text-align:center">Búsqueda de Audiencias por Causa</h2>
	<div class="padding_medio">
		<table style="margin:auto;table-layout: fixed;">
			<tr>
				<td>
					<button id="add_audiencia" name="add_audiencia" style="visibility:hidden;" onclick="abrir_dialogo_audiencia();" title="Ingresar una Audiencia para la Causa seleccionada">
						<img style="cursor:pointer;position:relative;bottom:1px;right:2px;"  src="../../assets/images/signo_mas.png" height="16px"></img>
						Ingresar Audiencia
					</button>
				</td>


				<td style="display:flex;align-items:center;">
					<label for="rol_causa">ROL/RIT:&nbsp;&nbsp;</label>
					<input id="rol_causa" type="text" class="input_readonly" readonly>
				</td>
				<td>
					<button type="button" onclick="dialogo_seleccionar_causa('trae_audiencias');"  style="margin-bottom:4px;" title="Seleccionar una Causa">
						<img style="cursor:pointer;"  src="../../assets/images/lupa.png" height="24px">
					</button>
					<!-- <button type="button" onclick="iniciar_busqueda_audiencias();" style="margin-bottom:4px;margin-left:20px;" title="Iniciar búsqueda">
						Buscar
					</button> -->
				</td>
			</tr>
		</table>
	</div>
</div>
<div class="" style="width:60%;margin:20px auto 5px auto;">
	<hr>
	<div id="res_audiencia"><!-- tabla aquí --></div>
</div>
<script type="text/javascript">
function iniciar_busqueda_audiencias() {
	var rol_causa = $("#rol_causa").val();
	var id_causa = $("#id_correlativa_causa").val();

	$("#add_audiencia").css('visibility','hidden');
	var miurl = base_url + index_page + "/audiencia/vista_busqueda_audiencias/?rol_causa="+rol_causa;

	$.ajax({url:miurl, success:function(result) {
		$("#res_audiencia").html(result);
		$("#add_audiencia").css('visibility','visible');
	}});
}
</script>
