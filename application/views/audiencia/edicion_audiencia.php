<?php
	$disabled_o_no = '';
	$editable = true;

	function rellenar_select_notas($valor_seleccionado=null) {
		$option_notas = '<option value=0 selected="selected">--</option>';
		for ( $i=1, $j=10; $i < 8; $i++, $j=$j+10 ) {
			if($j."" === $valor_seleccionado) {
				$option_notas .= '<option value='.$i.'0 selected="selected">'.$i.',0</option>';
			} else {
				$option_notas .= '<option value='.$i.'0>'.$i.',0</option>';
			}
		}
		return $option_notas;
	}

	$editar_detalles = isset($editar_detalles) ? $editar_detalles : FALSE;
	$editar_notas = isset($editar_notas) ? $editar_notas : FALSE;
	$fondo_ancho = isset($ancho) && $ancho ? 'wide' : '';
?>


<div class="fondo_formularios <?=$fondo_ancho?>">
	<form id="form_update_audiencia" action="index.html" method="post">
		<input id="id_audiencia" name="id_audiencia" class="dato_formulario" type="hidden" value="<?= $audiencia->id_audiencia?>">
		<?php if($editar_detalles): ?>
			<h2 style="text-align:center">Detalle Audiencia</h2>
			<table style="margin:auto;">
				<tr>
					<td>
						<label  for="tipo_audiencia">Tipo Audiencia: </label>
					</td>
					<td>
						<input id="tipo_audiencia" class="dato_formulario" name="tipo_audiencia" type="text" value="<?= $audiencia->tipo_audiencia?>" <?= $disabled_o_no?>>
					</td>
				</tr>
				<tr>
					<td>
						<label  for="tipo_audiencia">Fecha: </label>
					</td>
					<td >
						<input id="fecha_audiencia" class="dato_formulario" name="fecha_audiencia" type="text" value="<?= $audiencia->fecha?>" disabled="disabled">
					</td>
				</tr>
				<tr >
					<td>
						<label  for="tipo_audiencia">Hora: </label>
					</td>
					<td >
						<input id="hora_audiencia" class="dato_formulario" name="hora_audiencia" type="time" value="<?= $audiencia->hora?>" disabled="disabled">
					</td>
				</tr>
				<tr >
					<td>
						<label  for="descripcion">Descripcion: </label>
					</td>
					<td >
						<textarea class="dato_formulario" style="resize: none;" rows="5" id="descripcion" name="descripcion" <?= $disabled_o_no?>><?= $audiencia->descripcion?></textarea>
					</td>
				</tr>
			</table>
		<?php endif; ?>

		<?php if($editar_notas): ?>
			<hr>
			<?php $this->view('partial/partial_audiencia_notas', array('audiencia'=>$audiencia, 'disabled_o_no'=>$disabled_o_no, 'notas_editable'=>$editable)); ?>
		<?php endif; ?>

		<div class="flex_centro margen">
			<?php if($editable): ?>
				<button type="submit" style="margin-right:16px;">Guardar</button>
			<?php endif; ?>
			<button type="button" onclick="$(this).closest('.ui-dialog-content').dialog('close');">Salir</button>
		</div>
	</form>
</div>
<script>
  $('#form_update_audiencia').submit(function (e) {
	  e.preventDefault();
	  var formData = $(this).serializeArray();
	  update_audiencia(formData,<?=$funcion_exito?>);
	  return false;
  });
</script>
