<?php
	$fondo_ancho = isset($ancho) && $ancho ? 'wide' : '';
?>


<div class="fondo_formularios <?=$fondo_ancho?>">
  <h2 style="text-align:center">Editar Orientación Nº <?= $orientacion->id_orientacion ?></h2>

	<form id="form_edit_orientacion" action="index.html" method="post">

		<input type="hidden" name="id_orientacion" value="<?=$orientacion->id_orientacion?>">

		<table style="margin:auto;">
			<!-- <tr>
				<td><label  for="fecha">Fecha de Ingreso:</label></td>
				<td><input type="text" id="fecha" name="fecha"  value="<?= $orientacion->fec_ingreso ?>"></td>
			</tr> -->
			<tr><td><?php $this->view('partial/partial_selector_causa', [ 'id_causa'=>$causa->id, 'rol_causa'=>$causa->rol_causa, 'solo_con_rol'=>0 ]); ?></td></tr>
			<tr><td><?php $this->view('partial/partial_selector_materia', [ 'id_competencia'=>$id_competencia, 'id_procedimiento'=>$id_procedimiento, 'id_materia'=>$id_materia ]); ?></td></tr>
			<tr>
				<td>
					<div class="flex_vertical">
						<label for="resena" class="padding">Reseña de la Orientación:</label>
						<textarea id="resena" name="resena" type="text"><?= $orientacion->resena ?></textarea>
					</div>
				</td>
			</tr>
			<!-- <tr>
				<td><label for="rol_causa">Causa (ROL): </label></td>
				<td>
					<input type="text" id="rol_causa" name="rol_causa" value="<?= $orientacion->id_causa ?>">
					<input type="hidden" id="id_causa" name="id_causa" value="<?= $orientacion->rut_abogado ?>" readonly>
				</td>
			</tr> -->

		</table>

		<div class="botones padding" style="text-align:right;">
			<?php if($orientacion->resena === NULL): ?>
				<button type="submit" id="guardar" name="guardar" class="margen_derecha">Guardar</button>
			<?php endif; ?>
			<button type="button" id="cancelar" name="cancelar" onclick="$(this).closest('.ui-dialog-content').dialog('destroy');">Salir</button>
		</div>
	</form>

</div>

<script>
	$( "#form_edit_orientacion" ).submit(function( event ) {
		var formData = $( this ).serializeArray();
		event.preventDefault();
		actualizar_orientacion(formData, main_orientaciones_sin_resena);
	});
</script>
