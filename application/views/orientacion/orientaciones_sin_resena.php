<?php $this->load->helper("utilidades"); ?>
<h2 class="titulo_vista">Orientaciones pendientes de Reseña</h2>
<?php if(count($orientaciones) > 0) { ?>
	<hr>
	<div id="resultados_busquedas" >
		<table id="table_orientsinresena" class="display compact cell-border">
			<thead>
				<tr>
					<th>Ingreso</th>
					<th>Materia</th>
					<th>Usuario</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($orientaciones as $row) { ?>
							<tr>
								<td><?= $row->fec_ingreso?></td>
								<td><?= $row->nombre_materia?></td>
								<td><img class="click" onclick="causas_usuario('cliente',<?= $row->rut_usuario?>);" src="../../assets/images/lupa_roja.png" height="15px">
									<?= $row->nombre_cliente?>
								</td>
								<td>
									<button title="Rellenar Orientación" onclick="dialog_editar_orientacion(<?= $row->id_orientacion?>,'completo', 'por_rut');" class="icono_btn">
										<img class="click" src="<?= getRutaIcono('editar')?>">
									</button>
								</td>
							</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<script>
		$('#table_orientsinresena').DataTable({
				"pagingType": "simple_numbers"
		});
	</script>

<?php } else { ?>
	<div class="area_gris">
		<label >Usted no tiene Orientaciones pendientes por rellenar.</label>
	</div>
<?php } ?>
