<?php
	$id_vinculador = "div_vinculador_".$id_causa;
	$id_formulario = "form_vinc_oris_".$id_causa;
?>
<div id="<?= $id_vinculador ?>">
	<div class="fondo_formularios wide">
		<form id="<?= $id_formulario ?>" action="index.html" method="post">
			<?php $this->view('partial/partial_selector_orientaciones', array('opcional'=>false)) ?>
		</form>
	</div>
</div>

<script type="text/javascript">
	var $este_dialogo = $('#<?=$id_vinculador?>').closest('.ui-dialog-content');
	$este_dialogo.dialog({
		title: "Vincular Orientaciones a Causa Nº <?= $id_causa?>",
		buttons: [
			{
				text: "Ok",
				icons: { primary: "ui-icon-heart" },
				click: function() {
					var formData = $('#<?= $id_formulario ?>').serializeArray();
					$.ajax({method:'POST', url:base_url+index_page+'causa/vincular_orientaciones/<?= $id_causa?>', dataType:'json', data:formData, success:function(result){
						if(result.exito) { autoCloseSwal('', 'Orientaciones vinculadas.', 'success'); $este_dialogo.dialog("close"); }
						else { swal('', 'Algo salió mal.', 'error'); }
					}});
				}
			},
			{
				text: "Cancelar",
				click: function() { $(this).dialog("close"); }
			}
		]
	});
</script>
