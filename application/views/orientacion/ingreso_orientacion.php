<?php
	$required_orientacion = isset($required_orientacion) && $required_orientacion ? "required oninvalid=\"this.setCustomValidity('Reseña requerida')\" onchange=\"this.setCustomValidity('')\"" : '';
	$fondo_ancho = isset($ancho) && $ancho ? 'wide' : '';
 ?>
<div class="fondo_formularios <?=$fondo_ancho?>">
	<h2 style="text-align:center">Ficha de Orientacion Clinica Juridica UNAB</h2>

	<form id="form_orientacion" action="index.html" method="post">

		<?php if($agendar): $this->view('partial/partial_datos_evento'); ?>
		<?php else: ?>
			<?php if($abogado_cambiable): ?>
				<div class="padding_medium" style="width:90%;margin:auto;">
					<?php $this->view('partial/partial_set_abogado', [ 'required'=>TRUE]); ?>
				</div>
			<?php else: ?>
				<input id="input_rut_abogado" name="rut_abogado" type="hidden" value="<?= $rut_abogado ?>">
			<?php endif; ?>
		<?php endif; ?>

		<?php $this->view('partial/partial_selector_causa', array('opcional'=>true)); ?>

		<?php $this->view('partial/partial_selector_cliente', [ 'required_nombre'=>true ]);?>

		<?php $this->view('partial/partial_selector_materia', [ 'required_materia'=>$required_materia ]);?>

		<table style="margin:auto;">
			<tr>
				<td>
					<label>Breve reseña de la orientacion brindada:</label>
					<textarea id="resena" name="resena" cols="45" rows="8" <?=$required_orientacion?>></textarea>
				</td>
			</tr>
		</table>
		<div class="centro margen">
			<button type="submit" class="btn">Ingresar Orientacion</button>
		</div>
	</form>
</div>
<script type="text/javascript">
	// $("#fondo_formularios .required_readonly").keydown( function(e){ e.preventDefault(); } ).on('paste', function(e){ e.preventDefault(); } );
	$( "#form_orientacion" ).submit(function( event ) {
		var formData = $( this ).serializeArray();
		event.preventDefault();
		ingresar_orientacion(formData, <?= $agendar ?> );
		return false;
	});
</script>
