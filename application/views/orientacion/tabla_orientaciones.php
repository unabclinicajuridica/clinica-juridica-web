<?php
	$fila_clickable = isset($fila_clickable) ? $fila_clickable : false;
	$input_id_orientacion = isset($input_id_orientacion) ? $input_id_orientacion : 'frkhdergh';
	$id_tabla = isset($id_tabla) ? $id_tabla : 'table_orients';
	$this->load->helper('utilidades');
?>
<table id=<?=$id_tabla?> class="display cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Fecha Ingreso</th>
			<th>Materia de Orientación</th>
			<th>Abogado</th>
			<th>Usuario</th>
			<th>Reseña</th>
			<?= $mostrar_vincular ? '<th>Causa Vinculada</th>' : '' ?>
			<th>

			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($orientaciones as $orient) { ?>
			<tr data-id-orientacion="<?= $orient->id_orientacion?>">
				<td><?= $orient->fec_ingreso?></td>
				<td><?= $orient->nombre_materia ? $orient->nombre_materia : '<div class="purple"><b>PENDIENTE</b></div>' ?></td>
				<td>
					<?php if($mostrar_lupas): ?>
						<img class="click" onclick="causas_usuario('abogado',<?= $orient->RUT_ABOGADO?>,true);" src="<?= getRutaIcono('buscar_abogado')?>" height="15px">
					<?php endif; ?>
					<?= $orient->NOMBRE_ABOGADO?>
				</td>
				<td>
					<?php if($mostrar_lupas): ?>
						<img class="click" onclick="causas_usuario('cliente',<?= $orient->rut_cliente?>,true);" src="<?= getRutaIcono('buscar_cliente')?>" height="15px">
					<?php endif; ?>
					<?= $orient->nombre_cliente?>
				</td>
				<td>
					<div style="max-height:6.6em;overflow:hidden;">
						<?= $orient->resena ? $orient->resena : '<div class="purple centro"><b>PENDIENTE</b></div>' ?>
					</div>
				</td>
				<?php if($mostrar_vincular): ?>
					<td class="centro">
						<?php if ($orient->id_causa): ?>
							<?php $this->view('partial/click_detalle_causa', ['id' => $orient->id_causa]); ?>
						<?php else: ?>
							<button title="vincular una causa a esta orientación" class="icono_btn sombra" onclick="dialogo('dialog800', 'orientacion/vista_vincular_causa/<?= $orient->id_orientacion?>');">
								<img src="<?= getRutaIcono('vincular_causa_orientacion')?>">
							</button>
						<?php endif; ?>
					</td>
				<?php endif; ?>
				<td>
					<a title="generar ficha de la causa" class="icono_btn sombra" href="/ficha/orientacion/<?= $orient->id_orientacion?>" target="_blank">
						<img src="<?= getRutaIcono('imprimir')?>">
					</a>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	$('#<?=$id_tabla?>').DataTable({
			"pagingType": "simple_numbers",
			"order": [[ 0, "desc" ]]
	} );

	<?php if($fila_clickable): ?>
		$('#<?=$id_tabla?> tbody').addClass('click').on('click', 'tr',
			function() {
				var id_orientacion = this.getAttribute('data-id-orientacion');
				$('#<?= $input_id_orientacion?>').val(id_orientacion);
				$(this).closest('.ui-dialog-content').dialog('close');
			}
		);
	<?php endif; ?>
</script>

<style>
	<?php if($fila_clickable): ?>
		table.dataTable.hover tbody tr:hover, table.dataTable.display tbody tr:hover, table.dataTable.display tbody tr:hover > .sorting_1, table.dataTable.order-column.hover tbody tr:hover > .sorting_1 {
			background-color: #fcfeb0;
		}
	<?php endif; ?>
</style>
