<!DOCTYPE html>
<html lang="es">
<head>
<!--META-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="<?= base_url()?>/favicon.png" type="image/jpg">
<title>Inicie Sesión</title>

<!--STYLESHEETS-->
<link href="<?= base_url();?>assets/css/estilo_login.css" rel="stylesheet" type="text/css" />
<script src="<?= base_url(); ?>assets/js/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/sweetalert.css">
<!--SCRIPTS-->
<!-- SET GLOBAL BASE URL (https://codedump.io/share/vYcPimS5u6pl/1/codeigniter-base-url-didn39t-recongnized-in-javascript) -->
<script>
	var base_url = '<?php echo base_url() ?>';
	var index_page = '<?php echo index_page() ?>';
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-2.1.1.js"></script>
<!--Slider-in icons-->
<script type="text/javascript">



$(document).ready(function() {
	$(".username").focus(function() {
		$(".user-icon").css("left","-48px");
	});
	$(".username").blur(function() {
		$(".user-icon").css("left","0px");
	});

	$(".password").focus(function() {
		$(".pass-icon").css("left","-48px");
	});
	$(".password").blur(function() {
		$(".pass-icon").css("left","0px");
	});

	$('#lista_alertas').unbind();
	$('#lista_alertas').click(function(e){
		e.stopPropagation();
		e.preventDefault();
	});
});
</script>

</head>
<body>

<!--WRAPPER-->
<div id="wrapper">

	<!--SLIDE-IN ICONS-->
    <div class="user-icon"></div>
    <div class="pass-icon"></div>
    <!--END SLIDE-IN ICONS-->

<!--LOGIN FORM-->
<form name="login-form"  id="login-form" class="login-form" action="" method="post">

	<!--HEADER-->
    <div class="header">
    <!--TITLE--><h1>Iniciar Sesión</h1><!--END TITLE-->
    <!--DESCRIPTION--><span></span><!--END DESCRIPTION-->
    </div>
    <!--END HEADER-->

	<!--CONTENT-->
    <div class="content">
	<!--USERNAME--><input id="username" name="username" type="text" class="input username" placeholder="Rut o Usuario" /><!--END USERNAME-->
    <!--PASSWORD--><input id="password" name="password" type="password" class="input password" placeholder="Contraseña" /><!--END PASSWORD-->
    </div>
    <!--END CONTENT-->

    <!--FOOTER-->
    <div class="footer">
    <!--LOGIN BUTTON--><input type="submit" name="submit" value="Iniciar Sesión" class="button"  /><!--END LOGIN BUTTON-->
    </div>
    <!--END FOOTER-->

</form>
<!--END LOGIN FORM-->

<footer style="margin-top:20px;">
	<div class="login-form">
		<div class="content" style="padding:10px 30px 10px 25px">
			<p><a class="btn btn-primary" href="<?= site_url('Paginas/modelo_datos') ?>" target="_blank">Modelo de Datos</a></p>
			<p><a class="btn btn-primary" href="http://clinicajuridica.byethost9.com" target="_blank">Servidor de desarrollo</a></p>
		</div>
	</div>
</footer>
</div>
<!--END WRAPPER-->


<!--GRADIENT--><div class="gradient"></div><!--END GRADIENT-->


</body>
<script>
$(document).ready(function() {
	$("#login-form").submit(function(e) {
			e.preventDefault();
			var username = document.getElementById("username").value;
			var password = document.getElementById("password").value;
			$.ajax({
				url: base_url + index_page + <?= index_page() ? index_page()."/" : "''" ?> + 'menu/login',
				dataType: 'json',
				type: 'POST',
				data: {'username': username, 'password': password},
				success: function (result) {
					console.log(result);
					if(result.exito){
						 document.location.href = base_url + <?= index_page() ? index_page()."/" : "''" ?> + 'menu/menu_principal';
					}else{
						swal(result.mensaje);
					}
				}
			});
	});
});


</script>
</html>
