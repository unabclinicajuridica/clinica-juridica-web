<?php
	// $procedimientos = isset($procedimientos) ? $procedimientos : array();
	$materias = isset($materias) ? $materias : array();

	$id_procedimiento = isset($id_procedimiento) && $id_procedimiento? $id_procedimiento : 0;
	$id_materia = isset($id_materia) && $id_materia ? $id_materia : 0;
	$id_competencia = isset($id_competencia) && $id_competencia ? $id_competencia : 0;

	$materia_editable = isset($materia_editable) ? $materia_editable : TRUE; $disabled = $materia_editable ? '' : 'disabled';

	// $required_materia = isset($required_materia) && $required_materia ? "required oninvalid=\"this.setCustomValidity('Materia requerida')\" onchange=\"this.setCustomValidity('')\"" : '';
?>


<fieldset id="fs_selector_materia">
	<legend>Materia</legend>

	<div style="display:flex;flex-wrap:wrap;">
		<div class="columna3" style="flex-grow:25">
			<label>Competencia: </label>
			<select id="competencia" name="competencia" <?=$disabled?>>
				<option disabled selected value="0">Seleccionar</option>
				<?php foreach ($competencias as $compt) { ?>
							<option value='<?= $compt->id?>'><?= $compt->nombre?></option>
				<?php } ?>
			</select>
		</div>
		<div class="columna3" style="flex-grow:35">
			<label>Procedimiento: </label>
			<select id="procedimiento" name="procedimiento" <?=$disabled?>>
				<option disabled selected value="0">Seleccionar</option>
			</select>
		</div>
		<div class="columna3" style="flex-grow:40">
			<label>Materia: </label>
			<select id="materia" name="materia" <?=$disabled?>>
				<option disabled selected value='0'>Seleccionar</option>
				<?php foreach ($materias as $mat) { ?>
							<option value='<?= $mat->id?>'><?= $mat->nombre_materia?></option>
				<?php } ?>
			 </select>
		</div>
	</div>
</fieldset>
<style type="text/css" media="screen">
	.columna3 {
		display:flex;
		flex-direction: column;
		justify-content:center;
		align-items:center;
		padding-bottom: 10px;
	}
	.overflow {max-height:200px;}
	.ochenta {width:65%;}
</style>
<script type="text/javascript">
	var $select_competencia = $('#fs_selector_materia #competencia');
	var $select_procedimiento = $('#fs_selector_materia #procedimiento');
	var $select_materia = $('#fs_selector_materia #materia');

	// PREPARA LOS PROCEDIMIENTOS SIN AJAX.
	var procedimientos = {};

	<?php
		foreach($competencias as $comp):
			$opciones = '<option disabled selected value="0">Seleccionar</option>';
			foreach($comp->procedimientos as $pcd):
				$opciones .= '<option value="'.$pcd->id.'">'.$pcd->nombre.'</option>';
			endforeach;
			echo 'procedimientos['.$comp->id.'] = \''.$opciones.'\';';
		endforeach;
	?>

	<?php if($id_competencia): ?>
		$select_competencia.val("<?=$id_competencia?>").change();
	<?php endif; ?>
	<?php if($id_procedimiento): ?>
		$select_procedimiento.html(procedimientos[<?=$id_competencia?>]);
		$select_procedimiento.val("<?=$id_procedimiento?>").change();
	<?php endif; ?>
	<?php if($id_materia): ?>
		$select_materia.val("<?=$id_materia?>").change();
	<?php endif; ?>


	<?php if($materia_editable): ?>
		$select_competencia.selectmenu({
			change: function( event, ui ) {
				$select_materia.val("0").selectmenu( "refresh" );
				$select_procedimiento.html(procedimientos[ui.item.value]).selectmenu( "refresh" );
			},
			classes:{'ui-selectmenu-button':'ochenta'}}).selectmenu( "menuWidget" ).addClass( "overflow" );

		$select_materia.selectmenu( { classes:{'ui-selectmenu-button':'ochenta'} } )
		.selectmenu( "menuWidget" ).addClass( "overflow" );


		$select_procedimiento.selectmenu({
			change: function( event, ui ) {
				$.ajax({global:false, url:base_url+index_page+'busqueda/materia', data:{'id_procedimiento':ui.item.value}, dataType:'json',
				success: function(result) {
					var listitems='<option disabled selected value="0">Seleccionar</option>';
					$.each(result, function(key, value){
						 listitems += '<option value=' + value.id + '>' + value.nombre_materia + '</option>';
					});
					$select_materia.html(listitems).selectmenu( "refresh" );
				}})
			},
			classes:{'ui-selectmenu-button':'ochenta'}})
			.selectmenu( "menuWidget" ).addClass( "overflow" );
	<?php endif; ?>


</script>
