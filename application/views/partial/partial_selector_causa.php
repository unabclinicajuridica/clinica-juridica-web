<?php
	$opcional = isset($opcional) ? $opcional : false;
	$opcional_texto = $opcional ? " <em>(opcional)</em> " : "";
	$id_input_rol_causa = "input_rol_causa";
	$id_input_id_causa = "input_id_causa";

	$id_causa = isset($id_causa) ? $id_causa : '';
	$rol_causa = isset($rol_causa) ? $rol_causa : ($id_causa ? 'ID '.$id_causa.' (ROL Pendiente)' : '');
	$solo_con_rol = isset($solo_con_rol) ? $solo_con_rol : 1;
?>
<fieldset>
	<legend>Causa<?= $opcional_texto?></legend>

	<div style="">
		<div class="">
			<table>
				<tr>
					<td>
						<label for="rol_causa">ROL/RIT:&nbsp;</label>
					</td>
					<td>
						<input id=<?= $id_input_rol_causa?> name="rol_causa" class="dato_formulario input_readonly" type="text" readonly value="<?=$rol_causa?>">
						<input id=<?= $id_input_id_causa?> name="id_causa" type="hidden" value="<?=$id_causa?>">
						<button type="button" onclick="dialogo_seleccionar_causa('rellenar_input',<?=$solo_con_rol?>);"  style="margin-bottom:4px;" title="Buscar una causa">
							<img class="click" src="../../assets/images/lupa.png" height="24px">
						</button>
					</td>
				</tr>
			</table>
		</div>
	</div>
</fieldset>
<script type="text/javascript">
	function rellenar_input(objeto_fila) {
		var $fila = $(objeto_fila);
		var rol_causa = objeto_fila.getAttribute('data-rol');
		var id_causa = objeto_fila.getAttribute('data-id');
		if( ! rol_causa) rol_causa = 'ID '+id_causa+' (ROL Pendiente)';
		$("#<?= $id_input_rol_causa?>").val(rol_causa);
		$("#<?= $id_input_id_causa?>").val(id_causa);
	}
</script>
<style type="text/css" media="screen">
</style>
