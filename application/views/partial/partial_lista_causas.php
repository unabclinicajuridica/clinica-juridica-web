<?php $this->load->helper("utilidades"); ?>
<?php if(isset($id)) {} else { $id = 'table_selector_causa'; } ?>

<table id="<?= $id?>" class="display compact cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ROL/RIT</th>
			<th style="width: 64px;";>Ingreso</th>
			<th>Materia</th>
			<th>Alumno</th>
			<th>RUT Alumno</th>
			<th>Usuario</th>
			<th>RUT Usuario</th>
			<?php if(false) { ?>
				<th>Termino</th>
			<?php } ?>
			<th>Agendar</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($lista_causas as $causa) { ?>
					<tr>
						<td><img title="Haga click aqui para ver detalles de la causa"  style="cursor:pointer;" onclick="dialogo('dialog80p', 'busqueda/detalle_causa/<?= $causa->id?>', {});" src="../../assets/images/info.png" height="15px"><?= $causa->rol_causa?></td>
						<td><?= $causa->INGRESO?></td>
						<td><?= $causa->nombre_materia?></td>
						<td><?= $causa->NOMBRE_ALUMNO?></td>
						<td><?= $causa->RUT_ALUMNO."-".$causa->DV_ALUMNO?></td>
						<td><?= $causa->nombre_cliente?></td>
						<td><?= $causa->RUT_CLIENTE."-".$causa->DV_CLIENTE?></td>
				<?php
					if(false) {
						echo "<td>".$causa->TERMINO."</td>";
					}

					// Cuando agendar_abogado_especifico es verdadero, se
					// toma el rut en ", en vez de al profesor de
					// cada fila.
					if($agendar_abogado_especifico === true) {
						$rut_abogado = $rut_abogado_a_cargar;
					} else {
						$rut_abogado = $causa->RUT_ABOGADO;
					}
					?>
						<td>
							<button title="Agendar esta Causa" class="icono_btn sombra" onclick="<?= $funcion_de_agendar."('".$rut_abogado."', '".$dia."', ".$causa->id.", '".$hora."');" ?>">
								<img src="<?= getRutaIcono('agendar') ?>">
							</button>
						</td>
					</tr>
					<?php
				}
		?>
	</tbody>
</table>

<script>
	$('#<?= $id ?>').DataTable({
			pagingType: 'simple_numbers',
			pageLength: 5,
			"dom": '<"top"if>rt<"bottom"p><"clear">'
	});
</script>
