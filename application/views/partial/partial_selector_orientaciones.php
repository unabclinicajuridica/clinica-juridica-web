<?php
	$opcional = isset($opcional) ? $opcional : false;
	$opcional_texto = $opcional ? " <em>(opcional)</em> " : "";
	$this->load->helper('utilidades');
?>
<fieldset>
	<legend>Orientaciones<?= $opcional_texto?></legend>

	<div style="">
		<div class="">
			<table>
				<tbody id="contenedor_selectors">
					<tr>
						<td>
							<label>Orientaciones relacionadas a la Causa:</label>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</fieldset>

<template id="template_orientacion_selector">
		<tr class="orientacion_selector">
			<td>
				<label for="id_orientacion">ID de orientación:&nbsp;</label>
			</td>
			<td>
				<input id="id_orientacion" class="id_ori dato_formulario input_readonly" name="ids_orientaciones[]" type="text" readonly>
				<button class="btn_orientaciones" type="button" style="margin-bottom:4px;" title="Escoger una orientación">
					<img style="cursor:pointer;"  src="<?= getRutaIcono('buscar')?>" height="24px">
				</button>
			</td>
			<td>
				<a type="button" class="btn_clinica_compacto" title="Buscar una causa">
					<img class="btn_icono" style="cursor:pointer;"  src="<?= getRutaIcono('nuevo')?>" height="20px">
				</a>
			</td>
		</tr>
</template>

<script type="text/javascript">
	var n_selectores = 1;
	var orientacion_selector = $('#template_orientacion_selector').html();
	$orientacion_selector = $(orientacion_selector);
	$clon = $orientacion_selector.clone();

	$clon.find('img.btn_icono').attr({'src':'<?= getRutaIcono('cerrar')?>', 'height':'16px'});
	$clon.find('a.btn_clinica_compacto').attr('onclick', "$(this).closest('.orientacion_selector').remove();");


	$orientacion_selector.find('a.btn_clinica_compacto').click(function() {
		n_selectores++;
		var id_input = 'id_orientacion'+n_selectores;
		$clon.find('input.id_ori').attr('id', id_input);
		$clon.find('.btn_orientaciones').attr('onclick', "dialogo_seleccionar_orientacion('"+id_input+"');");

		clon_html = $clon[0].outerHTML;
		$('#contenedor_selectors').append(clon_html);
	});
	$orientacion_selector.find('input#id_orientacion').attr('id', 'id_orientacion'+n_selectores);
	$orientacion_selector.find('.btn_orientaciones').attr('onclick', "dialogo_seleccionar_orientacion('id_orientacion"+n_selectores+"');");
	$('#contenedor_selectors').append($orientacion_selector);
</script>
<style type="text/css" media="screen">
</style>
