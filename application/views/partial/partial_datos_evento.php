<fieldset>
	<legend>Evento</legend>
	<input type="hidden" id="sede" name="sede"  value="<?= $sede_actual ?>">
	<?php $this->view('partial/partial_set_fecha_hora', [ 'id'=>'tabla_fecha_hora4', 'fecha_hora_editable'=>false ]); ?>
	<table style="margin:auto;">
		<tr>
			<td>
				<label  for="input_nombre_abogado">Agendado para:</label>
			</td>
			<td>
				<div style="width:97%; display:flex;align-items:center;">
					<input class="input_readonly" type="text" id="input_nombre_abogado" value="<?= $rut_abogado ?>" readonly>
					<input type="hidden" id="input_rut_abogado" name="rut_abogado"  value="<?= $rut_abogado ?>" readonly>

					<?php if($rut_abogado === NULL): ?>
						<img style="cursor:pointer;margin-left:8px;" onclick="buscar_abogado('input_rut_abogado','input_nombre_abogado');" src="../../assets/images/lupa.png" height="25px">
					<?php	endif; ?>

				</div>
			</td>
		</tr>
		<tr>
			<td>
				<label  for="titulo_evento">Titulo:</label>
			</td>
			<td>
				<input id="titulo_evento" name="titulo_evento" type="text" style="width:96%;">
			</td>
		</tr>
		<tr>
			<td>
				<label for="descripcion_evento">Detalle o Descripción:</label>
			</td>
			<td>
				<textarea id="descripcion_evento" name="descripcion_evento" type="text" rows="3"></textarea>
			</td>
		</tr>
	</table>
</fieldset>
