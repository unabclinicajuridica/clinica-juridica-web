<?php
	$mostrar_info = true;

	if(isset($rol)) {
		$nombre_funcion = 'detalle_causa_rol';
		$identificador = $rol;
	}
	else if(isset($id)) {
		$nombre_funcion = 'detalle_causa';
		$identificador = $id;
	}
	else {
		$mostrar_info = false;
	}

	if($mostrar_info):
?>
	<img title="Haga click aqui para ver detalles de la causa"  class="click" onclick="dialogo('dialog90p', 'busqueda/<?= $nombre_funcion?>/<?=$identificador?>', {nuevo_dialogo:1});" src="../../assets/images/info.png" height="15px">&nbsp;<?= $identificador?>
<?php else: ?>
	&nbsp;<span class="purple"><b>PENDIENTE</b></span>
<?php endif; ?>
