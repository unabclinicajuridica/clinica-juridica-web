<?php
	$mostrar_comentar = isset($mostrar_comentar) ? $mostrar_comentar : FALSE;
	$notas_editable = isset($notas_editable) ? $notas_editable : FALSE;
?>


<div class="todo margin">
	<h2 style="text-align:center">Evaluación Audiencia</h2>
	<label  >I.- REGISTRO: </label>
	<div id="div_notas_audiencia">
		<table id="">
			<tr>
				<td>
				<label  for="nota_audiencia">a) Antecedentes y Documentos del caso (Certificados, Informes): </label>
				</td>
				<td >
				<select id="nota_registro_1" class="dato_formulario" name="nota_registro_1" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_registro_1)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">b) Copia audiencias anteriores, resoluciones, notificaciones, etc: </label>
				</td>
				<td >
					<select id="nota_registro_2" class="dato_formulario" name="nota_registro_2" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_registro_2)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">c) Minuta de esta audiencia: </label>
				</td>
				<td >
					<select id="nota_registro_3" class="dato_formulario" name="nota_registro_3" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_registro_3)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="tipo_audiencia">d) Otros Antecedentes ¿Cuáles?: </label>
				</td>
				<td >
					<input id="nota_registro_otros" class="dato_formulario" name="nota_registro_otros" type="text" value="<?= $audiencia->nota_registro_otros?>" <?= $disabled_o_no?>>
				</td>
			</tr>
		</table>
			<label  >II.- DESTREZAS EN LITIGACION: </label>
			<br>
		<table>
			<tr >
				<td>
				<label  for="nota_audiencia">a) Conocimiento de los hechos: </label>
				</td>
				<td >
				<select id="nota_destreza_1" class="dato_formulario" name="nota_destreza_1" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_1)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">b) Derecho aplicable: </label>
				</td>
				<td >
				<select id="nota_destreza_2" class="dato_formulario" name="nota_destreza_2" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_2)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">c) Uso adecuado vocabulario jurídico: </label>
				</td>
				<td >
				<select id="nota_destreza_3" class="dato_formulario" name="nota_destreza_3" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_3)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">d) Exposición coherente de la teoría del caso: </label>
				</td>
				<td >
				<select id="nota_destreza_4" class="dato_formulario" name="nota_destreza_4" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_4)?>
				</select>
				</td>
			</tr>
		</table>
			<label>e) MANEJO / DOMINIO, DE REGLAS DE LITIGACION ORAL: </label>
			<br>
		<table>
			<tr >
				<td>
				<label  for="nota_audiencia">Relación breve de demanda, contestación, etc: </label>
				</td>
				<td >
				<select id="nota_destreza_5" class="dato_formulario" name="nota_destreza_5" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_5)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">Litigación en incidente producido en audiencia: </label>
				</td>
				<td >
				<select id="nota_destreza_6" class="dato_formulario" name="nota_destreza_6" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_6)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">Ofrecimiento de prueba: </label>
				</td>
				<td >
				<select id="nota_destreza_7" class="dato_formulario" name="nota_destreza_7" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_7)?>
				</select>
				</td>
			</tr>
		</table>
			<label  >INCORPORACION PRUEBA: </label>
			<br>
		<table>
			<tr >
				<td>
				<label  for="nota_audiencia">Documental: </label>
				</td>
				<td >
				<select id="nota_destreza_8" class="dato_formulario" name="nota_destreza_8" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_8)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">Examen / Contraexamen de testigos: </label>
				</td>
				<td >
				<select id="nota_destreza_9" class="dato_formulario" name="nota_destreza_9" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_9)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">Examen / Contraexamen peritos: </label>
				</td>
				<td >
				<select id="nota_destreza_10" class="dato_formulario" name="nota_destreza_10" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_10)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">Alegato de clausura: </label>
				</td>
				<td >
				<select id="nota_destreza_11" class="dato_formulario" name="nota_destreza_11" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_11)?>
				</select>
				</td>
			</tr>
			<br>
			<tr >
				<td>
				<label  for="nota_audiencia">f) Capacidad para resolver situaciones inesperadas: </label>
				</td>
				<td >
				<select id="nota_destreza_12" class="dato_formulario" name="nota_destreza_12" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_destreza_12)?>
				</select>
				</td>
			</tr>
			<tr >
				<td>
				<label  for="nota_audiencia">III.- OTROS ASPECTO: </label>
				</td>

				<td >
				<select id="nota_item_3" class="dato_formulario" name="nota_item_3" <?= $disabled_o_no?>>
					<?= rellenar_select_notas($audiencia->nota_item_3)?>
				</select>
				</td>
			</tr>
		</table>
	</div>

	<?php if($notas_editable): ?>
		<div class="centro margin relleno_gris">
			<input id="chk_eval_sin_notas" name="chk_eval_sin_notas" class="dato_formulario" style="width:20px;" type="checkbox" <?= $disabled_o_no?>>
			<label for="chk_eval_sin_notas">seleccione la casilla si desea terminar la evaluación sin ingresar todas las notas.</label>
		</div>
		<script type="text/javascript">
			$('#chk_eval_sin_notas').click(function() {
				if($('#chk_eval_sin_notas').is(":checked")) {
					$("#div_notas_audiencia select, #div_notas_audiencia input").attr("disabled", "disabled").css('background-color', 'gainsboro').removeClass('dato_formulario').val('0');
					$("#div_notas_audiencia input").attr("disabled", "disabled").css('background-color', 'gainsboro').removeClass('dato_formulario').val('');
				} else {
					$("#div_notas_audiencia select, #div_notas_audiencia input").removeAttr("disabled").css('background-color', 'white').addClass('dato_formulario').val('0');
					$("#div_notas_audiencia input").removeAttr("disabled").css('background-color', 'white').addClass('dato_formulario').val('');
				}
			});
		</script>
	<?php endif; ?>

	<?php if(isset($comentario)) { ?>
			<section id="comentarios">
					<!-- <h2 class="centro">Comentarios</h2> -->
					<div class="flex-vertical derecha margin mediano relleno_gris" style="text-align:left;justify-content:flex-start;margin-top:20px;margin-right:20px;padding:0px 20px;">
						<div class="flex-vertical" style="margin-top: 25px;">
							<p>
								<label><strong>Comentario del Profesor:</strong><label>
							<br><em><?= $comentario?></em>
							</p>
						</div>
					</div>
			</section>
	<?php } else { ?>
		<?php if($notas_editable): ?>
			<section id="add_comentario" class="margin">
				<a id="a_add_comentario" class="off" href="#" >
					<img class="img" src="../../assets/images/signo_mas.png" height="16"><span style="margin-left:4px;">agregar comentario</span>
				</a>
				<div id="cont_nuevo_comentario" class="oculto" style="margin-top:5px;">
					<textarea id="nuevo_comentario" name="nuevo_comentario" class="dato_formulario" placeholder="escriba el comentario..."></textarea>
				</div>
			</section>
			<script>
				$(document).ready(function() {
					var $btn_comment = $('#a_add_comentario');
					var $cont_textfield = $('#cont_nuevo_comentario');
					var $textfield = $('#nuevo_comentario');
					// var $btn_confirm = $('#btn_confirmar')
					$btn_comment.click(function(e) {
						e.preventDefault();
						if($btn_comment.hasClass('off')) {
							$btn_comment.attr('class', 'on');
							$textfield.addClass('dato_formulario');
							$cont_textfield.show();
						}
						else if($btn_comment.hasClass('on')) {
							$btn_comment.attr('class', 'off');
							$textfield.removeClass('dato_formulario');
							$cont_textfield.hide();
						}
					});
				});
			</script>
		<?php endif; ?>
	<?php } ?>
</div>
