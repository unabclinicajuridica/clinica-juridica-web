<?php
	$required_nombre = isset($required_nombre) && $required_nombre ? 'required="required"' : '';
	$cliente_editable = isset($cliente_editable) ? $cliente_editable : TRUE; $disabled = $cliente_editable ? '' : 'disabled';
	$cliente = isset($cliente) ? $cliente : NULL;
?>
<fieldset>
	<legend>Usuario</legend>
	<div style="display:flex;justify-content:space-between;">
		<div class="" style="flex:45">

			<input type="hidden" id="id_cliente" name="id_cliente">

			<table style="margin:0;padding:0;width:100%;">
				<tr>
					<td class="label_tabla_right"><label  for="nombre_usuario">Nombre Completo</label></td>
					<td style="width:100%;">
						<input id="nombre_usuario" name="nombre_usuario" type="text" value="<?=$cliente ? $cliente->nombre_cliente : ''?>" <?=$required_nombre?> oninvalid="this.setCustomValidity('Nombre del Usuario requerido')" onchange="setCustomValidity('')" <?=$disabled?>>
						<?php if($cliente_editable): ?>
						<button class="button_icono" type="button" onclick="dialogo_buscar_usuarios();">
							<img style='cursor:pointer;' src='../../assets/images/lupa.png' height='25px'>
						</button>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td class="label_tabla_right"><label  for="rut_usuario">RUT</label></td>

					<?php $rutformateado = $cliente ? ($cliente->rut_cliente ? ($cliente->dv_cliente != NULL ? $cliente->rut_cliente."-".$cliente->dv_cliente : $cliente->rut_cliente) : '') : ''; ?>

					<td><input id="rut_usuario" name="rut_cliente" type="text" value="<?= $rutformateado?>" <?=$disabled?> pattern="\d{3,8}-[\d|kK]{1}" title="Debe ser un Rut válido"></td>
				</tr>
				<tr>
					<td class="label_tabla_right"><label  for="edad_usuario">Edad</label></td>
					<td ><input id="edad_usuario" name="edad_usuario" type="text" value="<?=$cliente ? $cliente->edad : ''?>" <?=$disabled?>></td>
				</tr>
			</table>
		</div>

		<div class="" style="flex:55; display:flex;justify-content:flex-end;">
			<table style="margin:0;padding:0;width:100%;">
				<tr>
					<td class="label_tabla_right"><label  for="telefono">Fono</label></td>
					<td><input id="telefono" name="telefono" type="text" value="<?=$cliente ? $cliente->telefono : ''?>" <?=$disabled?>></td>
				</tr>

				<tr>
					<td class="label_tabla_right"><label  for="correo_electronico">Correo</label></td>
					<td><input id="correo_electronico" name="email" type="text" value="<?=$cliente ? $cliente->email : ''?>" <?=$disabled?>></td>
				</tr>
				<tr>
					<td class="label_tabla_right"><label  for="domicilio">Domicilio</label></td>
					<td><input id="domicilio" name="domicilio" type="text" value="<?=$cliente ? $cliente->domicilio : ''?>" style="width:85%;" <?=$disabled?>></td>
				</tr>
			</table>
		</div>
	</div>
</fieldset>
