<?php if(isset($id)) {$id = $id;} else {$id = 'fieldset_agendar';} ?>

<fieldset id=<?=$id ?> class="fieldset_agendar">
	<legend>Agendar</legend>
	<div class="">
		<table>
			<tr>
				<td>
					<input type="radio" id="noag" name="tipo_agendar" value="no" style="width:initial" checked="checked">No agendar
				</td>
				<td>
					<input type="radio" id="mnag" name="tipo_agendar" value="manual" style="width:initial">Agendar Manual
				</td>
				<td>
					<input type="radio" id="auag" name="tipo_agendar" value="auto" style="width:initial">Auto-agendar
				</td>
			</tr>
		</table>
		
		
		<?php $this->view('partial/partial_set_fecha_hora', [ 'id' => 'tabla_fecha_hora3' ]); ?>
		<script>	
			$('#tabla_fecha_hora3').hide();
			$('input[type=radio][name=tipo_agendar]').change(function() {
				if (this.value == 'manual') {
					$('#tabla_fecha_hora3').show();
					$('input[name=input_agendar]').val(true);
				}
				else if(this.value == 'no') {
					$('#tabla_fecha_hora3').hide();
					$('input[name=input_agendar]').val(false);
				} else if(this.value == 'auto') {
					$('#tabla_fecha_hora3').hide();
					$('input[name=input_agendar]').val(true);
				}
			});
			
			if(<?= $agendar ?>) {
				$("#<?=$id ?> #mnag").click();
			} else {
				$("#<?=$id ?> #noag").prop("checked", true);
			}
		</script>
		
	</div>
</fieldset>
