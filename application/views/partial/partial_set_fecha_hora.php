<?php
	$id = isset($id) ? $id : 'tabla_fecha_hora';
	$fecha = isset($fecha) ? $fecha : '';
	$hora = isset($hora) ? $hora : '';
	$fechas_futuras = isset($fechas_futuras) ? $fechas_futuras : TRUE;
	$fecha_hora_editable = isset($fecha_hora_editable) ? $fecha_hora_editable : TRUE;
	$sin_hora = isset($sin_hora) ? $sin_hora : FALSE;
?>

<!-- jonthornton.timepicker -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.css">
<script type="text/javascript" src="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.min.js" ></script>
<div class="padding">
	<table id="<?=$id?>" class="tabla_fecha_hora" style="table-layout:auto;">
		<tr>
			<td class="centro">
				<label  for="fecha_agendada">Fecha:&nbsp;</label>
				<?php if($fecha_hora_editable): ?>
					<input id="fecha_agendada" name="fecha" type="text" class="" style="max-width:40%;" value="<?=$fecha?>" ></input>
					<?php if($fechas_futuras): ?>
						<script> ponerDatepicker($('#' + '<?=$id ?>'), '#fecha_agendada'); </script>
					<?php else: ?>
						<script> $('#fecha_agendada').datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true, maxDate: "+0D" }); </script>
					<?php endif; ?>
				<?php	else: ?>
					<input id="fecha_agendada" name="fecha" type="text" class="input_readonly" style="max-width:40%;" value="<?=$fecha?>" readonly></input>
				<?php	endif; ?>
			</td>
			<?php if( ! $sin_hora): ?>
				<td class="centro">
					<label  for="set_hora">Hora:&nbsp;</label>
					<?php if($fecha_hora_editable): ?>
						<input id="set_hora"  name="hora" type="time" class="" style="max-width: 40%;" value="<?=$hora?>" ></input>
						<?php if($fechas_futuras): ?>
							<script type="text/javascript">$('#set_hora').timepicker( { 'minTime': '08:00', 'maxTime': '22:00', 'timeFormat': 'H:i' } );</script>
						<?php else: ?>
							<!-- <script type="text/javascript">$('#set_hora').timepicker( { 'minTime': '08:00', 'maxTime': '22:00', 'timeFormat': 'H:i', 'disableTimeRanges': [ ['<?=date('H:i')?>', '23:59'] ] } );</script> -->
							<script type="text/javascript">$('#set_hora').timepicker( { 'minTime': '08:00', 'maxTime': '22:00', 'timeFormat': 'H:i' } );</script>
						<?php endif; ?>
					<?php	else: ?>
						<input id="set_hora"  name="hora" type="time" class="input_readonly" style="max-width: 40%;" value="<?=$hora?>" readonly></input>
					<?php	endif; ?>
				</td>
			<?php endif; ?>
		</tr>
	</table>
</div>
