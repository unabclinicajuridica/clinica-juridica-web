<?php
	$nombre_abogado = isset($nombre_abogado) ? $nombre_abogado : '';
	$rut_abogado = isset($rut_abogado) ? $rut_abogado : '';
	$required = isset($required) ? 'required=required' : '';
?>

<div id="super_selector_abogado" style="margin:2px 2px 10px 2px;">
	<label  for="input_nombre_abogado">Abogado:&nbsp;</label>
	<input class="input_readonly" type="text" id="input_nombre_abogado" name="nombre_abogado" value="<?=$nombre_abogado?>" autocomplete="off" <?=$required?>>
	<input type="hidden" id="input_rut_abogado" name="rut_abogado" value="<?=$rut_abogado?>">
	<button class="button_icono" type="button" onclick="buscar_abogado('input_rut_abogado','input_nombre_abogado');">
		<img class="click" src="../../assets/images/lupa.png" height="25px">
	</button>
</div>
<script type="text/javascript">
	$("#super_selector_abogado #input_nombre_abogado").keydown( function(e){ e.preventDefault(); } ).on('paste', function(e){ e.preventDefault(); } );
	$("#super_selector_abogado #input_nombre_abogado").on('invalid', function(e){ this.setCustomValidity('Abogado requerido'); } ).on('change', function(e){ this.setCustomValidity(''); });
</script>
