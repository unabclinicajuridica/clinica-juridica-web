<?php
$tabla_ancha = isset($tabla_ancha) ? 'width:100%;' : '';
if(! isset($datos_cliente)) {
	$this->load->library('My_DatosCliente');
	$datos_cliente = new My_DatosCliente();
}
?>
<style media="screen">
	#datos_adicionales table { width:initial; margin:0px; }

	#datos_adicionales > tbody > tr > td { vertical-align:top; padding:10px 10px 10px 10px; border:1px solid #797979; max-width:100%; }

	#datos_adicionales h3 { margin-top:0px; margin-bottom:10px; }

	#datos_adicionales input[type="radio"] { width:initial; height:initial; }

	#datos_adicionales caption { font-weight:bold; text-align:left; }

	input.num2 { width:3em; }
</style>
<fieldset>
	<legend>Datos Adicionales del Usuario</legend>
	<table id="datos_adicionales" style="padding:0px;<?= $tabla_ancha?>">
		<tbody>
			<tr>
				<td style="min-width:40%;">
					<div>
						<h3>Estado Civil</h3>
						<table>
							<tr>
								<td>Casado</td>
								<td><input name="estado_civil" type="radio" value="casado"></td>
								<td>Soltero</td>
								<td><input name="estado_civil" type="radio" value="soltero"></td>
							</tr>
							<tr>
								<td>Viudo</td>
								<td><input name="estado_civil" type="radio" value="viudo"></td>
								<td>AUC</td>
								<td><input id="rAUC" name="estado_civil" type="radio" value="AUC"></td>
							</tr>
							<tr>
								<td>Divorciado</td>
								<td><input name="estado_civil" type="radio" value="divorciado"></td>
								<td>Conviviente</td>
								<td><input name="estado_civil" type="radio" value="conviviente"></td>
							</tr>
							<tr>
								<td colspan="4" style="padding-top:20px;">Separado de hecho:</td>
							</tr>
							<tr>
								<td>Si<input name="separado_de_hecho" type="radio" value="1"></td>
								<td></td>
								<td>No<input name="separado_de_hecho" type="radio" value="0"></td>
							</tr>
						</table>
					</div>
				</td>
				<td>
					<h3>Situacion Laboral</h3>
					<table style="margin-bottom:10px;">
						<caption>Independiente:</caption>
						<tr>
							<td>Estable</td>
							<td> <input name="situacion_laboral" type="radio" value="estable"></td>
							<td>Eventual</td>
							<td> <input name="situacion_laboral" type="radio" value="eventual"></td>
							<td>Jubilado</td>
							<td> <input name="situacion_laboral" type="radio" value="jubilado"></td>
						</tr>
						<tr>
							<td>Cesante</td>
							<td> <input name="situacion_laboral" type="radio" value="cesante"></td>
							<td>Labores</td>
							<td> <input name="situacion_laboral" type="radio" value="labores"></td>
							<td>Estudiante</td>
							<td> <input name="situacion_laboral" type="radio" value="estudiante"></td>
						</tr>
					</table>
					<table style="margin-bottom:10px;">
						<caption>Dependiente:</caption>
						<tr>
							<td>Dependiente <input name="situacion_laboral" type="radio" value="dependiente"></td>
							<td>Con Contrato <input name="situacion_laboral" type="radio" value="con_contrado"></td>
							<td>Sin Contrato <input name="situacion_laboral" type="radio" value="sin_contrato"></td>
						</tr>
					</table>
					<table style="width:100%;">
						<!-- <caption>Actividad:</caption> -->
						<tr>
							<td style="vertical-align:top;"><b>Actividad:</b></td>
							<td style="width:100%;"><textarea class="compacto" name="actividad" rows="2"><?= $datos_cliente->actividad?></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<h3 >Tipo de Prevision</h3>
					<table>
						<tr>
							<td>AFP<input name="tipo_prevision" type="radio" value="AFP"></td>
							<td>IPS<input name="tipo_prevision" type="radio" value="IPS"></td>
							<td>S/P<input name="tipo_prevision" type="radio" value="SP"></td>
						</tr>
					</table>
				</td>
				<td>
					<h3 >Sistema de Salud</h3>
					<table>
						<tr>
							<td>ISAPRE</td>
							<td><input name="sistema_salud" type="radio" value="isapre"></td>
							<td>FONASA</td>
							<td><input name="sistema_salud" type="radio" value="fonasa"></td>
						</tr>
						<tr>
							<td>DIPRECA</td>
							<td><input name="sistema_salud" type="radio" value="dipreca"></td>
							<td>CAPREDENA</td>
							<td><input name="sistema_salud" type="radio" value="capredena"></td>
						</tr>
						<tr>
							<td colspan="4">OTRA: <input id="rSaludOtra" class="compacto" name="" type="text" value="<?= in_array($datos_cliente->sistema_salud, ['isapre','fonasa','dipreca','capredena']) ? '' : $datos_cliente->sistema_salud?>"></td>
							<script type="text/javascript">
								$('#rSaludOtra').keyup(function(){ var radio_gris = $(this).val().length > 0 ? true : false; $('input[name="sistema_salud"][type="radio"]').attr('disabled',radio_gris); $(this).attr('name', radio_gris ? 'sistema_salud' : '' ); });
								$('#rSaludOtra').keyup();
							</script>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<h3 >Escolaridad</h3>
					<table>
						<tr>
							<td>Analfabeto</td>
							<td><input name="escolaridad" type="radio" value="analfabeto"></td>
							<td>Básica</td>
							<td><input name="escolaridad" type="radio" value="basica"></td>
						</tr>
						<tr>
							<td>Media</td>
							<td><input name="escolaridad" type="radio" value="media"></td>
							<td>Técnica</td>
							<td><input name="escolaridad" type="radio" value="tecnica"></td>
						</tr>
						<tr>
							<td>Superior</td>
							<td><input name="escolaridad" type="radio" value="superior"></td>
							<td>Cursando</td>
							<td><input name="escolaridad" type="radio" value="cursando"></td>
						</tr>
					</table>
				</td>
				<td>
					<h3>Grupo Familiar</h3>
					<table>
						<tr>
							<td>Total Personas:&nbsp;<input class="compacto num2" name="total_personas" type="number" min="0" value="<?= $datos_cliente->total_personas?>"/></td>
							<td>Adultos:&nbsp;<input class="compacto num2" name="total_adultos" type="number" min="0" value="<?= $datos_cliente->total_adultos?>"></td>
							<td>Menores:&nbsp;<input class="compacto num2" name="total_menores" type="number" min="0" value="<?= $datos_cliente->total_menores?>"></td>
						</tr>
					</table>
					<table>
						<tr>
							<td>Discapacitados:&nbsp;</td>
							<td><label>SI</label><input name="discapacitados" type="radio" value="1"/></td>
							<td><label>NO</label><input name="discapacitados" type="radio" value="0"/></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<h3 >Situacion Habitacional</h3>
					<table>
						<tr>
							<td>Propia</td>
							<td><input name="situacion_habitacional" type="radio" value="propia"></td>
							<td>Arrendada</td>
							<td><input name="situacion_habitacional" type="radio" value="arrendada"></td>
						</tr>
						<tr>
							<td>Allegada</td>
							<td><input name="situacion_habitacional" type="radio" value="allegada"></td>
							<td>Prestada</td>
							<td><input name="situacion_habitacional" type="radio" value="prestada"></td>
						</tr>
					</table>
					<table>
						<tr>
							<td>Valor dividendo o arriendo:</td>
						</tr>
						<tr>
							<td><b>$</b> <input class="compacto" name="valor_dividendo" type="number" min="0" value="<?= $datos_cliente->valor_dividendo?>"/></td>
						</tr>
					</table>
				</td>
				<td style="padding:0px; border:none;">
					<table cellpadding="10" style="margin:-2px;">
						<tr>
							<td style="vertical-align:top;width:50%; border:1px solid #797979;">
								<h3>Ingresos</h3>
								<table>
									<tr>
										<td>$ <input class="compacto" name="ingresos_1" type="number" min="0" value="<?= $datos_cliente->ingresos_1?>"/></td>
									</tr>
									<tr>
										<td>$ <input class="compacto" name="ingresos_2" type="number" min="0" value="<?= $datos_cliente->ingresos_2?>"/></td>
									</tr>
									<tr>
										<td>$ <input class="compacto" name="ingresos_3" type="number" min="0" value="<?= $datos_cliente->ingresos_3?>"/></td>
									</tr>
									<tr>
										<td><b>Total</b> $ <input class="compacto" name="ingresos_total" type="number" min="0" value="<?= $datos_cliente->ingresos_total?>"/></td>
									</tr>
								</table>
							</td>
							<td style="vertical-align:top;width:50%; border:1px solid #797979;">
								<h3>Beneficios Estatales</h3>
								<table>
									<tr>
										<td>SI <input name="beneficios_estatales" type="radio" value="1"/></td>
										<td>NO <input name="beneficios_estatales" type="radio" value="0"/></td>
									</tr>
									<tr>
										<td colspan="2">
											<table style="width:100%;">
												<caption>Indicar cual:</caption>
												<tr>
													<td><textarea class="compacto" name="beneficio_estatal" rows="2"><?= $datos_cliente->beneficio_estatal?></textarea></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</fieldset>
<script type="text/javascript">
	$('input[name="estado_civil"][value="<?= $datos_cliente->estado_civil?>"]').prop('checked', 'checked');
	$('input[name="separado_de_hecho"][value="<?= $datos_cliente->separado_de_hecho?>"]').prop('checked', 'checked');
	$('input[name="situacion_laboral"][value="<?= $datos_cliente->situacion_laboral?>"]').prop('checked', 'checked');
	$('input[name="tipo_prevision"][value="<?= $datos_cliente->tipo_prevision?>"]').prop('checked', 'checked');
	$('input[name="sistema_salud"][value="<?= $datos_cliente->sistema_salud?>"]').prop('checked', 'checked');
	$('input[name="escolaridad"][value="<?= $datos_cliente->escolaridad?>"]').prop('checked', 'checked');
	$('input[name="discapacitados"][value="<?= $datos_cliente->discapacitados?>"]').prop('checked', 'checked');
	$('input[name="situacion_habitacional"][value="<?= $datos_cliente->situacion_habitacional?>"]').prop('checked', 'checked');
	$('input[name="beneficios_estatales"][value="<?= $datos_cliente->beneficios_estatales?>"]').prop('checked', 'checked');
</script>
