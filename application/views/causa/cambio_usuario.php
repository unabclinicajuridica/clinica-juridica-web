<div class="fondo_formularios" style="margin:auto;width:auto;">
	<h2 class="centro">Seleccione un <?php echo $tipo_usuarios ?> o ingrese su RUT (sin d.v.)</h2>
	<table >
		<tr>
			<td>
				<label for="">ROL/RIT</label>
			</td>
			<td >
				<label id="label_id_causa" for=""><?= $id_causa ? $id_causa : '<div class="purple"><b>PENDIENTE</b></div>' ?></label>
			</td>
		</tr>
		<tr>
			<td>
				<label  for="">Nombre <?php echo $tipo_usuarios ?> actual</label>
			</td>
			<td >
				<label id="label_nom_cur_usr"><?=$nombre_usuario_actual?></label>
			</td>
		</tr>
		<tr style="height:20px;"></tr>
		<tr>
			<td>
				<label for="in_rut_nuevo_usr">Rut nuevo <?php echo $tipo_usuarios ?></label>
			</td>
			<td >
				<input id="in_rut_nuevo_usr" class="input_readonly" name="in_rut_nuevo_usr" type="text">&nbsp;
				<button type="button" class="icono_btn" onclick="cuadro_seleccion_usuario();">
					<img src='../../assets/images/lupa.png'>
				</button>
			</td>
		</tr>
		<tr>
			<td>
				<label >Nombre nuevo <?php echo $tipo_usuarios ?></label>
			</td>
			<td >
				<label id="label_nom_nuevo_usr">&nbsp;---</label>
			</td>
		</tr>
		<tr style="height:60px;">
			<td>
			</td>
			<td>
				<input id="chk_reemplazar_en_todo" type="checkbox" name="chk_reemplazar_en_todo" style="width:20px;"/><label for="reemplazar_en_todo">seleccione la casilla si desea reemplazar al <?php echo $tipo_usuarios ?> actual en <b>Todas</b> sus causas vigentes.</label>
			</td>
		</tr>
	</table>
</div>
<style>
	.trespecial:hover .tdespecial {
		background:#819FF7
	}
</style>

<div id="template_tabla_usuarios" class="oculto" title="Seleccione un <?php echo $tipo_usuarios ?>">
	<div>
		<table id="tabla_usuarios" class="display cell-border">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Rut</th>
				</tr>
			</thead>
			<tbody style="cursor:pointer;">
				<?php
					foreach ($usuarios as $usr) {
						echo "<tr>";
						echo "<td>".$usr['nombre']."</td>";
						echo "<td>".$usr['rut']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>


<script>
	var $dialog_selusr = $('#template_tabla_usuarios');
	var $input_rut = $('#in_rut_nuevo_usr');
	var $nombre_nuevo = $('#label_nom_nuevo_usr');

	// url: https://datatables.net/examples/advanced_init/events_live.html
	var tabla_usrs = $dialog_selusr.find('#tabla_usuarios').DataTable({'retrieve':true});

	$('#tabla_usuarios tbody').on('click', 'tr', function () {
        var data = tabla_usrs.row( this ).data();

        $input_rut.val(data[1])
        $dialog_selusr.dialog('close');
        $nombre_nuevo.text(data[0]);
    } );

	function cuadro_seleccion_usuario(id_causa) {
		$dialog_selusr.dialog({
			draggable: false,
			resizable: false,
			modal: true,
			minWidth: 500,
			close: function(){$(this).dialog('destroy');}
		});
	}
</script>
