<?php
	$this->load->helper("utilidades");
	$titulo_tabla = isset($titulo_tabla) ? $titulo_tabla : 'MIS CAUSAS';
	$titulo_vacio = isset($titulo_vacio) ? $titulo_vacio : "No hay Causas.";
?>

<h1 class='centro padding'><b><?=$titulo_tabla?></b></h1>
<?php if(count($causas)): ?>
<table id="table_miscausas" class="display cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>ROL/RIT</th>
			<th>Ingreso</th>
			<th>Materia</th>
			<th>Abogado</th>
			<th>Alumno</th>
			<th>Usuario</th>
			<th>Término</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($causas as $ca): ?>
			<tr>
				<td><?php $this->view('partial/click_detalle_causa', ['rol' => $ca->rol_causa]); ?></td>
				<td><?= $ca->INGRESO?></td>
				<td><?= $ca->nombre_materia ? $ca->nombre_materia : '<div class="purple"><b>PENDIENTE</b></div>' ?></td>
				<td><?= $ca->NOMBRE_ABOGADO?></td>
				<td>
					<?php if($ca->RUT_ALUMNO): ?>
						<?= $ca->NOMBRE_ALUMNO?>
					<?php elseif($editable): ?>
						<button class="btn_clinica_compacto" onclick="dialog_cambiar_usuario('alumno', '<?= $ca->id?>', '<span class=\'rojo\'><b>SIN ASIGNAR</b></span>');">
							<img src="<?=getRutaIcono('add')?>" height="18px">
							<span>agregar</span>
						</button>
					<?php else: ?>
						<div class="purple"><b>PENDIENTE</b></div>
					<?php endif; ?>
				</td>
				<td><?= $ca->nombre_cliente ? $ca->nombre_cliente : '<span class="rojo"><b>SIN ASIGNAR</b></span>'?></td>
				<td><?= $ca->TERMINO ? '<b style="color:#444">'.$ca->TERMINO.'</b>' : '<span class="verde"><b>ACTIVA</b></span>' ?></td>
				<td>
				<?php if($mostrar_opcion_audiencia && $ca->rol_causa): ?>
					<button class="icono_btn sombra" title="Registrar Audiencia para esta Causa" onclick="dialogo_nueva_audiencia('<?= $ca->rol_causa?>');">
						<img  src="<?=getRutaIcono('audiencia')?>" width="24px" height="24px">
					</button>
				<?php endif; ?>
				<?php if($mostrar_opcion_terminar): ?>
					<button class="icono_btn sombra" title="Terminar la causa" type="button" onclick="terminar_causa('<?= $ca->id?>','main_mis_causas');">
						<img  src="<?=getRutaIcono('terminar_causa')?>" width="24px" height="24px">
					</button>
				<?php endif; ?>
				<?php if($mostrar_opcion_rellenar): ?>
					<button class="icono_btn sombra" title="Rellenar datos de la Causa" type="button" onclick="dialog_rellenar_causa('<?= $ca->id?>','main_mis_causas');">
						<img  src="<?=getRutaIcono('rellenar')?>" width="24px" height="24px">
					</button>
				<?php endif; ?>
				<?php if($mostrar_opcion_editar): ?>
					<button class="icono_btn sombra" title="Editar datos de la Causa" type="button" onclick="dialog_editar_causa('<?= $ca->id?>','main_mis_causas');">
						<img  src="<?=getRutaIcono('editar')?>" width="24px" height="24px">
					</button>
				<?php endif; ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php else: ?>
	<div class="area_gris">
		<label><?= $titulo_vacio?></label>
	</div>
<?php endif; ?>

<script>
	$('#table_miscausas').DataTable({
		"retrieve":true,
		"responsive":false,
		"autoWidth":false,
		"pagingType": "simple_numbers",
		"order": [[ 1, "desc" ], [6, "desc"]]
	} );
</script>
