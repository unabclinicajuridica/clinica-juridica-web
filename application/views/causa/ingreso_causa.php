﻿<?php
	$ancho = isset($ancho) && $ancho === 'true' ? "largo" : "";
	$abogado_cambiable = isset($abogado_cambiable) ? $abogado_cambiable : false;
	$recargar_agenda = 'false';
?>


<div class="fondo_formularios <?=$ancho?>">
  <h2 style="text-align:center">Ingreso Causa Clinica Juridica UNAB</h2>

	<form id="form_causa" class="" action="index.html" method="post">

		<?php
			if($agendar === 'true' && $estado_agendar === 'agendado') {
				$this->view('partial/partial_set_fecha_hora', [ 'id' => 'tabla_fecha_hora1', 'fecha_hora_editable'=>false ]);
				$recargar_agenda = 'true';
			}
		?>

		<table style="margin:auto">
			<tr>
				<td>
					<table>
						<tr>
							<td class="label_tabla_right">
								<label  for="id_causa">ROL/RIT&nbsp;</label>
								<input id="id_causa" name="id_causa" type="text">
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						<tr>
							<td class="label_tabla_right">
								<label>ID Interna:</label>
							</td>
							<td >
								<label id="id_correlativa" name="id_correlativa"> <?=$id_correlativa?> </label>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<?php $this->view('partial/partial_selector_orientaciones', array('opcional'=>true)) ?>

		<?php $this->view('partial/partial_selector_materia') ?>

		<?php $this->view('partial/partial_selector_cliente', [ 'required_nombre'=>true ]) ?>

		<?php $this->view('partial/partial_datos_cliente', [ 'tabla_ancha'=>$ancho ]); ?>


		<table style="margin:auto;margin-bottom:20px;">
			<tr>
				<td>
					<label  for="select_tribunal">Tribunal: &nbsp;</label>
					<select id="select_tribunal" name="id_tribunal">
						<option disabled selected value='0'>-- Seleccionar -- </option>
						<?php foreach($tribunales as $trib) { ?>
							<option value='<?= $trib->id ?>'><?= $trib->nombre ?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
		</table>

		<table style="margin:auto;">
			<tr>
				<td class="label_tabla_right">
					<label  for="list_abogado">Abogado</label>
				</td>

				<td>
					<?php if ($abogado_cambiable) { ?>
							<input class="input_readonly" type="text" id="nombre_abogado" name="nombre_abogado" value="<?= $nombre_profesor_a_cargar?>" readonly="readonly">
							<input type="hidden" id="list_abogados" name="abogado" value="<?= $rut_profesor_a_cargar?>">
					<?php	if($estado_agendar !== 'agendado') { ?>
								<button class="button_icono" type="button" onclick="buscar_abogado('list_abogados','nombre_abogado');">
									<img style="cursor:pointer;" src="../../assets/images/lupa.png" height="25px">
								</button>
					<?php	}
						} else { ?>
							<input type="text" id="nombre_abogado" name="nombre_abogado" value="<?= $nombre_usuario?>" readonly="readonly">
							<input type="hidden" id="list_abogados" name="abogado" value="<?= $rut_usuario?>">
					<?php	} ?>

				</td>

				<td class="label_tabla_right">
					<label for="list_alumnos">Alumno</label>
				</td>
				<td>
					<input type="text" id="nombre_alumno" name="nombre_alumno" value="" readonly="readonly">
					<input type="hidden" id="list_alumnos" name="alumno" value="">
					<button class="button_icono" type="button" onclick="buscar_alumno('list_alumnos', 'nombre_alumno');">
						<img style='cursor:pointer;' src='../../assets/images/lupa.png' height='25px'>
					</button>
				</td>
			</tr>
		</table>

		<?php
			if($agendar === 'true' && $estado_agendar === 'no_agendado') {
				$this->view('partial/partial_tipo_agendar', array('id' => 'fieldset_agendar1', 'agendar' => $agendar));
			}
		?>

		<?php
			if($agendar === 'false') { ?>
				<br>
				<li style="text-align:justify; margin-left:35px" >
					<p>
						La Causa ingresada no será agendada. Para agendarla se debe ir a la Agenda y seleccionar "Agendar Causa Asignada".
					</p>
				</li>
		<?php	} ?>
		<li style="text-align:justify; margin-left:35px" >
			<p><em>
				Declaro bajo juramento que los antecedentes entregados precedentemente son verdaderos; si así no fuere, estoy de acuerdo desde ya, para que se me aplique el Reglamento del Usuario de la Clínicia Jurídica UNAB y que se deje sin efecto o de revoque la presentación del servicio y/o patrocinio y poder que se haya conferio, sea que el caso se encuentre en etapa judicial o extrajudicial. Asimismo me obligo a consultar en la Clínica, periódicamente, sobre el estado de mi casom como fechs de auudiencias, documentos faltantes, etc; y me obligo a informar a la Clínica mis cambios de domicilio, de teléfonos, email y a proporcionar oportunamente todos los datos, antecedentes y pruebas necesarios para fundar mis pretensiones o que se me solicite por el abogado o alumno que atienden de mi caso.
			</em></p>
		</li>



		<input type="hidden" id="input_agendar" name="agendar" value=<?=$agendar?>>

		<div class="padding_medio centro">
			<button id="btn_actualizar_causa" type="submit">Guardar Causa</button>
		</div>
	</form>
</div>
<script type="text/javascript">
	$("#form_causa").submit(function(e) {
		var formData = $(this).serializeArray();
		e.preventDefault();
		ingresar_causa(<?= $recargar_agenda?>, formData);
	});

	$('#select_tribunal').selectmenu();
</script>
