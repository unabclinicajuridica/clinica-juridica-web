<?php
	$id_vinculador = "div_vinculador_".$id_orientacion;
	$id_formulario = "form_vinc_oris_".$id_orientacion;
?>
<div id="<?= $id_vinculador ?>">
	<div class="fondo_formularios wide">
		<form id="<?= $id_formulario ?>" class="" action="index.html" method="post">
			<?php $this->view('partial/partial_selector_causa', array('opcional'=>false)) ?>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('#<?=$id_vinculador?>').closest('.ui-dialog-content').dialog({
		title: "Vincular Causa a Orientación Nº <?= $id_orientacion?>",
		buttons: [
			{
				text: "Ok",
				icons: {primary: "ui-icon-heart"},
				click: function() {
					$(this).dialog( "close" );
				}
			},
			{
				text: "Cancelar",
				click: function() {
				  $(this).dialog( "close" );
				}
			}
		]
	});
</script>
