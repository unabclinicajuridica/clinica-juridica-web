<?php
	// $ancho = isset($ancho) && $ancho === 'true' ? "largo" : "";

	$rol_editable = isset($rol_editable) ? $rol_editable : TRUE;
	$orientaciones_editable = isset($orientaciones_editable) ? $orientaciones_editable : TRUE;
	$materia_editable = isset($materia_editable) ? $materia_editable : TRUE;
	$usuario_editable = isset($usuario_editable) ? $usuario_editable : TRUE;
	$tribunal_editable = isset($tribunal_editable) ? $tribunal_editable : TRUE; $tribunal_disabled = $tribunal_editable ? '' : 'disabled'; $id_tribunal = isset($id_tribunal) ? $id_tribunal : NULL;
	$abogado_cambiable = isset($abogado_cambiable) ? $abogado_cambiable : TRUE;
	$alumno_cambiable = isset($alumno_cambiable) ? $alumno_cambiable : TRUE;

	$id_procedimiento = isset($id_procedimiento) && $id_procedimiento? $id_procedimiento : 0;
	$id_materia = isset($id_materia) && $id_materia ? $id_materia : 0;
	$id_competencia = isset($id_competencia) && $id_competencia ? $id_competencia : 0;
?>


<div class="fondo_formularios wide">
	<h2 style="text-align:center">Editar Causa (ID <?=$id_correlativa_causa?>)</h2>

	<form id="form_edit_causa" class="" action="index.html" method="post">
<!--
		<input type="hidden" name="name" value="<?=$id_correlativa_causa?>">
-->
		<table style="margin:auto">
			<tr>
				<td>
					<table>
						<tr>
							<td class="label_tabla_right">
								<label  for="id_causa">ROL/RIT&nbsp;</label>
								<input id="id_causa" name="id_causa" type="text" value="<?=$rol_causa?>">
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<?php $this->view('partial/partial_selector_orientaciones', array('opcional'=>true)) ?>

		<?php $this->view('partial/partial_selector_materia', [ 'id_competencia'=>$id_competencia, 'id_procedimiento'=>$id_procedimiento, 'id_materia'=>$id_materia, 'materia_editable'=>$materia_editable ]) ?>

		<?php $this->view('partial/partial_selector_cliente', [ 'required_nombre'=>true, 'cliente_editable'=>$usuario_editable, 'cliente'=>$cliente ]) ?>

		<?php $this->view('partial/partial_datos_cliente', [ 'tabla_ancha'=>TRUE, 'datos_cliente'=>$datos_cliente ]) ?>

		<table style="margin:auto;margin-bottom:20px;">
			<tr>
				<td>
					<label for="select_tribunal">Tribunal: &nbsp;</label>
					<select id="select_tribunal" name="id_tribunal" <?=$tribunal_disabled?>>
						<option disabled selected value="0"> -- Seleccionar -- </option>
						<?php foreach($tribunales as $trib) { ?>
							<option value='<?= $trib->id ?>'><?= $trib->nombre ?></option>
						<?php } ?>
					</select>
					<?php if($id_tribunal): ?>
						<script type="text/javascript">$('#select_tribunal').val('<?=$id_tribunal?>').change();</script>
					<?php endif; ?>
				</td>
			</tr>
		</table>

		<table style="margin:auto;">
			<tr>
				<td class="label_tabla_right">
					<label  for="list_abogado">Abogado</label>
				</td>

				<td>
					<?php if ($abogado_cambiable): ?>
						<input class="input_readonly" type="text" id="nombre_abogado" name="nombre_abogado" value="<?= $nombre_abogado?>" readonly="readonly">
						<input type="hidden" id="rut_abogado" name="rut_abogado" value="<?= $rut_abogado?>">
						<button class="button_icono" type="button" onclick="buscar_abogado('rut_abogado','nombre_abogado');">
							<img style="cursor:pointer;" src="../../assets/images/lupa.png" height="25px">
						</button>
					<?php	else: ?>
						<input value="<?= $nombre_usuario?>" disabled>
					<?php	endif; ?>

				</td>

				<td class="label_tabla_right">
					<label for="list_alumnos">Alumno</label>
				</td>
				<td>
					<?php if($alumno_cambiable): ?>
						<input type="text" id="nombre_alumno" name="nombre_alumno" value="<?= $nombre_alumno?>" readonly="readonly">
						<input type="hidden" id="rut_alumno" name="rut_alumno" value="<?= $rut_alumno?>">
						<button class="button_icono" type="button" onclick="buscar_alumno('rut_alumno', 'nombre_alumno');">
							<img style='cursor:pointer;' src='../../assets/images/lupa.png' height='25px'>
						</button>
					<?php else: ?>
						<input value="<?= $nombre_alumno?>" disabled>
					<?php endif; ?>
				</td>
			</tr>
		</table>

		<li style="text-align:justify; margin-left:35px" >
			<p><em>
				Declaro bajo juramento que los antecedentes entregados precedentemente son verdaderos; si así no fuere, estoy de acuerdo desde ya, para que se me aplique el Reglamento del Usuario de la Clínicia Jurídica UNAB y que se deje sin efecto o de revoque la presentación del servicio y/o patrocinio y poder que se haya conferio, sea que el caso se encuentre en etapa judicial o extrajudicial. Asimismo me obligo a consultar en la Clínica, periódicamente, sobre el estado de mi casom como fechs de auudiencias, documentos faltantes, etc; y me obligo a informar a la Clínica mis cambios de domicilio, de teléfonos, email y a proporcionar oportunamente todos los datos, antecedentes y pruebas necesarios para fundar mis pretensiones o que se me solicite por el abogado o alumno que atienden de mi caso.
			</em></p>
		</li>

		<div class="padding_medio centro">
			<button id="btn_actualizar_causa" type="submit">Guardar Causa</button>
		</div>
	</form>
</div>
<script type="text/javascript">
	$("#form_edit_causa").submit(function( event ) {
		event.preventDefault();
		var formData = $(this).serializeArray();
		editar_causa(<?=$id_correlativa_causa?>, formData, <?=$callback?>);
		return false;
	});

	$('#select_tribunal').selectmenu();
</script>
