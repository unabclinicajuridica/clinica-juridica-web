<?php
	$this->load->helper('configuracion');
	$id_formulario = "form_terminar_causa_".$id_causa;
?>

<div class="centro">
	<p style="font-size:1.2em">ROL/RIT: <b><?=$rol_causa ? $rol_causa : '<b class="rojo">NO TIENE</b>'?></b></p>
	<form id="<?= $id_formulario ?>" class="" action="index.html" method="post">
		<input type="text" style="width: 0; height: 0; top: -100px; position: absolute;"/>
		<div class="flex_vertical" style="padding:10px 0px 3px 0px;">
			<label for="fecha_termino_causa" style="margin-bottom:3px;">Fecha de Término:&nbsp;</label>
			<input type="text" class="centro" id='fecha_termino_causa' name="fecha_termino_causa" value="<?=date(getFormatoFechaPHP())?>" required>
		</div>

		<div class="centro padding">
			<select id="causales" name="id_causal_termino" required>
				<option disabled selected value="no">Seleccione una Causal</option>
				<?php foreach ($causales_termino as $ct): ?>
					<option value="<?=$ct->id_causal_termino?>"><?=$ct->nom_causal?></option>
				<?php endforeach; ?>
			</select>
		</div>

	</form>
</div>
<script type="text/javascript">
	$('#causales').selectmenu();
	//TODO. RESOLVER CON PHP O JS.-
	$("#fecha_termino_causa" ).datepicker();

	var $este_dialogo = $('#<?=$id_formulario?>').closest('.ui-dialog-content');
	$este_dialogo.dialog({
		position:{my:'top', at:'center top+15%', of:window},
		title: "Terminar Causa",
		buttons: [
			{
				text: "Ok",
				icons: { primary: "ui-icon-heart" },
				click: function() {
					var formData = $('#<?= $id_formulario ?>').serializeArray();
					$.ajax({method:'POST', url:base_url+index_page+'ingreso/update_termino_causa/<?=$id_causa?>', dataType:'json', data:formData, success:function(result){
						if(result.exito) { autoCloseSwal('', 'Causa Terminada.', 'success'); $este_dialogo.dialog("close"); }
						else { swal('', result.mensaje, 'warning'); }
					}});
				}
			},
			{
				text: "Cancelar",
				click: function() { $(this).dialog("close"); }
			}
		]
	});
</script>
