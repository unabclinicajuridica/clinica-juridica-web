<div style="border: 1px solid #000000; margin-bottom:5px;">
	<label class="titulo_divset">Causas Asociadas</label>
	<table style="width:100%;margin-top:-20px;">
		<tr>
			<th>Nombre: <?= $usuario->nombre?></th>
			<th>Rut: <?= $usuario->rut."-".$usuario->dv?></th>
			<th>Cantidad de Causas: <?= count($causas)?></th>
			<?php if(isset($promedio)) { ?>
				<th>Promedio de Notas: <?= $promedio?></th>
			<?php } ?>
		</tr>
	</table>
</div>

<?php $this->view('busqueda/tabla_causas', ['id_tabla'=>'table_detalle_alumno', 'causas'=>$causas]); ?>
