<h2 class="centro">Ingreso Trámite</h2>
<div class="fondo_formularios">




	<form id="form_tramite">
<!--
		<input id="fecha_ingreso" name="fecha_agendada" type="hidden" value="">
		<input id="hora_ingreso" name="fecha_agendada" type="hidden" value="">
-->
		<div>
			<div id="p_result_ingreso" class="padding_medio"></div>
		</div>

		<table>
			<tr>
				<td style="padding:10px 0px;">
					<label for="rol_causa">Seleccionar Causa:&nbsp;</label>
					<input id="rol_causa" name="rol_causa" type="text"  class="readonly" readonly>

					<button type="button" onclick="dialogo_seleccionar_causa('trae_audiencias');"  style="margin:0px 6px 4px 6px;" title="Buscar una causa">
						<img style="cursor:pointer;"  src="../../assets/images/lupa.png" height="24px">
					</button>
				</td>
				<td>
					<label for="id_correlativa_causa">ID Interna:&nbsp;</label>
					<input id="id_correlativa_causa" name="id_correlativa_causa" type="text"  class="readonly" readonly>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td colspan="0" style="display:flex;flex-direction:column;">
					<label>Descripción del trámite:</label>
					<textarea id="it_descripcion" name="descripcion" cols="45" rows="8"></textarea>
					<div id="incorrect-word-list" style="background-color:orange;"></div>
				</td>
			 </tr>
			 <tr>
				<td id="td_uploaders" class="" style="display:flex;flex-direction:column;padding-top:5px;">
					<label>Adjuntar documento (opcional):</label>
					<div>
						<input name="archivos[]" type="file"  class="">
						<a id="btn_add_uploader" type="button" class="btn_clinica_compacto" title="Buscar una causa" style="padding:2px;position:relative;right:42px;">
							<img style="cursor:pointer;"  src="../../assets/images/signo_mas.png" height="22px">
						</a>
					</div>

				</td>
			 </tr>
			 <tr>
				<td class="centro" style="padding: 8px 0px;">
					<button type="submit" class="">Ingresar Trámite</button>
				</td>
			 </tr>
		</table>
	</form>
</div>
<template id="template_upload_selector">
	<div class="upload_selector">
		<input name="archivos[]" type="file"  class="">
		<a type="button" class="btn_clinica_compacto" title="Buscar una causa" onclick="$(this).closest('.upload_selector').remove();" style="padding:2px;position:relative;right:39px;">
			<img style="cursor:pointer;"  src="../../assets/images/equis.png" height="16px">
		</a>
	</div>
</template>
<script>
	var upload_selector = $('#template_upload_selector').html();
	$('#btn_add_uploader').click(function() {
		 $('#td_uploaders').append(upload_selector);
	});

	$('#form_tramite').submit(function (event) {
		event.preventDefault();
		var datos = $('#form_tramite')[0];
		console.log('datos= '+datos);
		var formdata = new FormData(datos);
		$('#p_result_ingreso').empty();
		jQuery.ajax({
			url: '<?=site_url('tramite/ingresar_tramite')?>',
			dataType: 'json',
			data: formdata,
			cache: false,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(data) {
				console.log('respuesta ingresar_tramite= '+JSON.stringify(data));

				if(data.exito) {
					var p = document.createElement('div');
					swal('', '<p style="color:green">'+data.mensaje+'</p>', 'success');
					main_nuevo_tramite();
				} else {
					$('#p_result_ingreso').html(data.mensaje).css({'color':'red', 'text-align':'center'});
				}

			}
		});
		return false;
	});


</script>
