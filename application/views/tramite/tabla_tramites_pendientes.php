<div class="centro" style="width:70%;margin:auto;">
	<h2 style="margin-bottom:30px;">Trámites pendientes de revisión</h2>
	<table id="table_tramites" class="display compact cell-border" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Hora</th>
				<th>Causa</th>
				<th>Alumno</th>
				<th>
					<img height="14" class="img" title="pendiente, revisado, leido o cerrado" src="../../assets/images/info2.png">
					<span>Estado</span>
				</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($tramites as $tram) { ?>
			<tr>
				<td><?= $tram['fecha_ingreso']?></td>
				<td><?= $tram['hora_ingreso']?></td>
				<td><?php $this->view('partial/click_detalle_causa', ['rol' => $tram['rol_causa']]); ?></td>
				<td><?= $tram['nombre_alumno']?></td>
				<td><span style="<?= $tram['estado_en_rojo'] ? "color:red" : ""?>"><?= $tram['estado_revision']?></span></td>
				<td>
					<button class="icono_btn" onclick="dialogo_detalle_tramite(<?= $tram['id_tramite']?>);">
						<img title="ver detalles del trámite" src="../../assets/images/eye_open.svg">
					</button>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
<script>
	$('#table_tramites').DataTable({
			"pagingType": "simple_numbers",
			"order": [[ 0, "desc" ],[ 1, "desc" ]],
			"dom": '<"top">rt<"bottom"p><"clear">'
	} );
</script>
