<div class="fondo_formularios" style="margin-top:-9px;width:90%">
	<h2 class="centro"><?= strftime("%e de %B del %Y", strtotime($tramite->fecha_ingreso))?></h2>

	<section id="detalles">
		<table>
			<tr>
				<td>
					<label>Hora:</label>

				</td>
				<td>
					<label><?= $tramite->hora_ingreso?></label>
				</td>
			</tr>
			<tr>
				<td>
					<label>Descripción:</label>


				</td>
				<td>
					<label><?= $tramite->descripcion ? $tramite->descripcion : "NINGUNA"?></label>
				</td>
			</tr>
		</table>
	</section>

	<?php if(count($archivos)) { ?>
		<hr>

		<section id="adjuntos">
			<h3>Archivos Adjuntos</h3>
			<div class="flex" style="padding: 0px 10px;">
			<?php foreach($archivos as $archi) {?>
				<a class="file_dl" style="padding:4px;margin: 0px 3px;" href="<?= base_url()?>paginas/descargar/<?= $archi['id']?>">
					<div class="flex_vertical" style="max-width:25%;word-break:break-all;font-size:11px;max-width:140px;text-align:center">
						<div><img src="../../assets/images/<?= getNombreIconoPorExtension($archi['file_ext'])?>"></div>
						<span><?= $archi['file_name']?></span>
					</div>
				</a>
			<?php } ?>
			</div>
		</section>
	<?php }?>

	<hr>

	<section id="comentarios">
		<?php if(count($comentarios)) { ?>
			<h2 class="centro">Comentarios</h2>
			<div class="flex-vertical derecha" style="text-align:left;justify-content:flex-start;margin-top:20px;margin-right:20px;font-size:0.9em;">
			<?php foreach($comentarios as $comnt) {?>
				<div class="flex-vertical" style="margin-top: 25px;">
					<p>
					<label><strong><?= $comnt->nombre_usuario?></strong> <em><?= $comnt->timestamp?></em><label>
					<br><?= $comnt->comentario?>
					</p>
				</div>
			<?php } ?>
			</div>
		<?php } else { ?>
			<h3 class="centro">No hay comentarios.</h3>
		<?php }?>
	</section>

	<?php if($mostrar_comentar) { ?>
		<section id="add_comentario">
			<a id="a_add_comentario" class="off" href="#" >
				<img class="img" src="../../assets/images/signo_mas.png" height="16"><span style="margin-left:4px;">agregar comentario</span>
			</a>
			<div id="cont_nuevo_comentario" class="oculto" style="margin-top:5px;">
				<textarea id="nuevo_comentario" placeholder="escriba el comentario..."></textarea>
			</div>
		</section>
		<script>
			$(document).ready(function() {
				var $btn_comment = $('#a_add_comentario');
				var $textfield = $('#cont_nuevo_comentario');
				var $btn_confirm = $('#btn_confirmar')
				$btn_comment.click(function() {
					if($btn_comment.hasClass('off')) {
						$btn_comment.attr('class', 'on');
						$textfield.show();
						$btn_confirm.attr('onclick', "ingresar_comentario(this, <?= $tramite->id_tramite?>)").html('Guardar y <?= $texto_boton_confirmar?>');
					}
					else if($btn_comment.hasClass('on')) {
						$btn_comment.attr('class', 'off');
						$textfield.hide();
						$btn_confirm.attr('onclick', "confirmar_lectura(this, <?= $tramite->id_tramite?>);").html('<?= $texto_boton_confirmar?>');
					}
				});
			});
		</script>
	<?php } ?>

	<hr>

	<div class="flex_centro">
		<?php if(isset($mostrar_boton_confirmar_revision) && $mostrar_boton_confirmar_revision) { ?>
			<button id="btn_confirmar" style="margin-right:16px;" onclick="confirmar_lectura(this, <?= $tramite->id_tramite?>, 'tramite');"><?= $texto_boton_confirmar?></button>
		<?php } ?>
		<button onclick="$(this).closest('.ui-dialog-content').dialog('close');">Salir</button>
	</div>

</div>

<script>
	function ingresar_comentario(e, id_tramite) {
		$.ajax({
			'type': 'POST',
			'dataType': 'json',
			'url':base_url+'tramite/ingresar_comentario',
			'data':{'id_tramite':id_tramite, 'comentario':$('#nuevo_comentario').val()},
			'success': function(data) {
				if(data.exito) {
					$(e).closest('.ui-dialog-content').dialog('close');
					swal('', 'comentario ingresado', 'success');
					//~ dialogo_tramites(<?= $tramite->id_causa?>);
				}
				else swal('', 'ocurrio un problema', 'error');
			}
		});
	}
</script>
<style>
	.file_dl:hover, .file_dl:active {
		background-color:#D7E3F0;
		border-radius:2px;
	}
</style>
