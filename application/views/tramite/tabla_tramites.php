<table id="table_tramites" class="display compact cell-border" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>Fecha</th>
			<th>Hora</th>
			<th>Alumno</th>
			<th>Detalle</th>
			<th>Archivos</th>
			<th>
				<img height="14" class="img" title="pendiente, revisado, leido o cerrado" src="../../assets/images/info2.png">
				<span>Estado</span>
			</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($tramites as $tram) { ?>
		<tr>
			<td><?= $tram['fecha_ingreso']?></td>
			<td><?= $tram['hora_ingreso']?></td>
			<td><?= $tram['nombre_alumno']?></td>
			<td><?= substr($tram['descripcion'], 0, 60)?><?= strlen($tram['descripcion']) > 60 ? "..." : ""?></td>
			<td>
			<?php foreach ($tram['archivos'] as $arch) { ?>
				<a href="<?= base_url()?>paginas/descargar/<?= $arch['id']?>" style="text-decoration:none">
				<div class="relleno_gris" style="display:inline-block;">
					<img class="icono_menu" title="<?= $arch['file_name']?>" src="../../assets/images/<?= getNombreIconoPorExtension($arch['file_ext'])?>">
				</div>
				</a>
			<?php } ?>
			</td>
			<td><span style="<?= $tram['estado_en_rojo'] ? "color:red" : ""?>"><?= $tram['estado_revision']?></span></td>
			<td>
				<button title="ver detalles del trámite" class="icono_btn" onclick="dialogo_detalle_tramite(<?= $tram['id_tramite']?>);">
					<img src="../../assets/images/eye_open.svg">
				</button>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<script>
	$('#table_tramites').DataTable({
			"pagingType": "simple_numbers",
			"order": [[ 0, "desc" ],[ 1, "desc" ]],
			"dom": '<"top">rt<"bottom"p><"clear">'
	} );
</script>
