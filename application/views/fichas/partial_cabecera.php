<style type="text/css" media="screen">
	.cabecera { font-size:9pt; font-family:Arial, Garamond, Serif; font-style:italic; font-weight:bold; text-align:justify; color:#000000; }
	.cabecera .contenido { width:100%; margin:0; margin:auto; }
	.cabecera .contenido tr td { text-align:center ! important; vertical-align:bottom; padding:0; border:none; border-bottom:0.01cm solid black; padding-bottom:8px; }
</style>
<div class="cabecera" style="margin-top:-20px;margin-bottom:12px;">
	<table border="0" cellspacing="0" cellpadding="0" class="contenido">
		<tr>
			<td class="columna_texto" style="width:2in;">
				<div>Escuela de Derecho</div>
				<div>Universidad Andrés Bello</div>
				<div>Campus Viña del Mar</div>
			</td>
			<td class="columna_logo">
				<div style="margin:0px 0px;margin-bottom:-5px">
					<img style="height:auto;width:1.9cm;" alt="" src="assets/images/logo.jpg"/>
				</div>
			</td>
			<td class="columna_texto" style="width:2in;">
				<div>Clínica Jurídica </div>
				<div>Quillota N° 980 Viña del Mar</div>
				<div>Teléfonos: 2845152 - 2845153</div>
			</td>
		</tr>
	</table>
</div>
