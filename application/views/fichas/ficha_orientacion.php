<!doctype html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Ficha de Ingreso ORIENTACIÓN</title>
		<style type="text/css" media="screen">
			table { border-collapse:collapse; border-spacing:0; empty-cells:show }
			/*td, th { vertical-align:top; font-size:12pt;}*/

			.P26 { font-size:10pt; }
			.P25 { font-size:8pt; }
			.
			.P7 { font-size:13pt; font-family:Calibri, DejaVu Sans, sans-serif; text-align:center ! important; }

			.T11 { font-family:Calibri, DejaVu Sans, sans-serif; font-weight:bold; }
			.T12 { font-family:Calibri, DejaVu Sans, sans-serif; font-size:15pt; font-weight:bold; }

			.P32 { font-size:12pt; font-family:Times New Roman, DejaVu Serif, serif;  margin-left:0in; margin-right:0in; text-align:left ! important; margin-bottom: 0.9em;}

			table { width: 100%; }

			.camponombre, .campotexto {
				font-family:Calibri, DejaVu Sans, sans-serif;
				font-weight:bold;
				vertical-align:bottom;
				white-space: nowrap;
			}

			td.campotexto {
				width:100%;
				vertical-align:bottom;
				padding-bottom:4px;
				position: relative;
				height: 1em;
			}
			div.campotexto {
				position: relative;
			}

			.linea_campo {
				vertical-align:bottom;
				font-weight:bold;
				border-bottom:2px dotted black;
			}

			.textoencima, .textoescrito {
				font-family: DejaVu Sans, sans-serif;
				font-weight: normal;
				font-size:inherit;
				color:#004db3;
			}
			.textoencima {
				position:absolute;
				text-align:left;


				top:-1.4em;
				left:4px;
			}

			.contenido { margin-bottom: 8em; }
			.footer { position: absolute; bottom: 0px; left: 0px; right: 0px; height: 8em;}
		</style>
	</head>

	<body>

		<div class="contenido">
			<?php $this->view('fichas/partial_cabecera'); ?>

			<div style="height:1.7em;"></div>

			<div class="P7">
				<span class="T11"><u>FICHA DE </u></span>
				<span class="T12"><u>ORIENTACIÓN </u></span>
				<span class="T11"><u>CLINICA JURIDICA UNAB</u></span>
			</div>

			<div style="height:2.8em;"></div>

			<div class="" style="padding: 0px 46px;">
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">Fecha</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $fecha_ingreso?></div></div></td>
						</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">Nombre USUARIO:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $nombre_cliente?></div></div></td>
						</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">RUN:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $rut_cliente?></div></div></td>
						</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">FONOS:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $telefonos_cliente?></div></div></td>
						</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">Domicilio:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $domicilio_cliente?></div></div></td>
						</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr>
							<td class="camponombre"><div class="">Email:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $email_cliente?></div></div></td>
						</tr>
					</table>
				</div>

				<div class="P32" style="margin-bottom:0px;"> </div>

				<div class="P32">
					<table>
						<tr>
					<?php if($materia_orientacion): ?>
							<td class="" style="line-height:1.7em;">
									<span class="camponombre">Materia de la consulta:</span>
									<span class="textoescrito" style="border-bottom: 2px dotted #000;"><?= $materia_orientacion?> ddddddddddd asd asd asd.</span>
							</td>
					<?php else: ?>
							<td class="camponombre"><div class="">Materia de la consulta:</div></td>
							<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td>
					<?php endif; ?>
						</tr>
					</table>
				</div>

				<div class="P32" style="margin-bottom:0px;"> </div>

				<?php if($resena): ?>
					<div class="P32">
						<table>
						<tr>
							<td class="" style="line-height:1.7em;">
									<span class="camponombre">Breve reseña de la orientación brindada:</span>
									<span class="textoescrito" style="border-bottom: 2px dotted #000;"><?= $resena?></span>
							</td>
						</tr>
						</table>
					</div>
				<?php else: ?>
				<div class="P32">
					<table>
					<tr>
					<td class="camponombre"><div class="">Breve reseña de la orientación brindada:</div></td>
					<td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td>
					</tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr><td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td></tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr><td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td></tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr><td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td></tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr><td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td></tr>
					</table>
				</div>
				<div class="P32">
					<table>
						<tr><td class="campotexto"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td></tr>
					</table>
				</div>
				<?php endif; ?>
			</div>
		</div>

		<div class="footer">
			<table>
				<tr>
					<td class="camponombre"><div class="">Abogado:</div></td>
					<td class="campotexto" style="width:50%;"><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"><?= $nombre_abogado?></div></div></td>
					<td class="camponombre"><div class="">Firma Usuario:</div></td>
					<td class="campotexto" ><div class="campotexto"><div class="linea_campo"></div><div class="textoencima"></div></div></td>
				</tr>
			</table>
		</div>

	</body>
</html>
