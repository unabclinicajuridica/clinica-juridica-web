<?php $seleccionado = '<span class="checkado"></span>'; ?>
<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Ficha de Ingreso CASO o CAUSA</title>
	<style type="text/css" media="screen">
		body { font-size:11pt; font-family:'Times New Roman'; writing-mode:lr-tb; text-align:justify; }
		table, tr, td { border-spacing:0px; border-collapse:collapse; border:0px; padding:0px; margin:0px }

		.titulo { text-decoration:underline; }
		.titulo b { font-size:13pt; }

		.campo { line-height:150%; }
		.campo table { width:100%; }
		.nombre_campo { font-weight:bold; }
		.nombre_campo div, .nombre_campo label { white-space:nowrap; }
		.linea_campo, .linea_campo_fina { width:100%; vertical-align:bottom; font-weight:bold; padding:0px 6px 6px 0px; }
		.linea_campo div { border-bottom:2px dotted black; padding-left:10px;font-weight:normal;font-family:Tahoma, Geneva, sans-serif;font-size:0.9em; }
		.linea_campo label { position:relative;margin-bottom:-4px; }
		/*.linea_campo label { position:relative; top:3px; }*/
		.linea_campo_fina div { border-bottom:1px dotted black; }


		.formulario_avanzado { font-size:9pt; font-family:'Times New Roman'; text-align:justify; margin:8px -8px; }
		.formulario_avanzado td { position: relative; }
		.Table2_A1, .Table2_B1 { vertical-align:top; padding:0in 0.07in 0in 0.07in; border:0.017cm solid black;}
		.Table2_A1 {  border-right-style:none; }
		.T2 { font-weight:bold; margin: 4px 0px 20px 0px; }
		.formulario_avanzado table table td { padding-bottom:5px; }
		.formulario_avanzado > table > tbody > tr > td { padding-bottom:12px; }
		.relativo { position:relative; }
		.checkado, .textoencima {
			font-family: DejaVu Sans, sans-serif;
			/*text-decoration:underline;*/
			font-size: 20px;
			position:absolute; margin:auto; left: 0; right: 0;
			/*bottom:0;*/
			top:-10px;
		 	text-align:center;
			color:#0058cc;
		}
		.textoencima { font-size:inherit; top:-4px; color:#003780; }
		.azuloscuro {  color:#003780; font-family: DejaVu Sans, sans-serif; }
		.bold { font-weight: bold;}
		.mayuscula { text-transform:uppercase; }
		.checkado:before { content:'\2714';}
		.izquierda { text-align:left; margin-right:0px;margin-left:3px; }
		.textoescrito {
			font-family: DejaVu Sans, sans-serif;
			font-size:inherit;
			color:#003780;
		}
	</style>
</head>

<body>
	<?php $this->view('fichas/partial_cabecera'); ?>

	<div class="titulo_ficha">
		<p class="titulo">
			<span>FICHA DE INGRESO <b>CASO O CAUSA A TRAMITACIÓN</b> CLINICA JURIDICA UNAB</span>
		</p>
	</div>

	<div class="formulario_basico" style="margin-bottom:10px;margin-top:2px;">
		<div class="campo">
			<table>
				<tr>
					<td width="25%">
						<table>
							<tr>
								<td class="nombre_campo"><label>FECHA</label></td>
								<td class="linea_campo"><div style=""><label><?= $causa->INGRESO?></label></div></td>
							</tr>
						</table>
					</td>
					<td width="36%">
						<table>
							<tr>
								<td class="nombre_campo"><label>ABOGADO</label></td>
								<td class="linea_campo"><div><label><?= $causa->NOMBRE_ABOGADO?></label></div></td>
							</tr>
						</table>
					</td>
					<td width="39%">
						<table>
							<tr>
								<td class="nombre_campo"><label>ALUMNO</label></td>
								<td class="linea_campo"><div><label><?= $causa->NOMBRE_ALUMNO?></label></div></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<div class="campo">
			<table>
				<tr>
					<td class="nombre_campo"><label>NOMBRE&nbsp;USUARIO&nbsp;(A):</label></td>
					<td class="linea_campo"><div><label><?= $cliente->nombre_cliente?></label></div></td>
				</tr>
			</table>
		</div>

		<div class="campo">
			<table>
				<tr>
					<td width="30%">
						<table>
							<tr>
								<td class="nombre_campo"><label>RUN</label></td>
								<td class="linea_campo"><div><label><?= $causa->rol_causa?></label></div></td>
							</tr>
						</table>
					</td>
					<td width="50%">
						<table>
							<tr>
								<td class="nombre_campo"><label>EDAD</label></td>
								<td class="linea_campo" style="padding-right:60%;"><div><label><?= $cliente->edad?></label></div></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div class="campo">
			<table>
				<tr>
					<td class="nombre_campo"><label>DOMICILIO</label></td>
					<td class="linea_campo"><div><label><?= $cliente->domicilio?></label></div></td>
				</tr>
			</table>
		</div>
		<div class="campo">
			<table>
				<tr>
					<td class="nombre_campo"><label>TELÉFONOS</label></td>
					<td class="linea_campo"><div><label><?= $cliente->telefono?></label></div></td>
				</tr>
			</table>
		</div>
		<div class="campo">
			<table>
				<tr>
					<td class="nombre_campo"><label>CORREO&nbsp;ELECTRÓNICO</label></td>
					<td class="linea_campo"><div><label><?= $cliente->email?></label></div></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="formulario_avanzado">
		<table style="width:100%;">
			<tr>
				<td style="text-align:left;" class="Table2_A1">
					<div class="T2">ESTADO CIVIL:</div>
					<table>
						<tr>
							<td >Casado:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'casado' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">Soltero:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'soltero' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Viudo:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'viudo' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">AUC:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'AUC' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Divorciado:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'divorciado' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">Conviviente:</td>
							<td class="relativo">____<?= $datos_cliente->estado_civil === 'conviviente' ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
						<tr><td><p></p></td></tr>
						<tr><td><p></p></td></tr>
					</table>
					<table>
						<tr>
							<td>Separado de hecho:</td>
							<td>Sí</td>
							<td>_____<?= $datos_cliente->separado_de_hecho == true ? $seleccionado : ''?></td>
							<td>No</td>
							<td>_____<?= $datos_cliente->separado_de_hecho == false ? $seleccionado : ''?></td>
						</tr>
					</table>
				</td>
				<td colspan="2" style="text-align:left; " class="Table2_B1">
					<div class="T2">SITUACIÓN LABORAL:</div>
					<table>
						<tr>
							<td >Independiente: Estable:</td>
							<td >____<?= $datos_cliente->situacion_laboral === 'estable' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">Eventual:</td>
							<td >____<?= $datos_cliente->situacion_laboral === 'eventual' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Jubilado:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'jubilado' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">Cesante:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'cesante' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Labores:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'labores' ? $seleccionado : ''?></td>
							<td style="padding-left:30px;">Estudiante:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'estudiante' ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
					</table>
					<table>
						<tr>
							<td>Dependiente:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'dependiente' ? $seleccionado : ''?></td>
							<td style="padding-left:20px;">Con Contrato:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'con_contrato' ? $seleccionado : ''?></td>
							<td style="padding-left:20px;">Sin Contrato:</td>
							<td>____<?= $datos_cliente->situacion_laboral === 'sin_contrato' ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
					</table>
					<?php if($datos_cliente->actividad): ?>
						<div class="vertical-align:bottom;">
							Actividad: <span class="azuloscuro"><?= $datos_cliente->actividad?></span>
						</div>
					<?php else: ?>
					<table style="width:100%;">
							<tr>
								<td style="vertical-align:bottom;">Actividad:</td>
								<td style="width:100%;vertical-align:bottom;padding-right:26px;"><div style="border-bottom:1px dotted black;"></div></td>
							</tr>
							<tr><td colspan="2" style="vertical-align:bottom;padding-right:26px;"><div style="border-bottom:1px dotted black;height:1.5em;"></div></td></tr>
					</table>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td style="text-align:left; " class="Table2_A1">
					<div class="T2">TIPO DE PREVISION:</div>
					<table>
						<tr>
							<td >A.F.P.:</td>
							<td >____<?= $datos_cliente->tipo_prevision === 'AFP' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">I.P.S.:</td>
							<td >____<?= $datos_cliente->tipo_prevision === 'IPS' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;"> S/P:</td>
							<td >____<?= $datos_cliente->tipo_prevision === 'SP' ? $seleccionado : ''?></td>
						</tr>
					</table>
				</td>
				<td colspan="2" style="text-align:left; " class="Table2_B1">
					<div class="T2">SISTEMA DE SALUD:</div>
					<table>
						<tr>
							<td >Isapre:</td>
							<td >____<?= $datos_cliente->sistema_salud === 'isapre' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Fonasa:</td>
							<td >____<?= $datos_cliente->sistema_salud === 'fonasa' ? $seleccionado : ''?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Dipreca:</td>
							<td >____<?= $datos_cliente->sistema_salud === 'dipreca' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Capredena:</td>
							<td >____<?= $datos_cliente->sistema_salud === 'capredena' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Otra:&nbsp;</td>
							<td ><?= ! in_array($datos_cliente->sistema_salud, ['isapre','fonasa','dipreca','capredena']) ? '<span class="azuloscuro mayuscula"> '.$datos_cliente->sistema_salud.'</span>' : '____'?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="text-align:left; " class="Table2_A1">
					<div class="T2">ESCOLARIDAD:</div>
					<table>
						<tr>
							<td >Analfabeto:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'analfabeto' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Básica:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'basica' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Media:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'media' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Técnica:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'tecnica' ? $seleccionado : ''?></td>
						</tr>
						<tr>
							<td>Superior:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'superior' ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">Cursando:</td>
							<td class="relativo">____<?= $datos_cliente->escolaridad === 'cursando' ? $seleccionado : ''?></td>
						</tr>
					</table>
				</td>
				<td colspan="2" style="text-align:left; " class="Table2_B1">
					<div class="T2">GRUPO FAMILIAR:</div>
					<table>
						<tr>
							<td >TOTAL PERSONAS:</td>
							<td >________<?= '<span class="textoencima">'.$datos_cliente->total_personas.'</span>'?></td>
							<td style="padding-left:15px;">Adultos:</td>
							<td >____<?= '<span class="textoencima">'.$datos_cliente->total_adultos.'</span>'?></td>
							<td style="padding-left:15px;">Menores:</td>
							<td >____<?= '<span class="textoencima">'.$datos_cliente->total_menores.'</span>'?></td>
						</tr>
						<tr><td><p></p></td></tr>
						<tr><td><p></p></td></tr>
					</table>
					<table>
						<tr>
							<td>- Discapacitados: SI:</td>
							<td >________<?= $datos_cliente->discapacitados == true ? $seleccionado : ''?></td>
							<td style="padding-left:10px;">NO:</td>
							<td >________<?= $datos_cliente->discapacitados == false ? $seleccionado : ''?></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="text-align:left; " class="Table2_A1">
					<div class="T2">SITUACIÓN HABITACIONAL:</div>
					<table>
						<tr>
							<td>Propia:</td>
							<td >____<?= $datos_cliente->situacion_habitacional === 'propia' ? $seleccionado : ''?></td>
							<td style="padding-left:10px;">Arrendada:</td>
							<td >____<?= $datos_cliente->situacion_habitacional === 'arrendada' ? $seleccionado : ''?></td>
							<td style="padding-left:10px;">Allegada:</td>
							<td >____<?= $datos_cliente->situacion_habitacional === 'allegada' ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
						<tr>
							<td>Prestada:</td>
							<td>____<?= $datos_cliente->situacion_habitacional === 'prestada' ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
					</table>
					<table>
						<tr>
							<td>Valor dividendo o arriendo:&nbsp;</td>
							<td>$</td><td>_______________<?= '<span class="textoencima izquierda">'.$datos_cliente->valor_dividendo.'</span>'?></td>
						</tr>
					</table>
				</td>
				<td style="text-align:left; " class="Table2_A1">
					<div class="T2">INGRESOS:</div>
					<table>
						<tr><td>$</td><td>_______________________<?= '<span class="textoencima izquierda">'.$datos_cliente->ingresos_1.'</span>'?></td></tr>
						<tr><td><p></p></td></tr>
						<tr><td>$</td><td>_______________________<?= '<span class="textoencima izquierda">'.$datos_cliente->ingresos_2.'</span>'?></td></tr>
						<tr><td><p></p></td></tr>
						<tr><td>$</td><td>_______________________<?= '<span class="textoencima izquierda">'.$datos_cliente->ingresos_3.'</span>'?></td></tr>
						<tr><td><p></p></td></tr>
						<tr><td><p></p></td></tr>
					</table>
					<table>
						<tr>
							<td><span class="T2">Total $</span></td>
							<td><span class="T2">_________________<?= '<span class="textoencima izquierda">'.$datos_cliente->ingresos_total.'</span>'?></span></td>
						</tr>
					</table>
				</td>
				<td style="text-align:left;max-width:30%" class="Table2_B1">
					<div class="T2">BENEFICIOS ESTATALES:</div>
					<table>
						<tr>
							<td >Si:</td>
							<td >____<?= $datos_cliente->beneficios_estatales == true ? $seleccionado : ''?></td>
							<td style="padding-left:18px;">No:</td>
							<td >____<?= $datos_cliente->beneficios_estatales == false ? $seleccionado : ''?></td>
						</tr>
						<tr><td><p></p></td></tr>
					</table>
					<table style="width:100%;">
						<tr>
							<td>Indicar cual:</td>
						</tr>
						<?php if($datos_cliente->beneficio_estatal): ?>
							<tr><td><span class="textoencima izquierda"><?= $datos_cliente->beneficio_estatal?></span></td></tr>
						<?php else: ?>
						<tr>
							<td colspan="2" class="linea_campo_fina" style="padding-right:26px;">
								<div style="height:1.5em;"></div>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="linea_campo_fina" style="padding-right:26px;">
								<div style="height:2em;"></div>
							</td>
						</tr>
						<?php endif; ?>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div class="legal">
		<p style="padding:0px 8px; font-size:11pt;">
			<span>Declaro bajo Juramento que los antecedentes entregados precedentemente son verdaderos; si así no fuere, estoy de acuerdo desde ya, para que se me aplique el Reglamento del Usuario de la Clínica Jurídica UNAB y que se deje sin efecto o de revoque la prestación del servicio y/o patrocinio y poder que se haya conferido, sea que el caso se encuentre en etapa judicial o extrajudicial. Asimismo me obligo a consultar en la Clínica, periódicamente, sobre el estado de mi caso,  como  fechas de audiencias, documentos faltantes, etc.; y me obligo a informar a la Clínica mis cambios de domicilio, de teléfonos, email y a proporcionar oportunamente todos los datos, antecedentes y pruebas necesarios para fundar mis pretensiones o que se me solicite por el abogado o alumno que atienden de mi caso.</span>
		</p>
		<p>&nbsp;</p>
		<div class="campo">
			<table>
				<tr>
					<td style="width:100%;"><div></div></td>
					<td style="padding-right:80px">
						<div class="" style="text-align:center;">
							<div>___________________________</div>
							<div>Firma usuario</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
