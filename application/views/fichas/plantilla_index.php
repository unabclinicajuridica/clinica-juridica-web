<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-2.0.4/css/bootstrap-2.0.4.min.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap-2.0.4/css/bootstrap-responsive-2.0.4.min.css"/>
<script src="<?php echo base_url(); ?>assets/bootstrap-2.0.4/js/bootstrap-2.0.4.min.js"></script>
<style media="screen">
   .todo { margin-top:9px; }
   .breadcrumbs {  }
   .categorias {  }
   .lista_plantillas {  }

	.item_plantilla.span3 { padding-left:10px; padding-right:10px; }
	.item_plantilla.span3:nth-child(4n+1) {
	  clear: both;
	}

   .thumbnail {
		position: relative;
		padding: 0px;
		margin-bottom: 20px;
		padding: 18px 12px 12px 12px;
		/*height: 371px;*/
	}
	.thumbnail .marco_imagen > img {
		max-height:200px; max-width:200px;
		/*height: 100%; */
		border: 1px solid #888;
		box-shadow: 1px 1px 2px #999;
	}

	.no_thumnbail {
		width:144px;
		height:199px;
		background-color: #ccc;
		text-align: center;
		vertical-align: middle;
		margin: auto;
		border: 1px solid rgb(90, 85, 90);
		border-radius: 2px;
	}

	.marco_imagen {
		margin-bottom:9px;
		text-align: center;
	}
   .nombre_archivo { margin-top:6px; font-size:0.7em; font-style:italic;}
   .titulo_plantilla { margin-bottom:2px; }
   .boton_descargar { text-align:right; padding-top:3px; }

	.item_plantilla .thumbnail:hover { background-color:rgb(238, 238, 238); border-color:rgb(170, 170, 170); }

	@media only screen and (max-width: 1500px) {
		.thumbnail .marco_imagen > img {
		 	max-width:90px;
		}
	}

	@media only screen and (max-width: 768px) {
		.thumbnail .marco_imagen > img {
			max-width:300px;
			max-height:300px;
		}
	}

	.descripcion p {
		display: block; /* or inline-block */
		text-overflow: ellipsis;
		word-wrap: break-word;
		overflow: hidden;
		max-height: 78px;
	}
</style>
<div class="todo">
   <div class="row-fluid">
		<div class="breadcrumbs span12">
			<ul class="breadcrumb">
			<?php if ($type == 'category'): ?>
				<li>
					 <a href="javascript://" onclick="main_plantillas();">Todos</a>
					 <span class="divider">/</span>
				</li>
				<?php $cat_total = count($cat); foreach ($cat as $key => $c): ?>
				<li>
					 <a href="javascript://" onclick="cargar('contenido','paginas/category/<?= $c['slug']?>')"><?php echo $c['name']; ?></a>
					 <?php if ($key+1 != $cat_total): ?>
					 <span class="divider">/</span>
					 <?php endif; ?>
				</li>
				<?php endforeach; ?>
				<?php else: ?>
				<li>
					 <a href="javascript://" onclick="main_plantillas();">Todos</a>
				</li>
			<?php endif; ?>
			</ul>
	   </div>
   </div>
   <div class="row-fluid">
	   <div class="lista_plantillas span9">
			<?php $last_index = count($plantillas) - 1; ?>
			<?php foreach ($plantillas as $index => $ptilla): ?>
			<?php if($index % 4 === 0): ?>
				<div class="fila row-fluid">
			<?php endif; ?>
				<div class="item_plantilla span3">
					<div class="thumbnail">
						<div class="marco_imagen">

							<?php if($ptilla->thumbnail): ?>
								<img src="<?= $ruta_thumbnails . $ptilla->thumbnail?>">
							<?php else: ?>
								<div class="no_thumnbail">
									<span style="display: inline-block;height: 100%;vertical-align: middle;"></span>
									<img src="/assets/images/doc.png">
								</div>
							<?php endif; ?>

							<div class="nombre_archivo"><?= $ptilla->orig_name?></div>
						</div>
						<div class="">
							<div class="titulo_plantilla">
								<h4><?= $ptilla->titulo?></h4>
							</div>
							<div class="descripcion">
								<?php if($ptilla->descripcion): ?>
									<p><?= $ptilla->descripcion?></p>
								<?php else: ?>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
								<?php endif; ?>
							</div>
							<div class="boton_descargar">
								<a href="javascript://" class="btn btn-info btn-xs" role="button">Info</a>
								<!-- <a href="<?= $controlador_descargar . $ptilla->id?>" class="btn btn-success btn-xs" role="button"> -->
								<a href="<?= site_url($controlador_descargar . '/' . $ptilla->id_archivo)?>" class="btn btn-success btn-xs" role="button">
									<span>Descargar</span>
									<i class="icon-download icon-white"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php if((($index+1) % 4 === 0) || ($index === $last_index)): ?>
				</div>
			<?php endif; ?>
			<?php endforeach; ?>
	   </div>
		<div class="categorias span3">
			<ul class="nav nav-tabs nav-stacked">
				 <li class="active">
				 <a href="javascript://"><b>Categorías</b></a>
				 </li>
				 <?php foreach($categories as $cat): ?>
				 <li><a href="javascript://" onclick="cargar('contenido','paginas/category/<?= $cat['slug']?>')"><?php echo $cat['name']; ?></a></li>
				 <?php endforeach; ?>
			</ul>
	   </div>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function(){

   });
</script>
