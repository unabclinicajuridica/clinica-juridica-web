<style media="screen">
	#form_plantilla select, #form_plantilla input, #form_plantilla textarea { width:70%; }
</style>

<h2 class="centro">Ingreso de Formulario al Sistema</h2>
<div class="fondo_formularios" style="width:50%;">
	<form id="form_plantilla" action="" method="post">
		<div>
			<div id="p_result_ingreso" class="padding_medio"></div>
		</div>

		<table class="tabla_geno">
			<tr>
				<td><label>Documento:</label></td>
			  <td>
				  <input name="documento" type="file"  class="" accept=".doc,.xls,.docx,.pdf,.xlsx,.odt,.ods,.odp" required>
			  </td>
			</tr>
			<tr>
				<td><label>Categoría</label></td>
				<td>
					<select name="id_ciforum_categoria" required>
						 <option value="0">-- ninguna --</option>
						 <?php foreach ($categories as $cat): ?>
						 <option value="<?php echo $cat['id']; ?>"><?php echo $cat['name']; ?></option>
						 <?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td><label>Título:</label></td>
				<td><input type="text" name="titulo" required></td>
			</tr>
			<tr>
				<td><label>Descripción:</label></td>
				<td><textarea name="descripcion" ></textarea></td>
			</tr>
			<tr>
				<td><label>Thumbnail (opcional):</label></td>
			  <td><input name="thumbnail" type="file"  accept="image/*"></td>
			</tr>
		</table>
		<div class="centro padding">
			<input type="hidden" name="btn_submit" value="1">
			<button type="submit" name="btn_submit">Subir Plantilla</button>
		</div>
	</form>
</div>

<script>
	$('#form_plantilla').submit(function (e) {
		e.preventDefault();
		// var formData = $(this).serializeArray();
		$('#p_result_ingreso').empty();
		jQuery.ajax({
			url: '<?=site_url($controlador)?>',
			dataType: 'JSON',
			data: new FormData(this),
			processData: false,
			contentType: false,
			type: 'POST',
			success: function(data) {
				if(data.exito) {
					var p = document.createElement('div');
					swal('', '<p style="color:green">Plantilla Subida</p>', 'success');
					main_subir_plantilla();
				} else {
					$('#p_result_ingreso').html(data.mensaje).css({'color':'red', 'text-align':'center'});
				}
			}
		});
		return false;
	});
</script>
