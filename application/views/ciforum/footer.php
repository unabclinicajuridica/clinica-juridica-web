        </div> <!-- row-fluid -->
        <div class="row-fluid">
            <div class="span12" style="border-top: 2px solid #f3f3f3;margin-top: 10px;padding:10px 0;text-align: right;font-size:11px;">
                Xperiment by <a href="http://superdit.com">Aditia Rahman</a> using <a href="http://codeigniter.com">Codeigniter</a>, <a href="http://twitter.github.com/bootstrap/">Twitter Bootstrap</a>, and <a href="http://netbeans.org">Netbeans</a>. <i>Free to use for any kind of purposes.</i>
            </div>
        </div>
        </div> <!-- container-fluid -->
    </body>
	 <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/jquery-ui-1.12.1.min.css">
	 <script src="<?= base_url()?>assets/js/jquery-ui-1.12.1.min.js"></script>
	 <script>
	 	var base_url = '<?= base_url() ?>';
	 	var index_page = '<?= index_page() ?>';
	 	// Muy Importante para terminar sesion automaticamente.
	 	var _timeoutSesion = <?= getTimeoutDeSesion($this) ?>;
	 </script>
	 <script src="<?= base_url()?>assets/js/timeout_sesion.js"></script>
</html>
