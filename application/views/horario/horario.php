<h2 class="titulo_fieldset"><?= $titulo_horario?></h2>
<hr>

<div class="mycal" style="width:100%;"></div>
<hr>

<script>
	$( document ).ready(function() { //una vez que cargo la pag y los datasets, cargo el calendario
		$('.mycal').easycal({

			// tambien puedo escribir la fecha como 31/10/2104, aca se selecciona la semana de la fecha
			startDate: '<?= $fecha_inicio_calendario?>',
			timeFormat: 'HH:mm',
			columnDateFormat: 'dddd, DD MMM',

			//cargo maxima fecha segun retorno
			minTime: '<?= $inicio_atencion?>',

			//cargo minima fecha segun retorno
			maxTime: '<?= $termino_atencion?>',

			// 30 diurno, 10 vespertino
			slotDuration: 30,

			// 30 diurno, 10 vespertino
			timeGranularity: 30,
			dayClick: function(el, startTime) {
				//~ var eventIds = JSON.parse(el.attr('data-event-ids'));
				// var fecha = el.attr('data-date');
				var fecha = $(el).closest('td.ec-slot-col').attr('data-date');
				swal('Permite agregar un evento el '+ fecha +' a las ' + startTime);
				console.log('eventIds = '+eventIds[0]);
			},
			eventClick: function(eventId, hora, dia){
				detalle_asignacion(eventId);
			},
			events: [
				//imprimo los dias con asignaciones
				<?php
					foreach($horario as $asignacion) {
						$fecha_formateada  = date("d-m-Y", strtotime($asignacion['fecha_asignacion']));
						switch($asignacion['tipo_asunto']) {
							case 'causa': $background_color = '#3CF8A0'; break;
							case 'asunto': $background_color = '#EDC857'; break;
							case 'audiencia': $background_color = '#EF665C'; break;
							case 'orientacion': $background_color = '#F99D48'; break;
						}
				?>
						{
							id: '<?= $asignacion['id']?>',
							start: '<?= $fecha_formateada.' '.$asignacion['hora_inicio']?>',
							end: '<?= $fecha_formateada.' '.$asignacion['hora_fin']?>',
							textColor: '#0d0d0d',
							libres: '',
							backgroundColor: '<?= $background_color?>'
						},
				<?php } ?>

			],
			//~ overlapColor : 'Tomato',
			overlapColor : '#C04438',
			overlapTextColor : 'White',
			overlapTitle : 'Multiple'
		});
	});
</script>
