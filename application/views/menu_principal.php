<?php
	 if (!isset($this->session->login_user)) {
		 header("Location: ".base_url());
	 }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<link rel="icon" href="<?= base_url()?>/favicon.png" type="image/jpg">
	<title>Clínica Jurídica Web</title>
	<!-- SET GLOBAL BASE URL (https://codedump.io/share/vYcPimS5u6pl/1/codeigniter-base-url-didn39t-recongnized-in-javascript) -->
	<script>
		var base_url = '<?= base_url()?>';
		var index_page = '<?= index_page()?>';
		// Muy Importante para terminar sesion automaticamente.
		var _timeoutSesion = '<?= $timeoutSesion ?>';
	</script>
	<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script> -->

	<script src="<?= base_url()?>assets/js/jquery-2.1.1.js"></script>

	<script src="<?= base_url()?>assets/js/jquery-ui-1.12.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/jquery-ui-1.12.1.min.css">

	<script src="<?= base_url()?>assets/js/responsive-menu-0.2.1.min.js" type="text/javascript"></script>

	<link href="<?= base_url()?>assets/css/responsive-menu-0.2.1.css" rel="stylesheet">

	<script src="<?= base_url()?>assets/jquery.dataTables/dataTables.min.js" type="text/javascript"></script>

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/jquery.dataTables/dataTables.min.css">

	<script src="<?= base_url()?>assets/js/funciones_clinica.js"></script>

	<script type="text/javascript" src="<?= base_url()?>assets/jquery.qtip2/jquery.qtip.min.js" ></script>
	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/jquery.qtip2/jquery.qtip.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/easycal.css">

	<!------------------ Scripts nesesarios para el calendario agenda	------------------->
	<script type="text/javascript" src="<?= base_url()?>assets/js/underscore-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>assets/js/moment.min.js"></script>
	<script type="text/javascript" src="<?= base_url()?>assets/js/datetime-moment.js" ></script>
	<script type="text/javascript">$(document).ready(function(){$.fn.dataTable.moment( 'DD-MM-YYYY' );});</script>
	<script type="text/javascript" src="<?= base_url()?>assets/js/easycal.js"></script>

	<script type="text/javascript" src="<?= base_url()?>assets/js/jquery.sldr.js"></script>


	<!-----------------  Sweet Alert	  ------------------->
	<script src="<?= base_url()?>assets/js/sweetalert.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/css/sweetalert.css">

	<link rel="stylesheet" href="<?= base_url()?>assets/css/estilo_clinica.css">

	<!--------------------------- Diccionario Auto Corrector ---------------------->
	<!--
	<script type="text/javascript" src="../../JavaScriptSpellCheck/include.js"></script>
	-->
	<script>
		jQuery(function ($) {
			//Responsive Menu
			var menu = $('.rm-nav').rMenu({
				minWidth: '960px'
			});

			var width_dialog = window.innerWidth < 1024 ? 'auto' : 1200;

			$("#dialog").dialog({
				autoOpen: false,
				resizable: false,
				modal: true,
				minWidth: 700,
				width: width_dialog,
				height: 'auto'
				//~ open: function(ev, ui){ }
			});
			$("#dialog2").dialog({
				autoOpen: false,
				resizable: false,
				modal: true,
				width: width_dialog,
				height: 'auto'
			});
			$("#dialog800").dialog({
				autoOpen: false,
				resizable: false,
				modal: true,
				width: 800,
				height: 'auto'
			});
			$("#dialog80p").dialog({
				autoOpen: false,
				resizable: false,
				modal: true,
				width: '75%',
				height: 'auto'
			});
			$("#dialog_alertas").dialog({
				autoOpen: false,
				resizable: false,
				modal: true,
				minWidth: 1000,
				maxWidth:1400
			});
		});


		$(document).ready(function() {
			$body = $("body");
			$(document).on({
				ajaxStart: function() { $body.addClass("loading"); },
				ajaxStop: function() { $body.removeClass("loading"); }
			});
		});
	</script>
</head>
<body>
	<div id="dialog"></div>
	<div id="dialog2"></div>
	<div id="dialog800"></div>
	<div id="dialog80p"></div>
	<div id="dialog_alertas" title="Asignaciones Rechazadas" class="oculto">
		<div class="div_tabla" style="border: 1px solid #cfd9db;margin:auto;padding:10px;">
			<table id="tabla_asig" class="display compact cell-border">
				<thead>
					<tr>
						<th>Fecha Evento</th>
						<th>Hora</th>
						<th>Pofesor</th>
						<th>Evento</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody><!-- TABLA PROGRAMATICA AQUÍ --></tbody>
			</table>
		</div>
		<div class="area_gris oculto">
			<span>No hay rechazos en este momento.</span>
		</div>
		<div id="dialog_borrar_asignacion" title="Aceptar Rechazo" class="oculto">
			<p class="centro validateTips">Seleccione el motivo de aceptación del rechazo</p>
		   <div style="margin:auto;text-align:center;">
		 	  <select id="motivos">
		    	  <option disabled selected value="0">Seleccione un Motivo</option>
		    	  <option value="mal_asignado">La cita se encontraba mal agendada</option>
		    	  <option value="reagendar">La cita se cambiará de fecha/hora</option>
		    	  <option value="cancelacion">El cliente cancelo la cita</option>
				  <option value="otro">Otro motivo</option>
		      </select>
		   </div>
		</div>
	</div>

	<div class="modal"></div>

	<div id="containerClinica">
		<div id="bodyClinica">
			<div class="header_boveda">
				<div class="header_content" style="display:flex;justify-content:space-between">
					<div class="logo_izquierdo_header">
						<img src="<?= base_url()?>assets/images/logo_unab_derecho.png" style="width: 200px; height: auto;">
					</div>

					<div class="_div_del_usuario" style="display:flex;">
						<div class="foto_header">
							<img style="width:70px; height:70px; border-radius: 50%;" src="../../assets/usuarios/<?= $this->session->rut ?>.jpg" onError="this.src='../../assets/usuarios/sin_foto.png';" >
						</div>
						<div class="usuario_header">
							<!-- Usuario Conectado -->
							<?= ucwords(strtolower($nombre."<br><div id='sede_act'>".$sede."</div>")); ?> <input type="text" id="user_logeado" value="<?= $this->session->rut ?>" style="display:none;">
							<?php if($cantidad_sedes>1): ?>
								<img onClick="cambiar_sede('<?= $this->session->rut ?>');" src="<?= base_url()?>assets/images/cambiar_sede.png" alt="Cambiar de Sede" style="cursor:pointer;width:40px;height:40px;margin-left:35%;">
							<?php endif; ?>
						</div>
					</div>

					<div class="_seccion_derecha" style="display:flex;">
						<div class="logo_derecho_header">
							<img class="logo_derecha" src="<?= base_url()?>assets/images/logo_unab_ingenieria.png" style="width: 200px; height: auto;">
						</div>
						<div class="log_off">
							<td><img src='<?= base_url()?>assets/images/cerrar.png' onclick="cerrar_sesion();" class="click" style="width:25px;height:25px;"></td>
						</div>
					</div>
				</div>
			</div>
			<div class="fondo_bu">
				 <div class="rm-container">
					  <a class="rm-toggle rm-button rm-nojs" href="#">Menu</a>

					  <nav class="rm-nav rm-nojs rm-lighten">
							<ul id="menu_margen">
						<?php if($rol->mantencion_todo) { ?>
							<li><a href="#">Mantenedor</a>
								<ul>
									<li><a href="#" onclick="main_mantenedor_bloques();">Horario de Abogados</a></li>
									<li><a href="../mantenedor/mantenedor_clientes" target="_blank">Usuarios</a></li>
									<!-- <li><a href="../mantenedor/mantenedor_admin" target="_blank">Administradores</a></li> -->
									<li><a href="../mantenedor/mantenedor_directores" target="_blank">Directores</a></li>
									<li><a href="../mantenedor/mantenedor_secretarios" target="_blank">Funcionarios</a></li>
									<li><a href="../mantenedor/mantenedor_profesores" target="_blank">Profesores</a></li>
									<li><a href="../mantenedor/mantenedor_alumnos" target="_blank">Alumnos</a></li>
									<li><a href="../mantenedor/mantenedor_materias" target="_blank">Materias</a></li>
									<li><a href="../mantenedor/mantenedor_causales" target="_blank">Causales de Termino</a></li>
									<li><a href="../mantenedor/mantenedor_roles" target="_blank">Roles</a></li>
									<li><a href="../mantenedor/mantenedor_sedes" target="_blank">Sedes</a></li>
									<li><a href="../mantenedor/mantenedor_usuariosede" target="_blank">Sedes para Usuarios de Sistema</a></li>
									<li><a href="../mantenedor/mantenedor_tribunal" target="_blank">Tribunales</a></li>
								</ul>
							</li>

						<?php } if($rol->busqueda_todo) { ?>
								<li><a href="#">Busquedas</a>
									<ul>
										<li><a href="#" onclick="form_busqueda();">Busqueda de Causas</a></li>
										<li><a href="#" onclick="form_busqueda_historica();" >Busqueda de Causas Historicas</a></li>
										<li><a href="#" onclick="form_busqueda_orientacion();" >Busqueda de Orientaciones</a></li>
										<li><a href="#" onclick="main_audiencias();">Busqueda de Audiencias</a></li>
									</ul>
								</li>
						<?php } ?>

						<?php if($rol->agenda_global) { ?>
								<li><a href="#" onclick="form_agenda_semanal();">Agenda</a></li>
						<?php } ?>
						<?php if($rol->estadisticas_ver) { ?>
								<li><a href="#" onclick="main_estadisticas();">Estadisticas</a></li>
						<?php } ?>


						<?php if($rol->agenda_personal) { ?>
								<li><a href="#" onclick="horario_personal();">Horario Personal</a></li>
						<?php } ?>
						<?php if($rol->causa_propio): ?>
								<li><a href="#" onclick="main_mis_causas();">Mis Causas</a>
									<ul>
										<li><a href="#" onclick="main_causas_incompletas();">Causas Incompletas</a></li>
									</ul>
								</li>
						<?php endif; ?>


							<li><a href="#" onclick="">Ingreso</a>
								<ul>
							<?php if($rol->causa_crear): ?>
									<li><a href="#" onclick="main_ingreso_causa();">Causas</a></li>
							<?php endif; ?>
							<?php if($rol->orientacion_crear): ?>
									<li><a href="#" onclick="main_ingreso_orientacion();">Orientaciones</a></li>
							<?php endif; ?>
							<?php if($rol->asunto_crear): ?>
									<li><a href="#" onclick="main_ingreso_asunto();">Asuntos</a></li>
							<?php endif; ?>
							<?php if($rol->audiencia_crear): ?>
									<li><a href="#" onclick="main_ingreso_audiencia();">Audiencia</a></li>
							<?php endif; ?>
							<?php if($rol->tramite_crear): ?>
									<li><a href="#" onclick="main_nuevo_tramite();">Tramite</a></li>
							<?php endif; ?>
								</ul>
						</li>

						<?php if($rol->evaluar) { ?>
							<li><a href="#">Pendiente</a>
								<ul>
									<li><a href="#" onclick="main_orientaciones_sin_resena();">Orientaciones Pendientes</a></li>
									<li><a href="#" onclick="main_evaluar_audiencias();">Audiencias Pendientes</a></li>
									<li><a href="#" onclick="main_tramites_pendientes();">Trámites Pendientes</a></li>
								</ul>
							</li>
						<?php } ?>

						<?php if($rol->audiencia_propia) { ?>
							<li><a href="#" onclick="main_mis_audiencias();">Mis Audiencias</a>
								<ul>
									<li><a href="#" onclick="main_audiencias_evaluadas();">Evaluadas</a></li>
									<?php if(! $rol->evaluar) { ?>
										<li><a href="#" onclick="main_mis_audiencias_por_rellenar();">Por Rellenar</a></li>
									<?php } ?>
								</ul>
							</li>
						<?php } ?>
						<li><a href="/<?= CIBB_CARPETA ?>" target="_blank">Foro</a></li>


							<div style="float:right;display:flex;">
							<?php if($rol->agenda_global): ?>
								<li style="display:flex;align-items:center;">
									<a href="#" onclick="mostrar_dialog_alertas()">
										<div class="relative">
											<img class="icono_menu" src="../../assets/images/bell-icon.png"/>
											<span id="contador_alertas" class="medalla oculto"></span>
										</div>
									</a>
								</li>
								<script type="text/javascript">
									//IMPORTANTE
									$(document).ready(function() { startWorkerAlertas(); });
								</script>
							<?php endif; ?>
								<li style="display:inline-block;">
									<a href="#" onclick="main_plantillas();">
										<img class="icono_menu" src="../../assets/icons/form.png"/>
										<span>&nbsp;Formularios</span>
									</a>
								<?php if($rol->plantilla_subir): ?>
									<ul>
										<li>
											<a href="#" onclick="main_subir_plantilla();">
												<img class="icono_menu" src="../../assets/icons/upload.png"/>
												<span>&nbsp;Subir Formulario</span>
											</a>
										</li>
									</ul>
								<?php endif; ?>
								</li>
								<li style="display:inline-block;">
									<a href="#">
										<img class="icono_menu" src="../../assets/images/icon-settings2b.png"/>
										<span>Opciones</span>
									</a>
									<ul>
										<li><a href="#" onclick="form_cambiar_correo();">Cambiar Correo</a></li>
										<li><a href="#" onclick="form_cambiar_contrasena();">Cambiar Contraseña</a></li>
									</ul>
								</li>
							</div>
							</ul>
					  </nav>
				 </div>
				 <!-- .rm-container -->
				<div id="contenido" class="content container-fluid" >
					<?php $this->view('slider') ?>
				</div>

			</div>
		</div>
		<footer id="footerClinica">
			<div class="margen">Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
		</footer>
	</div>
</body>

<style>
	ul.animate {
		-webkit-transition: -webkit-transform 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		-moz-transition: transform 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		-o-transition: transform 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		transition: transform 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000); /* ease-in-out */
	}
	.stage {
		width: 100%;text-align: center;overflow: hidden;clear: both;margin-right: auto;
		margin-bottom: 0;margin-left: auto;padding-bottom: 0;background-color:#12125f;
		border-bottom-width:3px;border-bottom-style:solid;border-color:#ef7f1b;padding-top:100px;
	}
	.sldr {
		max-width:100%;margin: 0 auto;overflow:hidden;position:relative;clear:both;display:block;
	}
	.sldr > ul > li {
		float: left;display: block;width: 100%;
	}
	div.skew {
		max-width: 825px;margin: 0 auto;display: block;overflow: hidden;
		-webkit-transform: skewX(16deg);-moz-transform: skewX(16deg);-ms-transform: skewX(16deg);transform: skewX(16deg);
	}
	div.skew > div.wrap {
		display: block;overflow: hidden;-webkit-transform: skewX(-16deg);-moz-transform: skewX(-16deg);
		-ms-transform: skewX(-16deg);transform: skewX(-16deg);margin-left: -10.1%;width: 122%;
	}
	.selectors {
		margin-bottom:30px;position:absolute;top:80px;left:1%;right:1%;
	}
	.selectors li {
		font-size: 80px!important;line-height: 32px;display: inline;padding: 0 2px;
	}
	.selectors li a {
		text-decoration: none;
	}
	.selectors li a:hover {
		text-decoration: none;opacity:0.5;
	}
	.selectors li.focalPoint a {
		color: #ef7f1b;cursor: default;
	}
	.captions div {
		left: 200%;position: fixed;opacity: 0;
		-webkit-transition: opacity 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		-moz-transition: opacity 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		-o-transition: opacity 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000);
		transition: opacity 0.75s cubic-bezier(0.860, 0.000, 0.070, 1.000); /* ease-in-out */
	}
	.captions div.focalPoint {
		opacity: 1;left: inherit;position: static;
	}
	.clear {
		display:block;width:100%;height:0px;overflow:hidden;clear:both;
	}
	#SLDR-ONE img {
		max-width: 1100px;width: 100%;display: block;margin-right: auto;margin-bottom: 0;
		margin-left: auto;height:auto;max-height:534px;
	}
</style>
<script src="<?= base_url()?>assets/js/timeout_sesion.js"></script>
</html>
