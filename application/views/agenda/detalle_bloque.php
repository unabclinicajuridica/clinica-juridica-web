<?php if (count($asignaciones) > 0): ?>
	<h2>Bloques Agendados</h2>
	<table id='tabla_asignados' class="display compact cell-border" style="margin-bottom:40px;">
		<thead>
			<tr>
				<th>Profesor</th>
				<th>Tipo Agendación</th>
				<th>Fecha</th>
				<th>Hora inicio</th>
				<th>Hora fin</th>
				<th>Opciones de la Agendacion</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($asignaciones as $evento) { ?>
			<tr>
				<th> <?= $evento->rut?></th>
				<th>
					<?= $evento->tipo_asunto?>
					<?php if($evento->id_causa): ?>
						&nbsp;
						<img class="click" title="ver detalle de la causa" onclick="dialogo('dialog2', 'busqueda/detalle_causa/<?= $evento->id_causa?>', {});" src="../../assets/images/lupa.png" height="18px">
						<?= $evento->id_causa?>
					<?php endif; ?>
				</th>
				<th> <?= $evento->fecha_asignacion?> </th>
				<th> <?= $evento->hora_inicio?></th>
				<th> <?= $evento->hora_fin?></th>
				<th>
					<button class="icono_btn small" title="Eliminar Agendación" onclick="dlg_borr_asig(<?= $evento->id?>);">
						<img src="../../assets/images/equis.png">
						<label>Eliminar</label>
					</button>
					<button class="icono_btn small" title="Ver detalles de la Agendación" onclick="detalle_agendacion(<?= $evento->id?>);">
						<img src="../../assets/images/lupa.png">
						<label>Detalles</label>
					</button>
				</th>
			</tr>
		<?php } ?>
		</tbody>
	</table>
<?php endif; ?>


<?php
	$this->load->helper('configuracion');
	$dt_Chile = new DateTime("now");
	$timestamp_ahora = $dt_Chile->getTimestamp();
	$dt_bloque = DateTime::createFromFormat("d-m-YH:i:s", $dia."".$hora);
	$timestamp_bloque = $dt_bloque->getTimestamp();
	if ($timestamp_bloque > $timestamp_ahora && count($profesores_disponibles) > 0): ?>

			<h2>Profesores Disponibles para Agendar</h2>

			<table id="tabla_profesores_disponibles" class="display compact cell-border">
				<thead>
					<tr>
						<th>Rut Profesor</th>
						<th>Nombre Profesor</th>
						<th>Opciones de Agenda</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($profesores_disponibles as $row): ?>
						<tr>
							<td> <?= $row->rut."-".$row->dv ?> </td>
							<td> <?= $row->nombre ?> </td>
							<td>
								<img title="crea una nueva causa y la agenda" class="click" onclick="dialog_nueva_agendacion_nueva_causa('<?= $dia ?>', '<?= $hora ?>','<?= $row->rut ?>')" src="../../assets/images/icono_causa.png" height="28px">
								<img title="agenda una causa ya existente" class="click" onclick="abrir_detalle_agendar('<?= $dia ?>', '<?= $hora ?>','<?= $row->rut ?>', 'C', false)" src="../../assets/images/icono_causa_asignada.png" height="28px">
								<img title="agenda una audiencia para una causa" class="click" onclick="abrir_detalle_agendar('<?= $dia ?>', '<?= $hora ?>','<?= $row->rut ?>', 'Au', true)" src="../../assets/images/icono_audiencia.png" height="28px">
								<img title="ingresa y agenda un Asunto" class="click" onclick="dialog_agendar_asunto('<?= $row->rut ?>', '<?= $dia ?>', '<?= $hora ?>')" src="../../assets/images/icono_asunto.png" height="28px">
								<img title="agenda una orientación" class="click" onclick="dialog_agendar_nueva_orientacion('<?= $row->rut ?>', '<?= $dia ?>', '<?= $hora ?>')" src="../../assets/icons/or_purple.png" height="28px">
							</td>
							<style> td > img {margin-right:4px;} </style>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

	<?php else: ?>
		<div class="margin padding">
			<h2>Hoy es <?= $dt_Chile->format(getFormatoFechaPHP().', H:i')?></h2>
			<p>este bloque comenzó a las <?= $dt_bloque->format('H:i') . ' del ' . $dt_bloque->format(getFormatoFechaPHP()) ?></p>
		</div>
	<?php endif;?>

<script>
	$('#tabla_asignados').DataTable({
			autoWidth: false,
			responsive: false,
			pagingType: "simple_numbers",
			ordering: false,
			dom: "tr"
	});

	$('#tabla_profesores_disponibles').DataTable({
			pagingType: "simple_numbers",
			"ordering": false,
			"dom": "tr"
	});
</script>
