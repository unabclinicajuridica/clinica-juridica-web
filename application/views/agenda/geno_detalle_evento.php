<div id="div_detalle_evento" class="fondo_formularios wide">
	<h2 class="centro"><?= $titulo_vista ?></h2>

	<input type="hidden" id="sede" name="sede"  value="<?= $sede_actual ?>">
	<input type="hidden" id="id_tipo_evento" name="id_tipo_evento"  value="<?= $id_tipo_evento ?>">

	<table class="tdjunto">
		<tr>
			<td><label for="nombre_asunto">Titulo: </label></td>
			<td><label><?= $titulo?></label></td>
		</tr>
		<tr>
			<td><label for="">Mensaje: </label></td>
			<td><label><?= $descripcion ?></label></td>
		</tr>
		<tr>
		</tr>
		<tr>
			<td><label for="">Agendado para:</label></td>		
			<td><label><?= $nombre_del_agendado?></label></td>
		</tr>
		<tr>
			<td><label for="">Fecha:</label></td>		
			<td><label><?= $fecha ?></label></td>
		</tr>
		<tr>
			<td><label for="">Hora:</label></td>		
			<td><label><?= $hora ?></label></td>
		</tr>
	</table>
	
	<div class="padding">
		<hr></hr>
	</div>

	<?php if($tipo_asignacion === 'orientacion') { ?>
		<div id="formulario_orientacion">
			<table style="margin:auto;">
				<tr>
					<td>
						<label  for="rut_usuario">Reseña de la Orientación</label>  
					</td>
					<?php if ($resena === NULL) { ?>
						</tr>
						<tr>
							<td>
								<textarea id="resena" name="resena" type="text"></textarea>
							</td>
					<?php } else { ?>
						<td>
						<label id="resena"> <?= $resena ?> </label>
					</td>
					<?php } ?>
				</tr>
			</table>
		</div>
	<?php } else if ( $tipo_asignacion === 'asunto') { ?>
		<div id="formulario_asunto">
			<table style="margin:auto;">
				<tr>
					<td>
						<label  for="observaciones">Observaciones de la Reunión:</label>  
					</td>
					<?php if ($resena === NULL) { ?>
						</tr>
						
						<tr>
							<td>
								<textarea id="observaciones" name="observaciones" type="text"></textarea>
							</td>
					<?php } else { ?>
						<td>
							<label id="resena"> <?= $resena ?> </label>
						</td>
					<?php } ?>
				</tr>
			</table>
		</div>
	<?php } ?>

  
	<div class="botones" style="text-align:right; padding:0px 20px;">
		<button id="cancelar" name="cancelar"  onClick="$('#div_detalle_evento').closest('.ui-dialog-content').empty().remove();">Cancelar</button>
		<?php if($resena === NULL) { 
					if($tipo_asignacion === 'orientacion') { ?>
						<button id="guardar" name="guardar"  onClick="guardar_resena(<?= $id_tipo_evento ?>, false)">Guardar</button>
		<?php 	} else if ( $tipo_asignacion === 'asunto') { ?>
						<button id="guardar" name="guardar"  onClick="guardar_observaciones(<?= $id_tipo_evento ?>)">Guardar</button>
		<?php 		} 
					} ?>
	</div>
	
</div>

<script>
</script>
