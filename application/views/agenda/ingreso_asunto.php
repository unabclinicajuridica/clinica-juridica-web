<?php $agendar = isset($agendar) ? $agendar : 0; $anchura = isset($fondo_ancho) && $fondo_ancho ? 'wide' : ''; ?>
<div class="fondo_formularios <?=$anchura?>">
	<h2 style="text-align:center">Ficha de Ingreso de Asunto</h2>

	<form id="form_asunto" action="index.html" method="post">

		<?php
			$this->load->helper('configuracion');
			if(!($agendar)) { $fecha_asignacion = getFechaFormateada(); $hora_asignacion = date('H:i'); }
			$this->view('partial/partial_set_fecha_hora', ['fecha'=>$fecha_asignacion, 'hora'=>$hora_asignacion, 'fechas_futuras'=>FALSE, 'fecha_hora_editable'=>$fecha_editable]);
		?>

		<?php if($abogado_cambiable): ?>
			<div style="width:90%;margin:auto;">
				<?php $this->view('partial/partial_set_abogado', [ 'required'=>true ] ); ?>
			</div>
		<?php else: ?>
			<input type="hidden" id="input_rut_abogado" name="rut_abogado" value="<?=$rut_abogado?>">
		<?php endif; ?>

		<table>
			<tr><td><?php $this->view('partial/partial_selector_causa', ['opcional'=>TRUE]); ?></td></tr>
			<tr>
				<td style="display:flex;align-items:center;margin-bottom:10px;">
					<label  for="titulo_evento">Titulo:&nbsp;&nbsp;</label>
					<input id="titulo_evento" name="titulo_evento" type="text" style="flex:1;" required="required">
				</td>
			</tr>

			<tr>
				<td style="display:flex;flex-wrap:wrap;">
					<div style="flex-basis:100%;"><label  for="descripcion_evento">Tema del Asunto:&nbsp;&nbsp;</label>  </div>
					<textarea style="flex-basis:100%;" id="descripcion_evento" name="descripcion_evento" type="text" cols="45" rows="3" required="required"></textarea>
				</td>
			</tr>

			<tr>
				<td style="display:flex;flex-wrap:wrap;">
					<label style="flex-basis:100%;" for="observaciones">Observaciones de la Reunión:&nbsp;&nbsp;</label>
					<textarea style="flex-basis:100%;" id="observaciones" name="observaciones" type="text" cols="45" rows="5"></textarea>
				</td>
			</tr>
		</table>

		<div class="centro margin">
			<button type="submit" class="btn">Ingresar Asunto</button>
		</div>
	</form>
</div>
<script type="text/javascript">
	$( "#form_asunto" ).submit(function( event ) {
		var formData = $( this ).serializeArray();
		event.preventDefault();
		ingresar_asunto(formData, <?=$agendar?>);
	});
</script>
