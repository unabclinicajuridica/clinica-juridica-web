<div class="cabecera_tabla">
	<h2 class="titulo_divset">Causas Asociadas al Abogado</h2>
	<table style="width:100%; margin:-28px 0px 0px 4px;">
		<tr>
			<td><b>Nombre:</b> <?= $abogado->nombre?></td>
			<td><b>Rut:</b> <?= $abogado->rut."-".$abogado->dv?></td>
			<td><b>Cantidad de Causas:</b> <?= count($causas)?></td>
		</tr>
	</table>
</div>
			
<?php $this->view('partial/partial_lista_causas', array('lista_causas' => $causas, 'agendar_abogado_especifico' => true, 'funcion_de_agendar' => $funcion_de_agendar)) ?>
