<h2 class="titulo_fieldset">AGENDA SEMANAL</h2>
<hr>
<div class="">
	<label for="">Seleccion de profesor</label>
	<select id="profesor_seleccionado">
		<option value="">Todos</option>
		<?php foreach ($profesores as $prof):
					if($profesor_seleccionado == $prof->rut): ?>
		<option value="<?=$prof->rut?>" selected='true'><?= $prof->nombre.' '.$prof->cantidad ?></option>
		<?php 	else: ?>
		<option value="<?=$prof->rut?>"><?= $prof->nombre.' '.$prof->cantidad ?></option>
		<?php 	endif;
				endforeach; ?>
	</select>
	<label for="">Seleccion de semana</label>
	<input id="fecha_causa" name="fecha_causa" type="text" value="<?=$fecha_hoy?>"/>
	<button onClick="cargar_calendario_prof_dia();">Cargar</button>
</div>
<hr>

<div class="mycal" style="width:100%;"></div>
<hr>

<script>
	$( document ).ready(function() { //una vez que cargo la pag y los datasets, cargo el calendario
		$('.mycal').easycal({
		startDate: '<?=$fecha_hoy?>', // tambien puedo escribir la fecha como 31/10/2104, aca se selecciona la semana de la fecha
		timeFormat: 'HH:mm',
		columnDateFormat: 'dddd, DD MMM',
		minTime: '<?=$inicio?>', //cargo maxima fecha segun retorno
		maxTime: '<?=$termino?>', //cargo minima fecha segun retorno
		slotDuration: 30, // 30 diurno, 10 vespertino
		timeGranularity: 30, // 30 diurno, 10 vespertino

		dayClick: function(el, startTime) {
			swal('Permite agregar un evento el: '+ dayColumn+' a las: ' + startTime);
		},
		
		eventClick: function(eventId, hora, dia){
			if(eventId=="ND") {
				swal('No disponible para Asignar Causas');
			} else {
				var profesor_a_cargar = $( "#profesor_seleccionado option:selected" ).val();
				profesor_a_cargar = profesor_a_cargar == "" ? null : profesor_a_cargar;

				console.log("profesor_a_cargar: " + profesor_a_cargar+", dia: "+dia+", hora: "+hora);

				detalle_bloque(dia, hora, profesor_a_cargar);
			}
		},

		events : [
			//imprimo los dias con asignaciones
			<?php

				if(isset($asignaciones)){
					foreach($asignaciones as $asignacion) {
						$fecha_formateada  = date("d-m-Y", strtotime($asignacion['dia']));
						echo  "{id : '".$asignacion['id']."',";
						echo  "start : '".$fecha_formateada." ".$asignacion['hora_inicio']."',";
						echo  "end : '".$fecha_formateada." ".$asignacion['hora_fin']."',";
						echo  "textColor : '#000',";
						echo  "libres : '".$asignacion['cantidad_disponible']."',";
						if($asignacion['hay_rechazados']) {
							echo "campana: true,";
						}
						//TODO. CAMBIAR COLOR SEGUN ACEPTA EL CLIENTE O NO.
						echo  "backgroundColor : '".$asignacion['tipo']."'";
						echo  "},";
					}
				}

			?>
		],

		overlapColor : '#FF0',
		overlapTextColor : '#000',
		overlapTitle : 'Multiple'
	});
	});

</script>
