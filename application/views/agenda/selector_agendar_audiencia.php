<div class="cabecera_tabla">
	<h2 class="titulo_divset">Causas Asociadas al Abogado</h2>
	<table style="width:100%; margin:-28px 0px 0px 4px;">
		<tr>
			<td><b>Nombre:</b> <?= $abogado->nombre?></td>
			<td><b>Rut:</b> <?= $abogado->rut."-".$abogado->dv?></td>
			<td><b>Cantidad de Causas:</b> <?= count($causas)?></td>
		</tr>
	</table>
</div>

<?php 
	if(count($causas)) { 
		$this->view('partial/partial_lista_causas', array('lista_causas' => $causas, 'agendar_abogado_especifico' => true, 'funcion_de_agendar' => $funcion_de_agendar));
	} else {
?>
	<div class="area_gris">
		<label>Este Abogado no tiene ninguna Causa asignada.</label>
		<button class="button_formularios" onclick="dialog_nueva_causa('<?= $dia?>', '<?= $hora?>', <?= $abogado->rut?>);"><img src="../../assets/images/signo_mas.png" height="20px">&nbsp;Ingresar Causa</button>
	</div>
	<button id="reload_selector_agendar_audiencia" style="display:none;" onclick="<?= $funcion_js_reload_dialog?>"></button>
<?php } ?>


<div class="cabecera_tabla" style="margin-top:30px;">
	<h2 class="titulo_divset">Causas de otros Abogados</h2>
	<table style="width:100%; margin:-28px 0px 0px 4px;">
		<tr>
			<td><b>Cantidad de Causas:</b> <?= count($causas_otros_abogados)?></td>
		</tr>
	</table>
</div>
<?php $this->view('partial/partial_lista_causas', array('id' => 'table_selector_causa2', 'lista_causas' => $causas_otros_abogados, 'agendar_abogado_especifico' => true, 'funcion_de_agendar' => $funcion_de_agendar)) ?>
