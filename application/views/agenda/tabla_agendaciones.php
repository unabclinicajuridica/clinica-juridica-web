<!-- <h2 class="centro">Agendaciones para Causa <?=$id_correlativa_causa?></h2> -->
<div class="grande padding">Agendaciones de la Causa ID <?=$id_correlativa_causa?></div><hr></hr>
<script>
	// Dialogo.
	function cambio_estado_agendacion(id_evento, id_causa) {
		var dialog = $( "#dialog-cambiar-estado" ).dialog({
	 	 minHeight: 200,
	 	 width: 350,
	 	 modal: true,
	 	 buttons: {
	 		"Cambiar": function() {
				var select_val = $("#dialog-cambiar-estado select#estados").val();
				if (select_val != null) {
					dialog.dialog("close");
					var link = base_url + index_page + "/ingreso/update_estado_asignacion/?id_evento="+id_evento+"&estado="+select_val;
					$.ajax({url:link,success:function(result) {
						cerrar_dialog_conteniendo('table_agendaciones');
						trae_agendaciones(id_causa);
					}});
				} else { alert('Debe seleccionar un estado'); }
	 		},
	 		Cancel: function() {dialog.dialog("close");}
	 	 },
      close: function() {dialog.dialog('destroy'); $("#dialog-cambiar-estado select").selectmenu("destroy");}
	   });
		$("#dialog-cambiar-estado select").selectmenu();
	}
</script>

<table id="table_agendaciones" class="display cell-border">
	<thead>
		<tr>
			<th>Fecha</th>
			<th>Hora Inicio</th>
			<th>Hora Fin</th>
			<th>Estado</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($agendaciones as $evento) { ?>
			<tr>
				<td><?=date('d/m/Y', strtotime($evento->fecha_asignacion))?></td>
				<td><?=$evento->hora_inicio?></td>
				<td><?=$evento->hora_fin?></td>
				<td>
					<!-- <label class="icono_btn" onclick="cambio_estado_agendacion('<?=$evento->id?>','<?=$evento->id_causa?>');"><?=$evento->estado?></label> -->
					<label class="icono_btn" onclick="cambio_estado_agendacion('<?=$evento->id?>','<?=$evento->id_causa?>');"><?=$evento->estado?></label>
				</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
<script>
	$("#table_agendaciones").DataTable({
			"retrieve": true,
			"pagingType": "simple_numbers",
			dom: 'tr',
			paging: false
	});
</script>
<div id="dialog-cambiar-estado" title="Cambiar estado" class="oculto">
  <p class="centro validateTips">Seleccione el estado de la asignación</p>
  <div style="margin:auto;text-align:center;">
	  <select id="estados">
   	  <option disabled selected value="0">Seleccione un Estado</option>
   	  <option value="Agendado">Agendado</option>
   	  <option value="Ausente">Ausente</option>
   	  <option value="Realizado">Realizado</option>
     </select>
  </div>
</div>
