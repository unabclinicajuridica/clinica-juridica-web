<?php
$ancho = isset($ancho) ? $ancho : FALSE;
$fondo_ancho = $ancho ? 'wide' : '';
?>
<div class="fondo_formularios <?=$fondo_ancho?>">
  <h2 class="centro">Agendacion de Audiencia de Causa</h2>
	<h3 class="centro">ID <?= $id_correlativa_causa?></h3>

	<div id="form_agendar_audiencia">
		<input id="id_correlativa_causa" type="hidden" value="<?= $id_correlativa_causa?>">

		<table class="tabla_geno" style="margin:auto;">
			<tr>
				<td><label for="nombre_asunto">Título:</label></td>
				<td><input id="nombre_asunto" name="nombre_asunto" type="text" ></td>
			</tr>
			<tr>
				<td> <label for="descripcion_asunto">Descripción breve:</label> </td>
				<td><textarea id="descripcion_asunto" name="descripcion_asunto" type="text"></textarea> </td>
			</tr>
			<tr>
				<td><label for="list_abogado">Abogado:</label></td>
				<td>
					<select id="rut_del_agendado" name="rut_del_agendado" disabled>
						<option value='<?= $rut_del_agendado ?>'> <?= $nombre_del_agendado ?></option>
					</select>
				</td>
			</tr>
			<tr>
				<td><label for="fecha_ingreso">Fecha:</label></td>
				<td>
					<?php if($dia): ?>
						<input id="fecha_ingreso" type="text" value="<?=$dia?>" disabled>
					<?php else: ?>
						<input id="fecha_ingreso" type="text">
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td><label for="set_hora">Hora:</label></td>
				<td>
					<?php if($hora): ?>
						<input id="set_hora"  type="text" value="<?=$hora?>" disabled>
					<?php endif; ?>
				</td>
			</tr>
		</table>

	  <div class="centro padding">
			<button id="nuevo_asunto" name="nuevo_asunto" onClick="agendar_audiencia();">Agendar</button>
	  </div>
	</div>
</div>
