<link rel="icon" href="<?= base_url()?>/favicon.png" type="image/jpg">
<title><?= $titulo?></title>
<?php foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<style media="screen">
	.contenido{ width:70%; margin: 0 auto; }
	@media only screen and (max-width: 1800px) { .contenido { width: 75%; } }
	@media only screen and (max-width: 1600px) { .contenido { width: 85%; } }
	@media only screen and (max-width: 1024px) { .contenido { width: auto; } }
</style>
<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<div class="contenido">
	<h2 style="text-align:center"><?= $titulo?></h2>
	<div style='height:20px;'></div>
	<div>
		<?= $output->output?>
    </div>
</div>
<script>
	var base_url = '<?= base_url() ?>';
	var index_page = '<?= index_page() ?>';
	// Muy Importante para terminar sesion automaticamente.
	var _timeoutSesion = <?= getTimeoutDeSesion($this) ?>;
</script>
<script src="<?= base_url()?>assets/js/timeout_sesion.js"></script>
