
<div class="fondo_formularios">
  <h2 style="text-align:center">Cambio de Correo</h2>


  <form id="form_correo" class="" action="" method="post">
     <table style="margin:auto;">
     <tr >
        <td colspan="2">
            <p>
               Su correo actual es <code style="font-size:1.2em;">&nbsp;<span id="span_correo"><?php echo $email; ?></span></code>
            </p>
        </td>
     </tr>
     <tr >
        <td>
            <label  for="fv_nuemail">Nuevo correo: </label>
        </td>
        <td >
            <input id="fv_nuemail" name="nuevo_email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" required title="Debe ser un correo válido">
        </td>
     </tr>
     </table>

     <div style="text-align:center;">
     <br><br>
   <button type="submit">Actualizar</button>
     </div>
  </form>

  <script type="text/javascript">
     $('#form_correo').submit(function(e) {
        e.preventDefault();
        $('#dialog').dialog('close');

        var nuevo_email = $('#fv_nuemail').val();
        console.log("nuevo_email = " + nuevo_email);
        // var ruta = "../ingreso/cambiar_correo/?email="+nuevo_email;
        $.ajax({
            type: "POST",
            url: "../ingreso/cambiar_correo",
            data: {email: nuevo_email},
            dataType: "text",
            success: function (data) {
              // fix parseo de json
              data = data.replace(/\s/g, '');
              datajson = JSON.parse(data);
              console.log("respuesta = "+ data);

              var result = datajson.result;
              if(result === "ok") {
              	sweetAlert('Éxito!',   'Actualización Exitosa',   'success' );
               $('#span_correo').html(nuevo_email);

              }
              else if(result === "error") {
              	sweetAlert('Oops...', '¡Error desconocido!', 'error');
              }
              else if(result === "vacio") {
              	sweetAlert('Oops...', 'Debe rellenar el campo', 'warning');
              }
              else {
              	sweetAlert('???!',   'No sabemos qué ha ocurrido',   'warning' );
              }
            }
        });
     });
  </script>
</div>
