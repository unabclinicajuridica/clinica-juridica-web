
<div class="fondo_formularios">
  <h2 style="text-align:center">Cambio de Contraseña</h2>

   <form id="form_password" class="" action="" method="post">
     <table style="margin:auto;">
   	<tr >
   		<td>
   			<label  for="fv_nupass">Nueva contraseña: </label>
   		</td>
   		<td >
   			<input id="fv_nupass" name="nuevo_pass" type="password" required>
   		</td>

   	</tr>
   	<tr >
   		<td>
   			<label  for="fv_reppass">Repetir contraseña: </label>
   		</td>
   		<td >
   			<input id="fv_reppass" name="rep_pass" type="password" required>
   		</td>

   	</tr>
     </table>

     <div style="text-align:center;">
     <br><br>
   	<button type="submit">Cambiar</button>
      </div>
   </form>

   <script type="text/javascript">
      $('#form_password').submit(function(e) {
         e.preventDefault();
         $('#dialog').dialog('close');



         var nuevo_pass = $('#fv_nupass').val();
         var rep_pass = $('#fv_reppass').val();

         if(nuevo_pass !== rep_pass) {
            sweetAlert('Contraseñas Diferentes', 'Ambos campos deben tener el mismo valor', 'warning');
            return;
         }

         $.ajax({
             type: "POST",
             url: "../ingreso/cambiar_contrasena",
             data: {nuevo_password: nuevo_pass},
             dataType: "text",
             success: function (data) {
               // fix parseo de json
               data = data.replace(/\s/g, '');
               datajson = JSON.parse(data);
               console.log("respuesta = "+ data);

               var result = datajson.result;
               if(result === "ok") {
               	sweetAlert('Éxito!',   'Cambio de contraseña Exitoso',   'success' );
               }
               else if(result === "error") {
               	sweetAlert('Oops...', '¡Error desconocido!', 'error');
               }
               else if(result === "vacio") {
               	sweetAlert('Oops...', 'Debe rellenar los campos', 'warning');
               }
               else {
               	sweetAlert('???!',   'No sabemos qué ha ocurrido',   'warning' );
               }
             }
         });
      });
   </script>
</div>
