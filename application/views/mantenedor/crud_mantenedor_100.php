<link rel="icon" href="<?= base_url()?>/favicon.png" type="image/jpg">
<title><?= $titulo?></title>
<?php foreach($output->css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($output->js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<div style="margin:auto; width:100%;">
	<h2 style="text-align:center"><?= $titulo?></h2>
	<div style='height:20px;'></div>
	<div>
		<?= $output->output?>
    </div>
</div>
<script>
	var base_url = '<?= base_url() ?>';
	var index_page = '<?= index_page() ?>';
	// Muy Importante para terminar sesion automaticamente.
	var _timeoutSesion = <?= getTimeoutDeSesion($this) ?>;
</script>
<script src="<?= base_url()?>assets/js/timeout_sesion.js"></script>
