﻿<div style="margin: 50px 0px auto auto;padding:10px 10px 20px 10px;">
	<table id="table_disponibilidad_profesor" class="display cell-border" width="90%" style="width:90%">
		 <thead>
			<tr>
				<td>Días</td>
				<td>Hora Inicio</td>
				<td>Hora Fin</td>
				<td>Sede</td>
				<td>Fecha Inicio</td>
				<td>Fecha Fin</td>
				<td></td>
			</tr>
		</thead>
		<?php foreach ($disponibilidad as $conf_a): ?>
			<tbody>
				<tr>
					<td class="centro"><?= ucfirst($conf_a->dia)?></td>

					<td><?= substr($conf_a->hora_inicio,0,5)?></td>
					<td><?= substr($conf_a->hora_fin,0,5)?></td>
					<td class="centro"><?= $conf_a->nombre_sede_corto?></td>
					<td><?= $conf_a->fecha_inicio?></td>
					<td><?= $conf_a->fecha_fin?></td>
					<td><button class="icono_btn" style="padding:3px 2px 3px 0px;" onclick="elim_bloq_horario_abog(<?= $conf_a->id_conf_agenda?>);"><span class="ui-icon ui-icon-closethick"></span>eliminar</button></td>
				</tr>
			</tbody>
		<?php endforeach; ?>
	</table>
</div>

<style type="text/css">
	#table_disponibilidad_profesor thead td {
		font-weight: bold;
	}
</style>
<script type="text/javascript">
$('#table_disponibilidad_profesor').DataTable({
		autoWidth: false,
		retrieve: true,
		ordering: false,
		dom: 'tr'
});
</script>
