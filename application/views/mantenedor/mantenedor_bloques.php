﻿<!-- timepicker -->
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.css">
<script type="text/javascript" src="<?= base_url(); ?>/assets/js/jonthornton.timepicker/jquery.timepicker.min.js" ></script>
<script type="text/javascript" src="<?= base_url(); ?>/assets/js/jonthornton.datepair/jquery.datepair.min.js" ></script>

<div class="" style="margin:auto;display:flex;justify-content:space-around;">
	<div class="fondo_formularios" >
	  <h2 style="text-align:center">Gestión de Horarios para Abogados de <?= $nombre_sede_corto ?></h2>

	  <table id="table_mantenedor_bloques" style="margin:40px auto 0px auto;">
		<tr>
			<td><label  for="tipo_audiencia">Rut Profesor (sin dv): </label></td>

			<td>
				<input id="rut" name="rut" type="text" >&nbsp;
				<button class="button_icono" onclick="buscar_abogado('rut', 'nombre_profesor', 'mostrar_bloques_abogado');">
					<img style='cursor:pointer;'  src='../../assets/images/lupa.png' height='15px'>
				</button>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<div style="display:block;margin-left:10px;margin-bottom:10px;">
					<label><em><span id="nombre_profesor"></span></em></label>
				</div>
			</td>
		</tr>
		<tr >
			<td>
				<label  for="tipo_audiencia">Dia Inicio: </label>
			</td>
			<td >
				<input id="fecha_inicio" name="fecha_inicio" type="text" >
			</td>

		</tr>
		<tr >
			<td>
				<label  for="tipo_audiencia">Dia Término: </label>
			</td>
			<td >
				<input id="fecha_termino" name="fecha_termino" type="text" >
			</td>

		</tr>

		<tr>
			<td>
				<label  for="tipo_audiencia">Hora Inicio: </label>
			</td>
			<td >
				<input class="time start" id="hora_inicio" name="hora_inicio" type="time" step="1800" >
			</td>

		</tr>
		<tr >
			<td>
				<label  for="tipo_audiencia">Hora Término: </label>
			</td>
			<td >
				<input class="time end" id="hora_fin" name="hora_fin" type="time" step="1800"  >
			</td>

		</tr>
		<tr >
			<td>
			<label  for="tipo_audiencia">Día: </label>
			</td>
			<td >
				<select id="dia" >
				  <option value="lunes">Lunes</option>
				  <option value="martes">Martes</option>
				  <option value="miercoles">Miercoles</option>
				  <option value="jueves">Jueves</option>
				  <option value="viernes">Viernes</option>
				</select>
			</td>

		</tr>



	  </table>

	  <div style="text-align:center;height:100px;display:flex;align-items:center;justify-content:center;">
			<button onclick="ingresar_bloque_profesor();" >Ingresar Bloque</button>
	  </div>

	</div>
	<div class="fondo_formularios" style="margin-left:20px">
		<h2 style="text-align:center">Horario</h2>
		<div id="disponibilidad_profesor"><!-- SE CARGA TABLA AQUÍ --></div>
	</div>
</div>
<script>
	//TODO. DESCUBRIR DONDE SE ESTÁ INICIALIZANDO LOS DATEPICKER.
	//TODO. UTILIZAR BOOTSTRAP DATEPICKER QUE ESTA DENTRO DE JONTHORNTON TIMEPICKER.
	$('#table_mantenedor_bloques input.time').timepicker( { 'minTime': '08:00', 'maxTime': '22:00', 'timeFormat': 'H:i'} );
	var basicExampleEl = document.getElementById('table_mantenedor_bloques');
	var datepair = new Datepair(basicExampleEl);

	ponerDatepicker($result, "#fecha_inicio");
	ponerDatepicker($result, "#fecha_termino");
</script>
