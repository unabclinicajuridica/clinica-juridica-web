<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	function getNombreIconoPorExtension($extension) {
		switch($extension) {
			case '.jpg':
			case '.jpgeg':
				return 'ext_jpg.png';
			case '.png':
				return 'ext_png.png';
			case '.doc':
			case '.docx':
				return 'ext_doc.png';
			case '.xls':
			case '.xlsx':
				return 'ext_xls.png';
			case '.ppt':
			case '.pptx':
				return 'ext_ppt.png';
			case '.pdf':
				return 'ext_pdf.png';
			case '.zip':
			case '.7zip':
			case '.rar':
				return 'ext_zip.png';
			case '.avi':
			case '.mov':
			case '.3gp':
			case '.mp4':
				return 'ext_video.png';
			case '.ogg':
			case '.aac':
			case '.wma':
			case '.mp3':
			case '.flac':
			case '.wav':
				return 'ext_audio.png';
			default:
				return 'ext_txt.png';
		}
	}

	function getRutaIcono($tag) {
			$ruta_imagenes = base_url()."assets/images/";
			$ruta_iconos = base_url()."assets/icons/";

			$ruta = $ruta_imagenes;

		switch($tag) {
			case 'detalles':
				$ruta .= 'eye_open.svg';
				break;
			case 'terminar':
			case 'terminar_causa':
				$ruta = $ruta_iconos.'martillo_corte.png';
				break;
			case 'editar':
				$ruta .= 'edit2.png';
				break;
			case 'evaluar':
				$ruta .= 'evaluar.png';
				break;
			case 'info':
				$ruta .= 'info.png';
				break;
			case 'imprimir':
				$ruta .= 'icono_imprimir.png';
				break;
			case 'agenda':
			case 'agendar':
				$ruta .= 'cal.png';
				break;
			case 'rellenar':
				$ruta .= 'rellenar.png';
				break;
			case 'nuevo':
			case 'add':
				$ruta .= 'signo_mas.png';
				break;
			case 'cerrar':
				$ruta .= 'equis.png';
				break;
			case 'eliminar':
			case 'borrar':
				$ruta = $ruta_iconos.'delete.png';
				break;
			case 'aceptar':
				$ruta = $ruta_iconos.'accept.png';
				break;
			case 'detener':
				$ruta = $ruta_iconos.'stop.png';
				break;
			case 'editar2':
				$ruta = $ruta_iconos.'pencil.png';
				break;
			case 'vincular_causa_orientacion':
				$ruta = $ruta_iconos.'add-document.png';
				break;
			case 'buscar_abogado':
			case 'buscar':
				$ruta .= 'lupa.png';
				break;
			case 'audiencia':
				$ruta = $ruta_iconos.'audiencia.png';
				break;
			case 'orientacion':
				$ruta = $ruta_iconos.'reunion.png';
				break;
			case 'buscar_cliente':
				$ruta .= 'lupa_roja.png';
				break;
			case 'tramite':
				$ruta .= 'tramite2.png';
				break;
		}
		return $ruta;
	}

	function string2Boolean($str) {
		$boolean = null;
		switch($str) {
			case 'si':
			case 'true':
				$boolean = true;
				break;
			case NULL:
			case '':
			case 'no':
			case 'false':
				$boolean = false;
				break;
			default:
				$boolean = false;
				break;
		}
		return $boolean;
	}
?>
