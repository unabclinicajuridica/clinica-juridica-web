<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	$service = NULL;

	function getGoogleCalendarService() {
		if(!(defined('APPLICATION_NAME'))){
			define('APPLICATION_NAME', 'Google Calendar API PHP Quickstart');
		}

		// define('CLIENT_SECRET_PATH', $CI->my_googlecalendarapi->PathClientSecret());
		$CI =& get_instance();

		if(!(defined('CLIENT_SECRET'))){
		define('CLIENT_SECRET', $CI->my_googlecalendarapi->getClientSecretFromDatabase());
		}
		if(!(defined('CREDENTIALS'))){
			define('CREDENTIALS', $CI->my_googlecalendarapi->getCredencialesSecretFromDatabase());
		}
		if(!(defined('SCOPES'))){
			define('SCOPES', implode(' ', array( Google_Service_Calendar::CALENDAR) ));
		}
	}


	function getServicio() {
		getGoogleCalendarService();
		$client = new Google_Client();
		$client->setApplicationName(APPLICATION_NAME);
		$client->setScopes(SCOPES);
		$client->setAuthConfig(CLIENT_SECRET);
		$client->setAccessType('offline');


		$accessToken = json_decode(CREDENTIALS, true);

		$client->setAccessToken($accessToken);

		// Refresh the token if it's expired.
		// url: https://github.com/google/google-api-php-client/issues/263#issuecomment-186557360
		if ($client->isAccessTokenExpired()) {
			$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			$accessToken["access_token"] = $client->getAccessToken()["access_token"];

			$CI =& get_instance();
			$CI->my_googlecalendarapi->actualizarCredenciales(json_encode($accessToken));
		}
		return new Google_Service_Calendar($client);
	}


	function enviarEventoPorCorreo($titulo, $descripcion, $timestamp_inicio, $timestamp_termino, $correoUsuario) {
		if(!(isset($service))) {
			$service = getServicio();
		}
		if(!($correoUsuario)) {
			return null;
		}

      $nuevoEvento = new Google_Service_Calendar_Event(array(
         'summary' => 'Clínica: '.$titulo,
         'location' => 'Campus UNAB Viña 13 Norte',
         'description' => $descripcion,
         'start' => array(
            'dateTime' => date("Y-m-d\TH:i:s", $timestamp_inicio),
            'timeZone' => 'America/Santiago',
         ),
         'end' => array(
            'dateTime' => date("Y-m-d\TH:i:s", $timestamp_termino),
            'timeZone' => 'America/Santiago',
         ),
         'attendees' => array(
				array('email' => $correoUsuario)
         ),
         'reminders' => array(
            'useDefault' => FALSE,
            'overrides' => array(
               array('method' => 'email', 'minutes' => 24 * 60),
               array('method' => 'popup', 'minutes' => 10),
            ),
         ),
      ));

      $calendarId = 'primary';
      $parametrosOpcionales = array('sendNotifications' => true);
      $eventoRespuesta = $service->events->insert($calendarId, $nuevoEvento, $parametrosOpcionales);

      $icaluid_evento = $eventoRespuesta->iCalUID;
      $icalid_evento = $eventoRespuesta->id;

      return array("icaluid" => $icaluid_evento, "id" => $icalid_evento);
   }


	function cancelarEvento($iCalUID_tipo_evento) {
		if(!(isset($service))) {
			$service = getServicio();
		}

		$calendarId = 'primary';

		// Para que todos sepan que ya no aplica el evento.
		$parametrosOpcionales = array('sendNotifications' => true);
		$eventoRespuesta = $service->events->delete($calendarId, $iCalUID_tipo_evento, $parametrosOpcionales);


		if($eventoRespuesta !== NULL) {
			return true;
		}
		return false;
	}


	function reenviarUpdateEvento($icalid_evento, $mensaje_reenvio) {
		$calendarId = 'primary';
		$parametrosOpcionales = array('sendNotifications' => true);

		if(!(isset($service))) {
			$service = getServicio();
		}
		$event = $service->events->get($calendarId, $icalid_evento);

		$event->setSummary('Reenvío '.$event->getSummary());
		$event->setDescription('MENSAJE DE REENVÍO: '.$mensaje_reenvio."\nMENSAJE ORIGINAL: ".$event->getDescription());

		$eventoRespuesta = $service->events->update($calendarId, $event->getId(), $event, $parametrosOpcionales);

		if(!(isset($eventoRespuesta))) {
			return false;
		}
		return true;
	}


	function enviarCorreoNormal($titulo, $descripcion_asunto, $emailCliente) {
		$mail = new PHPMailer(); // create a new object
		$mail->CharSet = "UTF-8";

		$mail->IsSMTP(); 			// enable SMTP
		$mail->SMTPDebug = 0; 		// debugging: 1 = errors and messages, 2 = messages only
		$mail->SMTPAuth = true; 	// authentication enabled
		$mail->SMTPSecure = 'ssl'; 	// secure transfer enabled REQUIRED for Gmail
		//~ $mail->Host = "smtp.gmail.com";
		$mail->Host = "ssl://smtp.gmail.com";
		$mail->Port = 465; 			// or 587
		$mail->IsHTML(FALSE);
		$mail->setFrom('clinicajuridica.prueba@gmail.com', 'Clinica Juridica UNAB');
        $mail->addReplyTo('clinicajuridica.prueba@gmail.com', 'Clinica Juridica UNAB');

		$mail->Username = "clinicajuridica.prueba@gmail.com";
		$mail->Password = "clinica123";
		$mail->Subject = "Clínica Jurídica: ".$titulo;
		$mail->Body = $descripcion_asunto;

		$mail->AddAddress($emailCliente);

		$bool_exito = $mail->Send();
		//~ if(!$bool_exito) {
			//~ echo "Mailer Error: " . $mail->ErrorInfo;
		//~ }
		//~ else {
			//~ echo "Message has been sent";
		//~ }
		return $bool_exito;
		//TODO. revisar si hay qyue cerrar el objeto phpmailer o no.
	}

?>
