<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
   function esta_logueado($contexto) {
      if ($contexto->session->login_user == null) {
         return false;
      }
      else {
         return true;
      }
   }

   // Para utilizar el Grocery Crud
	function esta_autorizado_mantenedor($contexto) {
		return $contexto->session->rol->mantencion_todo ? true : false;
	}

	// EN SEGUNDOS.
	function getTimeoutDeSesion($contexto) {
		$timeout = $contexto->config->item('sess_expiration');
		return $timeout;
	}
?>
