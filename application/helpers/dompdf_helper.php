<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;

// url: https://github.com/bcit-ci/CodeIgniter/wiki/PDF-generation-using-dompdf
function pdf_create($html, $filename='ficha', $stream=TRUE) {
	//~ require_once("dompdf/dompdf_config.inc.php");
	require_once ('dompdf/autoload.inc.php');

	$options = new Options();
	$options->set('isRemoteEnabled', true);
	$dompdf = new Dompdf($options);

	$dompdf->setPaper('folio', 'portrait');

	$dompdf->loadHtml($html);
	$dompdf->render();
	if ($stream) {
		$dompdf->stream($filename);
	} else {
		return $dompdf->output();
	}
}

?>
