<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	function estadoRevisionTramiteEsValido($estado) {
		return in_array($estado, ['PENDIENTE','REVISADO','LEIDO','CERRADO']);
	}

	function estadoRevisionAudienciaEsValido($estado) {
		return in_array($estado, ['EN_ESPERA_LLENADO','PENDIENTE','EVALUADA','LEIDA','CERRADA']);
	}


	// Decodifica el hash usando el texto. Si la decodificacion es correcta,
	// entrega TRUE, si no FALSE;
	function contrasenaCorrecta($hash_contrasena, $texto) {
		$coincide = FALSE;

		// Verify stored hash against plain-text password
		if (password_verify($texto, $hash_contrasena)) {
			// Check if a newer hashing algorithm is available
			// or the cost has changed
			if (password_needs_rehash($hash_contrasena, PASSWORD_BCRYPT, array("cost" => 8))) {
				// If so, create a new hash, and replace the old one
				//TODO. Insertar nuevo hash en BD.
				$newHash = password_hash($texto, PASSWORD_BCRYPT, array("cost" => 8));
			}
			$coincide = TRUE;
		}
		return $coincide;
	}

	// Encripta texto con bcrypt y devuelve el hash de encripción.
	function contrasenaEncriptar($texto) {
		$hash_texto = NULL;
		$hash_texto = password_hash($texto, PASSWORD_BCRYPT, array("cost" => 8));
		return $hash_texto;
	}

	// Entrega columna de Rut de Tabla Causas dependiendo del tipo de usuario.
	function getColumnaDeCausa($tipo_usuario) {
		$nombre_columna = NULL;
		switch($tipo_usuario) {
			case 'director':
			case 'profesor':
			case 'abogado':
				$nombre_columna = "RUT_ABOGADO";
			break;
			case 'alumno':
				$nombre_columna = "RUT_ALUMNO";
			break;
		}
		return $nombre_columna;
	}

	function getFormatoFecha($tag) {
		switch ($tag) {
			case 'php': return getFormatoFechaPHP(); break;
			case 'mysql': return getFormatoFechaMySQL(); break;
		}
	}

	function getFormatoFechaPHP() {
		return 'd-m-Y';
	}

	function getFormatoFechaMySQL() {
		return '%d-%m-%Y';
	}

	function getFechaFormateada() {
		return date(getFormatoFechaPHP());
	}
?>
