<?php
   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   class My_googlecalendarapi {
       //~ public function My_googlecalendarapi() {
           //~ require_once('google-api-php-client-2.1.0/vendor/autoload.php');
       //~ }
			 
			function __construct() {
           require_once('google-api-php-client-2.1.0/vendor/autoload.php');
       }

       public function PathCredenciales() {
          return __DIR__.'/google-api-php-client-2.1.0/credenciales/calendar-php-quickstart.json';
       }

       public function PathClientSecret() {
          return __DIR__ . '/google-api-php-client-2.1.0/client_secret.json';
       }

       public function getClientSecretFromDatabase() {
          $CI =& get_instance(); // url: http://stackoverflow.com/a/6327423
          $CI->load->model('modelo_googlecalendarapi');
          $clienteSecretJsonString = $CI->modelo_googlecalendarapi->getClienteSecret();
          return $clienteSecretJsonString;
       }

       public function getCredencialesSecretFromDatabase() {
          $CI =& get_instance();
          $CI->load->model('modelo_googlecalendarapi');
          $tokensJsonString = $CI->modelo_googlecalendarapi->getTokens();
          return $tokensJsonString;
       }

       public function actualizarCredenciales($nuevasCredenciales) {
          $CI =& get_instance();
          $CI->load->model('modelo_googlecalendarapi');
          $tokensJsonString = $CI->modelo_googlecalendarapi->updateTokens($nuevasCredenciales);
       }

   }
 ?>
