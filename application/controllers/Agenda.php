<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

	function __construct() {
		parent::__construct();

		// Por conveniencia, siempre se carga este modelo
		$this->load->model('modelo_busquedas');
		$this->load->model('modelo_agenda');
	}

	// Vista
	// Devuelve vista con detalles de la asignacion de agenda.
	public function vista_detalle_evento($id_evento) {
		$this->load->model('modelo_usuario');

		$agendacion = $this->modelo_agenda->getAsignacionAgendaPorId($id_evento);
		$usuario = $this->modelo_usuario->getPorRut($agendacion->rut);
		$tipo_asignacion = $agendacion->tipo_asunto;

		$var['sede_actual'] = $this->session->id_sede_actual;
		$var['nombre_del_agendado'] = $usuario->nombre;
		$var['rut_del_agendado'] = $usuario->rut;
		$var['fecha'] = $agendacion->fecha_asignacion;
		$var['hora'] = $agendacion->hora_inicio;


		//valores iniciales
		$var['titulo'] = NULL;
		$var['descripcion'] = NULL;
		$var['resena'] = NULL;

		switch($tipo_asignacion) {
			case 'asunto':
				$var['titulo_vista'] = 'Detalles del Asunto';
				$var['id_tipo_evento'] = $agendacion->id_asunto;
				$asunto = $this->modelo_agenda->getAsuntoPorId($agendacion->id_asunto);
				$var['titulo'] = $asunto->titulo_asunto;
				$var['descripcion'] = $asunto->descripcion;
				$var['tipo_asignacion'] = 'asunto';
				$var['resena'] = $asunto->observaciones;

				break;
			case 'orientacion':
				$var['titulo_vista'] = 'Detalles de la Orientación';
				$var['id_tipo_evento'] = $agendacion->id_orientacion;
				$orient = $this->modelo_agenda->getOrientacionPorIdCompleto($agendacion->id_orientacion);
				$var['titulo'] = $agendacion->titulo_evento;
				$var['descripcion'] = $agendacion->descripcion_evento;
				$var['tipo_asignacion'] = 'orientacion';
				$var['resena'] = $orient->resena;
				break;
			case 'causa':
				$var['id_tipo_evento'] = $agendacion->id_causa;
				$var['tipo_asignacion'] = 'causa';
				$var['titulo_vista'] = 'Detalles de la agendación de Causa';
				break;
			case 'audiencia':
				$var['id_tipo_evento'] = $agendacion->id_audiencia;
				$var['titulo'] = $agendacion->titulo_evento;
				$var['descripcion'] = $agendacion->descripcion_evento;
				$var['tipo_asignacion'] = 'audiencia';
				$var['titulo_vista'] = 'Detalles de la Audiencia';
				break;
		}

		$this->load->view('agenda/geno_detalle_evento', $var);
	}

	// Vista html para ingresar y luego agendar.
	public function vista_nueva_agendacion() {
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_ingresos');

		$tipo_agendacion = $this->input->get('tipo_agendacion');

		$var['dia'] =  $this->input->get('dia');
		$var['hora'] =  $this->input->get('hora');
		$var['sede_actual'] = $this->session->id_sede_actual;

		$var['rut_del_agendado'] = NULL;

		if($this->input->get('profesor_a_cargar') !== null) {
			$this->load->model('modelo_usuario');
			$var['rut_del_agendado'] = $this->input->get('profesor_a_cargar');

			$profe_row = $this->modelo_usuario->getPorRut($var['rut_del_agendado']);
			$var['nombre_del_agendado'] = $profe_row->nombre;
		}

		switch($tipo_agendacion) {
			case 'Au':
			$var['id_correlativa_causa'] = $this->input->get('id_correlativa_causa');
			$var['ancho'] = true;
			$this->load->view('agenda/agendar_audiencia', $var);
			break;
		}
	}

	// Vista
	public function vista_asunto() {
		$var['fecha_asignacion'] =  $this->input->get('fecha');
		$var['hora_asignacion'] =  $this->input->get('hora_inicio');
		$var['rut_abogado'] =  $this->input->get('rut_abogado');
		$var['agendar'] = 1;
		$var['abogado_cambiable'] = false;
		$var['fecha_editable'] = false;
		$var['fondo_ancho'] = true;
		$this->load->view('agenda/ingreso_asunto', $var);
	}

	public function bloque() {
		$var['dia'] =  $this->input->get('dia');
		$var['hora'] =  $this->input->get('hora');
		$var['profesor_a_cargar'] =  $this->input->get('profesor_a_cargar');

		$this->load->view('agenda/bloque', $var);
	}

	// Devuelve vista con lista de causas
	public function detalle_agendar() {
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_causa');
		$this->load->model('modelo_usuario');

		$dia = $this->input->get('dia');
		$hora =  $this->input->get('hora');

		$var['rut_abogado_a_cargar'] = $rut_abogado_a_cargar = $this->input->get('profesor_a_cargar');

		$var['dia'] =  $this->input->get('dia');
		$var['hora'] =  $this->input->get('hora');
		$tipo = $this->input->get('tipo');

		$var['abogado'] = $this->modelo_usuario->getPorRut($rut_abogado_a_cargar);
		$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($rut_abogado_a_cargar, 'abogado', true, true);
		$var['funcion_js_reload_dialog'] = $this->input->get('funcion_str');
		$var['funcion_de_agendar'] = "alert";
		if ($tipo === 'Au') {
			$var['funcion_de_agendar'] = "dialog_agendar_audiencia";
			$var['causas_otros_abogados'] = $this->modelo_causa->getCompletoExcepto($rut_abogado_a_cargar, 'abogado', true, true);
			$this->load->view('agenda/selector_agendar_audiencia', $var);
		}
		else if($tipo === 'C') {
			$var['funcion_de_agendar'] = "agendar_causa";
			$this->load->view('agenda/selector_agendar_causa', $var); //arreglar aca que traiga el form antiguo
		}
	}

	public function detalle_bloque() {
		$dia_texto = "";
		$this->load->model('modelo_agenda');
		$profesor_a_cargar = $this->input->get('profesor_a_cargar');
		$dia = $this->input->get('dia');
		$hora_inicio =  $this->input->get('hora');

		if(date("w",strtotime($dia))==0){
			$dia_texto = "DOMINGO";
		}else if(date("w",strtotime($dia))==1){
			$dia_texto = "LUNES";
		}else if(date("w",strtotime($dia))==2){
			$dia_texto = "MARTES";
		}else if(date("w",strtotime($dia))==3){
			$dia_texto = "MIERCOLES";
		}else if(date("w",strtotime($dia))==4){
			$dia_texto = "JUEVES";
		}else if(date("w",strtotime($dia))==5){
			$dia_texto = "VIERNES";
		}else if(date("w",strtotime($dia))==6){
			$dia_texto = "SABADO";
		}

		// REGLA DE NEGOCIO. Bloques duran 30 minutos.
		$hora_termino = date('H:i:s', strtotime('+30 minutes', strtotime($hora_inicio)));

		if($profesor_a_cargar == NULL) {
			$asignaciones = $this->modelo_busquedas->getAsignacionesPorBloque($dia, $hora_inicio);
		} else {
			$asignaciones = $this->modelo_busquedas->getAsignacionesPorBloqueYPorRut($dia, $hora_inicio, $profesor_a_cargar);
		}

		$var['dia'] = $dia;
		$var['hora'] = $hora_inicio;
		$var['dia_texto'] = $dia_texto;
		$var['profesores_disponibles'] = $this->modelo_agenda->getProfesoresDisponiblesEnBloque($dia, $hora_inicio, $hora_termino, $dia_texto);
		$var['asignaciones'] = $asignaciones;

		$this->load->view('agenda/detalle_bloque', $var);
	}

	public function semanal() {
		$this->load->helper('configuracion');
		// 'hoy' en realidad NO ES HOY.
		$hoy_string = $this->input->get('hoy') ? $this->input->get('hoy') : date(getFormatoFechaPHP());
		$profesor = $this->input->get('profesor');

		$hoy = DateTime::createFromFormat(getFormatoFechaPHP(), $hoy_string)->format('d-m-Y');

		$hoy_timestamp = strtotime($hoy);
		if(date("w",strtotime($hoy)) == 0) {
			$domingo = date('d-m-Y', strtotime($hoy));
		}else{
			$domingo = date('d-m-Y', strtotime('Next Sunday', $hoy_timestamp));
		}
		$lunes = date('d-m-Y', strtotime($domingo .' -6 day'));
		$martes = date('d-m-Y', strtotime($domingo .' -5 day'));
		$miercoles = date('d-m-Y', strtotime($domingo .' -4 day'));
		$jueves = date('d-m-Y', strtotime($domingo .' -3 day'));
		$viernes = date('d-m-Y', strtotime($domingo .' -2 day'));
		$sabado = date('d-m-Y', strtotime($domingo .' -1 day'));
		$var['profesor_seleccionado'] =  $profesor;
		$var['fecha_hoy'] = $hoy_string;
		$var['sabado']=$sabado;
		$var['domingo']=$domingo;
		$this->load->model('modelo_agenda');
		$var['termino'] = $this->modelo_agenda->traer_termino_atencion();
		$var['inicio'] = $this->modelo_agenda->traer_inicio_atencion();
		$var['profesores'] = $this->modelo_agenda->cantidad_asignaciones($lunes, $viernes);
		$inicio_tiempo = $var['inicio'];
		$termino_tiempo = $var['termino'];

		$inicio_tiempo = $inicio_tiempo;
		$inicio_tiempo = date("H:i:s",strtotime($inicio_tiempo));

		$termino_tiempo = $termino_tiempo;
		$termino_tiempo = date("H:i:s",strtotime($termino_tiempo));
		$termino_tiempo_original = $termino_tiempo;

		while($inicio_tiempo < $termino_tiempo_original ){
			$termino_tiempo = date('H:i:s', strtotime("$inicio_tiempo + 30 minutes") );
			$var['asignaciones'][] = $this->modelo_agenda->asignaciones($lunes,$inicio_tiempo, $termino_tiempo, 'lunes',$profesor);
			$var['asignaciones'][] = $this->modelo_agenda->asignaciones($martes,$inicio_tiempo, $termino_tiempo, 'martes',$profesor);
			$var['asignaciones'][] = $this->modelo_agenda->asignaciones($miercoles,$inicio_tiempo, $termino_tiempo, 'miercoles',$profesor);
			$var['asignaciones'][] = $this->modelo_agenda->asignaciones($jueves,$inicio_tiempo, $termino_tiempo, 'jueves',$profesor);
			$var['asignaciones'][] = $this->modelo_agenda->asignaciones($viernes,$inicio_tiempo, $termino_tiempo, 'viernes',$profesor);

			$inicio_tiempo = date('H:i:s', strtotime("$inicio_tiempo + 30 minutes") );
			$termino_tiempo = date('H:i:s', strtotime("$inicio_tiempo + 30 minutes") );
		}

		$this->load->view('agenda/agenda_semanal', $var);
	}

   // INSERT + Email
	public function agendar_orientacion() {
		$orientacion = $this->modelo_agenda->getOrientacionPorIdSimple($this->input->get('id_orientacion'));

		$datos['fecha_evento'] = $this->input->get('fecha');
		$datos['hora_inicio'] = $this->input->get('hora');
		$datos['rut_abogado'] = $orientacion->rut_abogado;
		$datos['id_tipo_evento'] = $this->input->get('id_orientacion');
		$datos['tipo_evento'] = 'orientacion';
		$datos['titulo_evento'] = $this->input->get('titulo_evento');
		$datos['descripcion_evento'] = $this->input->get('descripcion_evento');

		$output = $this->_ingresarAsignacionEInformar($datos);
		echo json_encode($output);
	}

	// INSERT + Email
	public function agendar_causa() {
		$datos['fecha_evento'] = $this->input->get('fecha');
		$datos['hora_inicio'] = $this->input->get('hora_inicio');
		$datos['rut_abogado'] = $this->input->get('rut');
		$datos['id_tipo_evento'] = $this->input->get('id_correlativa_causa');
		$datos['tipo_evento'] = 'causa';
		$datos['titulo_evento'] = null; //autogenerado en _enviarCorreoDeAsignacion()
		$datos['descripcion_evento'] = null; //autogenerado en _enviarCorreoDeAsignacion()

		$output = $this->_ingresarAsignacionEInformar($datos);
		echo json_encode($output);
	}

	// INSERT +  INSERT + Email
	public function agendar_audiencia() {
		$output = array('exito' => false, 'mensaje' => 'no se pudo encontrar la causa');
		$this->load->model('modelo_audiencia');
		$this->load->model('modelo_causa');

		$datos['fecha_evento'] = $this->input->get('fecha');
		$datos['hora_inicio'] = $this->input->get('hora');
		$datos['rut_abogado'] = $this->input->get('rut_abogado');
		$id_correlativa_causa = $this->input->get('id_correlativa_causa');
		$datos['tipo_evento'] = 'audiencia';
		$datos['titulo_evento'] = $this->input->get('titulo_evento');
		$datos['descripcion_evento'] = $this->input->get('descripcion_evento');

		$causa = $this->modelo_causa->get($id_correlativa_causa);
		if($causa) {
			$output['mensaje'] = 'no se pudo ingresar una nueva audiencia';
			$datos['id_tipo_evento'] = $this->modelo_audiencia->ingresarAudienciaParcial($causa->rol_causa, $datos['fecha_evento'], $datos['hora_inicio'], $this->session->id_sede_actual, $datos['rut_abogado'], $causa->RUT_ALUMNO);
			if($datos['id_tipo_evento']) $output = $this->_ingresarAsignacionEInformar($datos);
		}

		echo json_encode($output);
	}

	// ??? (Agendar) + Email
	public function agendar_asunto() {
		$output = array('exito' => false, 'mensaje' => 'no se pudo insertar iCal id en tabla');

		$id_tipo_evento = $this->input->get('id_asunto');
		$id_evento_agenda = $this->input->get('id_evento_agenda');
		$tipo_evento = 'asunto';

		$data_envio = $this->_enviarCorreoDeAsignacion($id_evento_agenda, $tipo_evento);
		if($data_envio['exito'] == true) {
			$output = $data_envio;
			$output['mensaje'] = 'exito de ingreso de asignacion y evento por correo';
		}

		$output['id_asig'] = $id_evento_agenda;
		echo json_encode($output);
	}

	// DELETE
	public function eliminar_bloque_horario_abogado() {
		$this->load->model('modelo_agenda');
		$id_bloque_horario_abogado = $this->input->get('id');
		$this->modelo_agenda->eliminar_bloque_horario_abogado($id_bloque_horario_abogado);
		echo "ok";
	}

	public function eliminar_evento() {
		$id_evento = $this->input->get('id_evento');
		$motivo =  $this->input->get('motivo');
		$quien_elimina =  $this->input->get('quien_elimina');

		$exito_bool = $this->modelo_agenda->eliminar_evento($id_evento,$motivo,$quien_elimina);

		echo json_encode(['exito'=>$exito_bool]);
	}


	// *********************************** //
	// ***** CANCELAMIENTO DE EVENTO ***** //
	// *********************************** //
	public function cancelar_evento_calendario() {
		$id_evento = $this->input->get('id_evento');
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_cliente');

		$mensaje1=""; $mensaje2="";$mensaje3="";$mensaje4="";

		$asig_agenda = $this->modelo_agenda->getAsignacionAgendaPorId($id_evento);
		if($asig_agenda == NULL) {
			header('Content-Type: application/json');
			echo json_encode(['exito'=>false, 'mensaje'=>"No hay ninguna asignacion-agenda con esta id =".$id_evento]);
			return;
		}
		$icalid_evento = $asig_agenda->icalid_evento;

		$this->load->helper("icalgoogle_helper");
		$exito_bool = cancelarEvento($icalid_evento);


		// SE LE INFORMA AL USUARIO CLIENTE
		// Primero se confirma que es una causa o una audiencia antes de proseguir.
		$SeInformaCliente = true;
		$id_causa = $asig_agenda->id_causa;
		if($id_causa === NULL) {
			$mensaje1 = "->id_causa es nulo";
			$id_causa = $asig_agenda->id_audiencia;
			if($asig_agenda->id_audiencia === NULL) {
				$mensaje2 = "->id_audiencia es nulo";
				$SeInformaCliente = false;
			}
		}
		if($SeInformaCliente) {
			$emailCliente = $this->modelo_cliente->getEmailPorIdCorrelativaCausa($id_causa);
			$mensaje3 = "emailCliente = ".$emailCliente;

			if($emailCliente) {
				$descripcion_evento = "Le informamos que el evento de la Clinica Jurídica UNAB, acordado para las ".$asig_agenda->hora_inicio." del ".$asig_agenda->fecha_asignacion." ha sido cancelado.";
				$mensaje4 = enviarCorreoNormal("Citación Cancelada", $descripcion_evento, $emailCliente);
			}
		} //FIN INFORMAR CLIENTE



		header('Content-Type: application/json');
		echo json_encode(array("exito" => $exito_bool, "mensaje1" => $mensaje1, "mensaje2" => $mensaje2, "mensaje3" => $mensaje3, "mensaje4" => $mensaje4));
	}


	// *********************************** //
	// ******* REENVÍO DE EVENTO ********* //
	// *********************************** //
	public function reenviar_asignacion() {
		$this->load->model('modelo_agenda');

		$id_evento = $this->input->get('id_evento');
		$mensaje_reenvio = $this->input->get('mensaje_reenvio');

		$la_asignacion = $this->modelo_agenda->getAsignacionAgendaPorId($id_evento);
		$icalid_evento = $la_asignacion->icalid_evento;

		$this->load->helper("icalgoogle_helper");
		$exito_bool = reenviarUpdateEvento($icalid_evento, $mensaje_reenvio);

		if($exito_bool) {
			$this->load->model('modelo_googlecalendarapi');
			$exito_bool = $this->modelo_googlecalendarapi->actualizarEstadoRechazo($icalid_evento, 'REENVIADO');
		}

		header('Content-Type: application/json');
		echo json_encode([ 'exito'=>$exito_bool ]);
	}

	private function _ingresarAsignacionEInformar(array $datos_asignacion) {
		$output = array('exito' => false, 'mensaje' => 'no se pudo ingresar asignacion de agenda en tabla');

		$id_evento_agenda = $this->modelo_agenda->ingresar_asignacion(
			$datos_asignacion['rut_abogado'],
			$datos_asignacion['fecha_evento'],
			$datos_asignacion['id_tipo_evento'],
			$datos_asignacion['hora_inicio'],
			$datos_asignacion['tipo_evento'],
			$datos_asignacion['titulo_evento'],
			$datos_asignacion['descripcion_evento']);
		if($id_evento_agenda) {
			$output['mensaje'] = 'no se pudo insertar iCal id en tabla';
			$data_envio = $this->_enviarCorreoDeAsignacion($id_evento_agenda, $datos_asignacion['tipo_evento']);
			if($data_envio['exito'] == true) {
				$output = $data_envio;
				$output['mensaje'] = 'exito de ingreso de asignacion y evento por correo';
			}
		}
		$output['id_asig'] = $id_evento_agenda;
		return $output;
	}

	// ************************************** //
	// ***** ENVIÓ DE EVENTO POR CORREO ***** //
	// ************************************** //
	// tipo_evento: 'causa' 'orientacion' 'asunto' 'audiencia'
	private function _enviarCorreoDeAsignacion($id_evento_agenda, $tipo_evento) {
		$data = array('exito'=>false, 'debug' => null);
		$this->load->model('modelo_ingresos');
		$this->load->model('modelo_cliente');
		$this->load->helper("icalgoogle_helper");

		$asignacion = $this->modelo_agenda->getAsignacionAgendaPorId($id_evento_agenda);
		$emailAbogado = $this->modelo_busquedas->emailUsuarioPorRut($asignacion->rut);
		$dtimeInicio = DateTime::createFromFormat("Y-m-dH:i:s", $asignacion->fecha_asignacion."".$asignacion->hora_inicio);
		$timestampInicio = $dtimeInicio->getTimestamp();
		$dtimeTermino = $dtimeInicio->modify('+30 minutes');
		$timestampTermino = $dtimeTermino->getTimestamp();

		switch($tipo_evento) {
			case 'orientacion':
				$id_tipo_evento = $asignacion->id_orientacion;
				$orientacion = $this->modelo_agenda->getOrientacionPorIdCompleto($id_tipo_evento);
				$titulo_evento = $asignacion->titulo_evento;
				$descripcion_evento = "<p>Se le ha asignado una orientación con el usuario ".$orientacion->USUARIO.", a las ".date('h:i A', $timestampInicio).".<br><b>Detalles:</b> ".$asignacion->descripcion_evento.".</p>";

				$emailCliente = $this->modelo_cliente->getEmailPorRut($orientacion->rut_usuario);
				if($emailCliente) {
					enviarEventoPorCorreo($titulo_evento, $descripcion_evento, $timestampInicio, $timestampTermino, $emailCliente);
				}
				break;

			case 'causa':
				$this->load->model('modelo_causa');
				$id_tipo_evento = $asignacion->id_causa;
				$causa = $this->modelo_causa->getFullJoin($id_tipo_evento);
				$rol_causa = $causa->rol_causa ? $causa->rol_causa : '';
				$titulo_evento = "Atender Causa ".$rol_causa."(".$id_tipo_evento.")";
				$descripcion_evento = "<p>Atender al usuario ".$causa->nombre_cliente.", por la causa ".$rol_causa."( ID interna: ".$id_tipo_evento."), a las ".date('h:i A', $timestampInicio).".</p>";

				// CORREO AL CLIENTE TODO.: cambiar el mensaje que recibe el cliente.
				$emailCliente = $this->modelo_cliente->getEmailPorIdCorrelativaCausa($id_tipo_evento);
				if($emailCliente) {
					enviarEventoPorCorreo($titulo_evento, $descripcion_evento, $timestampInicio, $timestampTermino, $emailCliente);
				}
				break;

			case 'audiencia':
				$this->load->model('modelo_audiencia');
				$id_tipo_evento = $asignacion->id_audiencia;
				$audiencia = $this->modelo_audiencia->getPorId($id_tipo_evento);
				$titulo_evento = "(Audiencia) ".$asignacion->titulo_evento;
				$descripcion_evento = "<p>".$asignacion->descripcion_evento."</p>";

				// CORREO AL CLIENTE TODO.: cambiar el mensaje que recibe el cliente.
				$emailCliente = $this->modelo_cliente->getEmailPorRolCausa($audiencia->rol_causa);
				if($emailCliente) {
					enviarEventoPorCorreo($titulo_evento, $descripcion_evento, $timestampInicio, $timestampTermino, $emailCliente);
				}
				break;

			case 'asunto':
				$id_asunto = $asignacion->id_asunto;
				$titulo_evento = $asignacion->titulo_evento;
				$descripcion_evento = "<p>".$asignacion->descripcion_evento."</p>";
				break;
		}
		$ids_evento = enviarEventoPorCorreo($titulo_evento, $descripcion_evento, $timestampInicio, $timestampTermino, $emailAbogado);

		$result_bool = $this->modelo_ingresos->ID_iCalUID_en_asignacionAgenda($ids_evento['id'], $ids_evento['icaluid'], $id_evento_agenda);

		$data['debug'] = $ids_evento;
		$data['exito'] = $result_bool;
		return $data;
	}
}
?>
