<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Paginas extends CI_Controller {

	public function modelo_datos() {

		// url: http://stackoverflow.com/questions/1911094/is-there-a-way-to-have-a-codeigniter-controller-return-an-image
		//
		$filename="./assets/images/modelo_datos.svg"; //<-- specify the image  file
		if(file_exists($filename)){
		  header('Content-Length: '.filesize($filename)); //<-- sends filesize header
		  header('Content-Type: image/svg+xml'); //<-- send mime-type header
		  header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
		  readfile($filename); //<--reads and outputs the file onto the output buffer
		  die(); //<--cleanup
		  exit; //and exit
		} else { echo "Imagen no se encuentra etc."; }
	}


	public function flujo_asig_causas()	{

		$filename="./assets/images/bpmn_rcj_asignar_causa_profesor_o_alumno.svg"; //<-- specify the image  file
		if(file_exists($filename)) {
		  header('Content-Length: '.filesize($filename)); //<-- sends filesize header
		  header('Content-Type: image/svg+xml'); //<-- send mime-type header
		  header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
		  readfile($filename); //<--reads and outputs the file onto the output buffer
		  die(); //<--cleanup
		  exit; //and exit
		} else { echo "Imagen no se encuentra etc."; }
	}


	public function proceso_atencion()	{
		$filename="./assets/images/bpmn_rcj_atender_usuario.svg"; //<-- specify the image  file
		if(file_exists($filename)) {
		  header('Content-Length: '.filesize($filename)); //<-- sends filesize header
		  header('Content-Type: image/svg+xml'); //<-- send mime-type header
		  header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
		  readfile($filename); //<--reads and outputs the file onto the output buffer
		  die(); //<--cleanup
		  exit; //and exit
		} else { echo "Imagen no se encuentra etc."; }
	}

	public function no_autorizado() {
		$this->load->view('no_autorizado');
	}

	public function descargar($id_archivo) {
		$this->load->model('modelo_tramite');
		if(esta_logueado($this)) {
			$archivo = $this->modelo_tramite->getArchivo($id_archivo);

			$data = file_get_contents($archivo->file_path.$archivo->file_name);

			$this->load->helper("download");
			force_download($archivo->file_name, $data, true);
		} else {
			$this->load->view('no_autorizado');
		}
	}

	public function hola() {
		$this->output->set_status_header(204);
	}


	// Vista
	public function plantillas() {
		$this->load->model('modelo_archivo');
		$this->load->model(CIBB_CARPETA.'admin_model');

		$var['plantillas'] = $this->modelo_archivo->getPlantillas();
		$var['type'] = NULL;
		$var['categories'] = $this->admin_model->category_get_all();
		$var['ruta_thumbnails'] = '/plantillas/thumbnails/';
		$var['controlador_descargar'] = 'paginas/descargar';
		$this->load->view('fichas/plantilla_index', $var);
	}

	// Vista || POST
	public function subir_plantilla() {
		if ( $this->input->post('btn_submit') ) {
			$output = array('exito'=>false, 'mensaje'=>'no se pudo ingresar plantilla.');
			$this->load->model('modelo_archivo');
			$datos['id_categoria'] = $this->input->post('id_ciforum_categoria');
			$datos['titulo'] = $this->input->post('titulo');
			$datos['descripcion'] = $this->input->post('descripcion');
			// SE GUARDA EL DOCUMENTO.
			$result = $this->modelo_archivo->ingresar('documento', 'plantillas');
			$output = $result;
			if($result['exito']) {
				$datos['id_archivo'] = $result['ids'][0];
				// SE GUARDA EL THUMBNAIL.
				$result2 = $this->modelo_archivo->ingresar('thumbnail', "plantillas/thumbnails");
				if ( $result2['exito'] ) {
					$datos['thumbnail'] = $this->upload->data()['file_name'];
					$output['ruta'] = $result2['ruta'];
				}
				$output['exito'] = $this->modelo_archivo->ingresarPlantilla($datos);
			}

			echo json_encode($output);

		} else {
			$this->load->model(CIBB_CARPETA.'admin_model');
         $var['categories'] = $this->admin_model->category_get_all();
			$var['controlador'] = 'paginas/subir_plantilla';
			$this->load->view('fichas/plantilla_subir', $var);
		}
	}

	// Descargar
	// public function plantilla($id_plantilla) {
	// 	$this->load->model('modelo_archivo');
	// 	$plantilla = $this->modelo_archivo->getPlantilla($id_plantilla);
	// 	$archivo = $this->modelo_archivo->get($plantilla->id_archivo);
	//
	// 	$real = realpath(FCPATH);
	// 	$data = file_get_contents($real."/plantillas/".$archivo->file_name);
	// 	$this->load->helper("download");
	// 	force_download($archivo->orig_name, $data, true);
	// }

	public function category($slug, $start = 0) {
		$this->load->model(CIBB_CARPETA.'thread_model');
		$this->load->model(CIBB_CARPETA.'user_model');

		$category = $this->db->get_where(TBL_CATEGORIES, array('slug' => $slug))->row();
		$this->load->model(CIBB_CARPETA.'admin_model');
		$this->data['cat']    = $this->admin_model->category_get_all_parent($category->id, 0);
		$this->data['thread'] = $category;

		$cat_id = array();
		$child_cat = $this->admin_model->category_get_all($category->id);
		$cat_id[0] = $category->id;
		foreach ($child_cat as $cat) {
		  $cat_id[] = $cat['id'];
		}

		// // set pagination
		// $this->load->library('pagination');
		// $this->page_config['base_url']    = site_url(CIBB_CARPETA.'thread/category/'.$slug);
		// $this->page_config['uri_segment'] = 5;
		// $this->page_config['total_rows']  = $this->thread_model->get_total_by_category($cat_id);
		// $this->page_config['per_page']    = 10;
		//
		// $this->set_pagination();
		//
		// $this->pagination->initialize($this->page_config);
		// $this->data['page']    = $this->pagination->create_links();

		// $this->data['threads'] = $this->thread_model->get_by_category($start, $this->page_config['per_page'], $cat_id);
		$this->load->model('modelo_archivo');
		$this->data['plantillas'] = $this->modelo_archivo->getPlantillasPorCategoria($cat_id);

		$this->data['type']    = 'category';
		$this->data['title']   = 'Category :: '.$category->name.CIBB_TITLE;

		$this->data['categories'] = $this->admin_model->category_get_all();
		$this->data['ruta_thumbnails'] = '/plantillas/thumbnails/';
		$this->data['controlador_descargar'] = 'paginas/descargar';

		$this->load->view('fichas/plantilla_index', $this->data);

		$this->load->model('modelo_archivo');
		$this->load->model(CIBB_CARPETA.'admin_model');
	 }
}
