<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Busqueda extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Por conveniencia, siempre se carga este modelo
		$this->load->model('modelo_busquedas');
	}

	// Vista + GET
	public function busqueda_causas() {
		$this->load->model('modelo_causa');
		$solo_vigentes=false; $solo_con_rol=false; $limite = 40;
		$var['causas'] = $this->modelo_causa->getCompleto($solo_vigentes, $solo_con_rol, $limite);
		$var['tipo_busqueda_causas'] = 'normal';
		$var['titulo'] = 'Búsqueda Unificada de Causas';
		$var['editable'] = true;
		$var['titulo_tabla'] = 'Mostrando las últimas '.$limite.' causas';
		$var['callback_cambios'] = "buscar_causas('normal')";
		$this->load->view('busqueda/buscador_causas', $var);
	}

	public function busqueda_causas_historicas() {
		$this->load->model('modelo_causa');
		$var['tipo_busqueda_causas'] = 'historica';
		$var['titulo'] = 'Búsqueda Unificada de Causas Historicas';
		$var['editable'] = true;
		$var['causas'] = $this->modelo_causa->getPorBusquedaHistoricaRUT(NULL, NULL, NULL, NULL, NULL);
		$var['callback_cambios'] = "buscar_causas('historica')";
		$this->load->view('busqueda/buscador_causas', $var);
	}



	public function disponibilidad_profesor() {
		$rut=$this->input->get('rut');
		$var['disponibilidad'] = $this->modelo_busquedas->buscar_disponibilidad_profesor($rut);
		$this->load->view('mantenedor/disponibilidad_profesor',$var);
	}

	public function vista_tabla_profesores() {
		$var['usuarios'] = $this->modelo_busquedas->buscar_profesores_sede();
		$var['input_id_rut'] = $this->input->get('input_id_rut');
		$var['input_id_nombre'] = $this->input->get('input_id_nombre');
		$var['callback_exito'] = $this->input->get('callback_exito');
		$this->load->view('busqueda/tabla_usuarios_numcausas',$var);
	}

	// Devuelve tabla con alumnos y nunmero de causas.
	// Si el usuario es Profesor, solo muestra alumnos propios.
	public function vista_tabla_alumnos() {
		$rut_usuario = $this->session->rut;

		$var['usuarios'] = array();
		if($this->session->rol->asignar_todo) {
			$var['usuarios'] = $this->modelo_busquedas->geno_alumnos_sede();
		}
		elseif($this->session->rol->evaluar) {
			$this->load->model('modelo_usuario');
			//~ $var['usuarios'] = $this->modelo_busquedas->geno_alumnos_profesor($rut_usuario);
			$id_usuario_sesion = $this->modelo_usuario->getIdPorRut($rut_usuario);
			$var['usuarios'] = $usuarios_fuerte_vinculado = $this->modelo_busquedas->getAlumnosDeAbogadoYPorVinculoCausas($id_usuario_sesion);
		}
		$var['input_id_rut'] = $this->input->get('input_id_rut');
		$var['input_id_nombre'] = $this->input->get('input_id_nombre');
		$this->load->view('busqueda/tabla_usuarios_numcausas',$var);
	}

	public function buscar_causas() {
		$this->load->model('modelo_causa');
		$rol = $this->input->get('rol');
		$abogado = $this->input->get('abogado');
		$alumno = $this->input->get('alumno');
		$usuario = $this->input->get('usuario');
		$tipo_busqueda =  $this->input->get('tipo_busqueda');
		$solo_vigentes =  $this->input->get('vigente');
		$id_correl = $this->input->get('id_correlativa');
		$tipo_busqueda_causas = $this->input->get('tipo_busqueda_causas');
		$sin_alumno =  $this->input->get('sin_alumno');


		$var['causas'] = null;
		if($tipo_busqueda=='rut') {
			switch($tipo_busqueda_causas) {
				case "normal":
					$var['callback_cambios'] = "buscar_causas('normal')";
					$var['causas'] = $this->modelo_causa->getPorBusquedaRUT($id_correl, $rol, $abogado, $alumno, $usuario, $solo_vigentes, $sin_alumno);
					break;
				case "historica":
					$var['callback_cambios'] = "buscar_causas('historica')";
					$var['causas'] = $this->modelo_causa->getPorBusquedaHistoricaRUT($id_correl, $rol, $abogado, $alumno, $usuario);
					break;
			}
		}
		else if($tipo_busqueda=='nombres') {
			switch($tipo_busqueda_causas) {
				case "normal":
					$var['callback_cambios'] = "buscar_causas('normal')";
					$var['causas'] = $this->modelo_causa->getPorBusquedaNombre($id_correl, $rol, $abogado, $alumno, $usuario, $solo_vigentes, $sin_alumno);
					break;
				case "historica":
					$var['callback_cambios'] = "buscar_causas('historica')";
					$var['causas'] = $this->modelo_causa->getPorBusquedaHistoricaNombre($id_correl, $rol, $abogado, $alumno, $usuario);
					break;
			}

		}
		else {
			echo "error";
		}
		$var['editable'] = $this->session->rol->causa_crear ? true : false;
		$var['mostrar_detalles'] = false;
		$var['mostrar_opciones'] = true;
		$this->load->view('busqueda/tabla_causas', $var);
	}

	// Vista + GET multiple
	// CAUSAS DEL USUARIO ACTUAL.
	public function buscar_mis_causas() {
		$this->load->model('modelo_causa');

		//~ $solo_vigentes = TRUE;
		$solo_vigentes = FALSE;
		$solo_con_rol = FALSE;

		$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($this->session->rut, $this->session->rol->nombre, $solo_vigentes, $solo_con_rol);
		$var['editable'] = $this->session->rol->causa_crear ? true : false;
		$var['mostrar_opcion_audiencia'] = true;
		$var['mostrar_opcion_terminar'] = $this->session->rol->causa_crear ? true : false;
		$var['mostrar_opcion_rellenar'] = false;
		$var['mostrar_opcion_editar'] = $this->session->rol->evaluar ? true : false;
		$this->load->view('causa/tabla_mis_causas', $var);
	}

	// Para dialogo de lista de agendaciones.
	public function agendaciones_causa() {
		$var['id_correlativa_causa'] = $id_correlativa_causa = $this->input->get('id_correlativa_causa');
		$var['agendaciones'] = $this->modelo_busquedas->agendacionesPorCausa($id_correlativa_causa);
		$this->load->view('agenda/tabla_agendaciones', $var);
	}


	// Devuelve vista de tabla con detalles de la asignación de agenda.
	public function vista_detalles_asignacion() {
		$this->load->model('modelo_causa');
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_cliente');

		$rol_causa = NULL;
		$nombre_usuario = NULL;
		$id_evento = $this->input->get('id_evento');
		$asignacion = $this->modelo_agenda->getAsignacionAgendaPorId($id_evento);

		switch($asignacion->tipo_asunto) {
			case 'causa':
				$id_correlativa_causa = $asignacion->id_causa;
				$causa = $this->modelo_causa->get($id_correlativa_causa);
				$rol_causa = $causa->rol_causa;
				$nombre_usuario = $this->modelo_cliente->get($causa->id_cliente)->nombre_cliente;
				break;
			case 'audiencia':
				$this->load->model('modelo_audiencia');
				$id_audiencia = $asignacion->id_audiencia;
				$audiencia = $this->modelo_audiencia->getPorId($id_audiencia);
				$rol_causa = $audiencia->rol_causa;
				$causa = $this->modelo_causa->getPorRol($rol_causa);
				if(isset($causa->RUT_CLIENTE)) {
					$rut_usuario = $causa->RUT_CLIENTE;
					$nombre_usuario = $this->modelo_cliente->getPorRut($rut_usuario)->nombre_cliente;
				}
				break;
			case 'orientacion':
				$nombre_usuario = $this->modelo_agenda->getOrientacionPorIdCompleto($asignacion->id_orientacion)->USUARIO;
				break;
		}

		$var['agendacion'] = $asignacion;
		$var['rol_causa'] = $rol_causa;
		$var['nombre_usuario'] = $nombre_usuario;
		$this->load->view('busqueda/detalle_asignacion', $var);
	}


	public function detalle_causa($id_correlativa_causa) {
		$this->load->model('modelo_causa');
		$causa = $this->modelo_causa->getDetalladaPorIdCorrelativa($id_correlativa_causa);
		if($this->session->rol->evaluar) $var['activar_terminar'] = true;
		if($this->session->rol->causa_todo && ($causa->TERMINO == NULL || $causa->TERMINO > date("Y-m-d"))) $var['activar_cambiar_usuario'] = true;
		$var['causa'] = $causa;
		$this->load->view('busqueda/detalle_causa', $var);
	}

	public function detalle_causa_rol($rol_causa) {
		$this->load->model('modelo_causa');
		$causa = $this->modelo_causa->getDetalladaPorRol($rol_causa);
		if($this->session->rol->evaluar) $var['activar_terminar'] = true;
		if($this->session->rol->causa_todo && ($causa->TERMINO == NULL || $causa->TERMINO > date("Y-m-d"))) $var['activar_cambiar_usuario'] = true;
		$var['causa'] = $causa;
		$this->load->view('busqueda/detalle_causa', $var);
	}

	public function detalle_abogado($rut, $solo_vigentes='false') {
		$this->load->model('modelo_usuario');
		$this->load->helper("utilidades");
		$solo_vigentes = string2Boolean($solo_vigentes);
		$this->load->model('modelo_causa');
		$var['usuario'] = $this->modelo_usuario->getPorRut($rut);
		$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($rut, 'abogado', $solo_vigentes);
		$var['editable'] = $this->session->rol->causa_todo || $this->session->rol->evaluar ? TRUE : FALSE;
		$var['mostrar_detalles'] = true;
		$var['mostrar_opciones'] = false;
		$this->load->view('causa/tabla_causas_usuario', $var);
	}

	public function detalle_alumno($rut, $solo_vigentes='false') {
		$this->load->model('modelo_usuario');
		$this->load->model('modelo_causa');
		$this->load->helper("utilidades");
		$solo_vigentes = string2Boolean($solo_vigentes);

		$notas = $this->modelo_busquedas->traer_promedio_usuario($rut);
		$acumulado = 0;
		$total = 0;
		$final = 0;
		if ($notas->num_rows() > 0) {
		   foreach ($notas->result() as $row) {
				$total = $total + 1;
				$acumulado = $acumulado + $row->nota_final;
		   }
		}
		if($acumulado==0) {
			$final = 0;
		} else {
			$final = $acumulado / $total;
		}

		$var['promedio'] = $final;
		$var['usuario'] = $this->modelo_usuario->getPorRut($rut);
		$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($rut, 'alumno', $solo_vigentes);
		$var['editable'] = $this->session->rol->causa_todo || $this->session->rol->evaluar ? TRUE : FALSE;
		$var['mostrar_detalles'] = true;
		$var['mostrar_opciones'] = false;
		$this->load->view('causa/tabla_causas_usuario', $var);
	}


	public function detalle_cliente($rut_cliente, $solo_vigentes='false') {
		$this->load->model('modelo_cliente');
		$this->load->model('modelo_causa');
		$this->load->helper("utilidades");
		$solo_vigentes = string2Boolean($solo_vigentes);
		$cliente = $this->modelo_cliente->getPorRut($rut_cliente);
		$var['cliente']['nombre'] = $cliente->nombre_cliente;
		$var['cliente']['rut'] = $cliente->rut_cliente;
		$var['cliente']['dv'] = $cliente->dv_cliente;
		$var['usuario'] = (object) $var['cliente'];
		$var['causas'] = $this->modelo_causa->getPorCliente($rut_cliente, $solo_vigentes);
		$var['editable'] = $this->session->rol->causa_todo || $this->session->rol->evaluar ? TRUE : FALSE;
		$var['mostrar_detalles'] = true;
		$var['mostrar_opciones'] = false;
		$this->load->view('causa/tabla_causas_usuario', $var);
	}

	public function buscar_usuarios() {
		$var['dialog_a_cerrar'] = $this->input->get('dialog_a_cerrar');
		$var['usuarios'] = $this->modelo_busquedas->getUsuarios();

		$this->load->view('busqueda/tabla_selector_cliente', $var);
	}

	public function materia() {
		$this->load->model('modelo_materias');
		$id_procedimiento = $this->input->get('id_procedimiento');
		$materias = $this->modelo_materias->getPorProcedimiento($id_procedimiento);

		echo json_encode($materias);
	}

	public function vista_terminar_causa($id_correlativa_causa) {
		$var['id_causa'] = $id_correlativa_causa;
		$var['rol_causa'] = $this->db->select('rol_causa')->get_where('causas', ['id'=>$id_correlativa_causa], 1)->row()->rol_causa;
		$var['causales_termino'] = $this->modelo_busquedas->causales_termino();
		$this->load->view('causa/dialog_terminar_causa', $var);
	}

	public function traer_sedes() {
		$sedes = $this->modelo_busquedas->traer_sedes();
		if ($sedes) {
			echo "<select id='sedes'>";
			echo "<option value='no'>Seleccione una Sede</option>";
		   foreach ($sedes as $sd) {
				echo "<option value='$sd->id_sede'>$sd->nombre_sede</option>";
		   }
		   echo "</select>";
		}
	}

	// Entrega ARRAY de Asignaciones (asig. agenda) rechazadas (DECLINED)
	// y que no han sido revisadas (estado_rechazo nulo)
	public function obtener_asig_rechazadas() {
		$this->load->model('modelo_agenda');

		$data = array();
		$asignaciones = $this->modelo_agenda->asignaciones_rechazadas($sin_estado_rechazo=TRUE);

		header('Content-Type: application/json');
		echo json_encode($asignaciones);
	}

	// Devuelve JSON con profesores filtrados por sede actual
	// datos: nombre y rut(id)
	public function lista_profesores() {
		$profesores = $this->modelo_busquedas->traer_profesores();
		$array_profs = array();

		foreach($profesores as $prof) {
			$array_profs[] = array('nombre' => $prof->rut, 'rut' => $prof->nombre);
		}

		header('Content-Type: application/json');
		echo json_encode($array_profs);
	}


	// Devuelve JSON con alumnos filtrados por sede actual
	// datos: nombre y rut(id)
	public function lista_alumnnos() {
		$alumnos = $this->modelo_busquedas->traer_alumnos();
		$array_alums = array();

		foreach($alumnos as $alum) {
			$array_alums[] = array('nombre' => $alum->rut, 'rut' => $alum->nombre);
		}
		header('Content-Type: application/json');
		echo json_encode($array_alums);
	}


	// Devuelve un html donde se muestra usuario actual y se puede
	// Seleccionar un nuevo usuario.
	// Tipo es "profesor", "alumno" etc
	//TODO. restringir por tipo de rol.
	public function vista_cambiar_usuario($id_correlativa_causa) {
		$tipo_usuario = $this->input->get('tipo_usuario');

		// $var['causa'] = $this->modelo_causa->get($id_correlativa_causa);
		$var['id_causa'] = $id_correlativa_causa;
		$var['nombre_usuario_actual'] = $this->input->get('nombre_usuario_actual');

		switch($tipo_usuario) {
			case 'alumno':
				$usuarios = $this->modelo_busquedas->traer_alumnos();
			break;
			case 'abogado':
				$usuarios = $this->modelo_busquedas->traer_profesores();
			break;
		}
		$array_usrs = array();

		foreach($usuarios as $usr) {
			$array_usrs[] = array('rut' => $usr->rut, 'nombre' => $usr->nombre, 'sede' => $usr->sede);
		}

		$var['tipo_usuarios'] = $tipo_usuario;
		$var['usuarios'] = $array_usrs;

		$this->load->view('causa/cambio_usuario', $var);
	}
}
?>
