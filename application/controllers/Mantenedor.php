<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Mantenedor extends CI_Controller {
		public $crud;

		function __construct() {
			parent::__construct();

			// esta_autorizado_helper.php
			if(esta_logueado($this)) {
				if(esta_autorizado_mantenedor($this) === false) {
					redirect('paginas/no_autorizado');
				}
			} else {
				redirect('login');

			}
			$this->load->model('Grocery_crud_model');
			$this->load->model('modelo_busquedas');
			$this->load->model('modelo_ingresos');
			$this->load->database();
			$this->load->helper("url");
			$this->load->library('grocery_CRUD');
			$this->crud = new grocery_CRUD();
			$this->crud->set_theme('flexigrid');
		}

		public function index() {
			redirect('mantenedor/mantenedor_profesores');
		}

		private function _prepararCrudUsuario() {


			$this->crud->set_table('usuarios');
			$this->crud->where('sede', $this->session->id_sede_actual);

			$this->crud->required_fields('rut', 'dv', 'pwd', 'nombre');
			$this->crud->fields('rut','dv','nombre_usuario','nombre', 'pwd','telefono', 'email', 'id_rol', 'sede');
			$this->crud->edit_fields('rut','dv','nombre_usuario', 'nombre', 'telefono', 'email'); //no password.
			$this->crud->columns('rut','dv','nombre_usuario','nombre', 'telefono', 'email');
			$this->crud->display_as('nombre_usuario', 'nickname (opcional)');
			$this->crud->display_as('pwd', 'Contraseña');
			$this->crud->display_as('rut', 'RUT');
			$this->crud->display_as('dv', 'DV (digito verificador)');

			$this->crud->change_field_type('sede','invisible');
			$this->crud->change_field_type('id_rol','invisible');
			$this->crud->change_field_type('pwd','password');
			$this->crud->change_field_type('telefono','phone');
			$this->crud->change_field_type('email','email');

			// unset the other name columns from view
			$this->crud->unset_columns('dv');

			$this->crud->callback_before_delete(array($this,'callback_borrar_usuariosedes'));
			$this->crud->callback_after_insert(array($this,'callback_usuario_after'));

			return $this->crud;
		}

		public function mantenedor_admin() {
			$this->crud = $this->_prepararCrudUsuario();
			$this->crud->set_subject('Administrador');
			$this->crud->where(array('id_rol'=>'5'));

			$this->crud->callback_column('rut',array($this,'cb_mostrar_rut'));
			$this->crud->callback_before_insert(array($this,'callback_admin_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Administradores';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}


		public function mantenedor_profesores() {
			$this->crud = $this->_prepararCrudUsuario();
			$this->crud->set_subject('Profesor');
			$this->crud->where(array('id_rol'=>'2'));
			
			// MUCHOS A MUCHOS
			$this->crud->set_relation_n_n('alumnos_asignados', 'abogado_alumnos', 'usuarios', 'id_abogado', 'id_alumno', 'nombre');
			// SE REPITEN LAS LINEAS.
			$this->crud->fields('rut','dv','nombre_usuario','nombre', 'pwd','telefono', 'email', 'id_rol', 'sede', 'alumnos_asignados');
			$this->crud->edit_fields('rut','dv','nombre_usuario', 'nombre', 'telefono', 'email', 'alumnos_asignados');

			$this->crud->callback_column('rut',array($this,'cb_mostrar_rut'));
			$this->crud->callback_before_insert(array($this,'callback_profesores_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Profesores';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}


		public function mantenedor_alumnos() {
			$this->crud = $this->_prepararCrudUsuario();
			$this->crud->set_subject('Alumno');
			$this->crud->where(array('id_rol'=>'1'));
			
			// MUCHOS A MUCHOS
			$this->crud->set_relation_n_n('profesores_asignados', 'abogado_alumnos', 'usuarios', 'id_alumno', 'id_abogado', 'nombre');
			// SE REPITEN LAS LINEAS.
			$this->crud->fields('rut','dv','nombre_usuario','nombre', 'pwd','telefono', 'email', 'id_rol', 'sede', 'profesores_asignados');
			$this->crud->edit_fields('rut','dv','nombre_usuario', 'nombre', 'telefono', 'email', 'profesores_asignados');

			$this->crud->callback_column('rut',array($this,'cb_mostrar_rut'));
			$this->crud->callback_before_insert(array($this,'callback_alumnos_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Alumnos';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}

		public function mantenedor_secretarios() {
			$this->crud = $this->_prepararCrudUsuario();
			$this->crud->set_subject('Funcionario');
			$this->crud->where('id_rol','3');

			$this->crud->callback_column('rut',array($this,'cb_mostrar_rut'));
			$this->crud->callback_before_insert(array($this,'callback_secretario_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Funcionarios';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}

		public function mantenedor_directores() {
			$this->crud = $this->_prepararCrudUsuario();
			$this->crud->set_subject('Director');
			$this->crud->where('id_rol','4');

			$this->crud->callback_column('rut',array($this,'cb_mostrar_rut'));
			$this->crud->callback_before_insert(array($this,'callback_director_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Directores';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}


		public function mantenedor_clientes() {

			$this->crud->set_table('clientes');
			$this->crud->set_subject('Usuario');
			$this->crud->required_fields('rut_cliente', 'dv_cliente', 'nombre_cliente');
			$this->crud->fields('rut_cliente','dv_cliente','nombre_cliente', 'telefono', 'domicilio', 'email', 'edad');
			$this->crud->columns('rut_cliente','dv_cliente','nombre_cliente', 'telefono','email');

			$this->crud->change_field_type('telefono','phone');
			$this->crud->change_field_type('email','email');

			$this->crud->unset_columns('dv_cliente');
			$this->crud->callback_column('rut_cliente',array($this,'_cb_col_rut'));

			$this->crud->display_as('rut_cliente', 'Rut');
			$this->crud->display_as('nombre_cliente', 'Nombre');

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor Usuarios';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}


		public function mantenedor_materias() {
			try{

				$this->crud->set_table('materia');
				$this->crud->set_subject('materia');
				$this->crud->required_fields('nombre_materia');

				$this->crud->fields('nombre_materia', 'nombre_largo', 'nombre_corto', 'id_procedimiento');
				$this->crud->columns('nombre_materia', 'id_procedimiento', 'id_competencia')
					->display_as('nombre_materia','Nombre Materia')
					->display_as('id_procedimiento','Procedimiento')
					->display_as('id_competencia','Competencia');

				$this->crud->set_relation('id_procedimiento','procedimiento','{nombre} - {id_competencia}');
				$this->crud->set_relation('id_competencia','competencia','nombre');

				//TODO. DESCUBRIR COMO MMOSTRAR DATOS DE OTRAS TABLAS(COMPETENCIA.NOMBRE) EN UN SELECT.
				$this->crud->unset_add();
				$this->crud->unset_edit();

				$var['output'] = $this->crud->render();
				$var['titulo'] = 'Mantenedor Materias';
				$this->load->view('mantenedor/crud_mantenedor', $var);

			} catch(Exception $e){
				show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
		}


		public function mantenedor_causales() {
			try {

				$this->crud->set_table('causal_termino');
				$this->crud->set_subject('causal de término');
				$this->crud->required_fields('nom_causal');
				$this->crud->display_as('nom_causal','Nombre Causal');
				$this->crud->columns('nom_causal');
				$var['output'] = $this->crud->render();
				$var['titulo'] = 'Mantenedor Causales';

				$this->load->view('mantenedor/crud_mantenedor', $var);

			} catch(Exception $e) {
				show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
		}


		public function mantenedor_roles() {

				$this->crud->set_table('rol');
				$this->crud->set_subject('Rol');
				// $this->crud->required_fields('rut', 'sede');
				$this->crud->columns('id', 'nombre', 'mantencion_todo', 'busqueda_todo', 'agenda_global', 'agenda_personal', 'evaluar', 'audiencia_crear', 'orientacion_crear', 'causa_crear', 'estadisticas_ver');
				$var['output'] = $this->crud->render();
				$var['titulo'] = 'Mantenedor Roles';

				$this->load->view('mantenedor/crud_mantenedor_100', $var);
		}


		public function mantenedor_sedes() {
			try{

				$this->crud->set_table('sedes');
				$this->crud->set_subject('sede');
				$this->crud->required_fields('nombre_sede');
				$this->crud->display_as('nombre_sede','Nombre Sede');
				$this->crud->columns('nombre_sede');
				$var['output'] = $this->crud->render();
				$var['titulo'] = 'Mantenedor Sedes';

				$this->load->view('mantenedor/crud_mantenedor', $var);

			}catch(Exception $e){
				show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
		}


		public function mantenedor_usuariosede() {
			try{

				$this->crud->set_table('usuario_sede');
				$this->crud->set_subject('Usuario-Sede');
				$this->crud->required_fields('rut', 'id_sede');

				$this->crud->set_primary_key('rut','usuarios');

				$this->crud->columns('rut', 'id_sede')
					->display_as('rut', 'Nombre del Usuario')
					->display_as('id_sede', 'Sede');
				$this->crud->set_relation('rut','usuarios','nombre');
				$this->crud->set_relation('id_sede','sedes','nombre_sede');

				$var['output'] = $this->crud->render();
				$var['titulo'] = 'Mantenedor para asignar Sedes a Usuarios';

				$this->load->view('mantenedor/crud_mantenedor', $var);

			} catch(Exception $e) {
				show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
		}
		
		public function mantenedor_tribunal() {
			$this->crud->set_table('tribunal');
			$this->crud->set_subject('Tribunal');
			$this->crud->required_fields('nombre', 'telefono', 'direccion', 'region', 'tipo_tribunal');

			$this->crud->columns('nombre', 'telefono', 'direccion', 'region', 'tipo_tribunal')
				->display_as('nombre', 'Nombre Práctico')
				->display_as('rut', 'RUT (con dígito verificador)')
				->display_as('nombre_largo', 'Nombre Completo (opcional)')
				->display_as('telefonos', 'Teléfonos (si tiene más de 1)')
				->display_as('correos', 'Correos (si tiene más de 1)');
			
			$this->crud->fields('region', 'tipo_tribunal', 'nombre','nombre_largo','rut', 'direccion','telefono','telefonos', 'cuenta_corriente', 'correo', 'correos');
			
			$this->crud->field_type('rut_sin_dv','invisible');
			$this->crud->field_type('dv','invisible');
			$this->crud->field_type('tipo_tribunal','dropdown',array('PRIMERA INSTANCIA'=>'PRIMERA INSTANCIA','CORTE APELACIONES'=>'CORTE APELACIONES','CORTE SUPREMA'=>'CORTE SUPREMA','MILITAR'=>'MILITAR', 'AMBIENTAL'=>'AMBIENTAL'));
			//~ $this->crud->field_type('region','dropdown',array('ARICA'=>'ARICA','IQUIQUE'=>'IQUIQUE','ANTOFAGASTA'=>'ANTOFAGASTA','COPIAPO'=>'COPIAPO','LA SERENA'=>'LA SERENA','VALPARAÍSO'=>'VALPARAÍSO','SANTIAGO'=>'SANTIAGO','SAN MIGUEL'=>'SAN MIGUEL','RANCAGUA'=>'RANCAGUA','TALCA'=>'TALCA','CHILLÁN'=>'CHILLÁN','CONCEPCIÓN'=>'CONCEPCIÓN','TEMUCO'=>'TEMUCO','VALDIVIA'=>'VALDIVIA','PUERTO MONTT'=>'PUERTO MONTT','COYHAIQUE'=>'COYHAIQUE','PUNTA ARENAS'=>'PUNTA ARENAS'));
			$this->crud->field_type('region','dropdown',array('ARICA'=>'Arica','IQUIQUE'=>'Iquique','ANTOFAGASTA'=>'Antofagasta','COPIAPO'=>'Copiapó','LA SERENA'=>'La Serena','VALPARAÍSO'=>'Valparaíso','SANTIAGO'=>'Santiago','SAN MIGUEL'=>'San Miguel','RANCAGUA'=>'Rancagua','TALCA'=>'Talca','CHILLÁN'=>'Chillán','CONCEPCIÓN'=>'Concepción','TEMUCO'=>'Temuco','VALDIVIA'=>'Valdivia','PUERTO MONTT'=>'Puerto Montt','COYHAIQUE'=>'Coyhaique','PUNTA ARENAS'=>'Punta Arenas'));


			$this->crud->set_rules('rut','RUT','regex_match[/\d{3,8}-[\d|kK]{1}/]');
			$this->crud->set_rules('correo','CORREO','valid_email');
			
			$this->crud->callback_before_insert(array($this,'callback_tribunal_before'));
			$this->crud->callback_before_update(array($this,'callback_tribunal_before'));

			$var['output'] = $this->crud->render();
			$var['titulo'] = 'Mantenedor de Tribunales';

			$this->load->view('mantenedor/crud_mantenedor', $var);
		}


		// ****** CALLBACKS ******
		public function callback_borrar_usuariosedes($id_usuario) {
			$this->load->model('modelo_usuario');
			$this->modelo_usuario->borrarUsuarioSedes($id_usuario);
		}

		public function _cb_col_rut($value, $row) {
			return $row->rut_cliente.'-'.$row->dv_cliente;
		}

		public function cb_mostrar_rut($value, $row) {
			return $row->rut.'-'.$row->dv;
		}

		function callback_alumnos_before($before_array) {
			return $this->_callback_usuario_before($before_array, 1);
		}
		function callback_profesores_before($before_array) {
			return $this->_callback_usuario_before($before_array, 2);
		}
		function callback_secretario_before($before_array) {
			return $this->_callback_usuario_before($before_array, 3);
		}
		function callback_director_before($before_array) {
			return $this->_callback_usuario_before($before_array, 4);
		}
		function callback_admin_before($before_array) {
			return $this->_callback_usuario_before($before_array, 5);
		}

		private function _callback_usuario_before($before_array, $id_rol) {
			$before_array['id_rol'] = $id_rol;
			$before_array['sede'] = $this->session->id_sede_actual;

			// ENCRIPTACIÓN DE CLAVE CON BCRYPT
			$this->load->helper("configuracion");
			$before_array['pwd'] = contrasenaEncriptar($before_array['pwd']);
			return $before_array;
		}

		function callback_usuario_after($post_array) {
			$rut = $post_array['rut'];
			$sede = $post_array['sede'];
			$this->modelo_ingresos->ingreso_sede_usuario($rut, $sede);
		}
		
		function callback_tribunal_before($before_array) {
			if($before_array['rut'] && count(explode('-', $before_array['rut'])) < 2) {
				return false;
			}
			list($before_array['rut_sin_dv'], $before_array['dv']) = explode('-', $before_array['rut']);
			return $before_array;
		}

	}
?>
