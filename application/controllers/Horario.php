<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Horario extends CI_Controller {

	function __construct() {
		parent::__construct();
	}

	// Por regla de negocio solo se trabaja
	// de Lunes a Viernes.
	public function vista_horario_personal() {
		$this->load->model('modelo_horario');
		$this->load->model('modelo_agenda');

		$titulo = 'HORARIO PERSONAL';

		$fecha_hoy =  date('Y-m-d');
		$timestamp_hoy = strtotime($fecha_hoy);

		// Convierte la fecha actual en un digito que representa el día.
		$id_dia_semana_hoy = date("w", $timestamp_hoy);

		switch($id_dia_semana_hoy) {
			case 5:
				$fecha_viernes = $fecha_hoy;
				break;
			case 6:
			case 0:
				$titulo = 'MOSTRANDO HORARIO DE LA PRÓXIMA SEMANA';
			default:
				$fecha_viernes = date('Y-m-d', strtotime('Next Friday', $timestamp_hoy));
		}

		$fecha_lunes = date('Y-m-d', strtotime($fecha_viernes.' -4 day'));

		$var['horario'] = $this->modelo_horario->traer_horario_personal($fecha_lunes, $fecha_viernes);

		// Por peticion: Cuando es Alumno, mostrar asignaciones de agenda
		// de Causa  y de Audiencia aunque no estén asignadas a él.
		if($this->session->rol->letra === 'A') {
			$asignaciones_causa_relacionadas = $this->modelo_horario->traer_asignaciones_relacionadas($fecha_lunes, $fecha_viernes);
			$var['horario'] = array_merge($var['horario'], $asignaciones_causa_relacionadas);
		}

		$var['inicio_atencion'] = $this->modelo_agenda->traer_inicio_atencion();
		$var['termino_atencion'] = $this->modelo_agenda->traer_termino_atencion();

		// El plugin de calendario (easycal) recibe fechas en formato
		// dia-mes-año.
		$var['fecha_inicio_calendario'] = date('d-m-Y', strtotime($fecha_lunes));
		$var['titulo_horario'] = $titulo;

		$this->load->view('horario/horario', $var);
	}

}
