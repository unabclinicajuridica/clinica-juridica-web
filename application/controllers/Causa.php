<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Causa extends CI_Controller {
	function __construct() {
		parent::__construct();
		// Por conveniencia, siempre se carga este modelo
		$this->load->model('modelo_busquedas');
		$this->load->model('modelo_causa');
	}

	// Vista
	public function vista_vincular_orientaciones($id_correlativa_causa) {
		$var['id_causa'] = $id_correlativa_causa;
		$this->load->view('orientacion/dialogo_vincular', $var);
	}

	// Vista
	public function vista_orientaciones($id_correlativa_causa) {
		$this->load->model('modelo_orientacion');

		$var['id_causa'] = $id_correlativa_causa;
		$var['id_tabla'] = 'table_orients_'.$id_correlativa_causa;
		$var['orientaciones'] = $this->modelo_orientacion->getPorCausa($id_correlativa_causa);
		$var['mostrar_vincular'] = true;
		$var['mostrar_lupas'] = true;
		$this->load->view('orientacion/tabla_orientaciones', $var);
	}

	// Vista + GET
	// Entrega tabla con Causas con datos pendientes, del usuario actual.
	public function vista_causas_incompletas() {
		$rut_sesion = $this->session->rut;
		$tipo_usuario = $this->session->rol->nombre;
		$var['causas'] = $this->modelo_causa->getIncompletasPorUsuario($rut_sesion, $tipo_usuario);
		$var['titulo_tabla'] = 'CAUSAS CON DATOS INCOMPLETOS';
		$var['titulo_vacio'] = 'Usted no tiene Causas Incompletas';
		$var['editable'] = false;
		$var['mostrar_opcion_audiencia'] = false;
		$var['mostrar_opcion_terminar'] = false;
		if($this->session->rol->evaluar) {
			$var['mostrar_opcion_rellenar'] = false;
			$var['mostrar_opcion_editar'] = true;
		} elseif($this->session->rol->causa_propio) {
			$var['mostrar_opcion_rellenar'] = true;
			$var['mostrar_opcion_editar'] = false;
		}


		$this->load->view('causa/tabla_mis_causas', $var);
	}

	// Vista
	// Se limitan privilegios según usuarioa actual.
	public function vista_editar($id_correlativa_causa) {
		$this->load->model('modelo_materias');

		$rut_sesion = $this->session->rut;
		$tipo_usuario = $this->session->rol->nombre;
		$causa = $this->modelo_causa->getDetalladaPorIdCorrelativa($id_correlativa_causa);
		if($this->session->rol->evaluar && $rut_sesion == $causa->RUT_ABOGADO) {

		} elseif($this->session->rol->causa_propio && $rut_sesion == $causa->RUT_ALUMNO) {

		}

		if($causa->id_materia) {
			$materia = $this->modelo_materias->getMateria($causa->id_materia);
			$var['materias'] = $this->modelo_materias->getPorProcedimiento($materia->id_procedimiento);
			$var['id_competencia'] = $materia->id_competencia;
			$var['id_procedimiento'] = $materia->id_procedimiento;
			$var['id_materia'] = $materia->id;
		}

		$cliente = NULL;
		if($causa->id_cliente) {
			$this->load->model('modelo_cliente');
			$cliente = $this->modelo_cliente->get($causa->id_cliente);
		}

		$var['causa'] = $causa;
		$var['callback'] = $this->input->get('callback');
		$var['id_correlativa_causa'] = $causa->id;
		$var['rol_causa'] = $causa->rol_causa;
		$var['competencias'] = $this->modelo_materias->getCompetenciaConProcedimiento();
		$var['cliente'] = $cliente;
		$var['tribunales'] = $this->modelo_busquedas->tribunales();
		$var['id_tribunal'] = $causa->ID_TRIBUNAL;
		$var['nombre_abogado'] = $causa->NOMBRE_ABOGADO;
		$var['rut_abogado'] = $causa->RUT_ABOGADO;
		$var['nombre_alumno'] = $causa->NOMBRE_ALUMNO;
		$var['rut_alumno'] = $causa->RUT_ALUMNO;
		$var['datos_cliente'] = $this->modelo_causa->getDatosCliente($id_correlativa_causa, $crear_if_null=TRUE);
		$this->load->view('causa/editar', $var);
	}

	// POST
	public function ingresar() {
		$this->load->model('modelo_causa');
		$this->load->model('modelo_cliente');
		$this->load->model('modelo_orientacion');
		$this->load->model('modelo_usuario');

		// capturo datos
		$materia = $this->input->post('materia');
		$rutAbogado = $this->input->post('abogado');
		$nombre_usuario = $this->input->post('nombre_usuario');
		$rut_cliente_full = $this->input->post('rut_cliente');

		if( ! $nombre_usuario) {
			echo json_encode(array('mensaje' => "Debe ingresar el nombre del Usuario", 'id_correlativa_causa' => NULL, 'exito' => false));
			return;
		}
		else if($rut_cliente_full && count(explode("-", $rut_cliente_full)) < 2) {
			echo json_encode(array('mensaje' => "Ingrese guión en el RUT del Usuario", 'id_correlativa_causa' => NULL, 'exito' => false));
			return;
		}
		else if($rutAbogado === '' || $rutAbogado === NULL) {
			echo json_encode(array('mensaje' => "Debe seleccionar a un Abogado", 'id_correlativa_causa' => NULL, 'exito' => false));
			return;
		}
		else if(! $rut_cliente_full) $rut_cliente_full = '-';

		list($rut_cliente, $dv_cliente) = explode("-", $rut_cliente_full);

		$edad_usuario = $this->input->post('edad_usuario');
		$telefono = $this->input->post('telefono');
		$domicilio = $this->input->post('domicilio');
		$email = $this->input->post('email');

		$id_tribunal = $this->input->post('id_tribunal');

		$ids_orientaciones = $this->input->post('ids_orientaciones');


		$dv_abogado = $this->modelo_usuario->getDV($rutAbogado);

		// Rol de Causa puede omitirse al ingresar nueva Causa.
		$rol_causa = $this->input->post('id_causa') ? $this->input->post('id_causa') : NULL;

		// Alumno puede omitirse al ingresar nueva Causa.
		$alumno = NULL;
		$dv_alumno = NULL;
		if( $this->input->post('alumno') !== '') {
			$alumno = $this->input->post('alumno');
			$dv_alumno = $this->modelo_usuario->getDV($alumno);
		}

		$id_cliente = $this->modelo_cliente->ingresar_cliente($rut_cliente, $dv_cliente, $nombre_usuario, $telefono, $domicilio, $email, $edad_usuario);

		// La fecha de ingreso es el presente.
		$fecha_de_ingreso = date('Y-m-d');

		$datos_ingreso = $this->modelo_causa->ingresar_causa($rol_causa, $materia, $fecha_de_ingreso, $alumno, $rutAbogado, $rut_cliente, $dv_abogado, $dv_alumno, $dv_cliente, $id_cliente, $id_tribunal);

		if($datos_ingreso['exito']) {
			$datos_cliente = array(
				'id_causa' => $datos_ingreso['id_correlativa_causa'],
				'estado_civil' => $this->input->post('estado_civil'),
				'separado_de_hecho' => $this->input->post('separado_de_hecho'),
				'situacion_laboral' => $this->input->post('situacion_laboral'),
				'actividad' => $this->input->post('actividad'),
				'tipo_prevision' => $this->input->post('tipo_prevision'),
				'sistema_salud' => $this->input->post('sistema_salud'),
				'escolaridad' => $this->input->post('escolaridad'),
				'total_personas' => $this->input->post('total_personas'),
				'total_adultos' => $this->input->post('total_adultos'),
				'total_menores' => $this->input->post('total_menores'),
				'discapacitados' => $this->input->post('discapacitados'),
				'situacion_habitacional' => $this->input->post('situacion_habitacional'),
				'valor_dividendo' => $this->input->post('valor_dividendo'),
				'ingresos_1' => $this->input->post('ingresos_1'),
				'ingresos_2' => $this->input->post('ingresos_2'),
				'ingresos_3' => $this->input->post('ingresos_3'),
				'ingresos_total' => $this->input->post('ingresos_total'),
				'beneficios_estatales' => $this->input->post('beneficios_estatales'),
				'beneficio_estatal' => $this->input->post('beneficio_estatal')
			);
			$exito = $this->modelo_causa->ingresarDatosCliente($datos_cliente);
			foreach ($ids_orientaciones as $value) {
				$datos_ingreso['exito'] = $this->modelo_orientacion->vincularCausa($value, $datos_ingreso['id_correlativa_causa']);
			}
		}

		// datos_ingreso = boolean 'exito', string 'mensaje', int 'id_correlativa_causa'.
		echo json_encode($datos_ingreso);
	}

	// UPDATE
	// POST.
	public function editar($id_correlativa_causa) {
		$this->load->model('modelo_causa');
		$this->load->model('modelo_cliente');
		$this->load->model('modelo_orientacion');
		$this->load->model('modelo_usuario');

		$exito = false;
		$id_materia = $this->input->post('materia');
		$rut_abogado = $this->input->post('rut_abogado');
		$nombre_cliente = $this->input->post('nombre_usuario');
		$rut_cliente_full = $this->input->post('rut_cliente');

		if( ! $nombre_cliente) { echo json_encode(['exito'=>false, 'mensaje'=>"Debe ingresar el nombre del Usuario"]); return; }
		else if($rut_cliente_full && count(explode("-", $rut_cliente_full)) < 2) { echo json_encode(['exito'=>false, 'mensaje'=>"Ingrese guión en el RUT del Usuario"]); return; }
		else if( ! $rut_abogado) { echo json_encode(['exito'=>false, 'mensaje'=>"Debe seleccionar a un Abogado"]); return; }
		else if(! $rut_cliente_full) $rut_cliente = $dv_cliente = NULL;
		else list($rut_cliente, $dv_cliente) = explode('-', $rut_cliente_full);

		$edad_usuario = $this->input->post('edad_usuario');
		$telefono = $this->input->post('telefono');
		$domicilio = $this->input->post('domicilio');
		$email = $this->input->post('email');
		$id_tribunal = $this->input->post('id_tribunal');
		$ids_orientaciones = $this->input->post('ids_orientaciones');
		$rol_causa = $this->input->post('id_causa');
		$rut_alumno = $this->input->post('rut_alumno');

		$dv_abogado = $this->modelo_usuario->getDV($rut_abogado);
		$dv_alumno = $this->modelo_usuario->getDV($rut_alumno);
		$id_cliente = $this->modelo_cliente->ingresar_cliente($rut_cliente, $dv_cliente, $nombre_cliente, $telefono, $domicilio, $email, $edad_usuario);

		if($id_cliente) {
			$exito = $this->modelo_causa->actualizar($id_correlativa_causa, $rol_causa, $id_materia, $rut_alumno, $rut_abogado, $rut_cliente, $dv_abogado, $dv_alumno, $dv_cliente, $id_cliente, $id_tribunal);
			if($exito) {
				$datos_cliente = array(
					'estado_civil' => $this->input->post('estado_civil'),
					'separado_de_hecho' => $this->input->post('separado_de_hecho'),
					'situacion_laboral' => $this->input->post('situacion_laboral'),
					'actividad' => $this->input->post('actividad'),
					'tipo_prevision' => $this->input->post('tipo_prevision'),
					'sistema_salud' => $this->input->post('sistema_salud'),
					'escolaridad' => $this->input->post('escolaridad'),
					'total_personas' => $this->input->post('total_personas'),
					'total_adultos' => $this->input->post('total_adultos'),
					'total_menores' => $this->input->post('total_menores'),
					'discapacitados' => $this->input->post('discapacitados'),
					'situacion_habitacional' => $this->input->post('situacion_habitacional'),
					'valor_dividendo' => $this->input->post('valor_dividendo'),
					'ingresos_1' => $this->input->post('ingresos_1'),
					'ingresos_2' => $this->input->post('ingresos_2'),
					'ingresos_3' => $this->input->post('ingresos_3'),
					'ingresos_total' => $this->input->post('ingresos_total'),
					'beneficios_estatales' => $this->input->post('beneficios_estatales'),
					'beneficio_estatal' => $this->input->post('beneficio_estatal')
				);
				$exito = $this->modelo_causa->actualizarDatosCliente($id_correlativa_causa, $datos_cliente);
			}
		}
		// TODO. ACTIVAR CUANDO LA VISTA EDITAR MUESTRE ORIENTACIONES VINCULADAS.
		//~ if($output['exito']) {
			//~ $this->modelo_causa->desvincularOrientaciones($id_correlativa_causa);
			//~ foreach ($ids_orientaciones as $id_or) {
				//~ $output['exito'] = $this->modelo_orientacion->vincularCausa($id_or, $id_correlativa_causa);
			//~ }
		//~ }
		echo json_encode([ 'exito'=>$exito ]);
	}

	// RESTRINGIDO POR USUARIO ACTUAL
	//TODO. CAMBIAR A USAR GET EN VEZ DE POST. (Me equivoque)
	public function vista_seleccionar() {
		$solo_vigentes = TRUE;
		$solo_con_rol = $this->input->post('solo_con_rol') ? TRUE : FALSE;

		$var = array();
		if($this->session->rol->nombre === 'alumno') {
			$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($this->session->rut, 'alumno', $solo_vigentes, $solo_con_rol);
		} else if(in_array($this->session->rol->nombre, ['profesor', 'director'])) {
			$var['causas'] = $this->modelo_causa->getCompletoPorUsuario($this->session->rut, 'abogado', $solo_vigentes, $solo_con_rol);
		} else if ($this->session->rol->causa_todo) {
			$var['causas'] = $this->modelo_causa->getCompleto($solo_vigentes, $solo_con_rol);
		}
		$var['funcion_callback'] = $this->input->post('funcion_callback');
		$this->load->view('busqueda/tabla_selector_causa', $var);
	}

	// UPDATE
	public function vincular_orientaciones($id_causa) {
		$this->load->model('modelo_orientacion');
		$exito = false;
		$ids_orientaciones = $this->input->post('ids_orientaciones');
		if($ids_orientaciones) {
			foreach ($ids_orientaciones as $id_or) {
				$exito = $this->modelo_orientacion->vincularCausa($id_or, $id_causa);
			}
		}
		echo json_encode([ 'exito'=>$exito ]);
	}

	// DELETE
	public function eliminar($id_causa) {
		$output = [ 'exito'=>false, 'mensaje'=>'No Autorizado' ];
		$causa = $this->modelo_causa->get($id_causa);
		if($causa) {
			$sesion_en_causa = in_array($this->session->rut, [ $causa->RUT_ALUMNO, $causa->RUT_ABOGADO ]);
			if($this->session->rol->causa_todo || ( $this->session->rol->causa_propio && $sesion_en_causa) ) {
				$confirm = $this->input->post('confirmacion');
				if($confirm) {
					$output = $this->modelo_causa->eliminar($id_causa);
				}
				else { $output = [ 'exito'=>true, 'mensaje'=>'Se borrará todo el historial, eventos, audiencias, orientaciones, trámites de la Causa.' ]; }
			}
		}
		echo json_encode($output);
	}
}
?>
