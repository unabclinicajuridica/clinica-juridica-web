<?php defined('BASEPATH') OR exit('No direct script access allowed');
	class Reporte extends CI_Controller {

		public function estadisticas_profesores()	{
			$this->load->model('modelo_busquedas');

			$var['cantidad_causas_materia'] = $this->modelo_busquedas->cantidad_causas_materia();
			$var['cantidad_causas_profesor'] = $this->modelo_busquedas->cantidad_causas_profesor();
			$var['cantidad_causas_causal'] = $this->modelo_busquedas->cantidad_causas_causal();
			$var['cantidad_agendaciones_profesor_h'] = $this->modelo_busquedas->cantidad_agendaciones_profesor_h();
			$var['cantidad_agendaciones_profesor_v'] = $this->modelo_busquedas->cantidad_agendaciones_profesor_v();
			$this->load->view('reporte/estadisticas_profesores',$var);
		}

	}
?>
