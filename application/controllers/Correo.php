<?php
   Class Correo extends CI_Controller {
      public function index() {
         $this->load->view('correo/lista_correos');
      }

      public function RevisarRespuestasEventos() {

         // Connect to gmail
         $imapPath = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
         $username = 'clinicajuridica.prueba@gmail.com';
         $password = 'clinica123';

         // try to connect
         $inbox = imap_open($imapPath,$username,$password) or die('Cannot connect to Gmail: ' . imap_last_error());

         // search and get unseen emails, function will return email ids
         $emails = imap_search($inbox, 'UNSEEN');
         if(!$emails) {
            echo "No han llegado nuevos mensajes :)";
            return;
         }

         $respuestasInvitaciones = array();

         // url: http://stackoverflow.com/a/23411065/3555293
         mb_internal_encoding('UTF-8');

         foreach($emails as $mail) {
            $respuestaInv = array();

            $evento = $this->_getMsgCalendario($inbox, $mail);
            // Confirma que existe un iCal.
            if (strpos($evento, 'BEGIN:VEVENT')) {
               // REGEXs para capturar valores del calendario.
               preg_match('/PARTSTAT=(.*?);/', $evento, $result);
               $respuestaInv['EVENTO_PARTSTAT'] = $result[1];

               preg_match('/UID:(.*?)[\s?\r?\n?]/', $evento, $result);
               $respuestaInv['EVENTO_UID'] = $result[1];
            } else {
               continue;
            }
            // $correo['imap_body'] = $this->getmsg($inbox, $mail);

            $respuestasInvitaciones[] = $respuestaInv;
         }

         // cierra la conexión.
         imap_expunge($inbox);
         imap_close($inbox);

         $this->load->model('modelo_googlecalendarapi');
         foreach ($respuestasInvitaciones as $respuesta) {
            echo $respuesta['EVENTO_UID'];
            echo $respuesta['EVENTO_PARTSTAT'];
            $this->modelo_googlecalendarapi->actualizarRespuestaEvento($respuesta['EVENTO_UID'], $respuesta['EVENTO_PARTSTAT']);
         }
      }

      private function _getMsgCalendario($mbox, $mid) {
         $estructura = imap_fetchstructure($mbox, $mid);
         $msgFinal = "";
         if (!isset($estructura->parts)) { // simple
            return $this->__getPartCalendario($mbox, $mid, $estructura, 0);  // pass 0 as part-number
         }
         else {  // multipart: cycle through each part
            foreach ($estructura->parts as $partno0=>$parte) {
               $msgContenido = $this->__getPartCalendario($mbox, $mid, $parte, $partno0+1);
               // if($msgContenido !== "NADA") {
               //    $msgFinal = $msgContenido;
               // }
               $msgFinal .= $msgContenido;
            }
            return $msgFinal;
         }
      }

      private function __getPartCalendario($mbox, $mid, $parte, $partno) {
         $plainCalendario = "";

         // DECODE DATA.Si $partno es 0 significa que no tiene multipartes.
         $data = ($partno) ? imap_fetchbody($mbox,$mid,$partno) : imap_body($mbox,$mid);

         // type 0 = 'text'
         if ( $parte->type == 0 && $data) {
            // MIME subtype = 'calendar'
            if ( $parte->subtype === 'CALENDAR' || strtolower($parte->subtype) === 'calendar' ) {

               if ($parte->encoding == 4) { $data = quoted_printable_decode($data); } // quoted-printable
               elseif ($parte->encoding == 3) { $data = base64_decode($data); } // Base64

               // $plainCalendario = "<strong> type=".$parte->type.", subtype=".$parte->subtype.", LOLOL:</strong><br>".$data ."<br><br>";
               $plainCalendario = $data;
               return $plainCalendario;
            }
         }

         // Si se llega hasta acá es porque no se encontró ningún calendario
         // aùn.
         if (isset($parte->parts)) {
            foreach ($parte->parts as $partno0=>$subpart) {
               $plainCalendario .= $this->__getPartCalendario($mbox,$mid,$subpart,$partno.".".($partno0+1));  // 1.2, 1.2.1, etc.
               if($plainCalendario !== "") {
                  return $plainCalendario;
               }
            }
         }
         return $plainCalendario;
      }

      private function iCalDecoder($eventoString) {
      //   $ical = file_get_contents($file);
        preg_match_all('/(BEGIN:VEVENT.*?END:VEVENT)/si', $eventoString, $result, PREG_PATTERN_ORDER);
        for ($i = 0; $i < count($result[0]); $i++) {
            $tmpbyline = explode("rn", $result[0][$i]);

            foreach ($tmpbyline as $item) {
                $tmpholderarray = explode(":",$item);
                if (count($tmpholderarray) >1) {
                    $majorarray[$tmpholderarray[0]] = $tmpholderarray[1];
                }
            }

            if (preg_match('/DESCRIPTION:(.*)END:VEVENT/si', $result[0][$i], $regs)) {
                $majorarray['DESCRIPTION'] = str_replace("  ", " ", str_replace("rn", "", $regs[1]));
            }
            $icalarray[] = $majorarray;
            unset($majorarray);

        }
        return $icalarray;
     }

}
?>
