<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orientacion extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('modelo_orientacion');
	}

	// Vista
	public function vista_ingreso() {
		$this->load->model('modelo_materias');
		$var['competencias'] = $this->modelo_materias->getCompetenciaConProcedimiento();
		if($this->session->rol->orientacion_todo) {
			$var['abogado_cambiable'] = true;
		} else if($this->session->rol->evaluar) {
			$var['abogado_cambiable'] = false;
			$var['rut_abogado'] = $this->session->rut;
		}

		$var['agendar'] = 0;
		$var['required_orientacion'] = TRUE;
		$var['required_materia'] = TRUE;
		$this->load->view('orientacion/ingreso_orientacion', $var);
	}

	public function vista_agendar_nueva() {
		$this->load->model('modelo_materias');
		$var = $this->input->get();
		$var['competencias'] = $this->modelo_materias->getCompetenciaConProcedimiento();
		$var['abogado_cambiable'] = true;
		$var['agendar'] = 1;
		$var['sede_actual'] = $this->session->sede_actual;
		$var['ancho'] = true;
		$var['required_orientacion'] = FALSE;
		$var['required_materia'] = FALSE;
		$this->load->view('orientacion/ingreso_orientacion', $var);
	}
	// Vista all
	public function vista_busqueda() {
		$var['orientaciones'] = $this->modelo_orientacion->get();
		$this->load->view('busqueda/buscador_orientaciones', $var);
	}

	// Entrega vista en donde el usuario activo,
	// si es Profesor o Director, puede ver sus Orientaciones
	// pendientes de escribir reseña.
	public function vista_orientaciones_sin_resena() {
		$solo_sin_resena = true;
		$rut_usuario = $this->session->rut;
		$var['orientaciones'] = $this->modelo_orientacion->getPorAbogado($rut_usuario, $solo_sin_resena);
		$var['rut_usuario'] = $rut_usuario;
		$this->load->view('orientacion/orientaciones_sin_resena', $var);
	}

	// modo_edicion: resena, completo, null
	// modo_privilegios: por_rut, por_tipo_usuario, null
	// resena: solo se edita la resena
	// completo: se puede editar todo
	// por_rut: se activa la edicion si el usuario actual esta vinculado a la orientacion
	// por_tipo_usuario: se activa la edicion si el usuario actual es Director o Admin
	// TODO. hacer uso de modo_edicion y modo_privilegios.
	public function vista_editar() {
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_materias');
		$this->load->model('modelo_causa');
		$rut_usuario = $this->session->rut;

		$id_orientacion = $this->input->get('id_orientacion');
		$modo_edicion = $this->input->get('modo_edicion');
		$modo_privilegios = $this->input->get('modo_privilegios');

		$var['rut_usuario'] = $rut_usuario;
		$var['orientacion'] = $this->modelo_agenda->getOrientacionPorIdCompleto($id_orientacion);
		$var['causa'] = $this->modelo_causa->get($var['orientacion']->id_causa);

		$var['competencias'] = $this->modelo_materias->getCompetenciaConProcedimiento();

		if($var['orientacion']->id_materia) {
			$materia = $this->modelo_materias->getMateria($var['orientacion']->id_materia);
			$var['materias'] = $this->modelo_materias->getPorProcedimiento($materia->id_procedimiento);
			$var['id_competencia'] = $materia->id_competencia;
			$var['id_procedimiento'] = $materia->id_procedimiento;
			$var['id_materia'] = $materia->id;
		} else {

		}

		$var['ancho'] = $this->input->get('ancho');

		$this->load->view('orientacion/editar', $var);
	}

	// Vista
	public function vista_seleccionar_orientacion() {
		$solo_sin_resena = FALSE;
		$solo_sin_causa = TRUE;
		if($this->session->rol->causa_todo) $var['orientaciones'] = $this->modelo_orientacion->getSinCausa();
		else if($this->session->rol->evaluar) $var['orientaciones'] = $this->modelo_orientacion->getPorAbogado($this->session->rut, $solo_sin_resena, $solo_sin_causa);
		else $var['orientaciones'] = array();

		$var['fila_clickable'] = true;
		$var['input_id_orientacion'] = $this->input->get('input_id_orientacion');
		$var['mostrar_vincular'] = false;
		$var['mostrar_lupas'] = false;
		$this->load->view('orientacion/tabla_orientaciones', $var);
	}

	// Vista
	public function vista_vincular_causa($id_orientacion) {
		$var['id_orientacion'] = $id_orientacion;
		$this->load->view('causa/dialogo_vincular', $var);
	}

	// GET + Vista tabla
	public function buscar() {
		$abogado = $this->input->get('abogado');
		$usuario = $this->input->get('usuario');
		$var['orientaciones'] = $this->modelo_orientacion->getPorNombres($abogado, $usuario);
		$var['mostrar_vincular'] = true;
		$var['mostrar_lupas'] = true;
		$this->load->view('orientacion/tabla_orientaciones', $var);
	}

	// INSERT
	public function ingresar() {
		$this->load->model('modelo_cliente');

		$sede = $this->session->id_sede_actual;

		$materia = $this->input->get('materia');
		// if(! $materia) {
		// 	echo json_encode( ['id_orientacion' => -1, 'mensaje'=>'Debe especificar Materia.'] );
		// 	return;
		// }
		$rut_profesor = $this->input->get('rut_abogado');
		$rut_cliente_full = $this->input->get('rut_cliente');
		if(! $rut_cliente_full) {
			echo json_encode( ['id_orientacion' => -1, 'mensaje'=>'Debe especificar Cliente.'] );
			return;
		}

		if(count(explode("-", $rut_cliente_full)) < 2) {
			echo json_encode(array('mensaje' => "Ingrese guión en el RUT del Usuario", 'id_orientacion' => -1, 'exito' => false));
			return;
		} else {
			list($rut_cliente, $dv_cliente) = explode("-", $rut_cliente_full);
		}

		$nombre_usuario = $this->input->get('nombre_usuario');
		$telefono = $this->input->get('telefono');
		$domicilio = $this->input->get('domicilio');
		$email = $this->input->get('email');
		$resena = $this->input->get('resena');
		$edad_usuario = $this->input->get('edad_usuario');

		$titulo_descriptivo = $this->input->get('titulo_evento');
		$descripcion = $this->input->get('descripcion_evento');
		$id_causa = $this->input->get('id_causa');
		// if(! $id_causa) {
		// 	echo json_encode( ['id_orientacion' => -1, 'mensaje'=>'Debe especificar Causa.'] );
		// 	return;
		// }

		$this->modelo_cliente->ingresar_cliente($rut_cliente, $dv_cliente, $nombre_usuario, $telefono, $domicilio, $email, $edad_usuario);

		// Se incluye el rut del usuario que realiza la orientacion. No discrimina por profesor o administrador.
		$id_orientacion = $this->modelo_orientacion->ingresar($titulo_descriptivo, $descripcion, $materia, $resena, $rut_profesor, $rut_cliente, $sede, $id_causa);
		echo json_encode(array('id_orientacion' => $id_orientacion));
	}

	// UPDATE
	//TODO. PASAR actualizarOrientacion A MODELO ORIENTACION
	public function actualizar() {
		$this->load->model('modelo_ingresos');
		$id_orientacion = $this->input->get('id_orientacion');
		$datos_array['id_causa'] = $this->input->get('id_causa');
		$datos_array['id_materia'] = $this->input->get('materia');
		$datos_array['resena'] = $this->input->get('resena');
		// $datos_array[''] = $this->input->get('');
		// $datos_array[''] = $this->input->get('');
		// $datos_array[''] = $this->input->get('');
		// $datos_array[''] = $this->input->get('');

		foreach ($datos_array as $key => $value) {
			$datos_array[$key] = $value !== '' ? $value : NULL;
		}

		$exito = $this->modelo_ingresos->actualizarOrientacion($id_orientacion, $datos_array);
		echo json_encode( [ 'exito'=>$exito ] );
	}
}

?>
