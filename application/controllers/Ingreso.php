<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingreso extends CI_Controller {

	function __construct() {
		parent::__construct();

		// esta_autorizado_helper.php
		if(esta_logueado($this) == false) {
			redirect('login');
		}

	  $this->load->model('modelo_ingresos');
	  $this->load->model('modelo_busquedas');
	}

	public function vista_asunto() {
		$var['abogado_cambiable'] = $this->session->rol->asignar_todo ? true : false;
		$var['rut_abogado'] = $this->session->rol->evaluar ? $this->session->rut : NULL;
		$var['fecha_editable'] = true;

		$this->load->view('agenda/ingreso_asunto', $var);
	}

	public function audiencias(){
		$this->load->view('audiencia/audiencias');
	}

	// Devuelve vista html de formulario para ingresar y agendar nueva Causa.
	public function vista_ingreso_causa() {
		$this->load->model('modelo_agenda');
		$this->load->model('modelo_causa');
		$this->load->model('modelo_materias');

		$this->db->select_max('id');
		$result= $this->db->get('causas')->row_array();
		$this->db->reset_query();

		$var['competencias'] = $this->modelo_materias->getCompetenciaConProcedimiento();
		$var['id_correlativa'] = $result['id'] + 1;

		$var['agendar'] = $this->input->get('agendar');
		$var['estado_agendar'] = $this->input->get('estado_agendar');

		$var['rut_profesor_a_cargar'] = $var['nombre_profesor_a_cargar'] = '';

		// Se provee abogado.
		if($this->input->get('profesor_a_cargar') !== null) {
			$this->load->model('modelo_usuario');
			$var['rut_profesor_a_cargar'] = $this->input->get('profesor_a_cargar');

			$profe_row = $this->modelo_usuario->getPorRut($var['rut_profesor_a_cargar']);
			$var['nombre_profesor_a_cargar'] = $profe_row->nombre;
		}
		// no se provee abogado, pero la sesión es un abogado.
		elseif ($this->session->rol->evaluar) {
			$var['rut_profesor_a_cargar'] = $this->session->rut;
			$var['nombre_profesor_a_cargar'] = $this->session->nombre_completo_user;
		}

		$var['abogado_cambiable'] = false;
		if($this->session->rol->causa_todo) {
			$var['abogado_cambiable'] = true;
		}

		$dia = $this->input->get('dia');
		$hora =  $this->input->get('hora');
		$tipo_agendacion = $this->input->get('tipo_agendacion');
		$var['fecha'] =  $this->input->get('dia');
		$var['hora'] =  $this->input->get('hora');
		$var['tribunales'] = $this->modelo_busquedas->tribunales();
		$var['nombre_usuario'] = $this->session->nombre_completo_user;
		$var['rut_usuario'] = $this->session->rut;
		$var['ancho'] = $this->input->get('ancho');

		$this->load->view('causa/ingreso_causa', $var);
	}

	public function mantenedor_bloques() {
		$id_sede_actual = $this->session->id_sede_actual;
		$sede_actual = $this->modelo_busquedas->getSedePorId($id_sede_actual);
		$var['nombre_sede_corto'] = $sede_actual->nombre_sede_corto;
		$this->load->view('mantenedor/mantenedor_bloques', $var);
	}

	public function cambio_correo() {
		$this->load->model('modelo_sesion');
		$email_sesion = $this->modelo_sesion->obtener_email();
		$data = array("email" => $email_sesion);
		$this->load->view('mantenedor/cambiar_correo', $data);
	}

	// POST INSERT
	public function cambiar_correo() {
		$email = $_POST['email'];
		if($_POST['email'] === "") {
			$result = "vacio";
		}
		else {
			//TODO. comprobar que el email es diferente al actual.
			//TODO. comprobar que el email no existe en la tabla usuarios.
			$this->load->model('modelo_sesion');
			$bool_result = $this->modelo_sesion->cambiar_email($email);
			if($bool_result) {
				$result = "ok";
			} else { $result = "error"; }
		}
		$array = array('result' => $result);
		header("Content-Type: text/plain");
		echo json_encode($array);
	}

	public function cambio_contrasena() {
		$this->load->view('mantenedor/cambiar_contrasena');
	}

	// POST INSERT
	public function cambiar_contrasena() {
		$nuevo_password = $_POST['nuevo_password'];
		if($_POST['nuevo_password'] === "") {
			$result = "vacio";
		}
		else {
			$this->load->model('modelo_sesion');
			$bool_result = $this->modelo_sesion->cambiar_contrasena($nuevo_password);
			if($bool_result) {
				$result = "ok";
			} else { $result = "error"; }
		}
		$array = array('result' => $result);
		header("Content-Type: text/plain");
		echo json_encode($array);
	}

	public function ingresar_conf_agenda() {
		$this->load->model('modelo_agenda');
		$sede_actual = $this->session->id_sede_actual;

		// Capturo datos
		$rut = $this->input->get('rut');
		$fecha_inicio_string = $this->input->get('fecha_inicio');
		$fecha_termino_string = $this->input->get('fecha_termino');
		$hora_inicio = $this->input->get('hora_inicio');
		$hora_termino = $this->input->get('hora_termino');
		$dia = $this->input->get('dia');

		// Convierto las fechas dia-mes-año a -> año-mes-dia
		$fecha_inicio = date('Y-m-d', strtotime($fecha_inicio_string));
		$fecha_termino = date('Y-m-d', strtotime($fecha_termino_string));
		//traigo inicio y fin de jornada
		$hora_termino_jornada = $this->modelo_agenda->traer_termino_atencion();
		$hora_inicio_jornada = $this->modelo_agenda->traer_inicio_atencion();
		// Las horas deben tener segundos para que la tabla las acepte.
		$hora_inicio = date("H:i:s",strtotime($hora_inicio));
		$hora_termino = date("H:i:s",strtotime($hora_termino));
		//comparo fechas y hora para el estado del ingresos

		$bool_result = false;
		$mensaje = "error desconocido";
		if ($fecha_inicio < $fecha_termino) {
			if ($hora_inicio_jornada <= $hora_inicio && $hora_termino_jornada >= $hora_termino) {
				if($hora_inicio < $hora_termino) {
					$bool_result = $this->modelo_ingresos->ingresarConfAgenda($rut,$fecha_inicio, $fecha_termino, $hora_inicio, $hora_termino,$dia, $sede_actual);
					$mensaje = "Actualización exitosa";
				} else {
					$mensaje = "La hora de término debe ser mayor a la hora de inicio";
				}
			} else {
				$mensaje = "El inicio de jornada empieza a las ".$hora_inicio_jornada." y termina a las ".$hora_termino_jornada;
			}
		} else {
			$mensaje = "El día de término debe ser mayor al día de inicio";
		}
		$array = array(
			'exito' => $bool_result,
			'mensaje' => $mensaje
		);
		header("Content-Type: text/plain");
		echo json_encode($array);
	}

	public function update_estado_asignacion() {
		$id_evento = $this->input->get('id_evento');
		$estado = $this->input->get('estado');
		$this->modelo_ingresos->up_estado_asignacion($id_evento, $estado);
	}

	// UPDATE
	public function update_termino_causa($id_correlativa_causa) {
		$this->load->model('modelo_causa');
		$id_causal_termino = $this->input->post('id_causal_termino');
		if( ! $id_causal_termino) {
			echo json_encode([ 'exito'=>false, 'mensaje'=>'¡Debe ingresar una Causal!' ]);
			return;
		}
		$fecha_termino = $this->input->post('fecha_termino_causa');
		$fecha_termino_formateada = date('Y-m-d', strtotime($fecha_termino));
		$exito = $this->modelo_causa->cambiarTermino($id_correlativa_causa, $id_causal_termino, $fecha_termino_formateada);
		echo json_encode([ 'exito'=>$exito ]);
	}

	public function update_sede_actual() {
		$sede_seleccionada = $this->input->get('sede_seleccionada');
		$this->session->id_sede_actual = $sede_seleccionada;
		$this->modelo_ingresos->actualiza_sede_actual($sede_seleccionada);
	}

	// Cada key del array es el nombre de una columna.
	//TODO. BORRAR Y USARLA CONTROLADOR ORIENTACION.
	public function actualizar_orientacion() {
		$array_datos = array();
		$id_orientacion = $this->input->get('id_orientacion');

		$resena = $this->input->get('resena');
		if($resena !== NULL) {
			$array_datos['resena'] = $resena;
		}
		$bool_exito = $this->modelo_ingresos->actualizarOrientacion($id_orientacion, $array_datos);
		echo json_encode($bool_exito);
	}

	// Cada key del array es el nombre de una columna.
	public function actualizar_asunto() {
		$array_datos = array();
		$id_asunto = $this->input->get('id_asunto');

		$observaciones = $this->input->get('observaciones');
		if($observaciones !== NULL) {
			$array_datos['observaciones'] = $observaciones;
		}
		$bool_exito = $this->modelo_ingresos->actualizarAsunto($id_asunto, $array_datos);
		echo json_encode($bool_exito);
	}

	public function ingresar_asunto() {
		$tipo_evento = 'asunto';
		$id_evento_agenda = null;
		$this->load->model('modelo_ingresos');
		$this->load->model('modelo_agenda');

		$titulo_evento = $this->input->get('titulo_evento');
		$descripcion_evento = $this->input->get('descripcion_evento');
		$rutAbogado = $this->input->get('rut_abogado');
		$observaciones = $this->input->get('observaciones');
		$id_causa = $this->input->get('id_causa');

		$sede = $this->session->id_sede_actual;

		$output['id_asunto'] = $this->modelo_ingresos->ingresar_asunto($titulo_evento, $descripcion_evento, $rutAbogado, $sede, $observaciones, $id_causa);

		// Se ingresa asignacion inmediatamente porque se requiere guardar la fecha y hora del asunto.
		if($output['id_asunto'] > 0) {
			$fecha_string = $this->input->get('fecha');
			$hora_inicio = $this->input->get('hora');
			$fecha = date('Y-m-d', strtotime($fecha_string)); // convierto la fecha a datatype date (para Base de Datos)
			$id_evento_agenda = $this->modelo_agenda->ingresar_asignacion($rutAbogado, $fecha, $output['id_asunto'], $hora_inicio, $tipo_evento, $titulo_evento, $descripcion_evento);
			$output['id_evento_agenda'] = $id_evento_agenda;
		}
		echo json_encode($output);
	}
}
?>
