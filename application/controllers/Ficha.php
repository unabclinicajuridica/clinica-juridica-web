<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Ficha extends CI_Controller {

		function __construct() {
			parent::__construct();

			// esta_autorizado_helper.php
			if(esta_logueado($this) == false) {
				redirect('login');
			}

			$this->load->model('modelo_busquedas');

			$this->load->helper(array("dompdf", "file"));
		}

		public function prueba_ficha() {
			$var['causa'] = array();
			$var['cliente'] = array();
			$var['datos_cliente'] = array();
			$html = $this->load->view('fichas/ficha_orientacion', $var, true);
			// $this->load->view('fichas/ficha_ingreso_causa', $var);
			// se usa la función del Helper:
			$data = pdf_create($html, 'ficha_ori_', true);
		}

		public function ingreso_causa($id_correlativa_causa) {
			$this->load->model('modelo_causa');
			$this->load->model('modelo_cliente');

			$causa = $this->modelo_causa->getDetalladaPorIdCorrelativa($id_correlativa_causa);
			$cliente = $this->modelo_cliente->get($causa->id_cliente);
			$datos_cliente = $this->modelo_causa->getDatosCliente($causa->id, $crear_if_null=TRUE);

			$var['causa'] = $causa;
			$var['cliente'] = $cliente;
			$var['datos_cliente'] = $datos_cliente;
			$html = $this->load->view('fichas/ficha_ingreso_causa', $var, true);
			// $this->load->view('fichas/ficha_ingreso_causa', $var);
			// se usa la función del Helper:
			$data = pdf_create($html, 'ficha_causa_'.$id_correlativa_causa, true);
		}

		public function orientacion($id_orientacion) {
			$this->load->model('modelo_orientacion');
			// $this->load->model('modelo_cliente');
			// $this->load->model('modelo_usuario');
			// $this->load->model('modelo_materia');

			$orient = $this->modelo_orientacion->get($id_orientacion);

			$var['fecha_ingreso'] = $orient->fec_ingreso;
			$var['nombre_cliente'] = $orient->nombre_cliente;
			$var['rut_cliente'] = $orient->rut_cliente;
			$var['telefonos_cliente'] = $orient->telefono_cliente;
			$var['domicilio_cliente'] = $orient->domicilio;
			$var['email_cliente'] = $orient->email_cliente;
			$var['materia_orientacion'] = $orient->nombre_materia;
			$var['resena'] = $orient->resena;
			$var['nombre_abogado'] = $orient->NOMBRE_ABOGADO;
			$html = $this->load->view('fichas/ficha_orientacion', $var, true);
			// $this->load->view('fichas/ficha_ingreso_causa', $var);
			// se usa la función del Helper:
			$data = pdf_create($html, 'ficha_orient_'.$id_orientacion, true);
		}
	}
?>
