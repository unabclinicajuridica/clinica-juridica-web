<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Audiencia extends CI_Controller {

		function __construct() {
			parent::__construct();

			// esta_autorizado_helper.php
			if(esta_logueado($this) == false) {
				redirect('login');
			}

			$this->load->model('modelo_audiencia');
			$this->load->model('modelo_busquedas');
		}

		// Vista multiples
		public function vista_busqueda_audiencias() {
			$rol_causa = $this->input->get('rol_causa');
			$this->load->model('modelo_audiencia');
			$var['rol_causa'] = $rol_causa;
			$var['audiencias'] = $this->modelo_audiencia->audienciasPorRolCausa($rol_causa);

			$this->load->view('audiencia/resultado_audiencia', $var);
		}

		// Vista
		// Muestra vista de ingreso audiencia con motivos de INGRESAR(editable)
		public function vista_ingreso_audiencia() {
			$this->load->model('modelo_causa');
			$this->load->library('My_Audiencia');

			$rol_causa = $this->input->get('rol_causa');
			$var['nombre_abogado'] = $var['rut_abogado'] = $var['nombre_alumno'] = $var['rut_alumno'] = '';
			if($rol_causa) {
				$var['causa'] = $this->modelo_causa->getDetalladaPorRol($rol_causa);
				$var['nombre_abogado'] = $var['causa']->NOMBRE_ABOGADO;
				$var['rut_abogado'] = $var['causa']->RUT_ABOGADO;
				$var['nombre_alumno'] = $var['causa']->NOMBRE_ALUMNO;
				$var['rut_alumno'] = $var['causa']->RUT_ALUMNO;
				$var['mostrar_selector_causa'] = FALSE;
			} else if($this->session->rol->evaluar) {

			}

			if($this->session->rol->audiencia_todo) {
				$var['mostrar_evaluacion'] = true;
				$var['seleccionar_abogado'] = true;
				$var['seleccionar_alumno'] = true;
			}

			// Regla de Negocio: Alumnos ('A') no pueden ingresar notas.
			else if($this->session->rol->evaluar) {
				$var['mostrar_evaluacion'] = true;
				$var['mostrar_comentar'] = true;
				$var['seleccionar_abogado'] = false;
				$var['seleccionar_alumno'] = true;
				$var['nombre_abogado'] = $this->session->nombre_completo_user;
				$var['rut_abogado'] = $this->session->rut;
			}
			else if($this->session->rol->audiencia_crear) {
				$var['mostrar_evaluacion'] = false;
				$var['mostrar_comentar'] = false;
				$var['seleccionar_abogado'] = true;
				$var['seleccionar_alumno'] = false;
			}
			else {
				redirect('paginas/no_autorizado');
			}

			$var['titulo_formulario'] = 'Ingreso Nueva Audiencia';
			$var['modo_detalle'] = false;
			$var['audiencia'] = new My_Audiencia();
			$var['mostrar_boton_confirmar_revision'] = false;
			$var['ancho'] = $this->input->get('ancho');
			$this->load->view('audiencia/ingreso_audiencia', $var);
		}

		// Vista
		// Devuelve vista de detalle de una audiencia.
		//TODO. Restringir a usuarios vinculados a la audiencia.
		public function vista_detalle($id_audiencia) {
			$audiencia = $this->modelo_audiencia->getPorId($id_audiencia);

			$var['mostrar_boton_confirmar_revision'] = FALSE;

			if($this->session->rol->letra === 'A') {
				$var['texto_boton_confirmar'] = 'Confirmar Lectura';
				if(in_array($audiencia->estado_revision, ['EVALUADA', 'LEIDA'])) {
					$var['mostrar_boton_confirmar_revision'] = TRUE;
					$bool_result = $this->modelo_audiencia->cambiarEstadoRevision($id_audiencia, 'LEIDA');
				}
			}
			// elseif ($this->session->rol->evaluar) {
			// 	$var['texto_boton_confirmar'] = 'Confirmar Lectura';
			// }

			$var['titulo_formulario'] = 'Detalles de la Audiencia';
			$var['ancho'] = $this->input->get('ancho');
			$var['audiencia'] = $audiencia;
			$var['comentario'] = $var['audiencia']->comentario_abogado;
			$var['modo_detalle'] = true;
			$var['seleccionar_abogado'] = false;
			$var['seleccionar_alumno'] = false;
			$var['mostrar_evaluacion'] = true;
			$var['mostrar_boton_salir'] = TRUE;
			$this->load->view('audiencia/ingreso_audiencia', $var);
		}

		// Vista
		public function vista_mis_audiencias() {
			$letra_rol_sesion = $this->session->rol->letra;
			$rut_sesion = $this->session->rut;
			$var['titulo'] = "Todas las Audiencias Asignadas";
			$var['titulo_vacio'] = "Usted no tiene Audiencias.";
			$var['funcion_exito'] = $this->input->get('funcion_exito');
			$estado_revision = $this->input->get('estado_revision');
			$evaluacion_completada = $this->input->get('evaluacion_completada');

			if (in_array( $letra_rol_sesion, ['D', 'P'])) {
				$tipo_usuario = 'abogado';
				$var['mostrar'] = 'Alumno';
				$var['evaluar'] = FALSE;
			}
			else if(in_array( $letra_rol_sesion, ['A'])) {
				$tipo_usuario = 'alumno';
				$var['mostrar'] = 'Abogado';
				$var['evaluar'] = FALSE;
			}

			if($evaluacion_completada) {
				$var['titulo'] = "Audiencias Evaluadas";
				$var['titulo_vacio'] = "Usted no tiene Audiencias Evaluadas.";
				$var['columna_nota'] = "Nota";
				$var['audiencias'] = $this->modelo_audiencia->getEvaluacionCompletada($rut_sesion, $tipo_usuario);
			}
			else if ($estado_revision) {
				if($estado_revision == 'evaluada') {
					$var['audiencias'] = $this->modelo_audiencia->getCompletoPorUsuario($rut_sesion, $tipo_usuario, 'EVALUADA');
					$var['titulo'] = "Audiencias Evaluadas";
					$var['titulo_vacio'] = "Usted no tiene Audiencias Evaluadas.";
					$var['columna_nota'] = "Nota";
				} else if ($estado_revision == 'en_espera_llenado') {
					$var['titulo'] = "Audiencias sin detalles rellenados";
					$var['titulo_vacio'] = "Usted no tiene Audiencias Incompletas.";
					$var['audiencias'] = $this->modelo_audiencia->getCompletoPorUsuario($rut_sesion, $tipo_usuario, 'EN_ESPERA_LLENADO');
					$var['rellenar_detalles'] = true;
					$var['evaluar'] = $tipo_usuario === 'abogado' ? TRUE : FALSE;
				} else {
					$var['audiencias'] = $this->modelo_audiencia->getCompletoPorUsuario($rut_sesion, $tipo_usuario);
				}
			}
			else {
				$var['rellenar_detalles'] = FALSE;
				$var['audiencias'] = $this->modelo_audiencia->getCompletoPorUsuario($rut_sesion, $tipo_usuario);
			}

			$this->load->view('audiencia/tabla_audiencias_evaluar', $var);
		}

		// View
		public function vista_audiencias_sin_evaluar() {
			$var['funcion_exito'] = $this->input->get('funcion_exito');
			// Confirma que posee permiso usando informacion de rol.
			$mostrar_vista = $this->session->rol->evaluar;
			$rut_user = $this->session->rut;
			$var['audiencias'] = $this->modelo_audiencia->getCompletoSinEvaluarPorUsuario($rut_user, 'abogado');
			if ($mostrar_vista) {
				$var['mostrar'] = 'Alumno';
				$var['evaluar'] = TRUE;
				$var['titulo'] = "Audiencias <b>Pendientes</b> de Evaluación";
				$this->load->view('audiencia/tabla_audiencias_evaluar', $var);
			} else {
				$this->load->view('no_autorizado');
			}
		}

		// Vista
		public function vista_editar() {
			$id_audiencia = $this->input->get('id_audiencia');
			$var['funcion_exito'] = $this->input->get('funcion_exito');

			// Confirma acceso a funcionalidad segun rol.
			$rol = $this->session->rol;
			$rut_user = $this->session->rut;

			$audiencia = $this->modelo_audiencia->getPorId($id_audiencia);
			$var['ancho'] = $this->input->get('ancho');
			$var['audiencia'] = $audiencia;
			$var['comentario'] = $audiencia->comentario_abogado;
			// El Usuario actual debe ser Director o Profesor, y debe
			// estar asignado a la Audiencia como asistente.
			$var['editar_detalles'] = $this->input->get('editar_detalles');
			if($rol->evaluar && $audiencia->rut_profesor === $rut_user) {
				$var['mostrar_comentar'] = true;

				$var['editar_notas'] = $this->input->get('editar_notas');
				$this->load->view('audiencia/edicion_audiencia', $var);
			} else if($rol->audiencia_propia && $audiencia->rut_alumno === $rut_user) {
				$var['mostrar_comentar'] = false;
				$var['editar_notas'] = false;
				$this->load->view('audiencia/edicion_audiencia', $var);
			} else {
				$this->load->view('no_autorizado');
			}
		}


		// Update
		public function actualizar_audiencia() {
			$id_audiencia = $this->input->get('id_audiencia');

			//~ $var = $this->input->get();
			$var['id_audiencia'] = $this->input->get('id_audiencia');
			$var['descripcion'] = $this->input->get('descripcion');
			$var['tipo_audiencia'] = $this->input->get('tipo_audiencia');

			if($var['descripcion'] && $var['tipo_audiencia']) {
				$var['estado_revision'] = 'PENDIENTE';
			}

			$evaluar_sin_notas = $this->input->get('chk_eval_sin_notas');
			if($evaluar_sin_notas != NULL) {
				$var['estado_revision'] = 'EVALUADA';
				$var['evaluacion_completada'] = TRUE;
			}

			$var['comentario_abogado'] = $this->input->get('nuevo_comentario');

			$var['nota_registro_1'] = $notas1[] = $this->input->get('nota_registro_1');
			$var['nota_registro_2'] = $notas1[] = $this->input->get('nota_registro_2');
			$var['nota_registro_3'] = $notas1[] = $this->input->get('nota_registro_3');
			$var['nota_registro_otros'] = $this->input->get('nota_registro_otros');
			$var['nota_destreza_1'] = $notas2[] = $this->input->get('nota_destreza_1');
			$var['nota_destreza_2'] = $notas2[] = $this->input->get('nota_destreza_2');
			$var['nota_destreza_3'] = $notas2[] = $this->input->get('nota_destreza_3');
			$var['nota_destreza_4'] = $notas2[] = $this->input->get('nota_destreza_4');
			$var['nota_destreza_5'] = $notas2[] = $this->input->get('nota_destreza_5');
			$var['nota_destreza_6'] = $notas2[] = $this->input->get('nota_destreza_6');
			$var['nota_destreza_7'] = $notas2[] = $this->input->get('nota_destreza_7');
			$var['nota_destreza_8'] = $notas2[] = $this->input->get('nota_destreza_8');
			$var['nota_destreza_9'] = $notas2[] = $this->input->get('nota_destreza_9');
			$var['nota_destreza_10'] = $notas2[] = $this->input->get('nota_destreza_10');
			$var['nota_destreza_11'] = $notas2[] = $this->input->get('nota_destreza_11');
			$var['nota_destreza_12'] = $notas2[] = $this->input->get('nota_destreza_12');
			$var['nota_item_3'] = $this->input->get('nota_item_3');

			//Calculos de notas
			$var['nota_item_1'] = round(array_sum($notas1)/3);
			$var['nota_item_2'] = round(array_sum($notas2)/12);
			$var['nota_final'] = round(($var['nota_item_1'] + $var['nota_item_2'] + $var['nota_item_3'])/3);

			//Remueve nulos
			foreach ($var as $key => $value) {
				 if($value ===  null) {
					unset($var[$key]);
				 }
			}
			$nota_final = null;
			$bool_exito = $this->modelo_audiencia->actualizarAudiencia($var);
			if($bool_exito) {
				$nota_final = $this->modelo_audiencia->tieneEvaluacionCompleta($var['id_audiencia']);
			}
			echo json_encode(array('exito'=>$bool_exito, 'nota_final'=>$nota_final));
		}

		// Update. Solo Alumnos Confirmar lectura. Abogados Evaluan con notas.
		function confirmar_lectura($id_audiencia) {
			$this->load->model('modelo_agenda');
			$exito = false; $mensaje='usuario o audiencia no válidos.';
			$rut_sesion = $this->session->rut;
			$audiencia = $this->modelo_audiencia->getPorId($id_audiencia);
			if($rut_sesion === $audiencia->rut_alumno && in_array($audiencia->estado_revision, ['EVALUADA', 'LEIDA'])) {
				$exito = $this->modelo_audiencia->cambiarEstadoRevision($id_audiencia, 'CERRADA');
				$mensaje = $exito ? 'lectura confirmada' : 'no se pudo confirmar lectura.';
			}
			// $rut_abogado = $audiencia->rut_profesor;
			// else if($rut_sesion === $rut_abogado && $audiencia->estado_revision === 'PENDIENTE') {
			// 	$exito = $this->modelo_audiencia->cambiarEstadoRevision($id_audiencia, 'EVALUADA');
			// }
			echo json_encode(['exito'=>$exito, 'mensaje'=>$mensaje]);
		}

		// INSERT
		function ingresar_audiencia() {
			$rol_causa = $this->input->get('rol_causa');
			$tipo_audiencia = $this->input->get('tipo_audiencia');
			$descripcion = $this->input->get('descripcion');
			$rut_alumno = $this->input->get('rut_alumno');
			$rut_profesor = $this->input->get('rut_profesor');

			$fecha = $this->input->get('fecha_audiencia');

			$fec_ingreso = date('Y-m-d', strtotime($fecha));
			$hora_ingreso = $this->input->get('hora_audiencia');

			$nuevo_comentario = $this->input->get('nuevo_comentario');

			$estado_revision = 'EN_ESPERA_LLENADO';
			if($descripcion && $tipo_audiencia) {
				$estado_revision = 'PENDIENTE';
			}

			$evaluar_sin_notas = $this->input->get('chk_eval_sin_notas');
			if($evaluar_sin_notas != NULL) {
				$estado_revision = 'EVALUADA';
			}

			$nota_item_1			= 0;
			$nota_item_2 			= 0;
			$nota_item_3 			= 0;
			$nota_final				= 0;

			//-- NOTAS REGISTRO --//

			$nota_registro_1 		=  $this->input->get('nota_registro_1');
			$nota_item_1			=  $nota_item_1 + $nota_registro_1;
			$nota_registro_2 		=  $this->input->get('nota_registro_2');
			$nota_item_1			=  $nota_item_1 + $nota_registro_2;
			$nota_registro_3 		=  $this->input->get('nota_registro_3');
			$nota_item_1			=  $nota_item_1 + $nota_registro_3;

			$nota_registro_otros 	=  $this->input->get('nota_registro_otros');

			$nota_item_1 			=  round(($nota_item_1)/3);
			$nota_final				=  $nota_final + $nota_item_1;

			//-- NOTAS DESTREZA --//

			$nota_destreza_1 		=  $this->input->get('nota_destreza_1');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_1;
			$nota_destreza_2 		=  $this->input->get('nota_destreza_2');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_2;
			$nota_destreza_3 		=  $this->input->get('nota_destreza_3');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_3;
			$nota_destreza_4 		=  $this->input->get('nota_destreza_4');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_4;
			$nota_destreza_5 		=  $this->input->get('nota_destreza_5');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_5;
			$nota_destreza_6 		=  $this->input->get('nota_destreza_6');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_6;
			$nota_destreza_7 		=  $this->input->get('nota_destreza_7');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_7;
			$nota_destreza_8 		=  $this->input->get('nota_destreza_8');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_8;
			$nota_destreza_9 		=  $this->input->get('nota_destreza_9');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_9;
			$nota_destreza_10 		=  $this->input->get('nota_destreza_10');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_10;
			$nota_destreza_11 		=  $this->input->get('nota_destreza_11');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_11;
			$nota_destreza_12 		=  $this->input->get('nota_destreza_12');
			$nota_item_2 			=  $nota_item_2 + $nota_destreza_12;

			$nota_item_2 			=  round(($nota_item_2)/12);
			$nota_final				=  $nota_final + $nota_item_2;

			//-- NOTA OTROS ASPECTOS --//

			$nota_item_3 			=  $this->input->get('nota_item_3');
			$nota_final				=  $nota_final + $nota_item_3;

			//-- NOTA FINAL --//

			$nota_final				=  round(($nota_final)/3);

			$sede = $this->session->id_sede_actual;

			$output['exito'] = $this->modelo_audiencia->ingresarAudiencia($descripcion, $rol_causa, $tipo_audiencia, $rut_profesor, $rut_alumno, $nota_registro_1,
													  $nota_registro_2, $nota_registro_3, $nota_registro_otros, $nota_destreza_1, $nota_destreza_2,
													  $nota_destreza_3, $nota_destreza_4, $nota_destreza_5, $nota_destreza_6, $nota_destreza_7,
													  $nota_destreza_8, $nota_destreza_9, $nota_destreza_10, $nota_destreza_11, $nota_destreza_12,
													  $nota_item_1, $nota_item_2, $nota_item_3, $nota_final, $fec_ingreso, $hora_ingreso, $sede,$estado_revision, $nuevo_comentario);

			$var['audiencias'] = $this->modelo_audiencia->audienciasPorRolCausa($rol_causa);
			$var['rol_causa'] = $rol_causa;
			$output['html'] = $this->load->view('audiencia/resultado_audiencia', $var, TRUE);

			echo json_encode($output);
		}
	}
?>
