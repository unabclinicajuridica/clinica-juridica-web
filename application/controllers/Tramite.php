<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tramite extends CI_Controller {
	function __construct() {
		parent::__construct();

		// esta_autorizado_helper.php
		if(esta_logueado($this) == false) {
			redirect('login');
		}

	  $this->load->model('modelo_tramite');
	  $this->load->model('modelo_busquedas');
	}

	// Vista
	public function vista_ingreso_tramite() {
		$this->load->view('tramite/ingreso_tramite', ['error'=>'']);
	}

	// Vista
	// Retorna Tramites de causas relacionadas con el usuario actual.
	// SÓLO Profesores / Directores.
	public function vista_tramites_pendientes() {
		$this->load->model('modelo_usuario');
		$rut_usuario = $this->session->rut;

		if($this->session->rol->evaluar) {
			$tramites = $this->modelo_tramite->getPorRutUsuario($rut_usuario);

			$this->load->helper("utilidades_helper");

			foreach($tramites as $key => $tram) {
				$tramites[$key]['nombre_alumno'] = $this->modelo_usuario->getNombrePorRut($tram['rut_usuario_alumno']);
				$tramites[$key]['archivos'] = $this->modelo_tramite->getArchivos($tram['id_tramite']);

				$tramites[$key]['estado_en_rojo'] = $tramites[$key]['estado_revision'] == 'PENDIENTE' ? true : false;
			}
			$var['tramites'] = $tramites;
			$this->load->view('tramite/tabla_tramites_pendientes', $var);
		}
	}

	// Vista
	//TODO. RESTRINGIR ACCESO SOLO A USUARIOS VINCULADOS A LA CAUSA.
	public function vista_tabla_tramites($id_correlativa_causa) {
		$this->load->model('modelo_usuario');
		$tramites = $this->modelo_tramite->getPorIdCausa($id_correlativa_causa);

		if($this->session->rol->letra == 'A') $campo_rojo = 'REVISADO';
		else $campo_rojo = 'PENDIENTE';

		$this->load->helper("utilidades_helper");

		foreach($tramites as $key => $tram) {
			$tramites[$key]['nombre_alumno'] = $this->modelo_usuario->getNombrePorRut($tram['rut_usuario_alumno']);
			$tramites[$key]['archivos'] = $this->modelo_tramite->getArchivos($tram['id_tramite']);

			$tramites[$key]['estado_en_rojo'] = $tramites[$key]['estado_revision'] == $campo_rojo ? true : false;
		}
		$var['tramites'] = $tramites;
		$this->load->view('tramite/tabla_tramites', $var);
	}

	// FILE UPLOAD + INSERT
	// Con datos del usuario actual.
	public function ingresar_tramite() {
		$datos_tramite['id_correlativa_causa'] = $this->input->post('id_correlativa_causa');
		$datos_tramite['descripcion'] = $this->input->post('descripcion');
		$datos_tramite['rut_usuario_alumno'] = $this->session->rut;

		$array_result = array('upload_data'=>'', 'exito'=>false, 'mensaje'=>'no se ingresó nada');

		$this->load->model('modelo_tramite');
		$id_tramite = $this->modelo_tramite->ingresar($datos_tramite);

		if($id_tramite != -1) {
			$array_result['exito'] = true;
			$array_result['mensaje'] = 'trámite ingresado';
			if($_FILES['archivos']['name'][0]) {
				$array_result = $this->modelo_tramite->uploadIngresarArchivos($id_tramite, 'archivos');
			}
		}
		echo json_encode($array_result);
	}

	// El comentario pertenece al usuario actual.
	// Regla Negocio: solo profesores/directores pueden comentar.
	// TODO. restringir a solo profesores de la causa asignada.
	public function ingresar_comentario()  {
		if($this->session->rol->evaluar) {
			$rut_usuario = $this->session->rut;
			$id_tramite = $this->input->post('id_tramite');
			$comentario = $this->input->post('comentario');
			$bool_exito = $this->modelo_tramite->ingresarComentario($id_tramite, $rut_usuario, $comentario);
			if($bool_exito) {
				$this->modelo_tramite->cambiarEstadoRevision($id_tramite, 'REVISADO');
			}
		} else {
			$bool_exito = false;
		}

		echo json_encode(['exito' => $bool_exito]);
	}

	// Vista
	//TODO. Restringir a usuarios vinculados a la causa del tramite.
	public function vista_detalle_tramite($id_tramite) {
		$var['tramite'] = $this->modelo_tramite->getPorId($id_tramite);
		$var['archivos'] = $this->modelo_tramite->getArchivos($id_tramite);
		$var['comentarios'] = $this->modelo_tramite->getComentarios($id_tramite);
		$var['mostrar_comentar'] = $this->session->rol->letra !== 'A' && $var['tramite']->estado_revision !== 'CERRADO' ? true : false;

		if($this->session->rol->evaluar) {
			$var['texto_boton_confirmar'] = 'Confirmar Revisión';
			if($var['tramite']->estado_revision !== 'CERRADO') {
				$var['mostrar_boton_confirmar_revision'] = true;
			}
		}
		else if($this->session->rol->letra === 'A') {
			$var['texto_boton_confirmar'] = 'Confirmar Lectura';
			if(in_array($var['tramite']->estado_revision, ['REVISADO', 'LEIDO'])) {
				$bool_result = $this->modelo_tramite->cambiarEstadoRevision($id_tramite, 'LEIDO');
				$var['mostrar_boton_confirmar_revision'] = true;
			}
		}

		$this->load->helper("utilidades_helper");
		$this->load->view('tramite/detalle_tramite', $var);
	}


	// Update - tramite. Es confirmadamente leido por el usuario actual.
	public function confirmar_lectura_tramite($id_tramite) {
		$this->load->model('modelo_causa');
		$rut_sesion = $this->session->rut;
		$tramite = $this->modelo_tramite->getPorId($id_tramite);
		$rut_abogado = $this->modelo_causa->getRutAbogado($tramite->id_causa);
		$bool_result = false;
		if($rut_sesion === $rut_abogado && $tramite->estado_revision === 'PENDIENTE') {
			$bool_result = $this->modelo_tramite->cambiarEstadoRevision($id_tramite, 'REVISADO');
		}
		else if($rut_sesion === $tramite->rut_usuario_alumno && $tramite->estado_revision !== 'PENDIENTE') {
			$bool_result = $this->modelo_tramite->cambiarEstadoRevision($id_tramite, 'CERRADO');
		}
		echo json_encode(['exito' => $bool_result]);
	}

}
?>
