<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function index() {
		if(esta_logueado($this)) {
			redirect('menu/menu_principal', 'menu_principal');
		}
		else {
			$this->load->view('inicio');
		}

	}

	public function menu_principal() {
		$this->load->model('modelo_privilegios');

		$var['rol'] = $this->session->rol;
		$var['nombre'] = $this->session->nombre_completo_user;
		$var['sede'] = $this->modelo_privilegios->sede_usuario();
		$var['cantidad_sedes'] = $this->modelo_privilegios->cantidad_sedes();
		$var['timeoutSesion'] = getTimeoutDeSesion($this);
		$this->load->view('menu_principal', $var);
	}

	public function cerrar_sesion() {
		// $this->load->view('logout');
		$this->session->sess_destroy();
		redirect('menu/index');
	}

	public function slider() {
		$this->load->view('slider');
	}

	public function login() {
		$usuario = $this->input->post('username');
		$clave = $this->input->post('password');
		$this->load->model('modelo_sesion');
		$output = $this->modelo_sesion->login($usuario, $clave);
		echo json_encode($output);
	}

}
