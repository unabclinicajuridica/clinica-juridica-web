<?php

class modelo_tramite extends CI_Model {


	// ******************************* //
	// *********** TRAMITE *********** //
	// ******************************* //

	// GET
	public function getPorId($id_tramite) {
		return $this->db->get_where('tramite', ['id_tramite' => $id_tramite])->row();
	}

	// GET multiple
	// Retorna ARRAY.
	public function getPorIdCausa($id_correlativa_causa) {
		return $this->db->get_where('tramite', ['id_causa' => $id_correlativa_causa])->result_array();
	}

	// GET multiple
	public function getPorRutUsuario($rut_usuario, $tipo_usuario='abogado') {
		return $this->db
			->select('tramite.*, causas.id_causa as rol_causa')
			->join('causas', 'tramite.id_causa = causas.id')
			->where('causas.rut_abogado', $rut_usuario)
			->where('tramite.estado_revision', 'PENDIENTE')
			->get('tramite')
			->result_array();
	}

	// GET multiple
	public function getComentarios($id_tramite) {
		return $this->db->get_where('tramite_comentario', ['id_tramite' => $id_tramite])->result();
	}

	// INSERT
	// Retorna id de la fila insertada.
	// Fecha y hora se calculan aquí.
	public function ingresar(array $datos_tramite) {
		$fecha_actual = date('Y-m-d');
		$hora_actual = date('H:i');

		if($datos_tramite['id_correlativa_causa'] == '' || $datos_tramite['rut_usuario_alumno'] == '' || $datos_tramite['descripcion'] == '') return -1;

		$data = array(
			'fecha_ingreso' => $fecha_actual,
			'hora_ingreso' => $hora_actual,
			'id_causa' => $datos_tramite['id_correlativa_causa'],
			'rut_usuario_alumno' => $datos_tramite['rut_usuario_alumno'],
			'descripcion' => $datos_tramite['descripcion']
		);
		$exito_bool = $this->db->insert('tramite', $data);
		if($exito_bool) return $this->db->insert_id();
		else return -1;
	}

	// INSERT
	public function ingresarComentario($id_tramite, $rut_usuario, $comentario) {
		$nombre_usuario = $this->db->select('nombre')->where('rut', $rut_usuario)->get('usuarios')->row()->nombre;

		$data = array(
			'id_tramite' => $id_tramite,
			'rut_usuario' => $rut_usuario,
			'nombre_usuario' => $nombre_usuario,
			'comentario' => $comentario
		);
		$exito_bool = $this->db->insert('tramite_comentario', $data);
		return $exito_bool;
	}

	// UPDATE
	public function cambiarEstadoRevision($id_tramite, $estado) {
		$this->load->helper("configuracion_helper");
		if(estadoRevisionTramiteEsValido($estado)) {
			$bool_result = $this->db->where('id_tramite', $id_tramite)->update('tramite', ['estado_revision' => $estado]);
		} else {
			$bool_result = false;
		}
		return $bool_result;
	}

	// DELETE
	public function eliminarPorCausa($id_causa) {
		$exito = $this->db->where('id_causa', $id_causa)->delete('tramite');
		return $exito;
	}


	// ****************************** //
	// ********** ARCHIVOS ********** //
	// ****************************** //

	// GET multiple
	public function getArchivo($id_archivo) {
		return $this->db->get_where('archivos', ['id' => $id_archivo])->row();
	}

	// GET multiple
	// Retorna ARRAY.
	public function getArchivos($id_tramite) {
		return $this->db->get_where('archivos', ['id_tramite' => $id_tramite])->result_array();
	}

	// TODO. BORRAR Y UTILIZAR LO QUE YA EXISTE EN MODELO_ARCHIVO.
	// UPLOAD + INSERT (acción atómica)
	// Guarda el archivo en el disco e ingresa fila en tabla Archivos
	public function uploadIngresarArchivos($id_tramite, $parametro_archivos) {
		$rut_alumno = $this->getPorId($id_tramite)->rut_usuario_alumno;
		$folder_to_save = './uploads/'.$rut_alumno.'/';

		if (!is_dir($folder_to_save)) {
			 mkdir($folder_to_save, 0777, true);
		}

		$config['upload_path']        = $folder_to_save;
		$config['allowed_types']      = 'gif|jpg|png|doc|txt|jpeg|xls|docx|pdf|zip|7zip|xlsx';
		$config['max_size']           = 5000;
		$config['max_width']          = 0;
		$config['max_height']         = 0;
		$config['multi']              = 'all';

		$this->load->library('upload', $config);

		$result = array();
		$result['exito'] = true;

		if ( ! $this->upload->do_upload($parametro_archivos)) {
			$result['mensaje'] = $this->upload->display_errors();
			$result['exito'] = false;
		}
		else {
			$result['mensaje'] = 'archivo unico subido sin insertar en fila';
			$result['upload_data'] = $upload_data = $this->upload->data();
			// UN ARCHIVO
			if(isset($upload_data['file_name'])) {
				$exito_bool = $this->_ingresarArchivo($id_tramite, $upload_data);
				if( !($exito_bool)) $result['exito'] = false;
				else $result['mensaje'] = 'archivo unico subido e insertado en fila';
			}

			// MULTIPLES ARCHIVOS
			else {
				$result['mensaje'] = 'Sólo se subieron los archivos.';
				foreach($upload_data as $ud) {
					$exito_bool = $this->_ingresarArchivo($id_tramite, $ud);
					if( !($exito_bool)) $result['exito'] = false;
					else $result['mensaje'] = 'Trámite ingresado.';
				}
			}
		}
		return $result;
	}

	// DELETE + DELETE
	// Borra archivos del disco, luego borra las filas.
	public function eliminarArchivos($id_tramite) {
		$archivos = $this->getArchivos($id_tramite);
		foreach ($archivos as $key => $archv) {
			unlink($archv->file_path.''.$archv->file_name);
			$exito = ! file_exists($archv->file_path.''.$archv->file_name);
		}
		if($exito) $exito = $this->db->where('id_tramite', $id_tramite)->delete('archivos');
		return $exito;
	}

	// TODO. BORRAR Y UTILIZAR LO QUE YA EXISTE EN MODELO_ARCHIVO.
	// INSERT privado (acción atómica)
	// Ingresa fila en tabla Archivos.
	private function _ingresarArchivo($id_tramite, $upload_data) {
		$data = array(
			'id_tramite' => $id_tramite,
			'file_ext' => $upload_data['file_ext'],
			'file_type' => $upload_data['file_type'],
			'file_path' => $upload_data['file_path'],
			'orig_name' => $upload_data['orig_name'],
			'file_name' => $upload_data['file_name'],
			'file_size' => $upload_data['file_size']
		);
		$exito_bool = $this->db->insert('archivos', $data);
		return $exito_bool;
	}

}
?>
