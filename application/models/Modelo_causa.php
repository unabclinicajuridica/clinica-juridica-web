<?php

class modelo_causa extends CI_Model {
	// ---
	// TIPOS DE CAMBIOS EN HISTORIAL_CAUSAS:
	// 'eliminacion', 'modificacion' o 'terminacion'
	// Sin acento.
	// ---

	function __construct() {
		parent::__construct();
		$this->load->helper('configuracion');
	}

	//GET
	public function getPorRol($rol_causa) {
		$causa = $this->db->get_where('causas', array('rol_causa' => $rol_causa), 1)->row();
		return $causa;
	}

	// GET
	public function get($id_correlativa_causa) {
		$causa = $this->db->where('id', $id_correlativa_causa)->get('causas', 1)->row();
		return $causa;
	}

	// GET
	public function getFullJoin($id_correlativa_causa) {
		$this->db->join('materia', 'materia.id = causas.id_materia', 'left');
		$this->db->join('clientes', 'clientes.id = causas.id_cliente', 'left');
		$this->db->where('causas.id', $id_correlativa_causa);
		$query = $this->db->get('causas');
		return $query->row();
	}

	// GET
	public function getDetalladaPorIdCorrelativa($id_correlativa_causa) {
		return $this->_getDetallada([ 'causas.id'=>$id_correlativa_causa ]);
	}

	// GET
	public function getDetalladaPorRol($rol_causa) {
		return $this->_getDetallada([ 'causas.rol_causa'=>$rol_causa ]);
	}

	// GET All
	public function getCompleto($solo_vigentes=false, $solo_con_rol=false, $limite=false) {
		$this->_prepararQueryCompleta();
		$this->db->order_by('INGRESO', 'DESC');
		$query = is_int($limite) ? $this->db->get('causas', $limite) : $this->db->get('causas');
		return $query->result();
	}

	// GET rut
	public function getRutAbogado($id_correlativa_causa) {
		return $this->db->select('RUT_ABOGADO')->get_where('causas', ['id' => $id_correlativa_causa], 1)->row()->RUT_ABOGADO;
	}

	// GET
	// Busca por id de la Causa. id de causa es UNIQUE.
	public function getDatosCliente($id_causa, $crear_if_null=FALSE)
	{
		$datos_c = $this->db->where('id_causa', $id_causa)->get('causa_datos_cliente', 1)->row();
		if($crear_if_null && ! $datos_c) {
			$id_datos_c = $this->ingresarDatosCliente([ 'id_causa'=>$id_causa ]);
			$datos_c = $this->db->where('id', $id_datos_c)->get('causa_datos_cliente', 1)->row();
		}
		return $datos_c;
	}

	//GET Multiple
	public function getPorUsuario($rut_usuario, $tipo_usuario) {
		$nombre_columna = getColumnaDeCausa($tipo_usuario);
		$multiples_causas = $this->db->get_where('causas', array($nombre_columna => $rut_usuario))->result();
		return $multiples_causas;
	}

	//GET Multiple
	// Entrega causas filtradas por usuario y por vigencia.
	// También puede entregar SOLO las que tengan rol.
	public function getCompletoPorUsuario($rut, $tipo_usuario, $solo_vigentes=false, $solo_con_rol=false) {
		$nombre_columna = getColumnaDeCausa($tipo_usuario);
		return $this->_getDetalladaMultiple([ 'causas.'.$nombre_columna=>$rut ], $solo_vigentes, $solo_con_rol);
	}

	//GET Multiple
	// Entrega causas excepto las del abogado especificado.
	public function getCompletoExcepto($rut, $tipo_usuario, $solo_vigentes=false, $solo_con_rol=false) {
		$nombre_columna = getColumnaDeCausa($tipo_usuario);
		return $this->_getDetalladaMultiple([ 'causas.'.$nombre_columna.' !='=>$rut ], $solo_vigentes, $solo_con_rol);
	}

	// GET multiple
	public function getPorCliente($rut_cliente, $solo_vigentes=false) {
		$solo_con_rol = FALSE;
		return $this->_getDetalladaMultiple(['clientes.rut_cliente'=>$rut_cliente], $solo_vigentes, $solo_con_rol);
	}

	// GET multiple
	public function getPorBusquedaNombre($id_correlativa_causa, $rol_causa, $nombre_abogado, $nombre_alumno, $nombre_cliente, $solo_vigentes, $solo_sin_alumno=FALSE) {
		$solo_con_rol = FALSE;
		$terminos = array(
			'causas.rol_causa'=>$rol_causa,
			'abogado.nombre'=>$nombre_abogado,
			'clientes.nombre_cliente'=>$nombre_cliente,
			'causas.id'=>$id_correlativa_causa
		);
		if($solo_sin_alumno) $this->db->where('alumno.nombre', NULL);
		else $terminos['alumno.nombre'] = $nombre_alumno;
		$terminos = array_filter($terminos);
		if(count($terminos)) $this->db->like($terminos);
		return $this->_getDetalladaMultiple(array(), $solo_vigentes, $solo_con_rol);
	}

	// GET multiple
	public function getPorBusquedaRUT($id_correlativa_causa, $rol_causa, $rut_abogado, $rut_alumno, $rut_cliente, $solo_vigentes, $solo_sin_alumno=FALSE) {
		$solo_con_rol = FALSE;
		$terminos = array(
			'causas.rol_causa'=>$rol_causa,
			'abogado.rut'=>$rut_abogado,
			'clientes.rut_cliente'=>$rut_cliente,
			'causas.id'=>$id_correlativa_causa
		);
		if($solo_sin_alumno) $this->db->where('alumno.rut', NULL);
		else $terminos['alumno.rut'] = $rut_alumno;
		$terminos = array_filter($terminos);
		if(count($terminos)) $this->db->like($terminos);
		return $this->_getDetalladaMultiple(array(), $solo_vigentes, $solo_con_rol);
	}

	// GET multiple
	public function getPorBusquedaHistoricaNombre($id_correlativa_causa, $rol_causa, $nombre_abogado, $nombre_alumno, $nombre_cliente) {
		$this->db->where('causas.TERMINO !=', NULL);
		return $this->getPorBusquedaNombre($id_correlativa_causa, $rol_causa, $nombre_abogado, $nombre_alumno, $nombre_cliente, $solo_vigentes=FALSE, $solo_sin_alumno=FALSE);
	}

	// GET multiple
	public function getPorBusquedaHistoricaRUT($id_correlativa_causa, $rol_causa, $rut_abogado, $rut_alumno, $rut_cliente) {
		$this->db->where('causas.TERMINO !=', NULL);
		return $this->getPorBusquedaRUT($id_correlativa_causa, $rol_causa, $rut_abogado, $rut_alumno, $rut_cliente, $solo_vigentes=FALSE, $solo_sin_alumno=FALSE);
	}

	// GET multiple
	// Causas con alguno de sus datos nulos o vacíos.
	public function getIncompletasPorUsuario($rut_usuario, $tipo_usuario) {
		$nombre_columna = getColumnaDeCausa($tipo_usuario);
		$this->db
			->where($nombre_columna, $rut_usuario)
			->group_start()
				->or_where(array('causas.rol_causa'=>NULL, 'causas.id_materia'=>NULL, 'causas.RUT_ALUMNO'=>NULL, 'causas.ID_TRIBUNAL'=>NULL, 'causas.RUT_CLIENTE'=>null, 'clientes.nombre_cliente'=>NULL, 'clientes.telefono'=>NULL, 'clientes.domicilio'=>NULL, 'clientes.email'=>NULL, 'clientes.edad'=>NULL) )
			->group_end();
		return $this->_getDetalladaMultiple(array(), $solo_vigentes=FALSE, $solo_con_rol=FALSE);
	}

	// UPDATE
	// Afecta solo causas Vigentes (sin término, o con termino en fecha futura)
	public function cambiarAbogadoEnVigentes($id_causa, $rut_profesor, $reemplazar_all) {
		$dv_profesor = $this->db->get_where('usuarios', array('rut' => $rut_profesor), 1)->row()->dv;
		$this->db->reset_query();
		$rut_original = $this->db->get_where('causas', array('id' => $id_causa), 1)->row()->RUT_ABOGADO;
		$this->db->reset_query();

		if($reemplazar_all === 'true') {
			$this->db->where('RUT_ABOGADO', $rut_original);
		}
		else {
			$this->db->where('id', $id_causa);
		}

		$this->db->group_start()
						->where('TERMINO', NULL)
						->or_where('TERMINO >', date("Y-m-d"))
					->group_end();

		$datos = array('RUT_ABOGADO' => $rut_profesor, 'DV_ABOGADO' => $dv_profesor);
		$exito = $this->db->update('causas', $datos);
		return $exito;
	}

	// UPDATE
	// Afecta solo causas Vigentes (sin término, o con termino en fecha futura)
	public function cambiarAlumnoEnVigentes($id_causa, $rut_nuevo, $reemplazar_all) {
		$dv_nuevo = $this->db->get_where('usuarios', array('rut' => $rut_nuevo), 1)->row()->dv;
		$this->db->reset_query();
		$rut_original = $this->db->get_where('causas', array('id' => $id_causa), 1)->row()->RUT_ALUMNO;
		$this->db->reset_query();

		// PERARANDO DATOS HISTORIAL
		if($reemplazar_all === 'true') {
			$causas = $this->getPorUsuario($rut_original, 'alumno');
		}
		else {
			$causas = array( $this->get($id_causa) );
		}
		$this->db->reset_query();


		if($reemplazar_all === 'true') {
			$this->db->where('RUT_ALUMNO', $rut_original);
		}
		else {
			$this->db->where('id', $id_causa);
		}

		$this->db->group_start()
						->where('TERMINO', NULL)
						->or_where('TERMINO >', date("Y-m-d"))
					->group_end();

		$datos = array('RUT_ALUMNO' => $rut_nuevo, 'DV_ALUMNO' => $dv_nuevo);
		$exito = $this->db->update('causas', $datos);

		//INSERTANDO DATOS HISTORIAL.
		// se usa insecion masiva (foreach en array) aunque sea
		// reemplazo simple.
		if($exito === true) {
			$muchas_filas = array();
			foreach($causas as $ca) {
				$muchas_filas[] = array('id_causa'=>$ca->id, 'tipo_cambio'=>'modificacion', 'nombre_columna'=>'RUT_ALUMNO', 'valor_previo'=>$ca->RUT_ALUMNO, 'valor_nuevo'=>$rut_nuevo);
				$muchas_filas[] = array('id_causa'=>$ca->id, 'tipo_cambio'=>'modificacion', 'nombre_columna'=>'DV_ALUMNO', 'valor_previo'=>$ca->DV_ALUMNO, 'valor_nuevo'=>$dv_nuevo);
			}
			$this->ingresar_historial_masivo($muchas_filas);
		} //FIN INSERCION

		return $exito;
	}

	// UPDATE
	public function cambiarTermino($id_correlativa_causa, $id_causal_termino, $fecha_termino) {
		$data = array(
		'TERMINO' => $fecha_termino,
		'CAUSAL_TERMINO' => $id_causal_termino
		);

		// PERARANDO DATOS HISTORIAL
		$la_causa = $this->get($id_correlativa_causa);
		$termino_previo = $la_causa->TERMINO;
		$causal_previo = $la_causa->CAUSAL_TERMINO;

		$this->db->set( $data );
		$this->db->where ( 'id', $id_correlativa_causa );
		$exito = $this->db->update( 'causas', $data );

		//INSERTANDO DATOS HISTORIAL
		if($exito === true) {
			$this->ingresar_historial_causa($id_correlativa_causa, 'terminacion', 'TERMINO', $termino_previo, $fecha_termino);
			$this->ingresar_historial_causa($id_correlativa_causa, 'terminacion', 'CAUSAL_TERMINO', $causal_previo, $id_causal_termino);
		}

		return $exito;
	}

	// UPDATE
	public function desvincularOrientaciones($id_correlativa_causa) {
		$exito = $this->db->where('id_causa', $id_correlativa_causa)->update('orientacion', ['id_causa'=>NULL]);
		return $exito;
	}

	// UPDATE
	public function actualizar($id_causa, $rol_causa, $id_materia, $rut_alumno, $rut_abogado, $rut_cliente, $dv_abogado, $dv_alumno, $dv_cliente, $id_cliente, $id_tribunal) {
		$datos = array('rol_causa'=>$rol_causa, 'id_materia'=>$id_materia,
							'RUT_ALUMNO'=>$rut_alumno, 'RUT_ABOGADO'=>$rut_abogado,
							'RUT_CLIENTE'=>$rut_cliente, 'DV_ABOGADO'=>$dv_abogado,
							'DV_ALUMNO'=>$dv_alumno, 'DV_CLIENTE'=>$dv_cliente,
							'id_cliente'=>$id_cliente, 'ID_TRIBUNAL'=>$id_tribunal);
		array_filter($datos);
		return $this->db->where('id', $id_causa)->update('causas', $datos);
	}

	public function actualizarDatosCliente($id_causa, $datos_por_columna)
	{
		$datos_por_columna = array_filter($datos_por_columna, function($v) {
		    return $v || $v == 0;
		});
		return $this->db->where('id_causa', $id_causa)->update('causa_datos_cliente', $datos_por_columna);
	}


	// INSERT
	// Devuelve array con: boolean 'exito', string 'mensaje', int 'id_correlativa_causa'.
	// Esta funcion valida los valores segun las reglas (profesor no nulo, etc)
	public function ingresar_causa($rol_causa, $id_materia, $fecha_ingreso, $rut_alumno,
		$rut_abogado, $rut_cliente, $dv_abogado, $dv_alumno, $dv_usuario, $id_cliente, $id_tribunal) {
		$sede = $this->session->id_sede_actual;

		// Permite insertar NULL en columna en vez de string vacío.
		if($rol_causa === '') $rol_causa = NULL;
		if($id_tribunal === '0') $id_tribunal = NULL;

		// Revisa tabla por si se repite la id de causa
		$this->db->where('rol_causa !=', NULL);
		$this->db->where('rol_causa', $rol_causa);
		$this->db->from('causas');
		$num_repeticiones = $this->db->count_all_results();
		$this->db->reset_query();

		$salida = ['mensaje' => "nada", 'id_correlativa_causa' => NULL, 'exito' => false];

		if($num_repeticiones == 0) {
			$datos = array('rol_causa'=>$rol_causa, 'id_materia'=>$id_materia, 'ingreso'=>$fecha_ingreso, 'RUT_ALUMNO'=>$rut_alumno,
			'RUT_CLIENTE'=>$rut_cliente, 'RUT_ABOGADO'=>$rut_abogado, 'DV_ALUMNO'=>$dv_alumno, 'DV_CLIENTE'=>$dv_usuario,
			'DV_ABOGADO'=>$dv_abogado, 'SEDE'=>$sede, 'id_cliente'=>$id_cliente, 'ID_TRIBUNAL'=>$id_tribunal);

			$salida['exito'] = $this->db->insert('causas', $datos);
			if ($salida['exito'] === true) {
				$salida['id_correlativa_causa'] = $this->db->insert_id();
			}
		} else {
			$salida['mensaje'] = "Ya existe una Causa con esa Id";
		}

		return $salida;
	}

	// INSERT
	// RETORNA ID INSERTADA.
	public function ingresarDatosCliente($datos_por_columna)
	{
		$datos_por_columna = array_filter($datos_por_columna);
		$exito = $this->db->insert('causa_datos_cliente', $datos_por_columna);
		if($exito) { return $this->db->insert_id(); }
		else { return 0; }
	}

	// DELETE
	//TODO. CONTINUAR AQUI
	public function eliminar($id_correlativa_causa) {
		$output = ['exito'=>false, 'mensaje'=>'error'];

		// obtener la causa
		$causa = $this->get($id_correlativa_causa);

		// si no existe, no hacer nada y retornar EXITO pero con mensaje 'NO EXISTE'
		if( ! $causa) { $output['mensaje'] = 'no se encontró dicha causa ('.$id_correlativa_causa.')'; return $output; }

		$this->db->trans_start();

		// eliminar agendaciones de la causa sin aviso
		$this->load->model('modelo_agenda');
		$output['exito'] = $this->modelo_agenda->eliminarEventosPorCausa($id_correlativa_causa);

		// eliminar audiencias de la causa
		if($causa->rol_causa) {
			$this->load->model('modelo_audiencia');
			$output['exito'] = $this->modelo_audiencia->eliminarPorCausa($causa->rol_causa);
		}

		// eliminar historial de la causa
		$output['exito'] = $this->eliminarHistorial($id_correlativa_causa);

		// eliminar orientaciones de la causa
		$this->load->model('modelo_orientacion');
		$output['exito'] = $this->modelo_orientacion->eliminarOrientacionesPorCausa($id_correlativa_causa);

		// eliminar tramites de la causa
		// eliminar cada archivo revisando tabla filas con tramite de la causa
		$this->load->model('modelo_tramite');
		$tramites = $this->modelo_tramite->getPorIdCausa($id_correlativa_causa);
		foreach ($tramites as $key => $tram) {
			$output['exito'] = $this->modelo_tramite->eliminarArchivos($tram->id);
		}
		// luego cada fila de tabla archivos con tramite de la causa

		// luego los tramites con id de la causa
		$output['exito'] = $this->modelo_tramite->eliminarPorCausa($id_correlativa_causa);
		// LA CAUSA.
		$output['exito'] = $this->db->where('id',$id_correlativa_causa)->delete('causas');

		$this->db->trans_complete();

		return $output;
	}

	public function eliminarHistorial($id_correlativa_causa) {
		$exito = $this->db->where('id_causa', $id_correlativa_causa)->delete('historial_causas');
		return $exito;
	}


	//INSERT PRIVATE
	private function ingresar_historial_causa($id_correlativa_causa, $tipo_cambio, $nombre_columna, $valor_previo, $valor_nuevo) {
		$dt_ahora = new DateTime("now");
		$fecha_cambio = $dt_ahora->format('Y-m-d');
		$hora_cambio = $dt_ahora->format('H:i:s');

		$data = array (
			'id_causa' => $id_correlativa_causa,
			'fecha_cambio' => $fecha_cambio,
			'hora_cambio' => $hora_cambio,
			'tipo_cambio' => $tipo_cambio,
			'nombre_columna' => $nombre_columna,
			'valor_previo' => $valor_previo,
			'valor_nuevo' => $valor_nuevo,
		);

		$this->db->insert('historial_causas', $data);
	}

	//INSERT MASIVO PRIVATE
	private function ingresar_historial_masivo(array $muchas_filas) {
		$dt_ahora = new DateTime("now");
		$fecha_cambio = $dt_ahora->format('Y-m-d');
		$hora_cambio = $dt_ahora->format('H:i:s');

		foreach($muchas_filas as $index => $ca) {
			$muchas_filas[$index]['fecha_cambio'] = $fecha_cambio;
			$muchas_filas[$index]['hora_cambio'] = $hora_cambio;
		}

		$num_rows = $this->db->insert_batch('historial_causas', $muchas_filas);
		return $num_rows !== FALSE ? true : false;
	}

	// Private
	private function _getDetallada($filtro) {
		$this->_prepararQueryCompleta();
		if($filtro && count($filtro)) $this->db->where($filtro);
		$causa = $this->db->get('causas')->row();
		return $causa;
	}

	// Private
	private function _getDetalladaMultiple(array $filtro, $solo_vigentes, $solo_con_rol) {
		$this->_prepararQueryCompleta();

		if($filtro && count($filtro)) $this->db->where($filtro);

		if($solo_vigentes) $this->db->where("causas.TERMINO IS NULL");
		if($solo_con_rol) $this->db->where("causas.rol_causa IS NOT NULL");

		$causas = $this->db->get('causas')->result();
		return $causas;
	}

	// Private.
	// SIEMPRE SE FILTRA POR SEDE SIEMPRE. SIEMPRE. (a menos que cambie)
	private function _prepararQueryCompleta() {
		$sede = $this->session->id_sede_actual;
		$this->db->select("causas.*, DATE_FORMAT(causas.INGRESO,'".getFormatoFechaMySQL()."') AS INGRESO, alumno.nombre as NOMBRE_ALUMNO, abogado.nombre as NOMBRE_ABOGADO, materia.nombre_materia, clientes.rut_cliente, clientes.dv_cliente, clientes.nombre_cliente, cauter.nom_causal, tribunal.nombre AS NOMBRE_TRIBUNAL");
		$this->db->select("(select count(*) from audiencias where audiencias.rol_causa = causas.rol_causa) as cantidad_audiencias");
		$this->db->select("(select count(*) from evento_agenda where evento_agenda.id_causa = causas.id) as cantidad_agendaciones");
		$this->db->select("(select count(*) from orientacion where orientacion.id_causa = causas.id) as cantidad_orientaciones");
		$this->db->select("(select count(*) from tramite where tramite.id_causa = causas.id) as cantidad_tramites");
		$this->db->join('materia', 'materia.id = causas.id_materia', 'left');
		$this->db->join('causal_termino as cauter', 'cauter.id_causal_termino = causas.CAUSAL_TERMINO', 'left');
		$this->db->join('usuarios as abogado', 'abogado.rut = causas.RUT_ABOGADO', 'left');
		$this->db->join('usuarios as alumno', 'alumno.rut = causas.RUT_ALUMNO', 'left');
		$this->db->join('clientes', 'clientes.id = causas.id_cliente', 'left');
		$this->db->join('tribunal', 'causas.ID_TRIBUNAL=tribunal.id', 'left');
		$this->db->where('causas.sede', $sede);
	}
}
?>
