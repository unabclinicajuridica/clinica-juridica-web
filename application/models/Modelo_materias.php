<?php

class modelo_materias extends CI_Model {

	//GET
	public function getMateria($id_materia) {
		$materia = $this->db->get_where('materia', ['id'=>$id_materia])->row();
		return $materia;
	}

	//GET
	public function getIdProcedimiento($id_materia) {
		$id_procedimiento = $this->db->select('id_procedimiento')->get_where('materia', ['id'=>$id_materia])->row()->id_procedimiento;
		return $id_procedimiento;
	}
	//GET
	public function getCompetencias() {
		$competencias = $this->db->order_by('nombre', 'ASC')->get('competencia')->result();
		return $competencias;
	}

	//GET
	public function getProcedimientos() {
		$procedimientos = $this->db->order_by('nombre', 'ASC')->get('procedimiento')->result();
		return $procedimientos;
	}

	//GET
	public function getMaterias() {
		$materias = $this->db->order_by('nombre_materia', 'ASC')->get('materia')->result();
		return $materias;
	}

	//GET Multiple
	public function getCompetenciaProcedimientoMaterias() {
		$out['competencias'] = $this->getCompetencias();
		$out['procedimientos'] = $this->getProcedimientos();
		$out['materias'] = $this->getMaterias();
		return $out;
	}

	//GET Multiple
	// Devuelve Todas las Competencias con objeto Procedimiento para cada una.
	public function getCompetenciaConProcedimiento() {
		$competencias = $this->db->get('competencia')->result_array();
		foreach ($competencias as $key => $value) {
			$competencias[$key]['procedimientos'] = $this->db->order_by('nombre', 'ASC')->get_where('procedimiento', [ 'id_competencia'=>$value['id'] ])->result();
			$competencias[$key] = (object) $competencias[$key];
		}
		return $competencias;
	}

	//GET Multiple
	// Devuelve Todas Competencias y Todos Procedimientos.
	public function getCompetenciaYProcedimiento() {
		$out['competencias'] = $this->getCompetencias();
		$out['procedimientos'] = $this->getProcedimientos();
		return $out;
	}

	//GET Multiple
	public function getPorProcedimiento($id_procedimiento) {
		$materias = $this->db->order_by('nombre_materia', 'ASC')->get_where('materia', ['id_procedimiento'=>$id_procedimiento])->result();
		return $materias;
	}

	//GET Multiple
	public function getProcedimientosPorCompetencia($id_competencia) {
		$procedimientos = $this->db->get_where('procedimiento', ['id_competencia'=>$id_competencia])->result();
		return $procedimientos;
	}
}
?>
