<?php

	class modelo_googlecalendarapi extends CI_Model {

      public function getClienteSecret(){
			$query = $this->db->query("SELECT json_string FROM google_calendar_api WHERE nombre = 'CLIENT_SECRET';");
			return $query->row()->json_string;
		}

      public function getTokens(){
			$query = $this->db->query("SELECT json_string FROM google_calendar_api WHERE nombre = 'TOKENS';");
			return $query->row()->json_string;
		}

		public function updateTokens($nuevasCredenciales){
			$query = $this->db->query("UPDATE google_calendar_api SET json_string = '$nuevasCredenciales' WHERE nombre = 'TOKENS';");
		}


		// partstat = Participation Status.
		public function actualizarRespuestaEvento($icaluid, $partstat_evento) {
			$this->partstat_evento = $partstat_evento;

			// Declined indica rechazo, plt se reinicia el estado de rechazo.
			if($partstat_evento == 'DECLINED') {
				$this->estado_rechazo = NULL;
			}
			$this->db->update('evento_agenda', $this, array('icaluid_evento' => $icaluid));
		}

		// UPDATE
		public function actualizarEstadoRechazo($icalid_evento, $estado_rechazo) {
			$this->estado_rechazo = $estado_rechazo;
			$exito = $this->db->update('evento_agenda', $this, array('icalid_evento' => $icalid_evento));
			return $exito;
		}

	}

?>
