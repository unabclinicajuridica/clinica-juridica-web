<?php

class modelo_agenda extends CI_Model {


	// GET
	public function eliminarEventosPorCausa($id_correlativa_causa) {
		$exito = $this->db->where('id_causa', $id_correlativa_causa)->delete('evento_agenda');
	}

	// DELETE
	public function eliminar_bloque_horario_abogado($id_bloque_horario_abogado) {
		$query = $this->db->where('id_conf_agenda', $id_bloque_horario_abogado)->delete('conf_agenda');
	}


	public function traer_bloques(){

		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		$query = $this->db->query("SELECT
			A.id_bloque,
			CONCAT(A.inicio, '-', A.termino) AS BLOQUE,
			A.horario,
			A.lunes,
			A.martes,
			A.miercoles,
			A.jueves,
			A.viernes,
			A.sabado,
			A.domingo,
			A.orden
		FROM
			bloque A
		WHERE sede = '$sede';
		");
		return $query;
	}


	// Entrega la hora de termino de atención más tardío de la clinica
	// de la sede actual.
	public function traer_termino_atencion(){
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		$bloques = $this->db->select('termino')
			->where(['tipo_horario' => 'D', 'sede' => $sede])
			->get('bloques')->result();

		$hora_termino = '00:00';
		foreach ($bloques as $bloque) {
			if($bloque->termino > $hora_termino) {
				$hora_termino = $bloque->termino;
			}
		}
		return $hora_termino;
	}


	// Entrega la hora de inicio de atención más temprana de la clinica
	// de la sede actual.
	public function traer_inicio_atencion() {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		$bloques = $this->db->select('inicio')
			->where(['tipo_horario' => 'D', 'sede' => $sede])
			->get('bloques')->result();

		$hora_inicio = '23:59';
		foreach ($bloques as $bloque) {
			if($bloque->inicio < $hora_inicio) {
				$hora_inicio = $bloque->inicio;
			}
		}

		return $hora_inicio;
	}


	public function asignaciones($dia_sf, $hora_inicio, $hora_fin, $dia_texto, $profesor) {
		// Indica si el calendario es Global o Personal.
		// El Global es el que se muestra por defecto, muestra el horario
		// de todos los profesores. El Personal es un filtro de horario.
		$esCalPersonal = false;

		$dia = date('Y-m-d', strtotime($dia_sf));
		$asignacion['dia'] = $dia;
		$asignacion['hora_inicio'] = $hora_inicio;
		$asignacion['hora_fin'] = $hora_fin;
		$asignacion['hay_rechazados'] = false;

		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		$filtro_profesores = array('fecha_inicio <=' => $dia, 'fecha_fin >=' => $dia, 'dia' => $dia_texto, 'hora_inicio <=' => $hora_inicio, 'hora_fin >=' => $hora_fin, 'sede' => $sede);
		$filtro_ocupados = array('fecha_asignacion' => $dia, 'hora_inicio <=' => $hora_inicio, 'hora_fin >=' => $hora_fin, 'sede' => $sede);

		if( $profesor ) {
			$filtro_profesores['rut'] = $profesor;
			$filtro_ocupados['rut'] = $profesor;
			$esCalPersonal = true;
		}

		$cantidad = $this->db->from('conf_agenda')->where($filtro_profesores)->count_all_results();

		// X = cantidad de profesores disponibles para asignacion en ese bloque
		if($cantidad === 0) {
			$asignacion['cantidad_disponible'] = 0;
			$asignacion['cantidad_no_disponible'] = 0;
			$asignacion['tipo'] = '#f9f9f9';
			$asignacion['id'] = 'ND';
			return $asignacion; //no disponible (blanco)
		}

		// $cantidad_ocupados = $this->db->from('evento_agenda')->where($filtro_ocupados)->count_all_results();
		$ocupados = $this->db->from('evento_agenda')->where($filtro_ocupados)->get();
		$cantidad_ocupados = $ocupados->num_rows();

		if($cantidad_ocupados > 0) {
			$diferencia = $cantidad - $cantidad_ocupados;
			$asignacion['cantidad_disponible'] = $diferencia;
			$asignacion['cantidad_no_disponible'] = $cantidad_ocupados;
			$asignacion['tipo'] = '#F9EF90'; //contiene ocupado (amarillo)


			if($esCalPersonal) {
				//TODO. que pasa cuando cambia estado_rechazo.
				if($ocupados->row()->partstat_evento === 'DECLINED' && $ocupados->row()->estado_rechazo === NULL) {
					$asignacion['tipo'] = '#FF6347'; //rechazada
					$asignacion['hay_rechazados'] = true;
				} elseif ($ocupados->row()->partstat_evento === 'ACCEPTED') {
					$asignacion['tipo'] = '#00FF7F'; //aceptada
				} else {
					$asignacion['tipo'] = '#F9EF90'; //asignada pero sin respuesta
				}
			//TODO. arreglar discrepanciasa y refactorizar.
			} else {
				if($ocupados->row()->partstat_evento === 'DECLINED' && $ocupados->row()->estado_rechazo === NULL) {
					$asignacion['hay_rechazados'] = true;
				}
			}
			$asignacion['id'] = 'O';
		}
		else {
			$asignacion['cantidad_disponible'] = $cantidad;
			$asignacion['cantidad_no_disponible'] = 0;
			$asignacion['tipo'] = '#A9D0F5'; // no contiene ocupados (celeste)
			$asignacion['id'] = 'L';
		}

		return $asignacion;
	}


	public function cantidad_asignaciones($fecha_inicio, $fecha_fin) {

		$fecha_inicio = date('Y-m-d', strtotime($fecha_inicio));
		$fecha_fin = date('Y-m-d', strtotime($fecha_fin));
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		//$fecha_inicio = date('Y-m-d', strtotime('23-05-2016'));
		//$fecha_fin = date('Y-m-d', strtotime('27-05-2016'));

		$sql_cantidad_asignaciones = $this->db->select('COUNT(*)')
			->where('evento_agenda.rut', 'usuarios.rut', FALSE)
			->where(['evento_agenda.fecha_asignacion >='=>$fecha_inicio, 'evento_agenda.fecha_asignacion <='=>$fecha_fin])
			->get_compiled_select('evento_agenda', TRUE);
		$query = $this->db->select('DISTINCT(usuarios.nombre), usuarios.rut, ('.$sql_cantidad_asignaciones.') as cantidad')
			->join('conf_agenda', 'conf_agenda.rut = usuarios.rut', 'left')
			// rol 2 = PROFESOR , rol 4 = DIRECTOR
			->where_in('usuarios.id_rol', [2, 4])
			->where(['conf_agenda.fecha_inicio <='=>$fecha_inicio, 'conf_agenda.fecha_fin >='=>$fecha_fin, 'usuarios.sede'=>$sede])
			->get('usuarios');

		 if($query->num_rows() > 0){
			 return $query->result();
		 }

		 else {
			return null;
		 }
	}

	// Puede ser ASUNTO o CAUSA o AUDIENCIA o ORIENTACION
	// Retorna la ID de la nueva evento_agenda insertada.
	public function ingresar_asignacion($rut, $fecha, $id_del_evento, $hora_inicio, $tipo_evento, $titulo_evento, $descripcion_evento) {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;
		$fecha = date('Y-m-d', strtotime($fecha));

		$titulo_evento = $titulo_evento === "" ? NULL : $titulo_evento;
		$descripcion_evento = $descripcion_evento === "" ? NULL : $descripcion_evento;

		// REGLA DE NEGOCIO. los bloques duran 30 minutos.
		$hora_termino = date('H:i:s', strtotime('+30 minutes', strtotime($hora_inicio)));
		$columna_para_id; // la columna para insertar id varía.
		switch ($tipo_evento) {
			case "causa":
				$columna_para_id = 'id_causa';
				break;
			case "audiencia":
				//TODO. arreglo temporal.
				$columna_para_id = 'id_audiencia';
				break;
			case "asunto":
				$columna_para_id = 'id_asunto';
				break;
			case "orientacion":
				$columna_para_id = 'id_orientacion';
				break;
		}
		$datos = array('rut' => $rut, 'fecha_asignacion' => $fecha, $columna_para_id => $id_del_evento,
			'hora_inicio' => $hora_inicio, 'hora_fin' => $hora_termino,
			'tipo_asunto' => $tipo_evento, 'estado' => 'Agendado',
			'sede' => $sede, 'titulo_evento' => $titulo_evento,
			'descripcion_evento' => $descripcion_evento);
		$exito = $this->db->insert('evento_agenda', $datos);

		//TODO. COMPROBAR DUPLICADOS!!! EJ CLAVE: 2017-06-02-13:30:00-12128457
		// $duplicado='';

		if ($exito === true) {
			$INSERT_ID = $this->db->insert_id();
			return $INSERT_ID;
		}
		// else if($duplicado) {
		// 	return -5;
		// }
		else {
			return 0;
		}
	}

	// Entrega lista de Profesores disponibles de la sede actual.
	// Especificando el día y el bloque de tiempo.
	public function getProfesoresDisponiblesEnBloque($fecha, $hora_inicio, $hora_termino, $dia_texto) {
		$fecha_query = date('Y-m-d', strtotime($fecha));

		$sede = $this->session->id_sede_actual;

		$sql_count_asignaciones = $this->db
			->select('count(*)')
			->from('evento_agenda')
			// Al comparar con campo en vez de variable, se desactivan
			// las comillas de escape con FALSE.
			->where('evento_agenda.rut', 'usuarios.rut', FALSE)
			->where([
				'evento_agenda.fecha_asignacion' => $fecha_query,
				'evento_agenda.hora_inicio <=' => $hora_inicio,
				'evento_agenda.hora_fin >=' => $hora_termino
				])
			->get_compiled_select();
		//TODO. Sanitizar $sede.
		$sql_sedes_usuario = $sede.' in (SELECT id_sede FROM usuario_sede WHERE usuario_sede.rut = usuarios.rut)';

		$query = $this->db
			->select('usuarios.rut, usuarios.nombre, usuarios.dv, ('.$sql_count_asignaciones.') AS cantidad', false)
			->from('usuarios')
			->join('conf_agenda', 'conf_agenda.rut = usuarios.rut')
			->where($sql_sedes_usuario)
			// rol 2 = PROFESOR , rol 4 = DIRECTOR
			->where_in('usuarios.id_rol', [2, 4])
			->where([
				'conf_agenda.fecha_inicio <=' => $fecha_query,
				'conf_agenda.fecha_fin >=' => $fecha_query,
				'conf_agenda.dia' => $dia_texto,
				'conf_agenda.hora_inicio <=' => $hora_inicio,
				'conf_agenda.hora_fin >' => $hora_inicio,
				'conf_agenda.hora_fin >=' => $hora_termino,
				'conf_agenda.hora_inicio <' => $hora_termino
			])
			->having('cantidad',  0) //VERY IMPORTANT MY FRIEND. CANTIDAD=0 == SIN AGENDACIONES EN EL BLOQUE == DISPONIBLE.
			->get();
		return $query->result();
	}

	public function eliminar_evento($id_evento_agenda, $motivo, $quien_elimina) {
		$this->db->trans_start();
		$this->db->query("INSERT INTO  evento_agenda_historico (id_evento_agenda ,rut ,fecha_asignacion ,id_asunto ,hora_inicio ,hora_fin ,tipo_asunto ,estado)
									SELECT id, rut, fecha_asignacion, id_asunto, hora_inicio, hora_fin,  tipo_asunto, estado FROM evento_agenda WHERE id = '$id_evento_agenda';");
		$this->db->query("UPDATE evento_agenda_historico SET motivo = '$motivo', quien_elimina = '$quien_elimina' where id_evento_agenda = '$id_evento_agenda'");
		$this->db->query("DELETE FROM evento_agenda WHERE id = '$id_evento_agenda'");
		$this->db->trans_complete();
		return $this->db->trans_status();
	}

	// soloConEstadoRechazoNulo: Si es True, indica que solo se quieren
	// las asignaciones rechazadas que no tengan estado_rechazo, o sea,
	// que aun no se gestiona su accion de rechazo.
	public function asignaciones_rechazadas($sin_estado_rechazo=FALSE) {
		$filtro['partstat_evento'] = 'DECLINED';
		if($sin_estado_rechazo) $filtro['estado_rechazo'] = NULL;

		$this->db->select('usuarios.*, evento_agenda.*, evento_agenda.id as id_evento');
		$this->db->from('evento_agenda');
		$this->db->join('usuarios', 'usuarios.rut = evento_agenda.rut');
		$this->db->where($filtro);
		$asignaciones = $this->db->get()->result();
		return $asignaciones;
	}

	public function getAsignacionAgendaPorId($id_evento) {
		$asignacion = $this->db->get_where('evento_agenda', ['id'=>$id_evento], 1)->row();
		return $asignacion;
	}

	// devuelve asunto por id.
	public function getAsuntoPorId($id) {
		$query = $this->db->get_where('asuntos', array('id_asunto' => $id), 1);
		return $query->row();
	}

	// Entrega fila de orientación + nombres de usuario y abogado
	// y ruts completos
	public function getOrientacionPorIdCompleto($id_orientacion) {
		$this->load->model('modelo_busquedas');
		//~ $sede = $this->session->id_sede_actual;
		$query = $this->db->query("
			SELECT *, C.nombre_cliente AS USUARIO,
			(SELECT
				 M.nombre_materia
				 FROM materia M
				 WHERE M.id = A.id_materia
			 ) AS MATERIA,
			(SELECT
				 d.nombre
				 FROM  usuarios d
				 WHERE d.rut = A.RUT_ABOGADO
			 ) AS ABOGADO,
			C.rut_cliente AS RUT_USUARIO,
			CONCAT(C.rut_cliente, '-', C.dv_cliente) AS RUT_USUARIO_COMPLETO,
			(SELECT
				 CONCAT(d.rut, '-', d.dv)
				 FROM usuarios d
				 WHERE d.rut = A.RUT_ABOGADO
			 ) AS RUT_ABOGADO_COMPLETO,
			(SELECT
				 d.rut
				 FROM usuarios d
				 WHERE d.rut = A.RUT_ABOGADO
			 ) AS RUT_ABOGADO
			FROM orientacion A
			JOIN clientes C ON C.rut_cliente = A.RUT_USUARIO
			WHERE A.id_orientacion = $id_orientacion
		");
		return $query->row();
	}

	// Entrega fila de orientación, nada más.
	public function getOrientacionPorIdSimple($id_orientacion) {
		$orientacion = $this->db->get_where('orientacion', array('id_orientacion' => $id_orientacion), 1);
		return $orientacion->row();
	}
}
?>
