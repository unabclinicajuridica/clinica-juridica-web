<?php

class modelo_audiencia extends CI_Model {

	//GET
	public function getPorId($id_audiencia) {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;

		$this->load->helper('configuracion');
		$query = $this->db->query("SELECT A.*, DATE_FORMAT(A.fecha, '".getFormatoFechaMySQL()."') AS fecha, Alum.nombre, Abog.nombre as nombre_abogado FROM audiencias A
			LEFT OUTER JOIN usuarios Alum ON Alum.rut = A.rut_alumno
			LEFT OUTER JOIN usuarios Abog ON Abog.rut = A.rut_profesor
			WHERE A.id_audiencia = ".$id_audiencia." AND A.sede = ".$sede.";");

		return $query->row();
	}

	//GET
	public function audienciasPorRolCausa($rol_causa) {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;
		$query = $this->db->get_where('audiencias', array('rol_causa' => $rol_causa, 'sede' => $sede));
		return $query;
	}

	//GET
	public function getPorUsuario($rut_usuario, $tipo_usuario) {
		$nombre_columna = $this->_getColumnaPorTipoUsuario($tipo_usuario);
		$multiples_audiencias = $this->db
			->where($nombre_columna, $rut_usuario)
			->get('audiencias')->result();
		return $multiples_audiencias;
	}

	//GET
	public function getCompletoPorUsuario($rut_usuario, $tipo_usuario, $estado_revision=NULL) {
		$nombre_columna = $this->_getColumnaPorTipoUsuario($tipo_usuario);
		$where_filtro = array($nombre_columna => $rut_usuario);
		if($estado_revision) {
			$where_filtro['estado_revision'] = $estado_revision;
		}

		$this->load->helper('configuracion');
		$multiples_audiencias = $this->db
			->select("audiencias.*, DATE_FORMAT(audiencias.fecha, '".getFormatoFechaMySQL()."') AS fecha, usralu.nombre AS nombre_alumno, usrabo.nombre AS nombre_abogado")
			->join('usuarios usralu', 'audiencias.rut_alumno = usralu.rut', 'left')
			->join('usuarios usrabo', 'audiencias.rut_profesor = usrabo.rut', 'left')
			->where($where_filtro)
			->get('audiencias')->result();
		return $multiples_audiencias;
	}

	//GET
	public function getCompletoSinEvaluarPorUsuario($rut_usuario, $tipo_usuario) {
		$nombre_columna = $this->_getColumnaPorTipoUsuario($tipo_usuario);

		$this->load->helper('configuracion');
		$multiples_audiencias = $this->db
			->select("audiencias.*, DATE_FORMAT(audiencias.fecha, '".getFormatoFechaMySQL()."') AS fecha, usralu.nombre AS nombre_alumno, usrabo.nombre AS nombre_abogado")
			->join('usuarios usralu', 'audiencias.rut_alumno = usralu.rut', 'left')
			->join('usuarios usrabo', 'audiencias.rut_profesor = usrabo.rut', 'left')
			->where($nombre_columna, $rut_usuario)
			// ->where('evaluacion_completada', 0)
			->where_in('estado_revision', ['EN_ESPERA_LLENADO', 'PENDIENTE'])
			->get('audiencias')->result();
		return $multiples_audiencias;
	}

	//GET
	public function getEvaluacionCompletada($rut_usuario, $tipo_usuario) {
		$nombre_columna = $this->_getColumnaPorTipoUsuario($tipo_usuario);

		$this->load->helper('configuracion');
		$multiples_audiencias = $this->db
			->select("audiencias.*, DATE_FORMAT(audiencias.fecha, '".getFormatoFechaMySQL()."') AS fecha, usralu.nombre AS nombre_alumno, usrabo.nombre AS nombre_abogado")
			->join('usuarios usralu', 'audiencias.rut_alumno = usralu.rut', 'left')
			->join('usuarios usrabo', 'audiencias.rut_profesor = usrabo.rut', 'left')
			->where($nombre_columna, $rut_usuario)
			->where_in('evaluacion_completada', TRUE)
			->get('audiencias')->result();
		return $multiples_audiencias;
	}

	//INSERT
	public function ingresarAudiencia($descripcion, $rol_causa, $tipo_audiencia, $rut_profesor, $rut_alumno, $nota_registro_1,
												  $nota_registro_2, $nota_registro_3, $nota_registro_otros, $nota_destreza_1, $nota_destreza_2,
												  $nota_destreza_3, $nota_destreza_4, $nota_destreza_5, $nota_destreza_6, $nota_destreza_7,
												  $nota_destreza_8, $nota_destreza_9, $nota_destreza_10, $nota_destreza_11, $nota_destreza_12,
												  $nota_item_1, $nota_item_2, $nota_item_3, $nota_final, $fec_ingreso, $hora_ingreso, $sede,$estado_revision, $comentario_abogado) {

		$num_audiencia = $this->db->where('rol_causa', $rol_causa)->count_all_results('audiencias');
		$num_audiencia = $num_audiencia + 1;
		if($comentario_abogado === '') $comentario_abogado = NULL;

		$datos = [
			'descripcion'=>$descripcion, 'rol_causa'=>$rol_causa, 'tipo_audiencia'=>$tipo_audiencia,
			'numero_audiencia'=>$num_audiencia, 'rut_alumno'=>$rut_alumno,
			'rut_profesor'=>$rut_profesor, 'nota_registro_1'=>$nota_registro_1,
			'nota_registro_2'=>$nota_registro_2, 'nota_registro_3'=>$nota_registro_3,
			'nota_registro_otros'=>$nota_registro_otros, 'nota_destreza_1'=>$nota_destreza_1,
			'nota_destreza_2'=>$nota_destreza_2, 'nota_destreza_3'=>$nota_destreza_3, 'nota_destreza_4'=>$nota_destreza_4,
			'nota_destreza_5'=>$nota_destreza_5, 'nota_destreza_6'=>$nota_destreza_6, 'nota_destreza_7'=>$nota_destreza_7,
			'nota_destreza_8'=>$nota_destreza_8, 'nota_destreza_9'=>$nota_destreza_9, 'nota_destreza_10'=>$nota_destreza_10,
			'nota_destreza_11'=>$nota_destreza_11, 'nota_destreza_12'=>$nota_destreza_12, 'nota_item_1'=>$nota_item_1,
			'nota_item_2'=>$nota_item_2, 'nota_item_3'=>$nota_item_3, 'nota_final'=>$nota_final, 'fecha'=>$fec_ingreso,
			'hora'=>$hora_ingreso, 'sede'=>$sede, 'estado_revision'=>$estado_revision,
			'comentario_abogado'=>$comentario_abogado
		];

		$exito = $this->db->insert('audiencias', $datos);
		if($exito) {
			$INSERT_ID = $this->db->insert_id();
			$exito = $this->_actualizarEstadoEvaluacion($INSERT_ID);
		}
		return $exito;
	}

	//INSERT
	// Retorna ID de la fila insertada. -1 si falla.
	public function ingresarAudienciaParcial($rol_causa, $fecha, $hora, $sede, $rut_profesor, $rut_alumno) {
		$num_audiencia = $this->db->where('rol_causa', $rol_causa)->count_all_results('audiencias');
		$num_audiencia = $num_audiencia + 1;
		$fecha = date('Y-m-d', strtotime($fecha));

		$datos = array(
			'numero_audiencia'=>$num_audiencia,
			'rol_causa'=>$rol_causa,
			'fecha'=>$fecha,
			'hora'=>$hora,
			'sede'=>$sede,
			'rut_profesor'=>$rut_profesor,
			'rut_alumno'=>$rut_alumno);

		$exito = $this->db->insert('audiencias', $datos);
		$INSERT_ID = 0;
		if ($exito) $INSERT_ID = $this->db->insert_id();
		return $INSERT_ID;
	}

	// UPDATE
	public function actualizarAudiencia(array $columnas) {
		if(isset($columnas['comentario_abogado']) == false || $columnas['comentario_abogado'] === '') $columnas['comentario_abogado'] = NULL;
		$this->db->where( 'id_audiencia' , $columnas['id_audiencia']);
		$bool_exito = $this->db->update( 'audiencias', $columnas );

		$this->_actualizarEstadoEvaluacion($columnas['id_audiencia']);

		return $bool_exito;
	}

	// UPDATE
	public function cambiarEstadoRevision($id_audiencia, $estado) {
		$this->load->helper("configuracion_helper");
		$exito = false;
		if(estadoRevisionAudienciaEsValido($estado)) {
			$datos['estado_revision'] = $estado;
			if($estado === 'EVALUADA') $datos['evaluacion_completada'] = TRUE;
			$exito = $this->db->where('id_audiencia', $id_audiencia)->update('audiencias', $datos);
		}
		return $exito;
	}

	// DELETE
	function eliminarPorCausa($rol_causa) {
		$exito = $this->db->where('rol_causa', $rol_causa)->get('audiencias');
	}

	// GET columna
	// Retorna nota final sólo si posee evaluación completa
	// (no hay ninguna nota con valor 0)
	public function tieneEvaluacionCompleta($id_audiencia) {
		$query = $this->db->select('nota_final')
			->where('id_audiencia', $id_audiencia)
			->where("nota_registro_1 != 0 AND nota_registro_2 != 0 AND nota_registro_3 != 0
				AND nota_destreza_1 != 0 AND nota_destreza_2 != 0 AND nota_destreza_3 != 0 AND nota_destreza_4 != 0
				AND nota_destreza_5 != 0 AND nota_destreza_6 != 0 AND nota_destreza_7 != 0 AND nota_destreza_8 != 0
				AND nota_destreza_9 != 0 AND nota_destreza_10 != 0 AND nota_destreza_11 != 0 AND nota_destreza_12 != 0
				AND nota_item_3 != 0")
			->get('audiencias');
		if($query->num_rows() === 0) {
			return false;
		} else {
			return $query->row()->nota_final;
		}
	}

	//NOTA: SI YA ESTÁ EVALUADA (evaluacion_completada) NO HACE NADA.
	private function _actualizarEstadoEvaluacion($id_audiencia) {
		$this->db->reset_query(); $exito = true;
		$audiencia = $this->getPorId($id_audiencia);
		if($audiencia->evaluacion_completada == FALSE && $this->tieneEvaluacionCompleta($id_audiencia) !== FALSE) {
			$this->db->where( 'id_audiencia' , $id_audiencia);
			$exito = $this->db->update('audiencias', ['evaluacion_completada'=>1, 'estado_revision'=>'EVALUADA']);
			$this->db->reset_query();
		}
		return $exito;
	}

	// Entrega nombre de columna de la BD segun el tipo de usuario.
	// Nota: directores y profesores son abogados.
	private function _getColumnaPorTipoUsuario($tipo_usuario) {
		switch($tipo_usuario) {
			case 'abogado':
				return "rut_profesor";
				break;
			case 'alumno':
				return"rut_alumno";
				break;
			default:
				return null;
		}
	}
}

?>
