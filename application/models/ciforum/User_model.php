<?php

class User_model extends CI_Model {
    public $error       = array();
    public $error_count = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function check_role()
    {
        $user_id = $this->session->userdata('cibb_user_id');
        // get roles
        if ($user_id) {
            $row = $this->db->get_where(TBL_USERS, array('id' => $user_id))->row();
            $roles = $this->db->get_where(TBL_ROLES, array('id' => $this->session->cibb_user_roleid))->row_array();
            foreach ($roles as $key => $value) {
                $this->session->set_userdata($key, $value);
            }
        }
    }

		public function revisar_login($row) {
		  $this->_check_login_parte_2($row);
		}
		public function check_login()
		{
		  $row = $this->input->post('row');
		  $this->_check_login_parte_2($row);
		}

	 private function _check_login_parte_2($row) {
		 $key = $this->config->item('encryption_key');

	  //   $data = array('username' => $row['username']);
	  //TODO. tomar en cuenta el rut como username como en login clinica.
		 $data = array('nombre_usuario' => $row['username']);

		 $query = $this->db->get_where(TBL_USERS, $data);

	  //   $plain_password = '';
	  $hash_contrasena = '';

		 if ( ($query->num_rows() == 1) ) {
			  $user = $query->row();
			  // $plain_password = $this->encrypt->decode($user->password, $key);
			  // $hash_contrasena = $user->password;
			  $hash_contrasena = $user->pwd;
		 }

		 $this->load->helper("configuracion");
		 // if user found
	  //   if ( ($query->num_rows() == 1) && ($plain_password == $row['password']))
		 if ( ($query->num_rows() == 1) && contrasenaCorrecta($hash_contrasena, $row['password']) ) {
			  $row = $query->row();
			  $this->session->set_userdata('cibb_logged_in', 1);
			  $this->session->set_userdata('cibb_user_id'  , $row->id);
			  // $this->session->set_userdata('cibb_user_id'  , $row->rut);
			  // $this->session->set_userdata('cibb_username' , $row->username);
			  $this->session->set_userdata('cibb_username' , $row->nombre_usuario);
			  $this->session->set_userdata('cibb_nombre_real' , $row->nombre);
			  // $this->session->set_userdata('cibb_user_roleid' , $row->role_id);
			  $this->session->set_userdata('cibb_user_roleid' , $row->id_ciforum_rol);

			   $this->session->rut = $row->rut;

			  // get roles
			  $roles = $this->db->get_where(TBL_ROLES, array('id' => $this->session->cibb_user_roleid))->row_array();
			  foreach ($roles as $key => $value) {
					$this->session->set_userdata($key, $value);
			  }
		 } else {
			  $this->error['login'] = 'User not found';
			  $this->error_count = 1;
		 }
	 }

    public function register()
    {
   //      $row = $this->input->post('row');
	 //
   //      // check username
   //      $is_exist_username = $this->db->get_where(TBL_USERS,
   //              array('username' => $row['username']))->num_rows();
   //      if ($is_exist_username > 0) {
   //          $this->error['username'] = 'Username already in use';
   //      }
   //      if (strlen($row['username']) < 5) {
   //          $this->error['username'] = 'Username minimum 5 character';
   //      }
	 //
   //      // check password
   //      if ($row['password'] != $this->input->post('password2')) {
   //          $this->error['password'] = 'Password not match';
   //      } else if (strlen($row['password']) < 5) {
   //          $this->error['password'] = 'Password minimum 5 character';
   //      }
	 //
   //      if (count($this->error) == 0) {
	// 		  $this->load->helper("configuracion");
   //          // $key = $this->config->item('encryption_key');
   //          // $row['password'] = $this->encrypt->encode($row['password'], $key);
	// 			$row['password'] = contrasenaEncriptar($row['password']);
	// 			$row['role_id'] = 3; // rol por defecto = 3 = miembro.
   //          $this->db->insert(TBL_USERS, $row);
   //      } else {
   //          $this->error_count = count($this->error);
   //      }
		$this->error_count = 1;
		$this->error['username'] = 'Registro desactivado';
    }

	 // GENESIS BUSTAMANTE
	 public function getUser($user_id) {
		 $user = $this->db->select(TBL_USERS.'.*, '.TBL_USERS.'.nombre_usuario as username, '.TBL_USERS.'.id_rol as role_id')->get_where(TBL_USERS, array('id' => $user_id))->row();
		 return $user;
	 }

	 // GENESIS BUSTAMANTE
	 public function getUsers() {
		 $user = $this->db->select(TBL_USERS.'.*, '.TBL_USERS.'.nombre_usuario as username, '.TBL_USERS.'.id_rol as role_id')->get(TBL_USERS)->result();
		 return $user;
	 }
}
