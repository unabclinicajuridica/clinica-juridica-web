<?php

class Thread_model extends CI_Model {
    public $error       = array();
    public $error_count = 0;
    public $data        = array();
    public $fields      = array();

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all($start, $limit)
    {
        $sql = "SELECT a.*, b.name as category_name, b.slug as category_slug, c.date_add
                FROM ".TBL_THREADS." a, ".TBL_CATEGORIES." b, ".TBL_POSTS." c
                WHERE a.category_id = b.id AND a.id = c.thread_id
                AND c.date_add = (SELECT MAX(date_add) FROM ".TBL_POSTS." WHERE thread_id = a.id LIMIT 1)
                ORDER BY c.date_add DESC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
    }

	 // GENESIS BUSTAMANTE
	 // Muestra Temas propios y los de otros alumnos y profesores de sus causas.
	 public function getPorRelacionConCausas($start, $limit)
	 {
		 // GENESIS BUSTAMANTE
		 $ruts_vinculados_sesion = $this->_getRUTsVinculadosASesion();
		 $ruts_sql = "('" . implode($ruts_vinculados_sesion, "','") . "')";

			$sql_master = "
				SELECT tema.*, catg.name as category_name, catg.slug as category_slug, post.date_add
				FROM ".TBL_THREADS." tema
				JOIN ".TBL_CATEGORIES." catg ON tema.category_id = catg.id
				JOIN ".TBL_POSTS." post ON tema.id = post.thread_id

				JOIN usuarios usu ON post.author_id = usu.id
				JOIN (SELECT aa.thread_id, MIN(aa.date_add) as fecha_primer_post, bb.rut as rut_op
				      FROM ".TBL_POSTS." aa, usuarios bb
				      WHERE aa.author_id = bb.id
				      GROUP BY aa.thread_id
				     ) PRIMER_POST_THREAD ON tema.id = PRIMER_POST_THREAD.thread_id
				WHERE rut_op IN $ruts_sql

				AND tema.category_id = catg.id AND tema.id = post.thread_id
				AND post.date_add = (SELECT MAX(date_add) FROM ".TBL_POSTS." WHERE thread_id = tema.id LIMIT 1)
				ORDER BY post.date_add DESC LIMIT ".$start.", ".$limit;
		  return $this->db->query($sql_master)->result();
	 }

	public function get_by_category($start, $limit, $cat_id)
	{
		$cat_string = "(";
		foreach ($cat_id as $key => $id) {
			if ($key == 0) {
				$cat_string .= " tema.category_id = ".$id;
			} else {
				$cat_string .= " OR tema.category_id = ".$id;
			}
		}
		$cat_string .= ")";

		// GENESIS BUSTAMANTE
		$ruts_vinculados_sesion = $this->_getRUTsVinculadosASesion();
		$ruts_sql = "('" . implode($ruts_vinculados_sesion, "','") . "')";

		$sql = "
				SELECT tema.*, catg.name as category_name, catg.slug as category_slug, post.date_add
				FROM ".TBL_THREADS." tema
				JOIN ".TBL_CATEGORIES." catg ON tema.category_id = catg.id
				JOIN ".TBL_POSTS." post ON tema.id = post.thread_id

				JOIN usuarios usu ON post.author_id = usu.id
				JOIN (SELECT aa.thread_id, MIN(aa.date_add) as fecha_primer_post, bb.rut as rut_op
				FROM ".TBL_POSTS." aa, usuarios bb
				WHERE aa.author_id = bb.id
				GROUP BY aa.thread_id
				) PRIMER_POST_THREAD ON tema.id = PRIMER_POST_THREAD.thread_id
				WHERE rut_op IN $ruts_sql

				AND tema.category_id = catg.id AND tema.id = post.thread_id
				AND post.date_add = (SELECT MAX(date_add) FROM ".TBL_POSTS." WHERE thread_id = tema.id LIMIT 1)
				AND ".$cat_string."
				ORDER BY post.date_add DESC LIMIT ".$start.", ".$limit;
		return $this->db->query($sql)->result();
	}

    public function get_total_by_category($cat_id)
    {
        $cat_string = "(";
        foreach ($cat_id as $key => $id) {
            if ($key == 0) {
                $cat_string .= " a.category_id = ".$id;
            } else {
                $cat_string .= " OR a.category_id = ".$id;
            }
        }
        $cat_string .= ")";

        $sql = "SELECT a.* FROM ".TBL_THREADS." a WHERE ".$cat_string;
        return $this->db->query($sql)->num_rows();
    }

    public function create()
    {
        $thread = $this->input->post('row');
        $post = $this->input->post('row_post');

        $this->fields = $thread;

        // check title
        if (strlen($thread['title']) == 0) {
            $this->error['title'] = 'Título no puede estar vacío';
        }

        // check slug
        if (strlen($thread['slug']) == 0) {
            $this->error['slug'] = 'Abreviación de Url no puede estar vacío';
        } else {
            $slug_check = $this->db->get_where(TBL_THREADS, array('slug' => $thread['slug']));
            if ($slug_check->num_rows() > 0) {
                $this->error['role'] = 'Slug "'.$thread['slug'].'" already in use';
            }
        }

        // check category
        if ($thread['category_id'] == "0") {
            $this->error['category'] = 'Escoja una Categoría';
        }

        // check post
        if (strlen($post['post']) == 0) {
            $this->error['post'] = 'Mensaje no puede estar vacío';
        }

        if (count($this->error) == 0) {
            // insert into thread
            $thread['date_add'] = $thread['date_last_post'] = date("Y-m-d H:i:s");
            $this->db->insert(TBL_THREADS, $thread);
            // insert into post
            $post['thread_id'] = $this->db->insert_id();
            $post['author_id'] = $this->session->userdata('cibb_user_id');
            $post['date_add']  = $thread['date_add'];
            $this->db->insert(TBL_POSTS, $post);
        } else {
            $this->error_count = count($this->error);
        }
    }

    public function get_posts($thread_id, $start, $limit)
    {
		//  $sql = "SELECT a.*, b.username, b.id as user_id FROM ".TBL_POSTS." a, ".TBL_USERS." b
        $sql = "SELECT a.*, b.nombre as username, b.id as user_id FROM ".TBL_POSTS." a, ".TBL_USERS." b
                WHERE a.thread_id = ".$thread_id." AND a.author_id = b.id
                ORDER BY a.date_add ASC LIMIT ".$start.", ".$limit;
        return $this->db->query($sql)->result();
    }

    public function reply()
    {
        $row = $this->input->post('row');

        // check post
        if (strlen($row['post']) == 0) {
            $this->error['post'] = 'Mensaje no puede estar vacío';
        }

        if (count($this->error) == 0) {
			  // Fecha de nuevo post.
			  $row['date_add'] = date('Y-m-d H:i:s');
            $exito_insert = $this->db->insert(TBL_POSTS, $row);
				// Actualizar fecha del ultimo post en el tema.
				if ($exito_insert) {
					$this->db->where('id', $row['thread_id'])->update(TBL_THREADS, array('date_last_post'=>$row['date_add']));
				}
        } else {
            $this->error_count = count($this->error);
        }
    }

	 // GENESIS BUSTAMANTE
 	// UPDATE
 	public function actualizarUltimaVisita($id_usuario, $id_tema) {
 		$sql_insertar = $this->db
 			->set(['id_usuario'=>$id_usuario, 'id_ciforum_tema'=>$id_tema, 'ultima_visita'=>date('Y-m-d H:i:s')])
 			->get_compiled_insert('ciforum_tema_visita');
 		$exito = $this->db->query($sql_insertar.' ON DUPLICATE KEY UPDATE ultima_visita=VALUES(ultima_visita)');
 		return $exito;
 	}
	// GENESIS BUSTAMANTE
	// GET
	public function _getRUTsVinculadosASesion() {
		$rut_sesion = $this->session->rut;
		$causas = $this->db->select('RUT_ALUMNO, RUT_ABOGADO')
		  ->where_in($rut_sesion, ['RUT_ALUMNO', 'RUT_ABOGADO'], FALSE)
		  ->get('causas')->result();
		  $ruts = array($rut_sesion => $rut_sesion);
	  foreach ($causas as $causa) {
		  foreach ($causa as $key => $rut) {
			  $ruts[$rut] = $rut;
		  }
	  }
	  return array_values($ruts);
	}
}
