<?php

class modelo_cliente extends CI_Model {

	//GET
	public function getEmailPorIdCorrelativaCausa($id_correlativa_causa) {
		$id_cliente = $this->db->get_where('causas', array('id' => $id_correlativa_causa), 1)->row()->RUT_CLIENTE;
		if($id_cliente === null) {
			return null;
		}
		$this->db->reset_query();
		//TODO. Utilizar id cliente y no rut.
		$cliente = $emailCliente = $this->db->get_where('clientes', array('rut_cliente' => $id_cliente), 1)->row();

		if($cliente) return $cliente->email;
		else return NULL;
	}

	//GET
	public function getEmailPorRolCausa($rol_causa) {
		$id_cliente = $this->db->get_where('causas', array('id_causa' => $rol_causa), 1)->row()->RUT_CLIENTE;
		if($id_cliente === null) {
			return null;
		}
		$this->db->reset_query();

		//TODO. Utilizar id cliente y no rut.
		$cliente = $emailCliente = $this->db->get_where('clientes', array('rut_cliente' => $id_cliente), 1)->row();

		if($cliente) return $cliente->email;
		else return NULL;
	}
	//GET
	public function get($id_cliente) {
		$filaCliente = $this->db->get_where('clientes', array('id' => $id_cliente), 1)->row();
		return $filaCliente;
	}

	//GET
	public function getPorRut($id_cliente) {
		$filaCliente = $this->db->get_where('clientes', array('rut_cliente' => $id_cliente), 1)->row();
		return $filaCliente;
	}

	//GET
	public function getEmailPorRut($rut) {
		$filaCliente = $this->db->get_where('clientes', array('rut_cliente' => $rut), 1);
		$email = $filaCliente->row()->email;
		if($email === "") { return NULL; }
		return $email;
	}

	//INSERT
	// Se ingresa un nuevo cliente, en el caso de que ya exista, se actualizan los datos.
	// RETORNA ID DE LA FILA.
	public function ingresar_cliente($rut_cliente, $dv_cliente, $nombre_cliente, $telefono, $domicilio, $email, $edad_usuario) {
		$datos = array(
			'rut_cliente'=>$rut_cliente,
			'nombre_cliente'=>$nombre_cliente, 'telefono'=>$telefono,
			'domicilio'=>$domicilio, 'email'=>$email,
			'edad'=>$edad_usuario
		);

		$datos =  array_filter($datos); $datos['dv_cliente'] = $dv_cliente; // dv puede ser 0.

		$existe_nombre = $existe_rut = 0;
		if($nombre_cliente) {
			$cliente = $this->db->where('nombre_cliente',$nombre_cliente)->get('clientes')->row();
			if($cliente != NULL) {
				$existe_nombre = 1;
				$id_cliente = $cliente->id;
			}
		}
		if($rut_cliente) {

			$cliente = $this->db->where('rut_cliente',$rut_cliente)->get('clientes')->row();
			if($cliente != NULL) {
				$existe_rut = 1;
				$id_cliente = $cliente->id;
			}
		}

		if($existe_nombre) { // Usuario existente, se actualizan los datos.
			$this->db->where('nombre_cliente', $nombre_cliente);
			$exito = $this->db->update('clientes', $datos);
		}
		else if($existe_rut) { // Usuario existente, se actualizan los datos.
			$this->db->where('rut_cliente', $rut_cliente);
			$exito = $this->db->update('clientes', $datos);
		}
		else {
			$exito = $this->db->insert('clientes', $datos);
			$id_cliente = $this->db->insert_id();
		}
		return $id_cliente;
	}
}
?>
