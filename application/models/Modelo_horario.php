<?php
	class modelo_horario extends CI_Model {

		// Entrega las asignaciones del usuario actual que se
		// encuentren entre el rango especificado con los parametros.
		public function traer_horario_personal($fecha_inicio_rango, $fecha_termino_rango) {
			$this->load->model('modelo_busquedas');
			$sede_actual = $this->session->id_sede_actual;

			$rut_sesion=$this->session->rut;

			// Consulta sanitizada con Codeigniter Query Builder.
			$asignaciones_semana = $this->db
				->select('id, fecha_asignacion, hora_inicio, hora_fin, tipo_asunto')
				->where([
					'rut' => $rut_sesion,
					'fecha_asignacion >=' => $fecha_inicio_rango,
					'fecha_asignacion <=' => $fecha_termino_rango,
					'sede' => $sede_actual
				])
				->get('evento_agenda')->result_array();

			return $asignaciones_semana;
		}

		// Entrega asignaciones que no son para el Alumno actual, pero la
		// Causa relacionada está asignada al Alumno, y la Audiencia es
		// de una Causa asignada al Alumno.
		public function traer_asignaciones_relacionadas($fecha_inicio_rango, $fecha_termino_rango) {
			$this->load->model('modelo_busquedas');
			$this->load->model('modelo_causa');
			$this->load->model('modelo_audiencia');
			$sede_actual = $this->session->id_sede_actual;

			// Busca las Causas asignadas a Alumno
			$rut_sesion = $this->session->rut;
			$causas_asignadas = $this->modelo_causa->getPorUsuario($rut_sesion, 'alumno');
			$this->db->reset_query();

			$ids_causas_asignadas = array(-1);
			foreach ($causas_asignadas as $causa) {
				$ids_causas_asignadas[] = $causa->id;
			}

			// Busca las Audiencias asignadas a Alumno
			$audiencias_asignadas = $this->modelo_audiencia->getPorUsuario($rut_sesion, 'alumno');
			$this->db->reset_query();

			$ids_audiencias_asignadas = array(-1);
			foreach ($audiencias_asignadas as $aud) {
				$ids_audiencias_asignadas[] = $aud->id_audiencia;
			}

			// Busca las asignaciones de agenda con dichas Causas
			// o Audiencias.
			$asignaciones_semana = $this->db
				->select('id, fecha_asignacion, hora_inicio, hora_fin, tipo_asunto')
				->where([
					'fecha_asignacion >=' => $fecha_inicio_rango,
					'fecha_asignacion <=' => $fecha_termino_rango,
					'sede' => $sede_actual,
					// Evita que se repita una asignación
					'rut !=' => $rut_sesion
				])
				->group_start()
					->where_in('id_causa', $ids_causas_asignadas)
					->or_where_in('id_audiencia', $ids_audiencias_asignadas)
				->group_end()
				->get('evento_agenda')->result_array();

			return $asignaciones_semana;

		}

	}
?>
