<?php
	class modelo_usuario extends CI_Model {

		// GET
		public function getNombrePorRut($rut) {
			$query = $this->db
			->select('nombre')
			->get_where('usuarios', ['rut' => $rut]);
			return $query->row()->nombre;
		}

		// GET
		// filtra por SEDE.
		public function getPorRut($rut) {
			$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
			$query = $this->db->get_where("usuarios", array('rut' => $rut, 'sede' => $sede));
			return $query->row();
		}
		
		// GET campo
		public function getIdPorRut($rut_usuario) {
         $this->db->select('id');
         $id_usuario = $this->db->get_where('usuarios', array('rut' => $rut_usuario), 1)->row()->id;
         return $id_usuario;
      }
      
      // GET campo
		public function getDV($rut_usuario){
			$usuario = $this->db->select('dv')->where('rut', $rut_usuario)->get('usuarios')->row();
			if($usuario) return $usuario->dv;
			else return NULL;
		}

		// DELETE
		public function borrarPorId($id_usuario) {
			$rut_usuario = $this->db->where('id', $id_usuario)->get('usuarios')->row()->rut;
			$this->borrar($rut_usuario);
		}

		// DELETE
		public function borrar($rut_usuario) {
			$this->db->where('rut', $rut_usuario)->delete('usuario_sede');
			$this->db->where('rut', $rut_usuario)->delete('conf_agenda');
			$this->db->where('rut', $rut_usuario)->delete('usuarios');
		}

		//	DELETE
		public function borrarUsuarioSedes($id_usuario) {
			$this->db->where('rut', $id_usuario)->delete('usuario_sede');
		}
	}
?>
