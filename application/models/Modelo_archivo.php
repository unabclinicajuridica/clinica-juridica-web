<?php

class modelo_archivo extends CI_Model {
	// ****************************** //
	// ********** ARCHIVOS ********** //
	// ****************************** //

	// GET
	public function get($id_archivo) {
		return $this->db->get_where('archivos', ['id' => $id_archivo])->row();
	}

	// GET multiple
	// Retorna ARRAY.
	public function getArchivos($id_tramite) {
		return $this->db->get_where('archivos', ['id_tramite' => $id_tramite])->result_array();
	}

	// UPLOAD + INSERT (acción atómica)
	// Guarda el archivo en el disco e ingresa fila en tabla Archivos
	public function ingresar($parametro_archivos, $ruta_local) {
		$folder_to_save = './'.$ruta_local.'/';

		if (!is_dir($folder_to_save)) {
			 mkdir($folder_to_save, 0777, true);
		}

		$config['upload_path']        = $folder_to_save;
		$config['allowed_types']      = 'gif|jpg|png|doc|txt|jpeg|xls|docx|pdf|zip|7zip|xlsx|odt|ods|odp';
		$config['max_size']           = 5000;
		$config['max_width']          = 0;
		$config['max_height']         = 0;
		$config['multi']              = 'all';

		$this->load->library('upload');
		$this->upload->initialize($config, TRUE);

		$result = array();
		$result['exito'] = true;

		if ( ! $this->upload->do_upload($parametro_archivos)) {
			$result['mensaje'] = $this->upload->display_errors();
			$result['exito'] = false;
		}
		else {
			$result['mensaje'] = 'archivo unico subido sin insertar en fila';
			$result['upload_data'] = $upload_data = $this->upload->data();
			// UN ARCHIVO
			if(isset($upload_data['file_name'])) {
				$insert_id = $this->_ingresarArchivo($upload_data);
				if( !($insert_id)) $result['exito'] = false;
				else { $result['mensaje'] = 'archivo unico subido e insertado en fila'; $result['ids'][] = $insert_id; }
			}

			// MULTIPLES ARCHIVOS
			else {
				$result['mensaje'] = 'Sólo se subieron los archivos.';
				foreach($upload_data as $ud) {
					$insert_id = $this->_ingresarArchivo($ud);
					if( !($insert_id)) $result['exito'] = false;
					else { $result['mensaje'] = 'Trámite ingresado.'; $result['ids'][] = $insert_id; }
				}
			}
		}
		$result['ruta'] = $folder_to_save;
		return $result;
	}

	// INSERT privado (acción atómica)
	// Ingresa fila en tabla Archivos.
	private function _ingresarArchivo($upload_data) {
		$data = array(
			'file_ext' => $upload_data['file_ext'],
			'file_type' => $upload_data['file_type'],
			'file_path' => $upload_data['file_path'],
			'orig_name' => $upload_data['orig_name'],
			'file_name' => $upload_data['file_name'],
			'file_size' => $upload_data['file_size']
		);
		$exito = $this->db->insert('archivos', $data);
		if($exito) return $this->db->insert_id();
		else return 0;
	}

	// ****************************** //
	// ********* PLANTILLAS ********* //
	// ****************************** //
	// GET multiple
	public function getPlantillas() {
		$plantillas = $this->db->order_by('titulo', 'ASC')
		->join('archivos', 'archivos.id = plantilla.id_archivo')
		->get('plantilla')->result();
		return $plantillas;
	}

	public function ingresarPlantilla($datos) {
		$datos = array_filter($datos);
		$exito = $this->db->insert('plantilla', $datos);
		return $exito;
	}

	public function getPlantillasPorCategoria($cat_id) {
		$cat_string = "(";
		foreach ($cat_id as $key => $id) {
			if ($key == 0) {
				$cat_string .= " plantilla.id_categoria = ".$id;
			} else {
				$cat_string .= " OR plantilla.id_categoria = ".$id;
			}
		}
		$cat_string .= ")";

		$sql = "
			SELECT plantilla.*, archivos.orig_name, catg.name as category_name, catg.slug as category_slug
			FROM plantilla
			JOIN archivos ON plantilla.id_archivo = archivos.id
			JOIN ".TBL_CATEGORIES." catg ON plantilla.id_categoria = catg.id

			WHERE ".$cat_string."
			ORDER BY plantilla.titulo ASC";
		return $this->db->query($sql)->result();
	}
}
?>
