<?php

	class modelo_sesion extends CI_Model {

      public function __construct() {
             // Call the CI_Model constructor
             parent::__construct();
      }


		public function login($username, $clave) {
			$this->load->helper("configuracion");

			// Se está usando RUT como nickname
			if (is_numeric($username)) {
				$usuario = $this->db->get_where('usuarios', ['rut' => $username], 1)->row();
			}
			// Se está usando nombre de usuario como nickname
			else {
				$usuario = $this->db->get_where('usuarios', ['nombre_usuario' => $username], 1)->row();
			}
			$output['exito'] = false;
			$output['mensaje'] = "El RUT o nombre de usuario no existe";
			if($usuario) {
				$clave_hash = $usuario->pwd;
				$output['mensaje'] = "La contraseña es incorrecta";
				// Para contraseñas encriptadas bcrypt || Para contraseñas No Encriptadas
				if( contrasenaCorrecta($clave_hash, $clave) || $clave === $clave_hash ) {
					$this->session->login_user = $username;
					$this->session->rut = $usuario->rut;
					$this->session->nombre_completo_user = $usuario->nombre;
					$this->session->id = $usuario->id;

					$this->session->id_sede_actual = $this->db->select('sede')->get_where('usuarios', array('rut' => $usuario->rut), 1)->row()->sede;

					$rol_usuario = $this->db->get_where('rol', array('id' => $usuario->id_rol))->row();
					$this->session->set_userdata('rol', $rol_usuario);

					// Logeando sincronicamente en foro
					$this->load->model(CIBB_CARPETA.'user_model');
					$this->user_model->revisar_login(['username'=>$username, 'password'=>$clave]);
					$output['exito'] = true;
				}
			}

			return $output;
		}


      public function cambiar_email($email) {
         $this->db->set('email', $email);
         $this->db->where('rut', $this->session->rut);
         $bool_result = $this->db->update('usuarios');
         return $bool_result;
		}


      public function obtener_email() {
         $this->db->select('email');
         $usuario = $this->db->get_where('usuarios', array('rut' => $this->session->rut), 1);
         return $usuario->row()->email;
      }


		public function cambiar_contrasena($password) {
			$this->load->helper("configuracion");
			$password_hash = contrasenaEncriptar($password);
			$this->db->set('pwd', $password_hash);
			$this->db->where('rut', $this->session->rut);
			$bool_result = $this->db->update('usuarios');
			return $bool_result;
		}
   }
?>
