<?php

class modelo_orientacion extends CI_Model {

	// GET + GET All
	public function get($id=NULL) {
		$this->_prepararConsultaFull();

		$orientaciones = $id ? $this->db->where('id_orientacion', $id)->get('orientacion')->row() : $this->db->get('orientacion')->result();
		return $orientaciones;
	}

	// GET multiple
	public function getPorNombres($abogado, $usuario) {
		$this->_prepararConsultaFull();

		$this->db->like('abogado.nombre', $abogado);
		$this->db->like('clientes.nombre_cliente', $usuario);
		$orientaciones = $this->db->get('orientacion')->result();
		return $orientaciones;
	}

	// GET multiple
	// Puede entregar solo las que no tengan resena.
	public function getPorAbogado($rut_abogado, $solo_sin_resena=false, $solo_sin_causa=false) {
		$this->_prepararConsultaFull();
		if($solo_sin_resena) $this->db->where('resena', NULL);
		if($solo_sin_causa) $this->db->where('id_causa', NULL);

		$this->db->where('rut_abogado', $rut_abogado);
		$orientaciones = $this->db->get('orientacion')->result();
		return $orientaciones;
	}

	// GET multiple
	public function getPorCausa($id_correlativa_causa) {
		$this->_prepararConsultaFull();

		$this->db->where('id_causa', $id_correlativa_causa);
		$orientaciones = $this->db->get('orientacion')->result();

		return $orientaciones;
	}
	// GET multiple
	// Devuelve sólo las que no tengan causa vinculada.
	public function getSinCausa() {
		$this->_prepararConsultaFull();
		$this->db->where('id_causa', NULL);
		$orientaciones = $this->db->get('orientacion')->result();
		return $orientaciones;
	}



	// COUNT
	public function getCantidadPorCausa($id_correlativa_causa) {
		$this->db->where('id_causa', $id_correlativa_causa);
		$this->db->from('orientacion');
		$cantidad = $this->db->count_all_results();
		return $cantidad;
	}

	// INSERT
	// retorna la ID de la orientación ingresada. -1 si falla.
	public function ingresar($titul_desc, $descrip, $id_materia, $resena, $rut_abogado, $rut_usuario, $sede, $id_causa) {
		$this->load->model('modelo_busquedas');

		$id_causa = $id_causa ? $id_causa : NULL;

		$data = array(
		  'fec_ingreso' => date('Y-m-d'),
		  'id_materia' => $id_materia,
		  'resena' => $resena,
		  'rut_abogado' => $rut_abogado,
		  'rut_usuario' => $rut_usuario,
		  'sede' => $sede,
		  'id_causa' => $id_causa
		);

		$data = array_filter($data);

		$exito = $this->db->insert('orientacion', $data); // boolean del resultado.

		if ($exito === true) {
			return $this->db->insert_id(); //id de la fila orientacion.
		} else {
			return -1;
		}
	}

	// UPDATE
	public function vincularCausa($id_orientacion, $id_causa) {
		$datos = array('id_causa' => $id_causa);
		$this->db->where('id_orientacion', $id_orientacion);
		$exito = $this->db->update('orientacion', $datos);
		return $exito;
	}

	// DELETE
	public function eliminarOrientacionesPorCausa($id_correlativa_causa) {
		$exito = $this->db->where('id_causa', $id_correlativa_causa)->delete('orientacion');
		return $exito;
	}

	// Preparar
	private function _prepararConsultaFull() {
		$this->load->helper('configuracion');
		$this->db->select("*, DATE_FORMAT(orientacion.fec_ingreso,'".getFormatoFechaMySQL()."') AS fec_ingreso, abogado.rut as RUT_ABOGADO, abogado.nombre as NOMBRE_ABOGADO, CONCAT(abogado.rut, '-', abogado.dv) as RUT_COMPLETO_ABOGADO, clientes.email as email_cliente, clientes.telefono as telefono_cliente");
		$this->db->join('materia', 'materia.id = orientacion.id_materia', 'left');
		$this->db->join('clientes', 'clientes.rut_cliente = orientacion.rut_usuario', 'left');
		$this->db->join('usuarios as abogado', 'abogado.rut = orientacion.rut_abogado', 'left');
		$sede = $this->session->id_sede_actual;
		$this->db->where('orientacion.sede', $sede);
	}
}
?>
