<?php

class modelo_ingresos extends CI_Model {

	public function ingresarConfAgenda($rut, $fecha_inicio, $fecha_termino, $hora_inicio, $hora_termino, $dia, $sede) {
		$queryString = "insert into conf_agenda values('','$fecha_inicio','$fecha_termino','$hora_inicio','$hora_termino','$dia','$rut','$sede',null)";
		$bool_result = $this->db->query($queryString);

		return $bool_result;
	}

	// Actualiza una fila de orientacion.
	// Recibe un array con el nombre de cada columna con su valor.
	public function actualizarOrientacion($id_orientacion, $datos_columnas) {
		$this->db->where('id_orientacion', $id_orientacion);
		$bool_exito = $this->db->update('orientacion', $datos_columnas);
		return $bool_exito;
	}

	// Actualiza una fila de asunto.
	// Recibe un array con el nombre de cada columna con su valor.
	public function actualizarAsunto($id_asunto, $datos_columnas) {
		$this->db->where('id_asunto', $id_asunto);
		$bool_exito = $this->db->update('asuntos', $datos_columnas);
		return $bool_exito;
	}

	// Nota: Retorna la ID del nuevo asunto insertado.
	public function ingresar_asunto($titulo, $descripcion, $rut_asociado, $sede, $observaciones, $id_causa=NULL) {
		$observaciones = $observaciones === '' ? NULL : $observaciones;
		$datos_fila = array('titulo_asunto'=>$titulo, 'descripcion'=>$descripcion, 'rut_asociado'=>$rut_asociado, 'sede'=>$sede, 'observaciones'=>$observaciones, 'id_causa'=>$id_causa);
		$exito = $this->db->insert('asuntos', $datos_fila);

		$INSERT_ID = $exito ? $this->db->insert_id() : 0;
		return $INSERT_ID;
	}

	public function ingresa_usuario($rut, $tipo_usuario) {
		$data = array(
		'tipo_usuario' => $tipo_usuario
		);
		$this->db->set( $data );
		$this->db->where( 'rut' , $rut );
		$this->db->update( 'usuarios', $data );
	}

	public function up_estado_asignacion($id_evento, $estado) {
		$data = array(
		'estado' => $estado
		);

		$this->db->set( $data );
		$this->db->where ( 'id', $id_evento );
		$this->db->update( 'evento_agenda', $data );
	}

	public function ingreso_privilegios($rut, $sede, $titulo_privilegio, $privilegio) {
		$query = $this->db->query("INSERT INTO privilegios(rut, sede, titulo_privilegio, privilegio)
		VALUES('$rut', '$sede', '$titulo_privilegio', '$privilegio');");
	}

	public function delete_privilegios($rut, $sede) {
		$query = $this->db->query("DELETE FROM privilegios WHERE rut = '$rut' AND sede = '$sede';");
	}

	public function ingreso_sede_usuario($rut, $sede) {
		$verificar = $this->db->query("SELECT * FROM usuario_sede WHERE rut = '$rut' AND id_sede = '$sede';");
		if(!($verificar->num_rows() > 0)){
			$query = $this->db->query("INSERT INTO usuario_sede(rut, id_sede) VALUES('$rut', '$sede');");
		}
	}

	public function actualiza_sede_actual($sede_seleccionada) {
		$rut_user=$this->session->rut;
		$query = $this->db->query("update usuarios set sede = '$sede_seleccionada' where rut = '$rut_user'");
	}

	public function ID_iCalUID_en_asignacionAgenda($icalid_evento, $icaluid_evento, $id_evento) {
		$exito = $this->db->where('id', $id_evento)->update('evento_agenda', ['icalid_evento'=>$icalid_evento, 'icaluid_evento'=>$icaluid_evento]);
		return $exito;
	}
}
?>
