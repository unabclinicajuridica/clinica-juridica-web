<?php

class modelo_busquedas extends CI_Model {

	// TODO. usar querybuilder.
	public function traer_profesores(){
		$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT * FROM ".$this->db->database.".usuarios WHERE id_rol IN (2, 4) and sede = '$sede';");
		return $query->result();
	}


	// TODO. usar querybuilder.
	public function traer_alumnos() {
		$sede = $this->session->id_sede_actual;
		$query=$this->db->query("SELECT * FROM ".$this->db->database.".usuarios WHERE id_rol = 1 and sede = '$sede';");
		return $query->result();
	}

	public function emailUsuarioPorRut($rutUsuario) {
		$query = $this->db->query("SELECT email from usuarios where rut='$rutUsuario'");
		return $query->row()->email;
	}

	// GET
	public function buscar_disponibilidad_profesor($rut_abogado) {
		$this->load->helper('configuracion');
		$conf_agenda = $this->db
			->select("conf_agenda.id_conf_agenda, DATE_FORMAT(conf_agenda.fecha_inicio,'".getFormatoFechaMySQL()."') as fecha_inicio, DATE_FORMAT(conf_agenda.fecha_fin,'".getFormatoFechaMySQL()."') as fecha_fin, conf_agenda.hora_inicio, conf_agenda.hora_fin, conf_agenda.dia, sedes.nombre_sede_corto")
			->join('sedes', 'conf_agenda.sede = sedes.id_sede', 'left')
			->where('conf_agenda.rut', $rut_abogado)
			->get('conf_agenda')->result();
		return $conf_agenda;
	}

	// Devuelve profesores con su numero de causas asignadas, sede actual.
	public function buscar_profesores_sede(){
		$this->load->model('modelo_busquedas');
		$sede_actual = $this->session->id_sede_actual;
		$query = $this->db->query("
		SELECT u.rut, u.nombre, IFNULL(cxa.numero_de_causas, 0) AS numero_de_causas
		FROM usuarios u LEFT OUTER JOIN (
			SELECT rut_abogado, COUNT(id) AS numero_de_causas
			FROM causas
			GROUP BY rut_abogado
		) AS cxa ON u.rut = cxa.rut_abogado
		WHERE id_rol IN (2, 4) and rut in (select rut from usuario_sede where id_sede = '$sede_actual')");
		return $query->result();
	}

	// Devuelve alumnos con su numero de causas asignadas, sede actual.
	public function geno_alumnos_sede() {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;
		$query = $this->db->query("
		SELECT u.rut, u.nombre, IFNULL(cxa.numero_de_causas, 0) AS numero_de_causas
		FROM usuarios u LEFT OUTER JOIN (
			SELECT rut_alumno, COUNT(id) AS numero_de_causas
			FROM causas
			GROUP BY rut_alumno
		) AS cxa ON u.rut = cxa.rut_alumno
		WHERE id_rol=1 and rut in (select rut from usuario_sede where id_sede = '$sede')");
		return $query->result();
	}

	//TODO.PENDIENTE: FILTRAR ADICIONALMENTE SEGUN TABLA ABOGADO_ALUMNOS.
	public function getAlumnosDeAbogadoYPorVinculoCausas($rut_profesor) {
		$this->load->model('modelo_busquedas');
		$sede = $this->session->id_sede_actual;
		$query = $this->db->query("
		SELECT u.rut, u.nombre, IFNULL(cxa.numero_de_causas, 0) AS numero_de_causas
		FROM usuarios u
		LEFT OUTER JOIN (
			SELECT rut_alumno, COUNT(id) AS numero_de_causas
			FROM causas
			GROUP BY rut_alumno) AS cxa ON u.rut = cxa.rut_alumno
		WHERE id_rol=1 and rut in (select rut from usuario_sede where id_sede = '$sede')");
		return $query->result();
	}

	// Devuelve un array con todos los usuarios (del sistema).
	public function getUsuarios() {
		$query = $this->db->get('clientes');
		return $query->result();
	}

	public function getAsignacionesPorBloqueYPorRut($fecha, $hora_inicio, $rut) {
		$this->load->model('modelo_busquedas');
		$sede_actual = $this->session->id_sede_actual;
		$fecha_query = date('Y-m-d', strtotime($fecha));

		$this->load->helper('configuracion');
		$this->db->select("evento_agenda.*, DATE_FORMAT(evento_agenda.fecha_asignacion,'".getFormatoFechaMySQL()."') as fecha_asignacion");
		$asignaciones = $this->db->where([
				'fecha_asignacion' => $fecha_query,
				'hora_inicio' => $hora_inicio,
				'rut' => $rut,
				'sede' => $sede_actual
			])->get('evento_agenda')->result();
		return $asignaciones;
	}

	public function getAsignacionesPorBloque($fecha, $hora_inicio) {
		$this->load->model('modelo_busquedas');
		$sede_actual = $this->session->id_sede_actual;
		$fecha_query = date('Y-m-d', strtotime($fecha));

		$this->load->helper('configuracion');
		$this->db->select("evento_agenda.*, DATE_FORMAT(evento_agenda.fecha_asignacion,'".getFormatoFechaMySQL()."') as fecha_asignacion");
		$asignaciones = $this->db->where([
				'fecha_asignacion' => $fecha_query,
				'hora_inicio' => $hora_inicio,
				'sede' => $sede_actual
			])->get('evento_agenda')->result();
		return $asignaciones;
	}

	public function agendacionesPorCausa($id_correlativa_causa) {
		$sede = $this->session->id_sede_actual;
		$agendaciones = $this->db->get_where('evento_agenda', ['id_causa'=>$id_correlativa_causa, 'sede'=>$sede])->result();
		return $agendaciones;
	}

	public function traer_promedio_usuario($rut){
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT nota_final FROM audiencias WHERE rut_alumno = '$rut' AND sede = '$sede'");
		return $query;
	}

	public function cantidad_causas_materia() {
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT A.id_materia, COUNT(A.id_materia) as cantidad , B.nombre_materia
									FROM causas A JOIN materia B ON B.id = A.id_materia WHERE termino IS NULL
									AND A.sede = '$sede'
									GROUP BY id_materia ORDER BY cantidad DESC;");
		return $query;
	}

	public function cantidad_causas_profesor(){
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT A.rut_abogado, COUNT(A.rut_abogado) as cantidad , B.nombre
									FROM causas A JOIN usuarios B ON B.rut = A.rut_abogado WHERE termino IS NULL
									AND A.sede = '$sede'
									GROUP BY rut_abogado ORDER BY cantidad DESC;");
		return $query;
	}

	public function cantidad_causas_causal(){
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT B.nom_causal , COUNT(A.id_materia) as cantidad
									FROM causas A JOIN causal_termino B ON B.id_causal_termino = A.causal_termino
									AND A.sede = '$sede'
									GROUP BY nom_causal ORDER BY cantidad DESC;");
		return $query;
	}

	public function cantidad_agendaciones_profesor_h(){
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT B.nombre , COUNT(A.rut) as cantidad
									FROM evento_agenda A JOIN usuarios B ON B.rut = A.rut
									WHERE A.fecha_asignacion < sysdate()
									AND A.sede = '$sede'
									GROUP BY nombre ORDER BY cantidad DESC;");
		return $query;
	}

	public function cantidad_agendaciones_profesor_v(){
		$this->load->model('modelo_busquedas');
			$sede = $this->session->id_sede_actual;
		$query = $this->db->query("SELECT B.nombre , COUNT(A.rut) as cantidad
									FROM evento_agenda A JOIN usuarios B ON B.rut = A.rut
									WHERE A.fecha_asignacion >= sysdate()
									AND A.sede = '$sede'
									GROUP BY nombre ORDER BY cantidad DESC;");
		return $query;
	}

	// GET
	public function causales_termino(){
		$causales_termino = $this->db->get('causal_termino')->result();
		return $causales_termino;
	}

	public function traer_sedes() {
		$rut_user=$this->session->rut;
		$sedes = $this->db->select('usuario_sede.id_sede, sedes.nombre_sede')->where('rut', $rut_user)->join('sedes', 'sedes.id_sede = usuario_sede.id_sede')->get('usuario_sede')->result();
		return $sedes;
	}

	// Devuelve todos los tribunales
	public function tribunales() {
		$query = $this->db->order_by('nombre')->get('tribunal');
		return $query->result();
	}

	// GET
	public function getTribunal($id_tribunal) {
		$tribunal = $this->db->get_where('tribunal', 'id', $id_tribunal)->row();
		return $tribunal;
	}

	// Simplemente retorna fila de sede por id.
	public function getSedePorId($id_sede) {
		$query = $this->db->get_where('sedes', array('id_sede'=>$id_sede));
		return $query->row();
	}
}
?>
