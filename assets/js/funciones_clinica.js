// Configuracion de datepicker segun ajustes para la app
$(document).ready(function() {
	opciones = {
		closeText: 'Cerrar',
		prevText: '<Ant',
		nextText: 'Sig>',
		currentText: 'Hoy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
		dayNamesShort: ['Dom','Lun','Mar','Mie','Juv','Vie','Sab'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sa'],
		weekHeader: 'Sm',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: true,
		yearSuffix: ''
	};
	$.datepicker.setDefaults(opciones);
});

function detalle_asignacion(id) {
	url = "../busqueda/vista_detalles_asignacion/?id_evento="+id;

	$.ajax({ url:url, success:function(result) {
		swal({
			title: '<u>Detalle de Bloque Horario</u>',
			type: 'info',
			html: result,
			imageSize: '50x50'
		});
	}});
}

function main_subir_plantilla() {
	cargar('contenido', 'paginas/subir_plantilla', null, false);
}
function main_plantillas() {
	cargar('contenido', 'paginas/plantillas', null, true);
}
function main_estadisticas() {
	cargar('contenido', 'reporte/estadisticas_profesores', null, true);
}
function main_ingreso_orientacion() {
	cargar('contenido', 'orientacion/vista_ingreso', null, true);
}
function main_ingreso_asunto() {
	cargar('contenido', 'ingreso/vista_asunto', null, true);
}
function main_ingreso_audiencia() {
	cargar('contenido', 'audiencia/vista_ingreso_audiencia', {}, true);
}
function main_orientaciones_sin_resena() {
	cargar('contenido', 'orientacion/vista_orientaciones_sin_resena', null, false);
}
function main_evaluar_audiencias() {
	cargar('contenido', 'audiencia/vista_audiencias_sin_evaluar', {'funcion_exito':'main_evaluar_audiencias'});
}
function main_ingreso_causa() {
	var opciones = {'agendar': false, 'estado_agendar': 'no_agendado', 'ancho':false};
	cargar('contenido', 'ingreso/vista_ingreso_causa', opciones, true);
}
function main_tramites_pendientes() {
	cargar('contenido', 'tramite/vista_tramites_pendientes', {}, true);
}
function main_mis_causas(){
	cargar('contenido', 'busqueda/buscar_mis_causas');
}
function main_audiencias() {
	cargar('contenido', 'ingreso/audiencias', null, true);
}
function main_mis_audiencias() {
	cargar('contenido', 'audiencia/vista_mis_audiencias', null, true);
}
function main_mis_audiencias_por_rellenar() {
	cargar('contenido', 'audiencia/vista_mis_audiencias', {'estado_revision':'en_espera_llenado', 'funcion_exito':'main_mis_audiencias_por_rellenar'}, true);
}
function main_audiencias_evaluadas() {
	cargar('contenido', 'audiencia/vista_mis_audiencias', {'evaluacion_completada':1, 'funcion_exito':'main_audiencias_evaluadas'}, true);
}
function main_nuevo_tramite() {
	cargar('contenido', 'tramite/vista_ingreso_tramite', null, true);
}
function form_busqueda() {
	cargar('contenido', 'busqueda/busqueda_causas', null, true);
}
function form_busqueda_historica() {
	cargar('contenido', 'busqueda/busqueda_causas_historicas', null, true);
}
function form_busqueda_orientacion() {
	cargar('contenido', 'orientacion/vista_busqueda', null, true);
}
function horario_personal() {
	cargar('contenido', 'horario/vista_horario_personal', null, true);
}
function main_causas_incompletas() {
	cargar('contenido', 'causa/vista_causas_incompletas', null, true);
}
function dialog_editar_causa(id_correlativa_causa, callback_exito) {
	callback_exito = '' + callback_exito;
	dialogo('dialog900', 'causa/vista_editar/'+id_correlativa_causa, {'callback':callback_exito, nuevo_dialogo:1});
}
function dialog_rellenar_causa(id_correlativa_causa, callback_exito) {
	callback_exito = '' + callback_exito;
	dialog_editar_causa(id_correlativa_causa, callback_exito);
}
function dialog_eliminar_causa(id_correlativa_causa, callback_exito) {
	la_url = base_url+index_page+'causa/eliminar/'+id_correlativa_causa;
	$.getJSON(la_url,{},function(res){
		$dlgv = nuevoDialogo('dialog300',false,'Eliminar Causa');
		if(res.exito) {
			$dlgv.dialog({
				buttons:{Eliminar:function(){
					$.ajax({url:la_url,method:'POST',data:{'confirmacion':1},dataType:'json',success:function(res2){
						if(res2.exito) { autoCloseSwal('Causa Eliminada','','success'); callback_exito();}
						else {swal('Error',res2.mensaje,'error');}
						cerrar_all_dialogs();
					}});
				},Cancelar:function(){$(this).dialog("close");}}
			});
		}
		$dlgv.append('<p>'+res.mensaje+'</p>'); $dlgv.dialog('open');
	});
}

function buscar_orientaciones() {
	var abogado = $('#abogado').val();
	var usuario = $('#usuario').val();
	cargar('resultados_busquedas', 'orientacion/buscar', {'abogado':abogado, 'usuario':usuario});
}

function abrir_dialogo_audiencia() {
	var rol_causa = $("#rol_causa").val();
	dialogo('dialog', 'audiencia/vista_ingreso_audiencia', {'rol_causa':rol_causa});
}
function dialogo_nueva_audiencia(rol_causa) {
	dialogo('dialog800', 'audiencia/vista_ingreso_audiencia', {'rol_causa':rol_causa,'ancho':1});
}
function dialogo_buscar_usuarios() {
	dialogo('dialog600', 'busqueda/buscar_usuarios', {'nuevo_dialogo':1});
}
function dialogo_seleccionar_causa(funcion_callback, solo_con_rol=1) {
	dialogo('dialog800', 'causa/vista_seleccionar/', {'funcion_callback':funcion_callback, 'nuevo_dialogo':true, 'solo_con_rol':solo_con_rol}, 'POST');
}
function dialogo_seleccionar_orientacion(input_id_orientacion) {
	dialogo('dialog1000', 'orientacion/vista_seleccionar_orientacion', {'input_id_orientacion':input_id_orientacion, 'nuevo_dialogo':1, 'solo_sin_causa':1});
}
function dialogo_detalle_tramite(id_tramite) {
	dialogo('dialog800', 'tramite/vista_detalle_tramite/'+id_tramite);
}
function dialogo_tramites(id_causa) {
	dialogo('dialog2', 'tramite/vista_tabla_tramites/'+id_causa, {titulo:'Trámites de la Causa ID '+id_causa});
}

function trae_audiencias_2(rol_causa) {
	dialogo('dialog', 'audiencia/vista_busqueda_audiencias', {'rol_causa':rol_causa, 'nuevo_dialogo':true});
}

function trae_agendaciones(id_correlativa_causa, nuevoDialogo=true) {
	dialogo('dialog800', 'busqueda/agendaciones_causa', {'id_correlativa_causa':id_correlativa_causa, 'nuevo_dialogo':nuevoDialogo});
}

function terminar_causa(id_correlativa_causa, callback_exito) {
	dialogo('dialog300', 'busqueda/vista_terminar_causa/'+id_correlativa_causa, {'nuevo_dialogo':1, 'callback':callback_exito});
}

function abrir_bloque(dia, hora,profesor_a_cargar) {
	dialogo('dialog', 'agenda/bloque', {'dia':dia, 'hora':hora, 'profesor_a_cargar':profesor_a_cargar});
}

function detalle_bloque(dia, hora, rut_profesor) {
	params = { 'dia':dia, 'hora':hora, 'nuevo_dialogo':1 };
	if(rut_profesor !== null) {
		params.profesor_a_cargar = rut_profesor;
	}
	dialogo('dialog900', 'agenda/detalle_bloque', params);
}

function abrir_detalle_agendar(dia, hora, profesor_a_cargar, tipo, sin_filtro) {
	var funcion_str = "abrir_detalle_agendar('"+dia+"', '"+hora+"', '"+profesor_a_cargar+"', '"+tipo+"', "+sin_filtro+");";
	var params = { 'dia':dia, 'hora':hora, 'profesor_a_cargar':profesor_a_cargar, 'tipo':tipo, 'sin_filtro':sin_filtro, 'funcion_str':funcion_str };
	dialogo('dialog', 'agenda/detalle_agendar', params );
}

function dialog_nueva_agendacion_nueva_causa(dia, hora, rut_abogado) {
	var params = {'dia': dia, 'hora': hora, 'profesor_a_cargar': rut_abogado, 'agendar': true, 'estado_agendar': 'agendado', 'ancho':true}
	dialogo('dialog800', 'ingreso/vista_ingreso_causa', params );
}
function dialog_agendar_nueva_orientacion(rut_abogado, fecha, hora) {
	var params = {'fecha': fecha, 'hora': hora, 'rut_abogado': rut_abogado};
	dialogo('dialog800', 'orientacion/vista_agendar_nueva', params );
}

// IMPORTANTE: estado 'agendado' indica que se le asigna al abogado
// de la sesion actual.
function dialog_nueva_causa(dia, hora, rut_abogado) {
	var params = {'dia': dia, 'hora': hora, 'profesor_a_cargar': rut_abogado, 'agendar': false, 'estado_agendar': 'agendado', 'ancho':true}
	dialogo('dialog2', 'ingreso/vista_ingreso_causa', params );
}

function form_agenda_semanal(dia=0, profesor=0) {
	$("#contenido").empty();
	var url = "../agenda/semanal?hoy="+dia+"&profesor="+profesor;
	$.ajax({
		url: url,
		success:function(result) {
			$result = $(result);
			$result.hide().appendTo('#contenido').fadeIn(1000);
			$('#fecha_causa').datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
		}
	});
}

function cargar_calendario_prof_dia() {
	var dia_a_cargar = $( "#fecha_causa" ).val();
	var profesor_a_cargar = $( "#profesor_seleccionado option:selected" ).val();
	form_agenda_semanal(dia_a_cargar, profesor_a_cargar);
}

function elim_bloq_horario_abog(id_bloque) {
	var ruta = base_url+index_page+'agenda/eliminar_bloque_horario_abogado';
	$.ajax({url:ruta, data:{'id':id_bloque}, success: function (returndata) {
			console.log(returndata);
			if(returndata=="ok"){
				sweetAlert('Éxito!',   'Actualización Exitosa',   'success' );
				var rut = document.getElementById("rut").value;
				mostrar_bloques_abogado(rut);
			}else if(returndata=="hora"){
				sweetAlert('Oops...', 'Error, Revisar Horas de Configuracion!', 'error');
			}else if(returndata=="fecha"){
				sweetAlert('Oops...', 'Error, Revisar Fechas de Configuracion!', 'error');
			}
		}
	});
	var rut=$('#rut').val();
	mostrar_bloques_abogado(rut);
}

// Muestra vista para escribir detalles de la audiencia a agendar.
function dialog_agendar_audiencia(rut_profesor, fecha, id_correlativa_causa, hora_inicio) {
	var opciones = {'dia':fecha, 'id_correlativa_causa':id_correlativa_causa, 'hora':hora_inicio, 'tipo_agendacion':"Au", 'profesor_a_cargar':rut_profesor, nuevo_dialogo:1};
	dialogo('dialog600', 'agenda/vista_nueva_agendacion', opciones);
}

function dialog_agendar_asunto(rut_abogado, fecha, hora_inicio) {
	var opciones = {'fecha': fecha, 'hora_inicio': hora_inicio, 'rut_abogado': rut_abogado};
	dialogo('dialog800', 'agenda/vista_asunto', opciones);
}

function detalle_agendacion(id_evento) {
	dialogo('dialog600', 'agenda/vista_detalle_evento/'+id_evento, {'nuevo_dialogo':1});
}

// modo_edicion: resena, completo, null
// modo_privilegios: por_rut, por_tipo_usuario, null
function dialog_editar_orientacion(id_orientacion, modo_edicion, modo_privilegios) {
	var opciones = {'id_orientacion': id_orientacion, 'modo_edicion':modo_edicion, 'modo_privilegios':modo_privilegios, nuevo_dialogo:1, ancho:1};
	dialogo('dialog800', 'orientacion/vista_editar', opciones);
}

function buscar_alumno(input_id_rut, input_id_nombre) {
	dialogo('dialog800', 'busqueda/vista_tabla_alumnos', {'input_id_rut':input_id_rut, 'input_id_nombre':input_id_nombre, 'nuevo_dialogo':true} );
}

// Abre un dialogo para seleccionar profesor.
// input_id_rut: id del nodo para guardar el rut
// input_id_nombre: id del nodo para guardar el nombre.
function buscar_abogado(input_id_rut, input_id_nombre, callback_exito=null) {
	dialogo('dialog800', 'busqueda/vista_tabla_profesores', {'input_id_rut':input_id_rut, 'input_id_nombre':input_id_nombre, 'nuevo_dialogo':true, 'callback_exito':callback_exito} );
}

// Abre el dialogo especificado y carga la respuesta recibida
// usando el Controlador especificado.
function dialogo(id_dialog, controlador, parametros={}, metodo='GET') {
	var default_params = {'nuevo_dialogo':false, 'titulo':''};
	parametros = $.extend(default_params, parametros);
	if(parametros.nuevo_dialogo == true) {
		var $the_dialogox = nuevoDialogo(id_dialog, false, parametros.titulo);
	} else {
		var $the_dialogox = $( '#'+id_dialog );
	}

	$.ajax({ url:base_url+index_page+controlador, method: metodo,
		data: parametros,
		success:function(result) {
			$the_dialogox.empty().append(result);
			$the_dialogox.find('img[title], button[title]').qtip({
				// position: {
            //  target: 'mouse', // Track the mouse as the positioning target
            //  adjust: { x: 8, y: -8 } // Offset it slightly from under the mouse
	         // }
			});
         if($the_dialogox.dialog('isOpen') === false) {
				$the_dialogox.dialog('open');
			}
		}
	});

}

// Inserta en el contenedor especificado la respuesta recibida
// usando el Controlador especificado.
function cargar(id_contenedor, controlador, params_opcional={}, fade_in=false) {
	var params = null;
	if(params_opcional !== 'undefined') {
		params = params_opcional;
	}
	var $contenedor = $( '#'+id_contenedor );
	$.ajax({ url:base_url+index_page+controlador, data: params,
		success:function(result) {
			$contenedor.empty();
			$result = $(result);
			$result.hide();
			$contenedor.append($result);
			$('#'+id_contenedor+' img[title], #'+id_contenedor+' button[title]').qtip({
				// position: {
            //  target: 'mouse', // Track the mouse as the positioning target
            //  adjust: { x: 8, y: -8 } // Offset it slightly from under the mouse
	         // }
			});
			if(fade_in) $result.fadeIn(900);
			else $result.show();
		}
	});
}

function mostrar_bloques_abogado(rut) {
	// cerrar_all_dialogs();
	// document.getElementById("rut").value=rut;
	cargar('disponibilidad_profesor', 'busqueda/disponibilidad_profesor', {'rut':rut});
}

function form_cambiar_correo() {
	cargar('contenido', 'ingreso/cambio_correo', null, true);
}
function form_cambiar_contrasena() {
	cargar('contenido', 'ingreso/cambio_contrasena', null, true);
}

function main_mantenedor_bloques() {
	cargar('contenido', 'ingreso/mantenedor_bloques', null, true);
}

function ingresar_bloque_profesor() {
	$('#dialog').dialog('close');
	var params = {};
	params.rut = $('#rut').val();
	params.fecha_inicio =$('#fecha_inicio').val();
	params.fecha_termino = $('#fecha_termino').val();
	params.hora_inicio = $('#hora_inicio').val();
	params.hora_termino = $('#hora_fin').val();
	params.dia = $('#dia').val();
	var ruta = "../ingreso/ingresar_conf_agenda";
	$.ajax({
		url: ruta,
		data: params,
		dataType: 'json',
		success: function (result) {
			if(result.exito) {
				sweetAlert('Éxito!',   result.mensaje,   'success' );
				var rut = document.getElementById("rut").value;
				mostrar_bloques_abogado(rut);
			} else {
				sweetAlert('Oops...', result.mensaje, 'warning');
			}
		}
	});
}

// extrae los datos de la vista parcial de seleccion de usuario
// 'partial/partial_selector_cliente.php'
function obtener_datos_usuario() {
	var datos = {};
	datos.nombre_usuario = $('#nombre_usuario').val();
	datos.rut_cliente = $('#rut_usuario').val();
	var split_rut = datos.rut_cliente.split("-");
	datos.usuario = split_rut[0];
	datos.dv = split_rut[1];
	datos.edad_usuario = $('#edad_usuario').val();
	datos.telefono = $('#telefono').val();
	datos.email = $('#correo_electronico').val();
	datos.domicilio = $('#domicilio').val();
	return datos;
}

function ingresar_causa(recargar_agenda=true, formdata) {
	var len = formdata.length;
 	var params = {};
	for (i=0; i<len; i++) {
	  params[formdata[i].name] = formdata[i].value;
	}
	var controlador = "causa/ingresar";
	$.ajax({url:base_url+index_page+controlador, data:formdata, dataType:'JSON', method:'POST', success:function (result) {
			if(result.exito == true) {
				if(params.agendar === 'true') { agendar_causa(params.abogado, params.fecha, result.id_correlativa_causa, params.hora); }
				else if(params.agendar === 'false') {
					if(recargar_agenda === false) {
						$reload = $('#reload_selector_agendar_audiencia');
						if($reload.length) {
							cerrar_all_dialogs();
							$reload.click();
						} else { main_ingreso_causa(); }
					}
					autoCloseSwal('Ingresada', 'Ingreso de Causa exitoso', 'success', 1200);
				}
				else { sweetAlert('Fallo', 'ocurrió un fallo. (mensaje = '+result.mensaje+')', 'error' ); }
			} else { sweetAlert('', result.mensaje, 'error'); }
	}});
}

// Editar POST
function editar_causa(id_correlativa_causa, formData, callback_exito) {
	$.ajax({url:base_url+index_page+'causa/editar/'+id_correlativa_causa, data:formData, dataType:'json', method:'POST', success:function(result) {
		if(result.exito == true) {
			autoCloseSwal('Causa Editada', '', 'success'); cerrar_all_dialogs(); callback_exito();
		} else { sweetAlert('', result.mensaje, 'error'); }
	}});
}

function agendar_causa(rut, fecha, id_correlativa_causa, hora_inicio) {
	var opciones = {'rut': rut, 'fecha': fecha, 'id_correlativa_causa': id_correlativa_causa, 'hora_inicio': hora_inicio};
	var controlador = "agenda/agendar_causa";

	$.ajax({
		dataType: 'json',
		url: base_url+index_page+controlador,
		data: opciones,
		success: function (data) {
			if(data.exito == true) {
				cerrar_all_dialogs();
				sweetAlert('Éxito!', 'Agendacion Exitosa', 'success' );
			} else {
				sweetAlert('Problema Desconocido', 'Ocurrió un error desconocido en "agendar_causa()" \n (' + data.mensaje + ')', 'info' );
			}
			cargar_calendario_prof_dia();
		}
	});
}

//TODO. utilizar formData :).
function agendar_audiencia() {
	var opciones = {
		'titulo_evento': $('#form_agendar_audiencia #nombre_asunto').val(),
		'descripcion_evento': $('#form_agendar_audiencia #descripcion_asunto').val(),
		'rut_abogado': $('#form_agendar_audiencia #rut_del_agendado').val(),
		'fecha': $('#form_agendar_audiencia #fecha_ingreso').val(),
		'hora': $('#form_agendar_audiencia #set_hora').val(),
		'id_correlativa_causa': $('#form_agendar_audiencia #id_correlativa_causa').val()
	};
	var controlador = "agenda/agendar_audiencia";

	$.ajax({
		dataType: 'json',
		url: base_url+index_page+controlador,
		data: opciones,
		success: function (data) {
			if(data.exito) {
				sweetAlert('Éxito', 'Agendacion exitosa', 'success' );
				cerrar_all_dialogs();
				cargar_calendario_prof_dia();
			} else {
				sweetAlert('Problema Desconocido', 'en agendar_audiencia()', 'info' );
			}

		}
	});
}

// Agenda y/o Ingresa un asunto.
function ingresar_asunto(formData, agendar=false) {
	$.getJSON(base_url+index_page+"ingreso/ingresar_asunto",formData, function(result) {
		if(result.id_asunto != 0) {
			if(agendar) {
				// formData.push({name:'id_asunto', value:id_asunto},{name:'id_evento', value:result.id_evento});
				$.getJSON(base_url+index_page+'/agenda/agendar_asunto', result, function(resul2) {
					if(resul2.exito == true) {
						sweetAlert('Éxito!',   'Asunto agendado exitosamente',   'success' );
						cerrar_all_dialogs(); cargar_calendario_prof_dia();
					} else sweetAlert('Problema', 'No se pudo ingresar el asunto.', 'warning');
				});
			} else {
				sweetAlert('Éxito', 'Asunto Ingresado.', 'success');
				main_ingreso_asunto(); }
		} else swal('Problema', 'No se pudo ingresar el asunto.', 'warning');
	});
}

// Agenda y/o Ingresa una orientacion.
function ingresar_orientacion(formData, agendar=false) {
	$.getJSON(base_url+index_page+'/orientacion/ingresar', formData, function(result) {
		if(result.id_orientacion !== -1) {
			if(agendar) {
				formData.push({name:'id_orientacion', value:result.id_orientacion});
				$.getJSON(base_url+index_page+'/agenda/agendar_orientacion', formData, function(result) {
					if(result.exito == true) {
						sweetAlert('Exito', 'Orientación asignada.', 'success');
						cerrar_all_dialogs(); cargar_calendario_prof_dia();
					} else sweetAlert('Problema', 'No se pudo ingresar la orientación.', 'warning');
				});
			} else {
				sweetAlert('Éxito!', 'Orientacion Ingresada Exitosamente', 'success');
				main_ingreso_orientacion();
			}
		} else sweetAlert('', result.mensaje, 'error');
	});
}

function cerrar_sesion() {
	window.location.href = base_url+index_page+"menu/cerrar_sesion";
}

function ingresar_audiencia(formData, funcion_exito) {
	var url = base_url+index_page+"audiencia/ingresar_audiencia/";
	$.getJSON(url, formData, function(result) {
		if(result.exito) {
			swal('', 'audiencia ingresada', 'success');
			cerrar_all_dialogs();
			funcion_exito();
		}
		else swal('', 'ocurrio un problema al ingresar audiencia', 'error');
	});
}

function update_audiencia(formData, funcion_exito=form_panel) {
	$.getJSON(base_url+index_page+"audiencia/actualizar_audiencia", formData, function(result) {
		if(result.exito) {
			funcion_exito(); cerrar_all_dialogs();
			if(result.nota_final != false) {
				autoCloseSwal('Éxito', '<div style="font-size:1.4em;margin-bottom:14px;">Audiencia Evaluada \n<b>Nota final:</b> <label style="font-weight:600;font-size:1.3em">'+result.nota_final+'</label></div>', 'success', 1600);
			} else autoCloseSwal('Éxito', 'audiencia actualizada', 'success', 900);
		}
	});
}

function ver_audiencia(id_audiencia) {
	dialogo('dialog800', 'audiencia/vista_detalle/'+id_audiencia, {'ancho':1});
}

function cambiar_sede(rut) {
	var link = base_url+index_page+"busqueda/traer_sedes";
	var sede ="";
	$.ajax({
		url:link,
		success: function(result) {
			swal({
				title: 'Seleccione Sede: ',
				html:
				result,
				preConfirm: function() {
					return new Promise(function(resolve, reject) {
						if ( !($('#sedes').val() === "no") ) {
							var link2 = base_url+index_page+"ingreso/update_sede_actual/?sede_seleccionada="+$('#sedes').val();
							sede = $("#sedes option:selected").text();
							$.ajax({url:link2, success:function(result){resolve();}});
						}
						else {
							reject('Debe seleccionar una Sede');
						}
					});
				}
			}).then(function(result) {
					$("#sede_act").empty();
					$("#sede_act").append(sede);
				  swal({
					type: 'success',
					html: 'Sede Actual Cambiada'
				  });
				  var url = "../menu/slider";
					$('#dialog').dialog('close');
					$("#contenido").empty();

					$.ajax({url:url,success:function(result){
					$(result).hide().appendTo('#contenido').fadeIn(1000);
					}});
				})
		}
	});
}

//WORKER PARA NOTIFICACIONES DE ALERTA
var w;
var ultimos_datos;
var tabla_dt_alertas;
//~ $tabla_asig = $("#tabla_asig");
function startWorkerAlertas() {
	if(typeof(Worker) !== "undefined") {
		if(typeof(w) == "undefined") {
			w = new Worker(base_url+"assets/js/worker_asig_rechazadas.js");
			w.postMessage({"base_url":base_url, "index_page":index_page});
		}
		w.onmessage = function(event) {
			//document.getElementById("result").innerHTML = event.data;
			console.log("iteración del worker: "+ event.data.numero);

			if(JSON.stringify(event.data.datos) !== JSON.stringify(ultimos_datos)) {
				console.log('Llegaron datos actualizados desde el Worker.');
				if(event.data.datos.length > 0) {
					ultimos_datos = event.data.datos;
					$('#contador_alertas').text(event.data.datos.length);
					$('#contador_alertas').show();

					var fila=""; // en realidad es filas.
					for (asig in event.data.datos) {
						var id_asig = event.data.datos[asig].id_evento;
						var id_fila_tr = 'fila_rechazado_' + id_asig;
						fila += '<tr id="' + id_fila_tr + '">';
						fila += "<td><label>" + event.data.datos[asig].fecha_asignacion + "<label></td>";
						fila += "<td><label>" + event.data.datos[asig].hora_inicio + "<label></td>";
						fila += "<td><label>" + event.data.datos[asig].nombre + "<label></td>";
						fila += "<td><label>(" + id_asig + ") " + event.data.datos[asig].tipo_asunto + "<label></td>";
						fila += '<td><button type="button" class="ui-button ui-widget ui-corner-all" onclick="reenviar_asignacion(\''+id_asig+'\',\''+id_fila_tr+'\');">';
						fila += '<span class="ui-icon ui-icon-transfer-e-w"></span> Reenviar</button>';
						fila += '<button type="button" class="ui-button ui-widget ui-corner-all" onclick="confirm_borrar_asignacion(\''+id_asig+'\',\''+id_fila_tr+'\');">';
						fila += '<span class="ui-icon ui-icon-triangle-1-s"></span> Aceptar Rechazo</button></td>';
						fila += "</tr>";
					}
					$('#dialog_alertas tbody:first').html(fila);

					if ( $.fn.dataTable.isDataTable('#tabla_asig') ) {
						$("#tabla_asig").DataTable();
					}
					else {
						tabla_dt_alertas = $("#tabla_asig").DataTable( {
							"responsive":false,
							"autoWidth":false,
							"pageLength": 5,
							"pagingType": "simple_numbers"
						} );
					}
					$('#dialog_alertas .area_gris').addClass('oculto');
					$('#dialog_alertas .div_tabla').removeClass('oculto');
				} else {
					$('#dialog_alertas .area_gris').removeClass('oculto');
					$('#dialog_alertas .div_tabla').addClass('oculto');
					$('#contador_alertas').hide();
				}
			}

		};
	} else {
		console.log("Navegador No Soportado.");
	}
}

function stopWorker() {
	w.terminate();
	w = undefined;
}

function mostrar_dialog_alertas() {
	$('#dialog_alertas').dialog('open');
}

function confirm_borrar_asignacion(id_evento, id_fila_tabla) {
	var $select_motivos = $("#dialog_borrar_asignacion select#motivos");
	var $dialog = $( "#dialog_borrar_asignacion" ).dialog({
		position:{my:'top', at:'center top+15%'},
		show: 'slideDown', hide: 'slideUp',
		minHeight: 200,
		width: 350,
		modal: true,
		buttons: [
			{text:"Aceptar", icons:{primary:"ui-icon-check"}, click:function() {
				var select_val = $select_motivos.val();
				if (select_val != null) {
					$dialog.dialog("close");
					$.getJSON(base_url+index_page+"agenda/cancelar_evento_calendario", {'id_evento':id_evento}, function(result) {
						if(result.exito) {
							var id_login = document.getElementById("user_logeado").value;
							$.ajax({url:base_url+index_page+'agenda/eliminar_evento', data:{'id_evento':id_evento,'motivo':select_val,'quien_elimina':id_login}, dataType:'json', success:function(result2) {
								if(result2.exito) {
									autoCloseSwal('Actualizacion Completa', '', 'success'); tabla_dt_alertas.row( $('#'+id_fila_tabla) ).remove().draw(); cargar_calendario_prof_dia();
								}
								else swal({type: 'error', html: result2.mensaje});
							}});
						}
						else swal({type: 'error', html: result.mensaje});
					});
				} else { alert('Debe seleccionar un motivo'); }
			}},
			{text:"Cancel", click:function() {$dialog.dialog("close");}}
		],
		close: function() { $dialog.dialog('destroy'); $select_motivos.selectmenu("destroy"); }
	});
	$select_motivos.selectmenu();
}

function dlg_borr_asig(id_evento) {
	var $select_motivos = $("#dialog_borrar_asignacion select#motivos");
	var $dialog = $( "#dialog_borrar_asignacion" ).dialog({
		position:{my:'top', at:'center top+15%'},
		show: 'slideDown', hide: 'slideUp',
		minHeight: 200,
		width: 350,
		modal: true,
		buttons: [
			{text:"Aceptar", icons:{primary:"ui-icon-check"}, click:function() {
				var select_val = $select_motivos.val();
				if (select_val != null) {
					$dialog.dialog("close");

					$.getJSON("../agenda/cancelar_evento_calendario", {'id_evento':id_evento}, function(result) {
						if(result.exito) {
							var id_login = document.getElementById("user_logeado").value;
							$.ajax({url:base_url+index_page+'agenda/eliminar_evento', data:{'id_evento':id_evento,'motivo':select_val,'quien_elimina':id_login}, dataType:'json', success:function(result2) {
								if(result2.exito) {
									autoCloseSwal('Agendación Borrada', '', 'success'); cerrar_all_dialogs(); cargar_calendario_prof_dia();
								}
								else swal({type: 'error', html: result2.mensaje});
							}});
						}
						else swal({type: 'error', html: result.mensaje});
					});
				} else { alert('Debe seleccionar un motivo'); }
			}},
			{text:"Cancel", click:function() {$dialog.dialog("close");}}
		],
		close: function() { $dialog.dialog('destroy'); $select_motivos.selectmenu("destroy"); }
	});
	$select_motivos.selectmenu();
}

function reenviar_asignacion(id_evento, id_fila_tabla) {
	console.log("id_asig="+id_evento+", id_fila="+id_fila_tabla);
	var $dialog = $('<div class="centro" title="Reenvío de Asignación"><h2>Mensaje:</h2><textarea style="width:100%" id="textarea_reenviar" name="resena" rows="4" id="resena"></textarea></div>').dialog({
		modal: true,
		show: 'slideDown', hide: 'slideUp',
		position:{my:'top', at:'center top+15%'},
		width: 350,
		buttons: [
			{
				text: "Enviar Correo",
				icons: { primary: "ui-icon-mail-closed" },
				click: function() {
					var mensaje_reenvio = $('#textarea_reenviar').val();
					$.getJSON(base_url+index_page+'agenda/reenviar_asignacion', {'id_evento': id_evento, 'mensaje_reenvio': mensaje_reenvio}, function(result) {
						console.log("data cancelar_evento_calendario: " + result);
						if(result.exito) {
							$dialog.dialog("close");
							autoCloseSwal('', 'Reenviado', 'success');
							tabla_dt_alertas.row( $('#'+id_fila_tabla) ).remove().draw();
						}
					});
				}
			},
			{ text:"Cancelar", click:function(){$dialog.dialog("close");} }
		],
		close: function() { $dialog.dialog('destroy');}
	});
}

function dialog_cambiar_usuario(tipo_usuario, id_causa, nom_usuario) {
	var url="../busqueda/vista_cambiar_usuario/"+id_causa;
	var $dialog_cambio_usr = $('<div></div>');
	$.ajax({url: url, data: {'tipo_usuario': tipo_usuario, 'nombre_usuario_actual':nom_usuario},
		success:function(result) {
			$dialog_cambio_usr.html(result);
			$dialog_cambio_usr.dialog({
				"buttons":
				  [
					{
						text: "Ok",
						icons: {primary: "ui-icon-check"},
						click: function() {
							// CODIGO DE CAMBIO DE USUARIO
							var rut_nuevo = $('#in_rut_nuevo_usr').val();
							if (rut_nuevo.length < 7) {
								swal("rut inválido");
								return;
							}
							var bool_rep_all = $('#chk_reemplazar_en_todo').is(":checked");
							if(bool_rep_all) {
								var rre = confirm("¿Está seguro que desea reemplazar a dicho "+tipo_usuario+" reemplazar en TODAS sus causas?");
								if (rre == false) {
									return;
								}
							}
							$.getJSON("../ingreso/cambiar_" + tipo_usuario, {'id_causa': id_causa, 'rut': rut_nuevo, 'reemplazar_all': bool_rep_all}, function(data) {
								if(data.bool_result == true) {
									autoCloseSwal(null, '¡'+tipo_usuario+' cambiado!', 'success');
								} else {
									swal("No se pudo cambiar "+tipo_usuario+".");
								}
							});
							$(this).dialog('destroy').remove();
						}
					},
					{
						text: "cancelar",
						click: function() {
						  $(this).dialog('destroy').remove();
						}
					}
				],
				width:700,
				close: function(){$(this).dialog('destroy').remove();}
			});
	}});
}

// agrega datepicker segun ajustes para la app
function ponerDatepicker($container, inputSelector) {
	$container.find(inputSelector).datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
}

// cargar_main = si es true, se refrezca la vista de orientaciones sin resena
function guardar_resena(id_orientacion, cargar_main) {
	var resena = $('#resena').val();
	$.getJSON(
		base_url+index_page+"ingreso/actualizar_orientacion",
		{'resena':resena, 'id_orientacion':id_orientacion},
		function(respuesta) {
			if(respuesta === true) {
				sweetAlert('Éxito', 'Reseña guardada exitosamente', 'success');
				$('#dialog2').dialog('close');
				if(cargar_main === true) {
					main_orientaciones_sin_resena();
				}
			} else {
				sweetAlert('Problema', 'No se pudo guardar la reseña.', 'warning');
			}
		}
	);
}

// actualiza orientacion.
// TODO. PASAR A POST.
function actualizar_orientacion(formData, funcion_exito) {
	$.getJSON(base_url+index_page+"orientacion/actualizar", formData,
		function(result) {
			if(result.exito) {
				sweetAlert('', 'Orientacion Actualizada', 'success');
				cerrar_all_dialogs();
				funcion_exito();
			} else {
				sweetAlert('Problema', 'No se pudo guardar la reseña.', 'warning');
			}
		}
	);
}

function guardar_observaciones(id_asunto) {
	var observaciones = $('#observaciones').val();
	$.getJSON(
		base_url+index_page+"ingreso/actualizar_asunto",
		{'observaciones':observaciones, 'id_asunto':id_asunto},
		function(respuesta) {
			if(respuesta === true) {
				sweetAlert('Éxito', 'Observación guardada exitosamente', 'success');
				$('#dialog2').dialog('close');
			} else {
				sweetAlert('Problema', 'No se pudo guardar la Observación.', 'warning');
			}
		}
	);
}

function cerrar_all_dialogs() {
	$.each($('div.ui-dialog-content'), function (i, e) {
		if ($(this).dialog("isOpen")) { $(this).dialog("close"); $(this).empty() };
	});
}

// Cierra el dialogo que contenga el nodo con id especificado.
// Asegurarse de usar ids que no se repitan.
function cerrar_dialog_conteniendo(id_nodo_hijo) {
	$dialogo_padre = $('#'+ id_nodo_hijo).closest('.ui-dialog-content.ui-widget-content');
	$dialogo_padre.dialog('close');
	$dialogo_padre.empty();
}

function autoCloseSwal(titulo, texto, tipo_swal='info', milisecs=1000) {
	if(titulo == null) titulo = '';
	swal({
	  title: titulo,
	  text: texto,
	  type: tipo_swal,
	  timer: milisecs,
	  showConfirmButton: false
	});
}

// Dialogo. la idea es usar el mismo dialogo para que se autoreemplaze.
function causas_usuario(tipo_usuario, rut, solo_vigentes=false) {
	dialogo('dialog80p', 'busqueda/detalle_'+tipo_usuario+'/'+rut+'/'+solo_vigentes);
}

function confirmar_lectura(btn, id_act, tipo_act) {
	var url = base_url+index_page;
	switch (tipo_act) {
		case 'tramite': url += 'tramite/confirmar_lectura_tramite/'; break;
		case 'audiencia': url += 'audiencia/confirmar_lectura/'; break;
	}
	$.getJSON(url+id_act, {}, function(result) {
		if(result.exito) {
			$(btn).closest('.ui-dialog-content').dialog('close');
			swal('', 'letura confirmada', 'success');
		}
		else swal('', 'ocurrio un problema: '+result.mensaje, 'error');
	});
}

// ¡Crea un nuevo dialogo programaticamente!
function nuevoDialogo(idDialogo, auto_abrir=false, titulo='') {
	var width_dialog = 1200;
	switch(idDialogo) {
		case 'dialog300': width_dialog = 300; break;
		case 'dialog600': width_dialog = 600; break;
		case 'dialog800': width_dialog = 800; break;
		case 'dialog900': width_dialog = 900; break;
		case 'dialog1000': width_dialog = 1000; break;
		case 'dialog80p': width_dialog = window.innerWidth < 1600 ? '80%' : 1200; break;
		case 'dialog90p': width_dialog = window.innerWidth < 1600 ? '90%' : 1600; break;
	}
	var $contenedor = $('<div class=""></div>');
	$contenedor.dialog({
		title: titulo,
		autoOpen: auto_abrir,
		resizable: false,
		modal: true,
		width: width_dialog,
		height: 'auto',
		close: function(event, ui) {
			$(this).dialog('destroy').empty().remove();
		}
	});
	return $contenedor;
}
