// url: http://stackoverflow.com/a/23024313/3555293

// La sesión se cierra al transcurrir este tiempo.
var timeoutSesion;

// Los últimos X segundos antes de cerrar la sesión.
var startWarningAt;

// Se muestra un mensaje al transcurrir este tiempo.
var timeoutWarning;

// Set timeout variables.
var warningTimer;
var timeoutTimer;
var updateIntervaler;

var contador;

onmessage = function(e) {
	if(e.data.hasOwnProperty('init')) {
		startWarningAt = e.data.init.startWarningAt;
		timeoutSesion = e.data.init.timeoutSesion;

		timeoutWarning = timeoutSesion - startWarningAt;
		postMessage({'mensaje':"Advertencia de cierre de sesión = dentro de "+timeoutWarning+" segundos.", accion:''});
		StartWarningTimer();
	}
	else if(e.data.hasOwnProperty('reiniciar_timer') && e.data.reiniciar_timer) {
		ResetTimeOutTimer();
	}
}

// Start warning timer.
function StartWarningTimer() {
	warningTimer = setTimeout(IdleWarning, timeoutWarning*1000);
}

// Reset timers.
function ResetTimeOutTimer() {
	clearTimeout(timeoutTimer);
	clearInterval(updateIntervaler);
	clearTimeout(warningTimer);
	StartWarningTimer();
	postMessage({'mensaje':'ResetTimeOutTimer()', accion:''});
}

// Show idle timeout warning dialog.
function IdleWarning() {
	clearTimeout(warningTimer);

	contador = startWarningAt;
	logoutUpdate();
	updateIntervaler = setInterval(logoutUpdate, 1000);
	timeoutTimer = setTimeout(IdleTimeout, startWarningAt*1000);

	postMessage({'mensaje':'IdleWarning()',accion:'MOSTRARALERT'});
	//TODO. usar ResetTimeOutTimer()
	// Add code in the #timeout element to call ResetTimeOutTimer() if
	// the "Stay Logged In" button is clicked
}

// Envia los segundos restantes al Cliente.
function logoutUpdate() {
	postMessage({'mensaje':'',accion:'TIMERUPDATE', 'segundos':contador});
	contador--;
}

// Logout the user.
function IdleTimeout() {
	clearTimeout(updateIntervaler);
	postMessage({'mensaje':'IdleTimeout()',accion:'CERRARSESION'});
}
