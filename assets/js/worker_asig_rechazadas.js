var i = 0;
var segundos_ciclo = 30;
var base_url;
var index_page;

onmessage = function(e) {
    base_url = e.data.base_url;
	 index_page = e.data.index_page;
};

function obtenerAsigRechazadas() {
    i = i + 1;


	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {

			// DEBUG:
			//~ console.log("responseText: \n"+this.responseText);

			datajson = JSON.parse(this.responseText);

			//
			// Formato de fecha a DIA-MES-AÑO
			var regex = /^(\d{4})[-/](\d{2})[-/](\d{2})$/;
			for (asig in datajson) {
				var fecha = datajson[asig].fecha_asignacion;
				var captura = regex.exec(fecha);

				if(captura === null) {
					console.log("fecha correctamente formateada ("+JSON.stringify(captura)+") (dia mes año)");
				}
				else {
					datajson[asig].fecha_asignacion = captura[3] +"-"+ captura[2] +"-"+ captura[1];
				}
			} //fin Formateo

			postMessage({"numero": i, "mensaje": "rechazadas", "datos": datajson});
		}
	};
	xhttp.open("GET", base_url+index_page+"/busqueda/obtener_asig_rechazadas", true);
	xhttp.send();

    setTimeout("obtenerAsigRechazadas()", segundos_ciclo*1000);
}

setTimeout("obtenerAsigRechazadas()", 500);
