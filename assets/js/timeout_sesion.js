// url: http://stackoverflow.com/a/23024313/3555293
// VALORES EN SEGUNDOS.
var startWarningAt = 20;

// Se le restan 10 segundos para asegurar que la peticion de refrescar sesión llegue al servidor a tiempo.
var timeoutSesion = _timeoutSesion - 10;

var $dialogLogout = $('<div style="display:none" title="Advertencia"><p>La sesión se cerrará en <span id="tiempo_restante">???</span> segundos.</p></div>');
if(typeof(timerSesion) == "undefined") {
	timerSesion = new Worker(base_url+"assets/js/worker_timeout_sesion.js");
	timerSesion.postMessage({'init':{'timeoutSesion':timeoutSesion,'startWarningAt':startWarningAt}});
	timerSesion.onmessage = function(e) {
		console.log('timer dice: '+e.data.mensaje);
		switch (e.data.accion) {
			case 'MOSTRARALERT':
			$dialogLogout.dialog({
				resizable: false,
				modal: true,
				width: 'auto',
				height: 'auto',
				close: function(event, ui) {

				}, buttons: [
					{
						text: "Continuar",
						icons: {primary: "ui-icon-check"},
						click: function() {
							$(this).dialog( "close" );
							timerSesion.postMessage({'reiniciar_timer':true});
							$.ajax({global:false,url:base_url+index_page+'paginas/hola'});
						}
					},
					{
						text: "Cerrar Sesión",
						icons: {primary: "ui-icon-power"},
						click: function() {
							window.open(base_url+index_page+"menu/cerrar_sesion",'_self');
						}
					}
			  ]
			});
			break;
			case 'CERRARSESION': window.open(base_url+index_page+"menu/cerrar_sesion",'_self'); break;
			case 'TIMERUPDATE': $dialogLogout.find('#tiempo_restante').html(e.data.segundos); break;
		}
	}
}
