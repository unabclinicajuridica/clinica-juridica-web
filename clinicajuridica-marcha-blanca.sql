-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2017 at 09:47 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinicajuridica-marcha-blanca`
--

-- --------------------------------------------------------

--
-- Table structure for table `abogado_alumnos`
--

CREATE TABLE `abogado_alumnos` (
  `id` int(11) NOT NULL,
  `id_abogado` int(11) NOT NULL COMMENT 'tabla usuarios',
  `id_alumno` int(11) NOT NULL COMMENT 'tabla usuarios',
  `fecha_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `año` year(4) DEFAULT NULL,
  `semestre` tinyint(4) DEFAULT NULL,
  `nrc` varchar(16) DEFAULT NULL,
  `seccion` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abogado_alumnos`
--

INSERT INTO `abogado_alumnos` (`id`, `id_abogado`, `id_alumno`, `fecha_ingreso`, `año`, `semestre`, `nrc`, `seccion`) VALUES
(1, 2, 33, '2017-06-08 15:04:22', NULL, NULL, NULL, NULL),
(2, 13, 34, '2017-06-08 16:18:26', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `archivos`
--

CREATE TABLE `archivos` (
  `id` int(11) NOT NULL,
  `id_tramite` int(11) DEFAULT NULL,
  `file_ext` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orig_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nombre del archivo subido por el usuario',
  `file_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nombre almacenado en el servidor',
  `file_size` float NOT NULL COMMENT 'expresado en kilobytes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `archivos`
--

INSERT INTO `archivos` (`id`, `id_tramite`, `file_ext`, `file_type`, `file_path`, `orig_name`, `file_name`, `file_size`) VALUES
(3, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'BITACORA_ESTADO_DEL_CASO.doc', 'BITACORA_ESTADO_DEL_CASO.doc', 40.5),
(4, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'BITACORA_ESTADO_DEL_CASO.jpg', 'BITACORA_ESTADO_DEL_CASO.jpg', 18.17),
(5, NULL, '.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.d', '/var/www/html/plantillas/', 'CATASTRO_TÉRMINO_DE_ASUNTOS_CLINICA.docx', 'CATASTRO_TÉRMINO_DE_ASUNTOS_CLINICA.docx', 44.67),
(6, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'CATASTRO_TÉRMINO_DE_ASUNTOS_CLINICA.jpg', 'CATASTRO_TÉRMINO_DE_ASUNTOS_CLINICA.jpg', 20.21),
(7, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'EVALUACION_DE_DESEMPEÑO_EN_AUDIENCIA.doc', 'EVALUACION_DE_DESEMPEÑO_EN_AUDIENCIA.doc', 39),
(8, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'EVALUACION_DE_DESEMPEÑO_EN_AUDIENCIA_CLINICA_UNAB.jpg', 'EVALUACION_DE_DESEMPEÑO_EN_AUDIENCIA_CLINICA_UNAB.jpg', 42.6),
(9, NULL, '.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.d', '/var/www/html/plantillas/', 'FICHA_DERIVACIÓN.docx', 'FICHA_DERIVACIÓN.docx', 153.48),
(10, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'FICHA_DERIVACIÓN.jpg', 'FICHA_DERIVACIÓN.jpg', 20.97),
(11, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'FICHA_UNICA_DE_INGRESO_A_CLÍNICA.doc', 'FICHA_UNICA_DE_INGRESO_A_CLÍNICA.doc', 47.5),
(12, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'FICHA_UNICA_DE_INGRESO_A_CLÍNICA.jpg', 'FICHA_UNICA_DE_INGRESO_A_CLÍNICA.jpg', 36.07),
(13, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'FORMULARIO_ENCARGO_DILIGENCIA.doc', 'FORMULARIO_ENCARGO_DILIGENCIA.doc', 27.5),
(14, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'FORMULARIO_ENCARGO_DILIGENCIA_RECEPTORES_TURNO.jpg', 'FORMULARIO_ENCARGO_DILIGENCIA_RECEPTORES_TURNO.jpg', 27.37),
(15, NULL, '.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.d', '/var/www/html/plantillas/', 'INFORME_MENSUAL_CASOS.docx', 'INFORME_MENSUAL_CASOS.docx', 15.24),
(16, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'INFORME_MENSUAL_CASOS.jpg', 'INFORME_MENSUAL_CASOS.jpg', 26.07),
(17, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'FORMULARIO_ACUERDO_ALIMENTOS.doc', 'FORMULARIO_ACUERDO_ALIMENTOS.doc', 37),
(18, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'FORMULARIO_PARA_USUARIO_DE_ACUERDO_EN_AUDIENCIA_ALIMENTOS.jpg', 'FORMULARIO_PARA_USUARIO_DE_ACUERDO_EN_AUDIENCIA_ALIMENTOS.jpg', 30.37),
(19, NULL, '.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.d', '/var/www/html/plantillas/', 'LISTA_DE_TESTIGOS.docx', 'LISTA_DE_TESTIGOS.docx', 95.3),
(20, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'LISTA_DE_TESTIGOS.jpg', 'LISTA_DE_TESTIGOS.jpg', 23.02),
(21, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'OBLIGACIONES_ALUMNOS_CONSULTORIO.doc', 'OBLIGACIONES_ALUMNOS_CONSULTORIO.doc', 34),
(22, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'OBLIGACIONES_ALUMNOS_CONSULTORIO.jpg', 'OBLIGACIONES_ALUMNOS_CONSULTORIO.jpg', 38.25),
(23, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'RECEPTOR_AD_HOC.doc', 'RECEPTOR_AD_HOC.doc', 35.5),
(24, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'RECEPTOR_AD_HOC.jpg', 'RECEPTOR_AD_HOC.jpg', 37.88),
(25, NULL, '.docx', 'application/vnd.openxmlformats-officedocument.wordprocessingml.d', '/var/www/html/plantillas/', 'REGLAMENTO_DE_USUARIOS.docx', 'REGLAMENTO_DE_USUARIOS.docx', 138.08),
(26, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'REGLAMENTO_DE_USUARIOS.jpg', 'REGLAMENTO_DE_USUARIOS.jpg', 38.77),
(27, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'REVISION_CARPETA_POR_PROFESOR.doc', 'REVISION_CARPETA_POR_PROFESOR.doc', 36.5),
(28, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'REVISION_CARPETA_POR_PROFESOR.jpg', 'REVISION_CARPETA_POR_PROFESOR.jpg', 16.91),
(29, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', 'AMONESTACIÓN_ESCRITA.doc', 'AMONESTACIÓN_ESCRITA.doc', 26),
(30, NULL, '.jpg', 'image/jpeg', '/var/www/html/plantillas/thumbnails/', 'amonestacion_escrita.jpg', 'amonestacion_escrita.jpg', 14.92),
(31, NULL, '.doc', 'application/msword', '/var/www/html/plantillas/', '2017-01-09_Acta_Reunion_CJ.doc', '2017-01-09_Acta_Reunion_CJ.doc', 90);

-- --------------------------------------------------------

--
-- Table structure for table `asuntos`
--

CREATE TABLE `asuntos` (
  `id_asunto` int(11) NOT NULL,
  `titulo_asunto` varchar(45) NOT NULL,
  `descripcion` varchar(45) NOT NULL,
  `rut_asociado` varchar(45) NOT NULL,
  `sede` varchar(45) NOT NULL,
  `observaciones` varchar(1024) DEFAULT NULL,
  `id_causa` int(11) DEFAULT NULL COMMENT 'id correlativa (de tabla)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asuntos`
--

INSERT INTO `asuntos` (`id_asunto`, `titulo_asunto`, `descripcion`, `rut_asociado`, `sede`, `observaciones`, `id_causa`) VALUES
(1, 'Comprar Tinta Impresora', 'De Color Azul Marca DHUHXXX.', '17791981', '1', NULL, 0),
(2, 'No venir hoy', 'Es importante.', '9633643', '1', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `audiencias`
--

CREATE TABLE `audiencias` (
  `id_audiencia` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `rol_causa` varchar(64) NOT NULL COMMENT 'ROL de la causa',
  `tipo_audiencia` varchar(45) DEFAULT NULL,
  `numero_audiencia` int(11) DEFAULT NULL,
  `rut_alumno` int(11) DEFAULT NULL,
  `rut_profesor` int(11) DEFAULT NULL,
  `nota_registro_1` int(2) DEFAULT NULL,
  `nota_registro_2` int(2) DEFAULT NULL,
  `nota_registro_3` int(2) DEFAULT NULL,
  `nota_registro_otros` varchar(45) DEFAULT NULL,
  `nota_destreza_1` int(2) DEFAULT NULL,
  `nota_destreza_2` int(2) DEFAULT NULL,
  `nota_destreza_3` int(2) DEFAULT NULL,
  `nota_destreza_4` int(2) DEFAULT NULL,
  `nota_destreza_5` int(2) DEFAULT NULL,
  `nota_destreza_6` int(2) DEFAULT NULL,
  `nota_destreza_7` int(2) DEFAULT NULL,
  `nota_destreza_8` int(2) DEFAULT NULL,
  `nota_destreza_9` int(2) DEFAULT NULL,
  `nota_destreza_10` int(2) DEFAULT NULL,
  `nota_destreza_11` int(2) DEFAULT NULL,
  `nota_destreza_12` int(2) DEFAULT NULL,
  `nota_item_1` int(2) DEFAULT NULL,
  `nota_item_2` int(2) DEFAULT NULL,
  `nota_item_3` int(2) DEFAULT NULL,
  `nota_final` int(2) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `sede` int(11) NOT NULL,
  `evaluacion_completada` tinyint(1) NOT NULL DEFAULT '0',
  `estado_revision` varchar(17) NOT NULL DEFAULT 'EN_ESPERA_LLENADO' COMMENT 'EN_ESPERA_LLENADO, PENDIENTE, EVALUADA, LEIDA, CERRADA',
  `comentario_abogado` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bloques`
--

CREATE TABLE `bloques` (
  `id` int(11) NOT NULL,
  `tipo_horario` varchar(45) NOT NULL,
  `inicio` varchar(45) NOT NULL,
  `termino` varchar(45) NOT NULL,
  `sede` int(11) DEFAULT NULL,
  `oficina` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bloques`
--

INSERT INTO `bloques` (`id`, `tipo_horario`, `inicio`, `termino`, `sede`, `oficina`) VALUES
(1, 'D', '08:30:00', '13:00:00', 1, NULL),
(2, 'D', '08:00:00', '20:00:00', 2, NULL),
(3, 'D', '08:00:00', '20:00:00', 3, NULL),
(4, 'D', '15:00:00', '18:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `causal_termino`
--

CREATE TABLE `causal_termino` (
  `id_causal_termino` int(11) NOT NULL,
  `nom_causal` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `causal_termino`
--

INSERT INTO `causal_termino` (`id_causal_termino`, `nom_causal`) VALUES
(1, 'ABANDONO DE PROPIEDAD'),
(2, 'FAVORABLE'),
(3, 'CONCILIACION'),
(4, 'PADRE RECONOCE MENOR'),
(5, 'DEMANDADO INUBICABLE'),
(6, 'FAVORABLE PARCIALMENTE'),
(7, 'INCOMPARECENCIA DE LA PARTE'),
(8, 'AVENIMIENTO'),
(9, 'DESISTIMIENTO'),
(10, 'EXCEPCION DE INCOMPETENCIA'),
(11, 'SIN NOTIFICACION '),
(12, 'ARCHIVADA POR TRIBUNAL'),
(13, 'DESFAVORABLE');

-- --------------------------------------------------------

--
-- Table structure for table `causas`
--

CREATE TABLE `causas` (
  `id` int(11) NOT NULL,
  `rol_causa` varchar(64) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  `INGRESO` date DEFAULT NULL,
  `TERMINO` date DEFAULT NULL,
  `RUT_ALUMNO` int(11) DEFAULT NULL,
  `RUT_CLIENTE` int(11) DEFAULT NULL,
  `CAUSAL_TERMINO` varchar(80) DEFAULT NULL,
  `RUT_ABOGADO` varchar(20) DEFAULT NULL,
  `DV_ALUMNO` varchar(1) DEFAULT NULL,
  `DV_CLIENTE` varchar(1) DEFAULT NULL,
  `DV_ABOGADO` varchar(1) DEFAULT NULL,
  `id_cliente` int(11) NOT NULL,
  `SEDE` varchar(45) NOT NULL,
  `ID_TRIBUNAL` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `causas`
--

INSERT INTO `causas` (`id`, `rol_causa`, `id_materia`, `INGRESO`, `TERMINO`, `RUT_ALUMNO`, `RUT_CLIENTE`, `CAUSAL_TERMINO`, `RUT_ABOGADO`, `DV_ALUMNO`, `DV_CLIENTE`, `DV_ABOGADO`, `id_cliente`, `SEDE`, `ID_TRIBUNAL`) VALUES
(1, '001006-2017', 63, '2017-06-06', NULL, NULL, 0, NULL, '17791981', NULL, '', '2', 11, '1', NULL),
(2, '002006-2017', 172, '2017-06-06', '2017-06-07', 33322233, 27791980, '3', '17791981', '7', '0', '2', 10, '1', 1),
(3, '', 35, '2017-06-07', NULL, 0, NULL, NULL, '10023354', NULL, NULL, '1', 12, '1', NULL),
(4, NULL, 12, '2017-06-07', NULL, NULL, 0, NULL, '10023354', NULL, '', '1', 13, '1', NULL),
(5, 'C-909-2017', 8, '2017-06-08', NULL, 18013428, NULL, NULL, '9633643', 'k', NULL, '8', 14, '1', NULL),
(6, 'C-1384-2017', 12, '2017-06-08', NULL, 17556747, 15081428, NULL, '0', 'k', '6', '0', 15, '1', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `causa_datos_cliente`
--

CREATE TABLE `causa_datos_cliente` (
  `id` int(11) NOT NULL,
  `id_causa` int(11) NOT NULL,
  `estado_civil` varchar(16) DEFAULT NULL,
  `separado_de_hecho` tinyint(1) DEFAULT NULL,
  `situacion_laboral` varchar(24) DEFAULT NULL,
  `actividad` text,
  `tipo_prevision` varchar(16) DEFAULT NULL,
  `sistema_salud` varchar(24) DEFAULT NULL,
  `escolaridad` varchar(16) DEFAULT NULL,
  `total_personas` smallint(6) DEFAULT NULL,
  `total_adultos` smallint(6) DEFAULT NULL,
  `total_menores` smallint(6) DEFAULT NULL,
  `discapacitados` tinyint(1) DEFAULT NULL,
  `situacion_habitacional` varchar(16) DEFAULT NULL,
  `valor_dividendo` int(11) DEFAULT NULL,
  `ingresos_1` int(11) DEFAULT NULL,
  `ingresos_2` int(11) DEFAULT NULL,
  `ingresos_3` int(11) DEFAULT NULL,
  `ingresos_total` int(11) DEFAULT NULL,
  `beneficios_estatales` tinyint(1) DEFAULT NULL,
  `beneficio_estatal` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ciforum_categorias`
--

CREATE TABLE `ciforum_categorias` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `publish` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciforum_categorias`
--

INSERT INTO `ciforum_categorias` (`id`, `parent_id`, `name`, `slug`, `date_added`, `date_edit`, `publish`) VALUES
(12, 11, 'PHP', 'php', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(14, 13, 'CSS', 'css', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(15, 12, 'Beginner & Installation', 'php-beginner-installation', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(16, 12, 'Session, Cookie, Security', 'php-session-cookie-security', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(17, 12, 'File & Image Manipulation', 'php-file-image-manipulation', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(18, 14, 'Responsive Layout', 'css-responsive-layout', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(19, 14, 'Beginner', 'css-beginner', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(20, 14, 'Effect, Animation, Gradient', 'css-effect-animation-gradient', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(21, 11, 'Javascript', 'javascript', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(22, 21, 'Jquery', 'jquery', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(24, 0, 'Causas', 'causas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(25, 0, 'Audiencia', 'audiencia', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(26, 0, 'Documentos', 'documentos', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(27, 0, 'Notas', 'notas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(28, 0, 'Orientaciones', 'orientaciones', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(29, 0, 'Tareas', 'tareas', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(30, 0, 'Bitácora', 'bitcora', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(31, 0, 'Otro', 'otro', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(32, 0, 'General', 'general', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ciforum_posts`
--

CREATE TABLE `ciforum_posts` (
  `id` int(11) NOT NULL,
  `thread_id` int(11) NOT NULL,
  `reply_to_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `post` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciforum_posts`
--

INSERT INTO `ciforum_posts` (`id`, `thread_id`, `reply_to_id`, `author_id`, `post`, `date_add`, `date_edit`) VALUES
(1, 1, 0, 2, 'Con fecha 08 de junio se efectuará conversatorio sobre temas de tramitación en tribunales, en hora de clínica.', '2017-06-08 12:10:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ciforum_roles`
--

CREATE TABLE `ciforum_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) NOT NULL,
  `admin_area` int(1) NOT NULL DEFAULT '0',
  `thread_create` int(1) NOT NULL DEFAULT '0',
  `thread_edit` int(1) NOT NULL DEFAULT '0',
  `thread_delete` int(1) NOT NULL DEFAULT '0',
  `post_create` int(1) NOT NULL DEFAULT '0',
  `post_edit` int(1) NOT NULL DEFAULT '0',
  `post_delete` int(1) NOT NULL DEFAULT '0',
  `role_create` int(1) NOT NULL DEFAULT '0',
  `role_edit` int(1) NOT NULL DEFAULT '0',
  `role_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciforum_roles`
--

INSERT INTO `ciforum_roles` (`id`, `role`, `admin_area`, `thread_create`, `thread_edit`, `thread_delete`, `post_create`, `post_edit`, `post_delete`, `role_create`, `role_edit`, `role_delete`) VALUES
(2, 'administrator', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 'member', 0, 1, 0, 0, 1, 0, 0, 0, 0, 0),
(4, 'editor', 0, 0, 1, 1, 0, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ciforum_temas`
--

CREATE TABLE `ciforum_temas` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `date_last_post` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ciforum_temas`
--

INSERT INTO `ciforum_temas` (`id`, `category_id`, `title`, `slug`, `date_add`, `date_edit`, `date_last_post`) VALUES
(1, 28, 'Conversatorio', 'conversatorio', '2017-06-08 12:10:38', '0000-00-00 00:00:00', '2017-06-08 12:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `ciforum_tema_visita`
--

CREATE TABLE `ciforum_tema_visita` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_ciforum_tema` int(11) NOT NULL,
  `ultima_visita` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ciforum_tema_visita`
--

INSERT INTO `ciforum_tema_visita` (`id`, `id_usuario`, `id_ciforum_tema`, `ultima_visita`) VALUES
(1, 2, 1, '2017-06-08 12:10:38');

-- --------------------------------------------------------

--
-- Table structure for table `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `rut_cliente` int(255) DEFAULT NULL,
  `dv_cliente` varchar(1) DEFAULT NULL,
  `nombre_cliente` varchar(80) NOT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `domicilio` varchar(255) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `edad` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clientes`
--

INSERT INTO `clientes` (`id`, `rut_cliente`, `dv_cliente`, `nombre_cliente`, `telefono`, `domicilio`, `email`, `edad`) VALUES
(3, 11807503, 'k', 'Maria Alejandra Berrios Rivero', '69053396', 'Los PeumosH5 Pasaje Puerto Aysen, Forestal Alto, Viña del Mar', 'gggg.falso.gggg@gmail.com', 55),
(4, 13019827, '9', 'Susana Andrea Jeria Basualto', '94754545', 'Manuel Ortuzar 590, Gomez Carreño, 1er Sector, Viña del Mar', '', 0),
(5, 13545490, '7', 'Rosa Ester Gonzalez Hoses', '87806210', 'Villa Indepencia, Manzana G, Casa #2, Achupallas', 'email.falso.hkl@gmail.com', 0),
(6, 17120775, '4', 'Denisce Lemus Cordova', '982831067', 'San Alvaro 22, Agua Santa, Viña del Mar', '', 0),
(7, 17141364, '8', 'Giovanna Avalos Godoy', '983463161', 'Calle Austral 4934 Block 33 Depto 41, Viña del Mar', 'M398NJSINJ3@gmail.com', 55),
(8, 18897681, '6', 'Marco Araya Gonzalez', '72193930', 'Limache 1150, Viña del Mar', 'qweqwe_1p0@hotmail.com', 88),
(9, 19136431, '2', 'Christopher Chamorro Torrealba', '57515152', 'Los Lunes 120, Canal Chacao, Quilpué', '', 0),
(10, 27791980, '0', 'Usuario de Prueba', '77777777', 'Las Rosas', 'cfkfck8fake@gmail.com', 32),
(11, NULL, NULL, 'Cliente de Prueba', NULL, NULL, NULL, 60),
(12, NULL, NULL, 'Jacqueline Silva', '96670764', NULL, NULL, 30),
(13, NULL, '', 'Natalie Bahamondes', '81447650', NULL, NULL, NULL),
(14, NULL, NULL, 'MARIANELA GARCIA ARAVENA ', NULL, NULL, NULL, NULL),
(15, 15081428, '6', 'Carla Haydé', '49455591', 'Rio Alvarez 67 Forestal', NULL, 35);

-- --------------------------------------------------------

--
-- Table structure for table `competencia`
--

CREATE TABLE `competencia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_corto` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_largo` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `competencia`
--

INSERT INTO `competencia` (`id`, `nombre`, `nombre_corto`, `nombre_largo`) VALUES
(1, 'Civil', 'Civil', 'Competencia Civil'),
(2, 'Familia', 'Familia', 'Competencia de Familia'),
(3, 'Otro', 'Otro', 'Otras Competencias');

-- --------------------------------------------------------

--
-- Table structure for table `conf_agenda`
--

CREATE TABLE `conf_agenda` (
  `id_conf_agenda` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `dia` varchar(45) NOT NULL,
  `rut` int(11) NOT NULL,
  `sede` int(11) NOT NULL,
  `oficina` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conf_agenda`
--

INSERT INTO `conf_agenda` (`id_conf_agenda`, `fecha_inicio`, `fecha_fin`, `hora_inicio`, `hora_fin`, `dia`, `rut`, `sede`, `oficina`) VALUES
(47, '2017-06-01', '2017-12-31', '08:30:00', '18:00:00', 'lunes', 9633643, 1, NULL),
(48, '2017-06-01', '2017-12-31', '08:30:00', '18:00:00', 'martes', 9633643, 1, NULL),
(49, '2017-06-01', '2017-12-31', '08:30:00', '18:00:00', 'miercoles', 9633643, 1, NULL),
(50, '2017-06-01', '2017-12-31', '08:30:00', '18:00:00', 'jueves', 9633643, 1, NULL),
(51, '2017-06-01', '2017-12-31', '08:30:00', '18:00:00', 'viernes', 9633643, 1, NULL),
(52, '2017-06-01', '2017-12-31', '08:30:00', '14:00:00', 'lunes', 10023354, 1, NULL),
(53, '2017-06-01', '2017-12-31', '08:30:00', '14:00:00', 'martes', 10023354, 1, NULL),
(54, '2017-06-01', '2017-12-31', '08:30:00', '14:00:00', 'miercoles', 10023354, 1, NULL),
(55, '2017-06-01', '2017-12-31', '08:30:00', '14:00:00', 'jueves', 10023354, 1, NULL),
(56, '2017-06-01', '2017-12-31', '08:30:00', '14:00:00', 'viernes', 10023354, 1, NULL),
(57, '2017-06-01', '2017-12-31', '14:00:00', '18:00:00', 'viernes', 10030513, 1, NULL),
(58, '2017-06-01', '2017-12-31', '14:00:00', '18:00:00', 'jueves', 10030513, 1, NULL),
(59, '2017-06-01', '2017-12-31', '14:00:00', '18:00:00', 'miercoles', 10030513, 1, NULL),
(60, '2017-06-01', '2017-12-31', '14:00:00', '18:00:00', 'martes', 10030513, 1, NULL),
(61, '2017-06-01', '2017-12-31', '14:00:00', '18:00:00', 'lunes', 10030513, 1, NULL),
(67, '2017-06-01', '2017-12-31', '08:30:00', '12:30:00', 'viernes', 17791981, 1, NULL),
(68, '2017-06-01', '2017-12-31', '08:30:00', '16:00:00', 'jueves', 17791981, 1, NULL),
(70, '2017-06-01', '2017-12-31', '12:00:00', '16:00:00', 'miercoles', 17791981, 1, NULL),
(71, '2017-06-01', '2017-12-31', '12:00:00', '18:00:00', 'martes', 17791981, 1, NULL),
(72, '2017-06-01', '2017-12-31', '08:30:00', '14:30:00', 'lunes', 17791981, 1, NULL),
(73, '2017-05-01', '2017-12-31', '14:00:00', '15:30:00', 'lunes', 15948485, 1, NULL),
(74, '2017-05-01', '2017-12-31', '14:00:00', '15:30:00', 'martes', 15948485, 1, NULL),
(75, '2017-05-01', '2017-12-31', '14:00:00', '15:30:00', 'miercoles', 15948485, 1, NULL),
(76, '2017-05-01', '2017-12-31', '14:00:00', '15:30:00', 'jueves', 15948485, 1, NULL),
(77, '2017-05-01', '2017-12-31', '14:00:00', '15:30:00', 'viernes', 15948485, 1, NULL),
(79, '2017-06-01', '2017-12-15', '09:00:00', '18:00:00', 'martes', 13877229, 1, NULL),
(80, '2017-06-01', '2017-08-31', '09:00:00', '18:00:00', 'viernes', 10023354, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `evento_agenda`
--

CREATE TABLE `evento_agenda` (
  `id` int(11) NOT NULL,
  `fecha_asignacion` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `rut` varchar(45) NOT NULL,
  `id_asunto` int(11) DEFAULT NULL,
  `id_orientacion` int(11) DEFAULT NULL,
  `id_causa` int(11) DEFAULT NULL,
  `id_audiencia` int(11) DEFAULT NULL,
  `tipo_asunto` varchar(45) NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `sede` int(11) NOT NULL,
  `icalid_evento` varchar(64) NOT NULL,
  `partstat_evento` varchar(12) NOT NULL DEFAULT 'NEEDS-ACTION' COMMENT 'Participation Status del Evento',
  `estado_rechazo` varchar(24) DEFAULT NULL COMMENT 'REENVIADO, ELIMINADO',
  `icaluid_evento` varchar(64) DEFAULT NULL,
  `titulo_evento` varchar(128) DEFAULT NULL,
  `descripcion_evento` varchar(512) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evento_agenda`
--

INSERT INTO `evento_agenda` (`id`, `fecha_asignacion`, `hora_inicio`, `hora_fin`, `rut`, `id_asunto`, `id_orientacion`, `id_causa`, `id_audiencia`, `tipo_asunto`, `estado`, `sede`, `icalid_evento`, `partstat_evento`, `estado_rechazo`, `icaluid_evento`, `titulo_evento`, `descripcion_evento`) VALUES
(2, '2017-06-09', '15:00:00', '15:30:00', '10023354', NULL, NULL, 3, NULL, 'causa', 'Agendado', 1, 'dcvh42jj3248cp7kekskttv1ug', 'NEEDS-ACTION', NULL, 'dcvh42jj3248cp7kekskttv1ug@google.com', NULL, NULL),
(3, '2017-06-09', '15:30:00', '16:00:00', '10023354', NULL, NULL, 4, NULL, 'causa', 'Agendado', 1, 'mrhgvkh5ec6djf416b21nmlba4', 'NEEDS-ACTION', NULL, 'mrhgvkh5ec6djf416b21nmlba4@google.com', NULL, NULL),
(4, '2017-06-09', '12:00:00', '12:30:00', '9633643', 2, NULL, NULL, NULL, 'asunto', 'Agendado', 1, '6fbq7sgs9be2ip1v607k3ootio', 'NEEDS-ACTION', NULL, '6fbq7sgs9be2ip1v607k3ootio@google.com', 'No venir hoy', 'Es importante.');

-- --------------------------------------------------------

--
-- Table structure for table `evento_agenda_historico`
--

CREATE TABLE `evento_agenda_historico` (
  `id` int(11) NOT NULL,
  `id_evento_agenda` int(11) NOT NULL,
  `rut` varchar(45) NOT NULL,
  `fecha_asignacion` date NOT NULL,
  `id_asunto` varchar(45) NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `tipo_asunto` varchar(45) NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `fecha_eliminacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `motivo` varchar(255) NOT NULL,
  `quien_elimina` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evento_agenda_historico`
--

INSERT INTO `evento_agenda_historico` (`id`, `id_evento_agenda`, `rut`, `fecha_asignacion`, `id_asunto`, `hora_inicio`, `hora_fin`, `tipo_asunto`, `estado`, `fecha_eliminacion`, `motivo`, `quien_elimina`) VALUES
(1, 1, '17791981', '2017-06-09', '1', '08:30:00', '09:00:00', 'asunto', 'Agendado', '2017-06-07 20:08:41', 'otro', '17791980');

-- --------------------------------------------------------

--
-- Table structure for table `google_calendar_api`
--

CREATE TABLE `google_calendar_api` (
  `nombre` varchar(24) NOT NULL,
  `json_string` varchar(1024) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `google_calendar_api`
--

INSERT INTO `google_calendar_api` (`nombre`, `json_string`, `id`) VALUES
('CLIENT_SECRET', '{"installed":{"client_id":"969123885314-f1ueks9nq2bc0skenlpo1jfp776dpuik.apps.googleusercontent.com","project_id":"still-emissary-149314","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://accounts.google.com/o/oauth2/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"4648HYLctqqpIW1zu6cT-jkR","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}', 1),
('TOKENS', '{"access_token":"ya29.Gl1jBBFe7oBdRPPKE5ByJiJsrgcwyMFbhznEFimj-o1o76GVrUfO6fTBDr5XpZC3ybVDK_lS1smkoyT6YaXi7JLklygtXpujfmtZepTHfNz_IAxf03UnOspQ8yzMSIY","token_type":"Bearer","expires_in":3600,"refresh_token":"1/GrmSfZ1mTp3sH7SbOSc-kuFYWSAuvr6QUXOQWO4la7Q","created":1479298816}', 2);

-- --------------------------------------------------------

--
-- Table structure for table `historial_causas`
--

CREATE TABLE `historial_causas` (
  `id_historial` int(11) NOT NULL,
  `id_causa` int(11) NOT NULL COMMENT 'fk a tabla ''causas''',
  `fecha_cambio` date NOT NULL COMMENT 'fecha',
  `hora_cambio` time NOT NULL COMMENT 'con segundos',
  `tipo_cambio` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'eliminacion, modificacion o terminacion',
  `nombre_columna` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'si es eliminación, no aplica',
  `valor_previo` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `valor_nuevo` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='los cambios hechos en alguna causa se registran aquí';

--
-- Dumping data for table `historial_causas`
--

INSERT INTO `historial_causas` (`id_historial`, `id_causa`, `fecha_cambio`, `hora_cambio`, `tipo_cambio`, `nombre_columna`, `valor_previo`, `valor_nuevo`) VALUES
(1, 2, '2017-06-07', '12:31:15', 'terminacion', 'TERMINO', NULL, '2017-06-07'),
(2, 2, '2017-06-07', '12:31:15', 'terminacion', 'CAUSAL_TERMINO', NULL, '3');

-- --------------------------------------------------------

--
-- Table structure for table `materia`
--

CREATE TABLE `materia` (
  `id` int(11) NOT NULL,
  `nombre_materia` varchar(80) NOT NULL,
  `nombre_corto` varchar(32) DEFAULT NULL,
  `nombre_largo` varchar(128) DEFAULT NULL,
  `id_procedimiento` int(11) NOT NULL,
  `id_competencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materia`
--

INSERT INTO `materia` (`id`, `nombre_materia`, `nombre_corto`, `nombre_largo`, `id_procedimiento`, `id_competencia`) VALUES
(1, 'POSESION EFECTIVA', NULL, NULL, 17, 3),
(2, 'EXTRAVIO  DE TITULO DE CREDITO PRIMER JUZGADO CIVIL', NULL, NULL, 17, 3),
(3, 'TERMINO CONTRATO ARRENDAMIENTO', NULL, NULL, 17, 3),
(4, 'PROTECCION', NULL, NULL, 17, 3),
(5, 'ARRENDAMIENTO', NULL, NULL, 17, 3),
(6, 'Rebaja Alimentos', NULL, 'Alimentos, Rebaja', 9, 2),
(7, 'REGULACION RDR', NULL, NULL, 17, 3),
(8, 'DIVORCIO CON COMPENSACION ECONOMICA', NULL, NULL, 17, 3),
(9, 'RECLAMACION FILIACION NO MATRIMONIAL', NULL, NULL, 17, 3),
(10, 'DIVORCIO UNILATERAL', NULL, NULL, 17, 3),
(11, 'RECLAMACION DE PATERNIDAD', NULL, 'Paternidad, Reconocimiento de', 9, 2),
(12, 'Alimentos', NULL, 'Alimentos', 9, 2),
(13, 'INTERDICCION Y NOMBRAMIENTO CURADOR', NULL, NULL, 17, 3),
(14, 'DIVORCIO MUTUO ACUERDO', NULL, NULL, 17, 3),
(15, 'DIVORCIO UNILATERAL POR CESE CONVIVENCIA', NULL, NULL, 17, 3),
(16, 'MODIFICACION RDR', NULL, NULL, 17, 3),
(17, 'DIVORCIO UNILATERAL SIN COMPENSACION', NULL, NULL, 17, 3),
(18, 'CUIDADO PERSONAL', NULL, NULL, 17, 3),
(19, 'EMBARGO', NULL, NULL, 17, 3),
(20, 'Divorcio Por Cese De Convivencia', NULL, 'Divorcio Por Cese De Convivencia', 9, 2),
(21, 'TRANSACCION EXTRAJUDICIAL', NULL, NULL, 17, 3),
(22, 'RDR', NULL, NULL, 17, 3),
(23, 'DEMANDA DE PRECARIO', NULL, NULL, 17, 3),
(24, 'JUICIO ORDINARIO DE MENOR CUANTIA', NULL, NULL, 17, 3),
(25, 'Divorcio Comun Acuerdo', NULL, 'Divorcio De Comun Acuerdo', 9, 2),
(26, 'VIF', NULL, NULL, 17, 3),
(27, 'FILIACION', NULL, NULL, 17, 3),
(28, 'HERENCIA JULIA ROJAS VALENZUELA', NULL, NULL, 17, 3),
(29, 'SEGUNDAS NUPCIAS', NULL, NULL, 17, 3),
(30, 'ALIMENTOS SUBSIDIARIOS', NULL, NULL, 17, 3),
(31, 'Aumento Alimentos', NULL, 'Alimentos, Aumento', 9, 2),
(32, 'JUICIO ARRENDAMIENTO', NULL, NULL, 17, 3),
(33, 'CUMPLIMIENTO DE TITULO EJECUTIVO', NULL, NULL, 17, 3),
(35, 'Alimentos, Otros', NULL, NULL, 9, 2),
(36, 'Entrega Menor y/o Especies del Menor/Costo de Crianza y Educación', NULL, 'Entrega Menor y/o Especies del Menor/Costo de Crianza y Educación Tasados Por El Juez (Art. 239 cc)', 9, 2),
(37, 'Cuidado personal del niño, declaracion', NULL, NULL, 9, 2),
(38, 'Cuidado personal del niño, modificacion', NULL, NULL, 9, 2),
(39, 'Cuidado del Niño, Otros', NULL, NULL, 9, 2),
(40, 'Relacion directa y regular con el niño', NULL, NULL, 9, 2),
(41, 'Declaracion De Susceptibilidad', NULL, NULL, 9, 2),
(42, 'Separacion Judicial de Bienes', NULL, NULL, 9, 2),
(43, 'Maternidad, Impugnacion y Reconocimiento De', NULL, NULL, 9, 2),
(44, 'Maternidad, Reconocimiento De', NULL, NULL, 9, 2),
(45, 'Paternidad, Impugnacion y Reconocimiento De', NULL, NULL, 9, 2),
(46, 'Paternidad, Reconocimiento De', NULL, NULL, 9, 2),
(47, 'Secuestro Internacional De Menores', NULL, NULL, 9, 2),
(48, 'Otros Asuntos de Tramitacion Ordinaria', NULL, NULL, 9, 2),
(49, 'Desafectacion De Bienes Familiares', NULL, NULL, 9, 2),
(50, 'Divorcio Por Culpa', NULL, NULL, 9, 2),
(51, 'Alimentos, Cesacion', NULL, NULL, 9, 2),
(52, 'Autorizacion Salida Del Pais', NULL, NULL, 9, 2),
(53, 'Relacion Directa y Regular, Otros', NULL, NULL, 9, 2),
(54, 'Relacion Directa y Regular, Suspención', NULL, NULL, 9, 2),
(55, 'Separacion Matrimonial', NULL, NULL, 9, 2),
(56, 'Nulidad Matrimonial', NULL, NULL, 9, 2),
(57, 'Declaracion De Bienes Familiares', NULL, NULL, 9, 2),
(58, 'Cuidado Personal Del Niño', NULL, NULL, 9, 2),
(59, 'Compensacion Economica', NULL, NULL, 9, 2),
(60, 'Declaracion De Susceptibilidad', NULL, NULL, 12, 2),
(61, 'Adopción', NULL, NULL, 13, 2),
(62, 'Infracción A La Ley Penal', NULL, NULL, 11, 2),
(63, 'Vulneración De Derechos', NULL, NULL, 11, 2),
(64, 'Alimento', NULL, NULL, 10, 2),
(65, 'Adopción', NULL, NULL, 10, 2),
(66, 'Separación Judicial de Bienes', NULL, NULL, 10, 2),
(67, 'Maternidad, Reconocimiento De', NULL, NULL, 10, 2),
(68, 'Paternidad, Impugnacion y Reconocimiento De', NULL, NULL, 10, 2),
(69, 'Violencia Intrafamiliar', NULL, NULL, 10, 2),
(70, 'Alimentos, Otros', NULL, NULL, 10, 2),
(71, 'Divorcio Por Culpa', NULL, NULL, 10, 2),
(72, 'Relacion Directa y Regular, Suspensión', NULL, NULL, 10, 2),
(73, 'Alimentos, Rebaja', NULL, NULL, 10, 2),
(74, 'Relacion Directa y Regular, Otros', NULL, NULL, 10, 2),
(75, 'Declaracion De Bienes Familiares', NULL, NULL, 10, 2),
(76, 'Maternidad, Impugnacion y Reconocimiento', NULL, NULL, 10, 2),
(77, 'Paternidad, Reconocimiento De', NULL, NULL, 10, 2),
(78, 'Desafectacion De Bienes Familiares', NULL, NULL, 10, 2),
(79, 'Divorcio Por Cese de Convivencia', NULL, NULL, 10, 2),
(80, 'Compensacion Economica', NULL, NULL, 10, 2),
(81, 'Declaracion de Susceptibilidad', NULL, NULL, 10, 2),
(82, 'Alimentos, Aumento', NULL, NULL, 10, 2),
(83, 'Cuidado Del Niño, Otros', NULL, NULL, 10, 2),
(84, 'Relacion Directa y Regular Con El Niño', NULL, NULL, 10, 2),
(85, 'Relacion Directa y Regular Modificacion', NULL, NULL, 10, 2),
(86, 'Violación De Derechos', 'Violación De Derechos', 'Violación De Derechos', 16, 2),
(87, 'Infracción A La Ley Penal', 'Infracción A La Ley Penal', 'Infracción A La Ley Penal', 16, 2),
(88, 'Alimentos, Rebaja', NULL, NULL, 8, 2),
(89, 'Cuidado Personal Del Niño', NULL, NULL, 8, 2),
(90, 'Cuidado Personal Del Niño, Declaracion', NULL, NULL, 8, 2),
(91, 'Cuidado Personal Del Niño, Modificacion', NULL, NULL, 8, 2),
(92, 'Relacion Directoa y Regular Con El Niño', NULL, NULL, 8, 2),
(93, 'Relacion Directa Y Regular Modificacion', NULL, NULL, 8, 2),
(94, 'Autorizacion Salida Del Pais', NULL, NULL, 8, 2),
(95, 'Matrimonio, Disenso Para Contraer', NULL, NULL, 8, 2),
(96, 'Patria Protestad (Emancipacion Judicial)', NULL, NULL, 8, 2),
(97, 'Patria Potestad, Solicitud', NULL, NULL, 8, 2),
(98, 'Guardador Menores De Edad, Nombramiento', NULL, NULL, 8, 2),
(99, 'Patria Porestad, Otros', NULL, NULL, 8, 2),
(100, 'Alimentos', NULL, NULL, 8, 2),
(101, 'Alimentos, Aumento', NULL, NULL, 8, 2),
(102, 'Alimentos, Cesacion', NULL, NULL, 8, 2),
(103, 'Alimentos, Otros', NULL, NULL, 8, 2),
(104, 'Patria Potestad, Renuncia', NULL, NULL, 8, 2),
(105, 'Guardador Menores De Edad, Remoción', NULL, NULL, 8, 2),
(106, 'Autorizaciones', NULL, NULL, 8, 2),
(107, 'Relación Directa Y Regular Suspensión', NULL, NULL, 8, 2),
(108, 'Nulidad Matrimonial', NULL, NULL, 8, 2),
(109, 'Speracion Judicial De Bienes', NULL, NULL, 8, 2),
(110, 'Separacion Matrimonial', NULL, NULL, 8, 2),
(111, 'Separacion Judicial Por Comun Acuerdo', NULL, NULL, 8, 2),
(112, 'Declaracion De Bienes Familiares', NULL, NULL, 8, 2),
(113, 'Relacion Directa y regular, Otros', NULL, NULL, 8, 2),
(114, 'Patria Potestad, Suspensión', NULL, NULL, 8, 2),
(115, 'Violencia Intrafamiliar', 'Violencia Intrafamiliar', 'Violencia Intrafamiliar', 15, 2),
(116, 'Patria Potestad (Emancipacion Judicial)', NULL, NULL, 14, 2),
(117, 'Patria Potestad, Solicitud', NULL, NULL, 14, 2),
(118, 'Guardador Menores De Edad, Remoción', NULL, NULL, 14, 2),
(119, 'Patria Potestad, Otros', NULL, NULL, 14, 2),
(120, 'Autorizaciones', NULL, NULL, 14, 2),
(121, 'Otros Asuntos Voluntarios', NULL, NULL, 14, 2),
(122, 'Autorizacion Para Enajenar Bienes Raices', NULL, NULL, 14, 2),
(123, 'Matrimonio, Disenso Para Contraer', NULL, NULL, 14, 2),
(124, 'Patria Potestad, Renuncia', NULL, NULL, 14, 2),
(125, 'Guardador Menores De Edad, Nombramiento', NULL, NULL, 14, 2),
(126, 'Convivencia, Notificacion Cese', NULL, NULL, 14, 2),
(127, 'Separacion Judicial Por Comun Acuerdo', NULL, NULL, 14, 2),
(128, 'Patria Potestad, Suspención', NULL, NULL, 14, 2),
(129, 'Insolvencia Transfronteriza', NULL, NULL, 7, 1),
(130, 'Reorganización Concursal Extrajudicial (Simplificado)', NULL, NULL, 7, 1),
(131, 'Liquidación Forzosa Empresa Deudora', NULL, NULL, 7, 1),
(132, 'Liquidación Forzosa Empresa Deudora', NULL, NULL, 7, 1),
(133, 'Liquidación Forzosa Persona Natural', NULL, NULL, 7, 1),
(134, 'Liquidación Voluntaria Persona Natural', NULL, NULL, 7, 1),
(135, 'Reorganización Concursal', NULL, NULL, 7, 1),
(136, 'Avaluación, Gestión De', NULL, NULL, 3, 1),
(137, 'Cheque, Notificacion Protesto', NULL, NULL, 3, 1),
(138, 'Confesión de Deuda, Citacion', NULL, NULL, 3, 1),
(139, 'Confrontación, Gestión de', NULL, NULL, 3, 1),
(140, 'Desposeimiento, Notificación De', NULL, NULL, 3, 1),
(141, 'Herederos, Notificación Titulo Ejecutivo', NULL, NULL, 3, 1),
(142, 'Letra, Notificación De Protesto', NULL, NULL, 3, 1),
(143, 'Pagaré, Notificación De Protesto', NULL, NULL, 3, 1),
(144, 'Reconocimiento De Firma, Citación', NULL, NULL, 3, 1),
(145, 'Reconocimiento Firma, Citación y Confesión De Deuda', NULL, NULL, 3, 1),
(146, 'Sentencia Extranjera, Validación De', NULL, NULL, 3, 1),
(147, 'Factura, Notificación De', NULL, NULL, 3, 1),
(148, 'Medida Prejudicial Precautoria', NULL, NULL, 6, 1),
(149, 'Medida Prejudicial Preparatoria', NULL, NULL, 6, 1),
(150, 'Medida Prejudicial Probatoria', NULL, NULL, 6, 1),
(151, 'Otras Medidas Prejudiciales', NULL, NULL, 6, 1),
(152, 'Cobro De Gastos Comunes', NULL, NULL, 4, 1),
(153, 'Sentencia Judicial, Cobro Ejecutivo De', NULL, NULL, 4, 1),
(154, 'Otras Prendas, Realizacón De', NULL, NULL, 4, 1),
(155, 'Concesión Minera, Remate De', NULL, NULL, 4, 1),
(156, 'Bienes Raíces, Remate Por No Pago Contribuciones', NULL, NULL, 4, 1),
(157, 'Minas, Remate De, Por No Pago De Patentes', NULL, NULL, 4, 1),
(158, 'Obligación De Dar, Cumplimiento', NULL, NULL, 4, 1),
(159, 'Obligación De Hacer, Cumplimiento', NULL, NULL, 4, 1),
(160, 'Obligación De No Hacer, Cumplimiento', NULL, NULL, 4, 1),
(161, 'Otros Ejecutivos', NULL, NULL, 4, 1),
(162, 'Prenda Sin Desplazamiento, Realización', NULL, NULL, 4, 1),
(163, 'Prenda Agraria, Realización', NULL, NULL, 4, 1),
(164, 'Prenda Compraventa Cosa Mueble, Realización', NULL, NULL, 4, 1),
(165, 'Prenda Industrial, Realización', NULL, NULL, 4, 1),
(166, 'Prenda Ordinaria, Realización', NULL, NULL, 4, 1),
(167, 'Desposeimiento, Acción De', NULL, NULL, 4, 1),
(168, 'Factura, Cobro De', NULL, NULL, 4, 1),
(169, 'Hipotecaria, Acción', NULL, NULL, 4, 1),
(170, 'Hipotecaria, Acción Seg´un Ley Corvi', NULL, NULL, 4, 1),
(171, 'Hipotecaria, Acción Seg´un Ley De Bancos', NULL, NULL, 4, 1),
(172, 'Cheque, Cobro De', NULL, NULL, 4, 1),
(173, 'Letra De Cambio, Cobro De', NULL, NULL, 4, 1),
(174, 'Pagaré, Cobro De', NULL, NULL, 4, 1),
(175, 'Tributarias Obligaciones, Cobro en Dinero', NULL, NULL, 4, 1),
(176, 'Consignación, Pago Por Art. 1.600 c.c', NULL, NULL, 5, 1),
(177, 'Bienes Raíces, Autorización Arrendar', NULL, NULL, 5, 1),
(178, 'Bienes Raíces, Autorización Enajenar', NULL, NULL, 5, 1),
(179, 'Bienes Raíces, Autorización Gravar', NULL, NULL, 5, 1),
(180, 'Bienes Raíces, Reclamo Negativa del Conservador', NULL, NULL, 5, 1),
(181, 'Cargo Judicial, Concurso A', NULL, NULL, 5, 1),
(182, 'Crédito, Extravío De Título', NULL, NULL, 5, 1),
(183, 'Curador, Nombramiento De', NULL, NULL, 5, 1),
(184, 'Defunción, Autorización Inscripción Fuera Plazo Legal', NULL, NULL, 5, 1),
(185, 'Donar o Insinuación, Autorización Para', NULL, NULL, 5, 1),
(186, 'Expropiación, Notificación De', NULL, NULL, 5, 1),
(187, 'Expropiar, Gestión de Pago Para', NULL, NULL, 5, 1),
(188, 'Herencia Yacente, Declaración De', NULL, NULL, 5, 1),
(189, 'Inventario Solemne', NULL, NULL, 5, 1),
(190, 'Muerte Presunta', NULL, '', 5, 1),
(191, 'Nombre, Autorización Cambio De', NULL, NULL, 5, 1),
(192, 'Notificaciones Judicias Variaas, Incluyendo Árbitros Voluntar', NULL, NULL, 5, 1),
(193, 'Otros Voluntarios', NULL, NULL, 5, 1),
(194, 'Partición, Aprobación De Escrituras De', NULL, NULL, 5, 1),
(195, 'Peerpetua Memoria, Información', NULL, NULL, 5, 1),
(196, 'Prenda y/o Mandato, Notificación', NULL, NULL, 5, 1),
(197, 'Registro Civil, Autorización Nombramiento Curador Especial', NULL, NULL, 5, 1),
(198, 'Registro Civil, Reclamo a Negativa', NULL, NULL, 5, 1),
(199, 'Registro Civil, Rectificación Partidas', NULL, NULL, 5, 1),
(200, 'Sanitario Código, Alzamiento De Clausura', NULL, NULL, 5, 1),
(201, 'Sellos, Aprobación De', NULL, NULL, 5, 1),
(202, 'Sociedad, Autorización Constituir', NULL, NULL, 5, 1),
(203, 'Sociedad, Autorización Modificar', NULL, NULL, 5, 1),
(204, 'Subasta Pública, Venta Voluntaria En', NULL, NULL, 5, 1),
(205, 'Tasación Judicial', NULL, NULL, 5, 1),
(207, 'Testamento Verbal, Poner Por Escrito o Publicación', NULL, NULL, 5, 1),
(208, 'Tertamento, Apertura, Protoc. y Publicación', NULL, NULL, 5, 1),
(209, 'Transigir, Autorización Para', NULL, NULL, 5, 1),
(210, 'Vehículos Motorizados, Inscripciones en Registro', NULL, NULL, 5, 1),
(211, 'Minera, Manifestación(Concesión Para Explotación)', NULL, NULL, 5, 1),
(212, 'Minero, Pedimento(Concesión De Exploración)', NULL, NULL, 5, 1),
(213, 'Posesión Efectiva', NULL, NULL, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orientacion`
--

CREATE TABLE `orientacion` (
  `id_orientacion` int(11) NOT NULL,
  `fec_ingreso` date NOT NULL,
  `id_materia` varchar(45) NOT NULL,
  `resena` varchar(1024) DEFAULT NULL,
  `rut_abogado` varchar(45) NOT NULL,
  `rut_usuario` varchar(45) NOT NULL,
  `sede` varchar(45) NOT NULL,
  `id_causa` int(11) DEFAULT NULL COMMENT 'id correlativa (de tabla)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plantilla`
--

CREATE TABLE `plantilla` (
  `id` int(11) NOT NULL,
  `id_archivo` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `thumbnail` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `titulo` varchar(128) CHARACTER SET utf8 NOT NULL,
  `descripcion` varchar(512) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plantilla`
--

INSERT INTO `plantilla` (`id`, `id_archivo`, `id_categoria`, `thumbnail`, `titulo`, `descripcion`) VALUES
(2, 3, 30, 'BITACORA_ESTADO_DEL_CASO.jpg', 'Bitácora De La Causa', 'REFLEJA ESTADO DEL CASO: TODO TRAMITE JUDICIAL O EXTRAJUDIAL EFECTUADO.'),
(3, 5, 24, 'CATASTRO_TÉRMINO_DE_ASUNTOS_CLINICA.jpg', 'Catastro De Término De Causas', NULL),
(4, 7, 25, 'EVALUACION_DE_DESEMPEÑO_EN_AUDIENCIA_CLINICA_UNAB.jpg', 'Evaluación De Desempeño de Audiencia', NULL),
(5, 9, 31, 'FICHA_DERIVACIÓN.jpg', 'Ficha De Derivación', NULL),
(6, 11, 24, 'FICHA_UNICA_DE_INGRESO_A_CLÍNICA.jpg', 'Ficha De Ingreso Caso o Causa', NULL),
(7, 13, 31, 'FORMULARIO_ENCARGO_DILIGENCIA_RECEPTORES_TURNO.jpg', 'Formulario Encargo Diligencia Receptores Turno', NULL),
(8, 15, 24, 'INFORME_MENSUAL_CASOS.jpg', 'Formulario Informe Mensual De Casos', NULL),
(9, 17, 25, 'FORMULARIO_PARA_USUARIO_DE_ACUERDO_EN_AUDIENCIA_ALIMENTOS.jpg', 'Constancia De Acuerdo De Alimentos', NULL),
(10, 19, 31, 'LISTA_DE_TESTIGOS.jpg', 'Lista De Testigos', 'Solo traer el listado. No a las personas.'),
(11, 21, 32, 'OBLIGACIONES_ALUMNOS_CONSULTORIO.jpg', 'Obligaciones De Alumnos De Clínica Jurídica', '10 Obligaciones para los Alumnos de Consultorio Jurídico I y II.'),
(12, 23, 31, 'RECEPTOR_AD_HOC.jpg', 'Solicitud Receptor AD HOC', NULL),
(13, 25, 32, 'REGLAMENTO_DE_USUARIOS.jpg', 'Reglamento De Usuarios Clínica Jurídica', NULL),
(14, 27, 27, 'REVISION_CARPETA_POR_PROFESOR.jpg', 'Formulario Revisión De Carpeta Por Profesor', NULL),
(15, 29, 31, 'amonestacion_escrita.jpg', 'Amonestación Escrita', NULL),
(16, 31, 26, NULL, 'oooooooo', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `privilegios`
--

CREATE TABLE `privilegios` (
  `id_privilegio` int(255) NOT NULL,
  `rut` int(11) NOT NULL,
  `sede` int(11) NOT NULL,
  `titulo_privilegio` varchar(255) NOT NULL,
  `privilegio` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `privilegios`
--

INSERT INTO `privilegios` (`id_privilegio`, `rut`, `sede`, `titulo_privilegio`, `privilegio`) VALUES
(64, 9909751, 2, 'Mantencion', 0),
(65, 9909751, 2, 'Busquedas', 0),
(66, 9909751, 2, 'Agenda', 0),
(67, 9909751, 2, 'Orientaciones', 1),
(68, 9909751, 2, 'Audiencia', 0),
(69, 9909751, 2, 'Panel', 0),
(70, 9909751, 2, 'Reportes', 0),
(78, 17791980, 2, 'Mantencion', 1),
(79, 17791980, 2, 'Busquedas', 1),
(80, 17791980, 2, 'Agenda', 1),
(81, 17791980, 2, 'Orientaciones', 1),
(82, 17791980, 2, 'Audiencia', 1),
(83, 17791980, 2, 'Panel', 1),
(84, 17791980, 2, 'Reportes', 1),
(85, 10000004, 2, 'Mantencion', 1),
(86, 10000004, 2, 'Busquedas', 1),
(87, 10000004, 2, 'Agenda', 1),
(88, 10000004, 2, 'Orientaciones', 1),
(89, 10000004, 2, 'Audiencia', 1),
(90, 10000004, 2, 'Panel', 1),
(91, 10000004, 2, 'Reportes', 1),
(92, 10000005, 2, 'Mantencion', 1),
(93, 10000005, 2, 'Busquedas', 1),
(94, 10000005, 2, 'Agenda', 1),
(95, 10000005, 2, 'Orientaciones', 1),
(96, 10000005, 2, 'Audiencia', 1),
(97, 10000005, 2, 'Panel', 1),
(98, 10000005, 2, 'Reportes', 1),
(99, 8344235, 1, 'Mantencion', 1),
(100, 8344235, 1, 'Busquedas', 1),
(101, 8344235, 1, 'Agenda', 1),
(102, 8344235, 1, 'Orientaciones', 1),
(103, 8344235, 1, 'Audiencia', 1),
(104, 8344235, 1, 'Panel', 1),
(105, 8344235, 1, 'Reportes', 1),
(106, 15366940, 1, 'Mantencion', 1),
(107, 15366940, 1, 'Busquedas', 1),
(108, 15366940, 1, 'Agenda', 1),
(109, 15366940, 1, 'Orientaciones', 1),
(110, 15366940, 1, 'Audiencia', 1),
(111, 15366940, 1, 'Panel', 1),
(112, 15366940, 1, 'Reportes', 1),
(113, 10030513, 1, 'Mantencion', 0),
(114, 10030513, 1, 'Busquedas', 0),
(115, 10030513, 1, 'Agenda', 0),
(116, 10030513, 1, 'Orientaciones', 1),
(117, 10030513, 1, 'Audiencia', 0),
(118, 10030513, 1, 'Panel', 0),
(119, 10030513, 1, 'Reportes', 0),
(120, 15948485, 1, 'Mantencion', 0),
(121, 15948485, 1, 'Busquedas', 0),
(122, 15948485, 1, 'Agenda', 0),
(123, 15948485, 1, 'Orientaciones', 1),
(124, 15948485, 1, 'Audiencia', 0),
(125, 15948485, 1, 'Panel', 0),
(126, 15948485, 1, 'Reportes', 0),
(134, 0, 1, 'Mantencion', 0),
(135, 0, 1, 'Busquedas', 0),
(136, 0, 1, 'Agenda', 0),
(137, 0, 1, 'Orientaciones', 1),
(138, 0, 1, 'Audiencia', 0),
(139, 0, 1, 'Panel', 0),
(140, 0, 1, 'Reportes', 0),
(141, 9633643, 1, 'Mantencion', 0),
(142, 9633643, 1, 'Busquedas', 0),
(143, 9633643, 1, 'Agenda', 0),
(144, 9633643, 1, 'Orientaciones', 1),
(145, 9633643, 1, 'Audiencia', 0),
(146, 9633643, 1, 'Panel', 0),
(147, 9633643, 1, 'Reportes', 0),
(148, 10023354, 1, 'Mantencion', 0),
(149, 10023354, 1, 'Busquedas', 0),
(150, 10023354, 1, 'Agenda', 0),
(151, 10023354, 1, 'Orientaciones', 1),
(152, 10023354, 1, 'Audiencia', 0),
(153, 10023354, 1, 'Panel', 0),
(154, 10023354, 1, 'Reportes', 0),
(162, 17354965, 1, 'Mantencion', 0),
(163, 17354965, 1, 'Busquedas', 0),
(164, 17354965, 1, 'Agenda', 0),
(165, 17354965, 1, 'Orientaciones', 0),
(166, 17354965, 1, 'Audiencia', 0),
(167, 17354965, 1, 'Panel', 0),
(168, 17354965, 1, 'Reportes', 0),
(169, 18841434, 1, 'Mantencion', 0),
(170, 18841434, 1, 'Busquedas', 0),
(171, 18841434, 1, 'Agenda', 0),
(172, 18841434, 1, 'Orientaciones', 0),
(173, 18841434, 1, 'Audiencia', 0),
(174, 18841434, 1, 'Panel', 0),
(175, 18841434, 1, 'Reportes', 0),
(176, 18584987, 1, 'Mantencion', 0),
(177, 18584987, 1, 'Busquedas', 0),
(178, 18584987, 1, 'Agenda', 0),
(179, 18584987, 1, 'Orientaciones', 0),
(180, 18584987, 1, 'Audiencia', 0),
(181, 18584987, 1, 'Panel', 0),
(182, 18584987, 1, 'Reportes', 0),
(183, 18237562, 1, 'Mantencion', 0),
(184, 18237562, 1, 'Busquedas', 0),
(185, 18237562, 1, 'Agenda', 0),
(186, 18237562, 1, 'Orientaciones', 0),
(187, 18237562, 1, 'Audiencia', 0),
(188, 18237562, 1, 'Panel', 0),
(189, 18237562, 1, 'Reportes', 0),
(190, 18378322, 1, 'Mantencion', 0),
(191, 18378322, 1, 'Busquedas', 0),
(192, 18378322, 1, 'Agenda', 0),
(193, 18378322, 1, 'Orientaciones', 0),
(194, 18378322, 1, 'Audiencia', 0),
(195, 18378322, 1, 'Panel', 0),
(196, 18378322, 1, 'Reportes', 0),
(197, 14719316, 1, 'Mantencion', 0),
(198, 14719316, 1, 'Busquedas', 0),
(199, 14719316, 1, 'Agenda', 0),
(200, 14719316, 1, 'Orientaciones', 0),
(201, 14719316, 1, 'Audiencia', 0),
(202, 14719316, 1, 'Panel', 0),
(203, 14719316, 1, 'Reportes', 0),
(204, 16573072, 1, 'Mantencion', 0),
(205, 16573072, 1, 'Busquedas', 0),
(206, 16573072, 1, 'Agenda', 0),
(207, 16573072, 1, 'Orientaciones', 0),
(208, 16573072, 1, 'Audiencia', 0),
(209, 16573072, 1, 'Panel', 0),
(210, 16573072, 1, 'Reportes', 0),
(232, 17791981, 1, 'Mantencion', 0),
(233, 17791981, 1, 'Busquedas', 0),
(234, 17791981, 1, 'Agenda', 0),
(235, 17791981, 1, 'Orientaciones', 1),
(236, 17791981, 1, 'Audiencia', 0),
(237, 17791981, 1, 'Panel', 0),
(238, 17791981, 1, 'Reportes', 0),
(239, 17791982, 2, 'Mantencion', 1),
(240, 17791982, 2, 'Busquedas', 1),
(241, 17791982, 2, 'Agenda', 1),
(242, 17791982, 2, 'Orientaciones', 1),
(243, 17791982, 2, 'Audiencia', 1),
(244, 17791982, 2, 'Panel', 1),
(245, 17791982, 2, 'Reportes', 1),
(246, 17791983, 2, 'Mantencion', 0),
(247, 17791983, 2, 'Busquedas', 0),
(248, 17791983, 2, 'Agenda', 0),
(249, 17791983, 2, 'Orientaciones', 1),
(250, 17791983, 2, 'Audiencia', 0),
(251, 17791983, 2, 'Panel', 0),
(252, 17791983, 2, 'Reportes', 0),
(253, 17791985, 2, 'Mantencion', 1),
(254, 17791985, 2, 'Busquedas', 1),
(255, 17791985, 2, 'Agenda', 1),
(256, 17791985, 2, 'Orientaciones', 1),
(257, 17791985, 2, 'Audiencia', 1),
(258, 17791985, 2, 'Panel', 1),
(259, 17791985, 2, 'Reportes', 1),
(625, 33322233, 1, 'Mantencion', 0),
(626, 33322233, 1, 'Busquedas', 0),
(627, 33322233, 1, 'Agenda', 0),
(628, 33322233, 1, 'Orientaciones', 0),
(629, 33322233, 1, 'Audiencia', 0),
(630, 33322233, 1, 'Panel', 0),
(631, 33322233, 1, 'Reportes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `procedimiento`
--

CREATE TABLE `procedimiento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_corto` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_largo` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_competencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `procedimiento`
--

INSERT INTO `procedimiento` (`id`, `nombre`, `nombre_corto`, `nombre_largo`, `id_competencia`) VALUES
(1, 'Ordinario', 'Ordinario', 'Ordinario', 1),
(2, 'Sumario', 'Sumario', 'Sumario', 1),
(3, 'Gestión Preparatoria', 'Gestión Preparatoria', 'Gestión Preparatoria', 1),
(4, 'Ejecutivo', 'Ejecutivo', 'Ejecutivo', 1),
(5, 'Voluntario', 'Voluntario', 'Voluntario', 1),
(6, 'Medidas Prejudiciales', 'Medidas Prejudiciales', 'Medidas Prejudiciales', 1),
(7, 'Procedimiento Concursal', 'Procedimiento Concursal', 'Procedimiento Concursal', 1),
(8, 'Transacción', 'Transacción', 'Transacción', 2),
(9, 'Ordinario', 'Ordinario', 'Ordinario', 2),
(10, 'Cumplimiento(Z)', 'Cumplimiento(Z)', 'Cumplimiento(Z)', 2),
(11, 'Cumplimiento(X)', 'Cumplimiento(X)', 'Cumplimiento(X)', 2),
(12, 'Adopción Susceptibilidad', 'Adopción Susceptibilidad', 'Adopción Susceptibilidad', 2),
(13, 'Adopción', 'Adopción', 'Adopción', 2),
(14, 'Voluntario', 'Voluntario', 'Voluntario', 2),
(15, 'Violencia Intrafamiliar', 'Violencia Intrafamiliar', 'Violencia Intrafamiliar', 2),
(16, 'Protección', 'Protección', 'Protección', 2),
(17, 'Otros', 'Otros', 'Otros', 3);

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `letra` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mantencion_todo` tinyint(1) NOT NULL DEFAULT '0',
  `busqueda_todo` tinyint(1) NOT NULL DEFAULT '0',
  `agenda_global` tinyint(1) NOT NULL DEFAULT '0',
  `agenda_personal` tinyint(1) NOT NULL DEFAULT '0',
  `agenda_agendar` tinyint(1) NOT NULL DEFAULT '0',
  `asignar_todo` tinyint(1) NOT NULL DEFAULT '0',
  `evaluar` tinyint(1) NOT NULL DEFAULT '0',
  `audiencia_todo` tinyint(1) NOT NULL DEFAULT '0',
  `audiencia_propia` tinyint(1) NOT NULL DEFAULT '0',
  `audiencia_crear` tinyint(1) NOT NULL DEFAULT '0',
  `orientacion_todo` tinyint(1) NOT NULL DEFAULT '0',
  `orientacion_propio` tinyint(1) NOT NULL DEFAULT '0',
  `orientacion_crear` tinyint(1) NOT NULL DEFAULT '0',
  `causa_todo` tinyint(1) NOT NULL DEFAULT '0',
  `causa_propio` tinyint(1) NOT NULL DEFAULT '0',
  `causa_crear` tinyint(1) NOT NULL DEFAULT '0',
  `estadisticas_ver` tinyint(1) NOT NULL DEFAULT '0',
  `reportes_ver` tinyint(1) NOT NULL DEFAULT '0',
  `asunto_crear` tinyint(1) NOT NULL DEFAULT '0',
  `tramite_crear` tinyint(1) NOT NULL DEFAULT '0',
  `plantilla_subir` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `letra`, `mantencion_todo`, `busqueda_todo`, `agenda_global`, `agenda_personal`, `agenda_agendar`, `asignar_todo`, `evaluar`, `audiencia_todo`, `audiencia_propia`, `audiencia_crear`, `orientacion_todo`, `orientacion_propio`, `orientacion_crear`, `causa_todo`, `causa_propio`, `causa_crear`, `estadisticas_ver`, `reportes_ver`, `asunto_crear`, `tramite_crear`, `plantilla_subir`) VALUES
(1, 'alumno', 'A', 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0),
(2, 'profesor', 'P', 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1),
(3, 'secretario', 'F', 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1),
(4, 'director', 'D', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1),
(5, 'administrador', 'Z', 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sedes`
--

CREATE TABLE `sedes` (
  `id_sede` int(11) NOT NULL,
  `nombre_sede` varchar(255) NOT NULL,
  `nombre_sede_corto` varchar(24) NOT NULL DEFAULT 'Sede'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sedes`
--

INSERT INTO `sedes` (`id_sede`, `nombre_sede`, `nombre_sede_corto`) VALUES
(3, 'Campus Los Leones', 'Concepción'),
(2, 'Campus Republica', 'Santiago'),
(1, 'Campus Viña del mar', 'Viña');

-- --------------------------------------------------------

--
-- Table structure for table `tramite`
--

CREATE TABLE `tramite` (
  `id_tramite` int(11) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `hora_ingreso` time NOT NULL,
  `id_causa` int(11) NOT NULL,
  `rut_usuario_alumno` int(11) NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado_revision` varchar(9) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDIENTE' COMMENT 'PENDIENTE, REVISADO, LEIDO, o CERRADO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tramite_comentario`
--

CREATE TABLE `tramite_comentario` (
  `id` int(11) NOT NULL,
  `id_tramite` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rut_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comentario` varchar(4096) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tribunal`
--

CREATE TABLE `tribunal` (
  `id` int(11) NOT NULL,
  `nombre` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rut` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cuenta_corriente` char(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_largo` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rut_sin_dv` int(9) NOT NULL,
  `dv` int(1) NOT NULL,
  `telefonos` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correos` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_tribunal` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tribunal`
--

INSERT INTO `tribunal` (`id`, `nombre`, `rut`, `telefono`, `correo`, `cuenta_corriente`, `direccion`, `nombre_largo`, `rut_sin_dv`, `dv`, `telefonos`, `correos`, `region`, `tipo_tribunal`) VALUES
(1, 'JUICIO ORAL PENAL VIÑA DEL MAR', '61973400-9', '(32) 2795243', 'notifica_top_vinadelmar@pjud.cl', '238-0-000630-9', 'ALVAREZ N° 1330', 'TRIBUNAL JUICIO ORAL EN LO PENAL DE VIÑA DEL MAR', 61973400, 9, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(2, '1° JUZGADO CIVIL VIÑA DEL MAR', '', '(32) 682593', 'jc1_vinadelmar@pjud.cl', 'null', '3 NORTE N° 104 - VIÑA DEL MAR', '1° JUZGADO CIVIL DE VIÑA DEL MAR', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(3, 'CORTE DE APELACIONES DE VALPARAISO', '60305000-2', '(32) 2258357', 'ca_valparaiso@pjud.cl', '23.900.213.701', 'PLAZA DE JUSTICIA S/N. - VALPARAISO', 'CORTE DE APELACIONES DE VALPARAISO', 60305000, 2, NULL, NULL, 'VALPARAÍSO', 'CORTE APELACIONES'),
(4, 'JUZGADO FAMILIAR QUILPUE', '65552550-5', '(32) 2916965', 'jfquilpue@pjud.cl', '25.300.069.536', 'THOMPSON Nº 1282', 'JUZGADO DE FAMILIA DE QUILPUE', 65552550, 5, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(7, 'JUZGADO FAMILIAR VIÑA DEL MAR', '65552790-7', '(32) 326900', 'jfvinadelmar@pjud.cl', NULL, '10 NORTE Nº 655 - VIÑA DEL MAR', 'JUZGADO DE FAMILIA DE VIÑA DEL MAR', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(8, '3° JUZGADO CIVIL VIÑA DEL MAR', '60305011-8', '(32) 2976760 ', 'jc3_vinadelmar@pjud.cl', '23500115190', 'LIBERTAD N°1325 - VIÑA DEL MAR ', '3° JUZGADO CIVIL DE VIÑA DEL MAR', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(9, '2° JUZGADO CIVIL VIÑA DEL MAR', '', '(32) 682606', 'jc2_vinadelmar@pjud.cl', NULL, '4 ORIENTE Nº 108 - VIÑA DEL MAR ', '2° JUZGADO CIVIL DE VIÑA DEL MAR', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(10, '1º JUZGADO CIVIL VALPARAISO', '60305007-K', '(32) 255665 ', 'jc1_valparaiso@pjud.cl', '23.900.212.658', 'CALLE PRAT 779 - 4º PISO - VALPARAISO ', '1º JUZGADO CIVIL DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(11, '2º JUZGADO CIVIL VALPARAISO', '60305015-0', '(32) 255808 ', 'jc2_valparaiso@pjud.cl', '23.900.213.662', 'PRAT 779 - 5º PISO - VALPARAÍSO ', '2º JUZGADO CIVIL DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(12, '3º JUZGADO CIVIL VALPARAISO', '60305033-9', '(32) 256943 ', 'jc3_valparaiso@pjud.cl', '23.900.213.727', 'PRAT Nº 779 - 6º PISO - VALPARAISO ', '3º JUZGADO CIVIL DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(13, '4° JUZGADO CIVIL VALPARAISO', '', '(32) 221381 ', NULL, NULL, 'CALLE PRAT Nº 779 - 7º PISO - VALPARAISO ', '4° JUZGADO CIVIL DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(14, '5° JUZGADO CIVIL VALPARAISO', '', '(32) 256618 ', NULL, NULL, 'PRAT Nº 779 - 8º PISO - VALPARAISO ', '5° JUZGADO CIVIL DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(15, 'JUICIO ORAL PENAL VALPARAISO', '61971800-3', '(32)2320950', 'topvalparaiso@pjud.cl', '24700106175', 'VICTORIA Nº 3022 - VALPARAISO ', 'TRIBUNAL DE JUICIO ORAL EN LO PENAL DE VALPARAISO', 0, 0, '(32)2320950, (32)2592335, (32) 2591950 ', NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(16, 'JUZGADO FAMILIAR VALPARAISO', '65552400-2', '(32) 2324800 ', 'jfvalparaiso@pjud.cl', '23900323671', 'TOMAS RAMOS N° 98 ', 'JUZGADO DE FAMILIA DE VALPARAISO', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(17, 'JUZGADO LETRAS DEL TRABAJO VALPARAISO', '61979260-2', '(32) 2219573', 'jlabvalparaiso@pjud.cl', '24700007207', 'YUNGAY 2434 - VALPARAISO ', 'JUZGADO DE LETRAS DEL TRABAJO DE VALPARAISO', 0, 0, '(32) 2219573, (32) 2232574, (32) 2250690 ', NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(18, 'JUZGADO GARANTIA VALPARAISO', '61965800-0', '(32) 320900 ', 'jgvalparaiso@pjud.cl', '24700105993', 'VICTORIA N° 3022 - VALPARAISO ', 'JUZGADO DE GARANTIA DE VALPARAISO', 0, 0, '+56 9 98837618', NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA'),
(19, 'JUZGADO GARANTIA VIÑA DEL MAR', '61967500-2', '(32) 2327800 ', 'notifica_jg_vinadelmar@pjud.cl', '23800005019', 'ALVAREZ 1330 - VIÑA DEL MAR ', 'JUZGADO DE GARANTIA DE VIÑA DEL MAR', 0, 0, NULL, NULL, 'VALPARAÍSO', 'PRIMERA INSTANCIA');

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `dv` varchar(1) NOT NULL,
  `nombre_usuario` varchar(24) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `pwd` varchar(80) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `sede` varchar(45) DEFAULT NULL,
  `id_rol` int(11) NOT NULL,
  `id_ciforum_rol` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `rut`, `dv`, `nombre_usuario`, `nombre`, `pwd`, `telefono`, `email`, `sede`, `id_rol`, `id_ciforum_rol`) VALUES
(1, 8344235, '2', 'ana.vasquez.u', 'Ana Veronica Vasquez', '$2y$08$l6alveM29MgBZZyXoVSiuOBPSJiP6QeyVd2ow89BiE6.Q9q/neE6y', '32-284-5153', 'ana.vasquez.u@unab.cl', '1', 3, 3),
(2, 9633643, '8', 'alberto', 'Alberto Clemente Parkes', '$2y$08$m3R/2J/V0su0kmSptckXbOC3/2C2tcN8zCNOLLHWBG4sM6JBsOfxu', '93319725 ', 'albertoclemente@vtr.net', '1', 2, 3),
(3, 9909751, '5', 'opinto', 'Oscar Pinto', '$2y$08$EJ1hoKir0l6Fs/cuJGdCmerrS13xBCyiUHx.hoje5B/DNqn4l67La', '99909455', 'osc.pinto@unab.cl', '2', 2, 3),
(4, 17791980, '2', 'gen.bustamante', 'Genesis Bustamante (Administrador)', '$2y$08$/hYqKuj8jyM.9IBBnhVUdOQDyfbpt6VN/av/VdrXwiXtLPvxDt4rK', '988880855', 'genmarc@gmail.com', '1', 3, 2),
(5, 10000004, '2', 'admin2', 'Administrador República', '$2y$08$YuVUW7TpEOq9maK2b9wP4ujyWZBL9HWgyqWZ3E/qhD8ylYQbJ7SRy', '12345678', 'admin2@example.com', '2', 3, 3),
(6, 10000005, '2', 'admin3', 'Administrador Los Leones', '$2y$08$cf6zHK/mFfSJpakbd8yF2uslwdr3MPkt2gPyLiQbKiyCGDQOqOoJW', '12345678', 'admin3@example.com', '3', 3, 3),
(7, 10023354, '1', NULL, 'Pablo Villanueva Romero', '$2y$08$Ru4OgBrP7ncLXQkbaqIcQ.8cKEvLQoSCAQ04ADBWKQBUJaqJqlq6S', '94771935', 'pvillanuevaromero@hotmail.com', '1', 2, 3),
(8, 10030513, '5', 'maria', 'Maria Trinidad Alomar Merino', '$2y$08$7/u6eNwRWfczDmsRtOMiEeGQ1L11UBFpnYb3NQ40x3eOT4gVpF8VC', '89051056', 'trinidadalomar@hotmail.com', '1', 2, 3),
(12, 14719316, '5', NULL, 'Steven Santana Carrera', '$2y$08$QnHYtnGxxqOiGFMFWf/km.H0du2FMvgbeiqbqdCf6Acr.hx2wYPW.', '79789466', 's.santanacarrera@gmail.com', '1', 1, 3),
(13, 0, '0', 'veronicagoiri', 'Verónica Goiri', '$2y$08$BrFySo14PTSR.R.Gm9pBxexNTlOqdWISPj5j0PXTZ0MgBo1A8XLZe', '0000000', 'veronicagoiri@gmail.com', '1', 2, 3),
(14, 15366940, '6', 'maria.ibarra', 'Maria Jose Ibarra', '$2y$08$ZU2hTmGmJtWFtNn1/TA6s.6tfjGMpRsDbzK8SayIrH0Bo07DM9nP.', '32-284-5152', 'maria.ibarra@unab.cl', '1', 3, 3),
(15, 15948485, '8', NULL, 'Daniel Vallejos Navarro', '$2y$08$9SeI0nVN1NpnkTuZ3/qBcu0n/2MQ8YZuJMd1tNFU4khJaRasaZnAS', '93690089', 'danielvallejosnavarro@gmail.com', '1', 2, 3),
(16, 16573072, '0', NULL, 'Carlos Andres Oñate Astudillo', '$2y$08$VMiMbl.atBSTcLkfcJ7ueOu7.zxW/dleg.KuL5uuxUjD/8z7gkrzi', '89411606', 'ca.onate@uandresbello.edu', '1', 1, 3),
(17, 17354965, '2', NULL, 'Carlos Castro Daniou', '$2y$08$5pyIvcWNOOyDgXTFm/gYteeKCDQ3KCAgvQps06B8mJHL9gob9L0JC', '97199816', 's.castrodaniou@gmail.com', '1', 1, 3),
(18, 17791981, '2', 'profesor.viña', 'Profesor de Viña de Prueba', '$2y$08$.v1aCb4mdKVMc9UBqxxiOeK9ADTTsWYHgX3WEAMX/J9WGn/Eb3bpO', '123456', 'genomaster1337@gmail.com', '1', 2, 3),
(19, 17791982, '2', 'director.santiago', 'Director de Santiago', '$2y$08$ajWXCz.rEgjKQmJeJ1xsDOOaPNN8rXeEacAdTn0jb7cm.ejdkr81S', '123456', 'falso.correo.6422@gmail.com', '2', 3, 3),
(20, 17791983, '2', 'profesor.santiago', 'Profesor de Santiago', '$2y$08$DKgMiWUo3dUauqM04vgciejfjJQHbNfUs8Df3lSd884odWbDbAtIq', '123456', 'falso.correo.6423@gmail.com', '2', 2, 3),
(21, 17791985, '2', 'secretario.santiago', 'Secretario de Santiago', '$2y$08$C.v9l/kY3QRz8.nU1j6tR..8hTg8i8PQdrVvmlqxZQTdEqJDZ7H7W', '123456', 'falso.correo.6425@gmail.com', '2', 3, 3),
(22, 17996177, '6', NULL, 'Fernanda Dapik Cabrera', '$2y$08$V744y.3zIgHDwoSMoUaDIOjN.34bgAYx42x5OcrCagZwX74Ix5Bnm', '322493605', 'fernanda.dapik@hotmail.com', '1', 1, 3),
(23, 18237562, '4', NULL, 'Barbara Alejandra Rico Mendez', '$2y$08$OfGhi8J1FWn2k/rkXCos0eF7p1Y2LmzqYa1AwK4o3lLZAvKZGz0jm', '92299353', 'b.ricomendez@uandresbello.edu', '1', 1, 3),
(24, 18378322, 'k', NULL, 'Felipe Celis Gonzalez', '$2y$08$9Vv.hQQFmhNlE7h7oELmFOWihVfY0heC3yeO6Tumz8eLsIT4bRtFC', '82942082', 'felipe.celis26@gmail.com', '1', 1, 3),
(25, 18584987, '2', NULL, 'Maria Ignacia Suarez Zapata', '$2y$08$9Q66V5ADoLEpoVJ8XZInKuX4dMd7SFiDh2Yg88TczvxvturcLpICG', '92728016', 'm.ignaciasuarez@gmail.com', '1', 1, 3),
(26, 18841434, '6', NULL, 'Moises Enrique Lopez Arredondo', '$2y$08$ERvhlBl8bBQWHmub7kmTSuF9sE6vJhpSe7g4anoyefAQLeT8OiBqq', '76710816', 'me.lopezarredondo@gmail.com', '1', 1, 3),
(27, 33322233, '7', 'alumno.viña', 'Alumno de Prueba', '$2y$08$hnxDQNmxMQiPE/dP7iyW1OuAlFXQqEIFr3mqzHS5lUi2EONTnc5ia', '7777777', 'gen.bustamante@uandresbello.edu', '1', 1, 3),
(29, 191234567, '9', 'admin', 'Administrador Global', '$2y$08$/zsH/LdferxMnq7WMKhah.Zg7BXp6Wbb/AD41xjN5tQ/ywQuZdRBu', '3232323', 'falso4fake9fals@gmail.com', '2', 5, 3),
(31, 12523000, '1', 'alumno.santiago', 'Alumno de Santiago', '$2y$08$YTIajIBORbsa9jdmbIE9be62g/wD0rn46oi8xBfVydGBXylxEB0n.', '54271460', 'falso4fake9fals03@gmail.com', '2', 1, 3),
(32, 13877229, '2', NULL, 'Gilda Benita Chamorro Maldonado', '$2y$08$6abFGrO31JTP10UdF3H0aebVi1PZhfwTHkqjT64dPu4XIg9Nfu/Ni', '95932625', 'gildachamorromaldonado@gmail.com', '1', 2, 3),
(33, 18013428, 'k', NULL, 'Camila Correa Sologuren', '$2y$08$kmnq16TE64fjMFtoqXoBmOsfrvCys1ki3rufadIV7pe4tDL9UaW.u', '964648474', 'c.correasologuren@uandresbello.edu', '1', 1, 3),
(34, 17556747, 'k', 'c.floreszarate', 'Carlos Flores Zarate', '$2y$08$Idu34WhWlVEYIC/Wjo51qOL6.T8I0akQRCJCyvYCFM0EUhMHdH2SC', '952138151', 'c.floreszarate@uandresbello.edu', '1', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `usuario_sede`
--

CREATE TABLE `usuario_sede` (
  `id_usuario_sede` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `id_sede` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario_sede`
--

INSERT INTO `usuario_sede` (`id_usuario_sede`, `rut`, `id_sede`) VALUES
(107, 9909751, 2),
(109, 17791980, 1),
(111, 10000004, 2),
(112, 10000005, 3),
(113, 8344235, 1),
(114, 15366940, 1),
(115, 10030513, 1),
(116, 15948485, 1),
(118, 0, 1),
(119, 9633643, 1),
(120, 10023354, 1),
(121, 17996177, 1),
(122, 17354965, 1),
(123, 18841434, 1),
(124, 18584987, 1),
(125, 18237562, 1),
(126, 18378322, 1),
(127, 14719316, 1),
(128, 16573072, 1),
(131, 33322233, 1),
(133, 17791981, 1),
(134, 17791982, 2),
(135, 17791983, 2),
(136, 17791985, 2),
(137, 191234567, 1),
(139, 12523000, 2),
(140, 191234567, 2),
(141, 13877229, 1),
(142, 18013428, 1),
(143, 17556747, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abogado_alumnos`
--
ALTER TABLE `abogado_alumnos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_abogado` (`id_abogado`),
  ADD KEY `id_alumno` (`id_alumno`);

--
-- Indexes for table `archivos`
--
ALTER TABLE `archivos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tramite` (`id_tramite`);

--
-- Indexes for table `asuntos`
--
ALTER TABLE `asuntos`
  ADD PRIMARY KEY (`id_asunto`),
  ADD KEY `id_causa` (`id_causa`);

--
-- Indexes for table `audiencias`
--
ALTER TABLE `audiencias`
  ADD PRIMARY KEY (`id_audiencia`),
  ADD KEY `id_causa` (`rol_causa`),
  ADD KEY `rut_alumno` (`rut_alumno`),
  ADD KEY `rut_profesor` (`rut_profesor`),
  ADD KEY `sede` (`sede`);

--
-- Indexes for table `bloques`
--
ALTER TABLE `bloques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sede` (`sede`);

--
-- Indexes for table `causal_termino`
--
ALTER TABLE `causal_termino`
  ADD PRIMARY KEY (`id_causal_termino`);

--
-- Indexes for table `causas`
--
ALTER TABLE `causas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `id_causa` (`rol_causa`),
  ADD KEY `ID_TRIBUNAL` (`ID_TRIBUNAL`),
  ADD KEY `RUT_ALUMNO` (`RUT_ALUMNO`,`RUT_CLIENTE`,`RUT_ABOGADO`,`SEDE`),
  ADD KEY `RUT_CLIENTE` (`RUT_CLIENTE`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `causa_datos_cliente`
--
ALTER TABLE `causa_datos_cliente`
  ADD UNIQUE KEY `id_causa` (`id_causa`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `ciforum_categorias`
--
ALTER TABLE `ciforum_categorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `ciforum_posts`
--
ALTER TABLE `ciforum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `reply_to_id` (`reply_to_id`);

--
-- Indexes for table `ciforum_roles`
--
ALTER TABLE `ciforum_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ciforum_temas`
--
ALTER TABLE `ciforum_temas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `ciforum_tema_visita`
--
ALTER TABLE `ciforum_tema_visita`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_usuario` (`id_usuario`,`id_ciforum_tema`),
  ADD KEY `fk_visita_tema` (`id_ciforum_tema`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `rut_cliente` (`rut_cliente`),
  ADD KEY `nombre_cliente` (`nombre_cliente`);

--
-- Indexes for table `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `conf_agenda`
--
ALTER TABLE `conf_agenda`
  ADD PRIMARY KEY (`id_conf_agenda`),
  ADD KEY `FK_Usuario_Confagenda` (`rut`),
  ADD KEY `sede` (`sede`);

--
-- Indexes for table `evento_agenda`
--
ALTER TABLE `evento_agenda`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fecha_asignacion` (`fecha_asignacion`,`hora_inicio`,`rut`),
  ADD KEY `id_causa_2` (`id_causa`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_asunto` (`id_asunto`,`id_orientacion`,`id_audiencia`,`sede`),
  ADD KEY `fk_asisagen_sede` (`sede`),
  ADD KEY `icaluid_evento` (`icaluid_evento`);

--
-- Indexes for table `evento_agenda_historico`
--
ALTER TABLE `evento_agenda_historico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_evento_agenda` (`id_evento_agenda`);

--
-- Indexes for table `google_calendar_api`
--
ALTER TABLE `google_calendar_api`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `historial_causas`
--
ALTER TABLE `historial_causas`
  ADD PRIMARY KEY (`id_historial`),
  ADD KEY `id_causa` (`id_causa`);

--
-- Indexes for table `materia`
--
ALTER TABLE `materia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_procedimiento` (`id_procedimiento`),
  ADD KEY `id_competencia` (`id_competencia`);

--
-- Indexes for table `orientacion`
--
ALTER TABLE `orientacion`
  ADD PRIMARY KEY (`id_orientacion`),
  ADD UNIQUE KEY `id_orientacion_UNIQUE` (`id_orientacion`),
  ADD KEY `id_causa` (`id_causa`);

--
-- Indexes for table `plantilla`
--
ALTER TABLE `plantilla`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_archivo` (`id_archivo`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indexes for table `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`id_privilegio`),
  ADD KEY `rut` (`rut`),
  ADD KEY `sede` (`sede`);

--
-- Indexes for table `procedimiento`
--
ALTER TABLE `procedimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_competencia` (`id_competencia`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `letra` (`letra`),
  ADD KEY `letra_2` (`letra`);

--
-- Indexes for table `sedes`
--
ALTER TABLE `sedes`
  ADD PRIMARY KEY (`id_sede`),
  ADD UNIQUE KEY `nombre_sede` (`nombre_sede`,`nombre_sede_corto`),
  ADD KEY `id_sede` (`id_sede`);

--
-- Indexes for table `tramite`
--
ALTER TABLE `tramite`
  ADD PRIMARY KEY (`id_tramite`),
  ADD KEY `id_usuario_alumno` (`rut_usuario_alumno`),
  ADD KEY `id_causa` (`id_causa`);

--
-- Indexes for table `tramite_comentario`
--
ALTER TABLE `tramite_comentario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tramite` (`id_tramite`),
  ADD KEY `rut_usuario` (`rut_usuario`);

--
-- Indexes for table `tribunal`
--
ALTER TABLE `tribunal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD UNIQUE KEY `username_unico` (`nombre_usuario`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `sede` (`sede`),
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_ciforum_rol` (`id_ciforum_rol`),
  ADD KEY `rut_2` (`rut`);

--
-- Indexes for table `usuario_sede`
--
ALTER TABLE `usuario_sede`
  ADD PRIMARY KEY (`id_usuario_sede`),
  ADD KEY `rut` (`rut`),
  ADD KEY `id_sede` (`id_sede`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abogado_alumnos`
--
ALTER TABLE `abogado_alumnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `archivos`
--
ALTER TABLE `archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `asuntos`
--
ALTER TABLE `asuntos`
  MODIFY `id_asunto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `audiencias`
--
ALTER TABLE `audiencias`
  MODIFY `id_audiencia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bloques`
--
ALTER TABLE `bloques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `causal_termino`
--
ALTER TABLE `causal_termino`
  MODIFY `id_causal_termino` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `causas`
--
ALTER TABLE `causas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `causa_datos_cliente`
--
ALTER TABLE `causa_datos_cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ciforum_categorias`
--
ALTER TABLE `ciforum_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `ciforum_posts`
--
ALTER TABLE `ciforum_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ciforum_roles`
--
ALTER TABLE `ciforum_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ciforum_temas`
--
ALTER TABLE `ciforum_temas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ciforum_tema_visita`
--
ALTER TABLE `ciforum_tema_visita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `competencia`
--
ALTER TABLE `competencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `conf_agenda`
--
ALTER TABLE `conf_agenda`
  MODIFY `id_conf_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `evento_agenda`
--
ALTER TABLE `evento_agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `evento_agenda_historico`
--
ALTER TABLE `evento_agenda_historico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `google_calendar_api`
--
ALTER TABLE `google_calendar_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `historial_causas`
--
ALTER TABLE `historial_causas`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `materia`
--
ALTER TABLE `materia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT for table `orientacion`
--
ALTER TABLE `orientacion`
  MODIFY `id_orientacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `plantilla`
--
ALTER TABLE `plantilla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `privilegios`
--
ALTER TABLE `privilegios`
  MODIFY `id_privilegio` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=632;
--
-- AUTO_INCREMENT for table `procedimiento`
--
ALTER TABLE `procedimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `sedes`
--
ALTER TABLE `sedes`
  MODIFY `id_sede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tramite`
--
ALTER TABLE `tramite`
  MODIFY `id_tramite` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tramite_comentario`
--
ALTER TABLE `tramite_comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tribunal`
--
ALTER TABLE `tribunal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `usuario_sede`
--
ALTER TABLE `usuario_sede`
  MODIFY `id_usuario_sede` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `archivos`
--
ALTER TABLE `archivos`
  ADD CONSTRAINT `FK_Archivo_Tramite` FOREIGN KEY (`id_tramite`) REFERENCES `tramite` (`id_tramite`);

--
-- Constraints for table `audiencias`
--
ALTER TABLE `audiencias`
  ADD CONSTRAINT `fk_audiencia_abogado` FOREIGN KEY (`rut_profesor`) REFERENCES `usuarios` (`rut`),
  ADD CONSTRAINT `fk_audiencia_alumno` FOREIGN KEY (`rut_alumno`) REFERENCES `usuarios` (`rut`),
  ADD CONSTRAINT `fk_audiencia_causa` FOREIGN KEY (`rol_causa`) REFERENCES `causas` (`rol_causa`),
  ADD CONSTRAINT `fk_audiencia_sede` FOREIGN KEY (`sede`) REFERENCES `sedes` (`id_sede`);

--
-- Constraints for table `bloques`
--
ALTER TABLE `bloques`
  ADD CONSTRAINT `bloques_ibfk_1` FOREIGN KEY (`sede`) REFERENCES `sedes` (`id_sede`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `causas`
--
ALTER TABLE `causas`
  ADD CONSTRAINT `FK_Causas_Tribunal` FOREIGN KEY (`ID_TRIBUNAL`) REFERENCES `tribunal` (`id`),
  ADD CONSTRAINT `FK_causas_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`);

--
-- Constraints for table `causa_datos_cliente`
--
ALTER TABLE `causa_datos_cliente`
  ADD CONSTRAINT `fk_datos_cliente_causa` FOREIGN KEY (`id_causa`) REFERENCES `causas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ciforum_posts`
--
ALTER TABLE `ciforum_posts`
  ADD CONSTRAINT `ciforum_posts_ibfk_1` FOREIGN KEY (`thread_id`) REFERENCES `ciforum_temas` (`id`),
  ADD CONSTRAINT `ciforum_posts_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ciforum_temas`
--
ALTER TABLE `ciforum_temas`
  ADD CONSTRAINT `ciforum_temas_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `ciforum_categorias` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `ciforum_tema_visita`
--
ALTER TABLE `ciforum_tema_visita`
  ADD CONSTRAINT `fk_visita_tema` FOREIGN KEY (`id_ciforum_tema`) REFERENCES `ciforum_temas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_visita_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `conf_agenda`
--
ALTER TABLE `conf_agenda`
  ADD CONSTRAINT `conf_agenda_ibfk_1` FOREIGN KEY (`sede`) REFERENCES `sedes` (`id_sede`),
  ADD CONSTRAINT `fk_confag_usuario` FOREIGN KEY (`rut`) REFERENCES `usuarios` (`rut`);

--
-- Constraints for table `evento_agenda`
--
ALTER TABLE `evento_agenda`
  ADD CONSTRAINT `fk_asigagen_asunto` FOREIGN KEY (`id_asunto`) REFERENCES `asuntos` (`id_asunto`),
  ADD CONSTRAINT `fk_asigagen_causa` FOREIGN KEY (`id_causa`) REFERENCES `causas` (`id`),
  ADD CONSTRAINT `fk_asisagen_sede` FOREIGN KEY (`sede`) REFERENCES `sedes` (`id_sede`);

--
-- Constraints for table `historial_causas`
--
ALTER TABLE `historial_causas`
  ADD CONSTRAINT `FK_historialCausa_Causa` FOREIGN KEY (`id_causa`) REFERENCES `causas` (`id`);

--
-- Constraints for table `orientacion`
--
ALTER TABLE `orientacion`
  ADD CONSTRAINT `fk_orientacion_causa` FOREIGN KEY (`id_causa`) REFERENCES `causas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `plantilla`
--
ALTER TABLE `plantilla`
  ADD CONSTRAINT `fk_plantilla_archivo` FOREIGN KEY (`id_archivo`) REFERENCES `archivos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_plantilla_categoria` FOREIGN KEY (`id_categoria`) REFERENCES `ciforum_categorias` (`id`);

--
-- Constraints for table `privilegios`
--
ALTER TABLE `privilegios`
  ADD CONSTRAINT `fk_privilegio_usuario` FOREIGN KEY (`rut`) REFERENCES `usuarios` (`rut`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `privilegios_ibfk_1` FOREIGN KEY (`sede`) REFERENCES `sedes` (`id_sede`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `procedimiento`
--
ALTER TABLE `procedimiento`
  ADD CONSTRAINT `fk_procedi_competencia` FOREIGN KEY (`id_competencia`) REFERENCES `competencia` (`id`);

--
-- Constraints for table `tramite`
--
ALTER TABLE `tramite`
  ADD CONSTRAINT `FK_Tramite_Causa` FOREIGN KEY (`id_causa`) REFERENCES `causas` (`id`),
  ADD CONSTRAINT `FK_Tramite_Usuario` FOREIGN KEY (`rut_usuario_alumno`) REFERENCES `usuarios` (`rut`);

--
-- Constraints for table `tramite_comentario`
--
ALTER TABLE `tramite_comentario`
  ADD CONSTRAINT `FK_comentario_tramite` FOREIGN KEY (`id_tramite`) REFERENCES `tramite` (`id_tramite`),
  ADD CONSTRAINT `FK_comentario_usuario` FOREIGN KEY (`rut_usuario`) REFERENCES `usuarios` (`rut`);

--
-- Constraints for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuario_ciforum_rol` FOREIGN KEY (`id_ciforum_rol`) REFERENCES `ciforum_roles` (`id`),
  ADD CONSTRAINT `fk_usuario_roles` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`);

--
-- Constraints for table `usuario_sede`
--
ALTER TABLE `usuario_sede`
  ADD CONSTRAINT `fk_ussede_usuario` FOREIGN KEY (`rut`) REFERENCES `usuarios` (`rut`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_sede_ibfk_1` FOREIGN KEY (`id_sede`) REFERENCES `sedes` (`id_sede`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
